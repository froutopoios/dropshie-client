# Summary

Date : 2020-02-10 13:08:14

Directory d:\react_projects\Dropshie-client

Total : 204 files,  46315 codes, 880 comments, 1485 blanks, all 48680 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 7 | 25,528 | 43 | 4 | 25,575 |
| XML | 29 | 11,977 | 1 | 11 | 11,989 |
| JavaScript | 131 | 7,437 | 580 | 1,091 | 9,108 |
| SCSS | 32 | 1,139 | 193 | 302 | 1,634 |
| HTTP | 2 | 153 | 35 | 39 | 227 |
| Markdown | 1 | 34 | 0 | 30 | 64 |
| HTML | 1 | 25 | 26 | 4 | 55 |
| Shell Script | 1 | 22 | 2 | 4 | 28 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 204 | 46,315 | 880 | 1,485 | 48,680 |
| .vs | 1 | 3 | 0 | 0 | 3 |
| public | 5 | 98 | 26 | 8 | 132 |
| rest_client | 2 | 153 | 35 | 39 | 227 |
| src | 189 | 20,497 | 773 | 1,396 | 22,666 |
| src\actions | 9 | 446 | 53 | 95 | 594 |
| src\api | 1 | 9 | 2 | 3 | 14 |
| src\assets | 32 | 757 | 2 | 75 | 834 |
| src\assets\favicon_package_v0.16 | 3 | 53 | 0 | 3 | 56 |
| src\assets\fonts | 1 | 39 | 0 | 7 | 46 |
| src\assets\vectors | 28 | 665 | 2 | 65 | 732 |
| src\assets\vectors\countryFlags | 9 | 158 | 0 | 18 | 176 |
| src\assets\vectors\supliers | 17 | 414 | 1 | 40 | 455 |
| src\calculators | 1 | 2 | 22 | 10 | 34 |
| src\components | 77 | 6,240 | 395 | 942 | 7,577 |
| src\components\Auth | 5 | 423 | 10 | 63 | 496 |
| src\components\Auth\Login | 2 | 102 | 3 | 16 | 121 |
| src\components\Auth\Register | 2 | 265 | 5 | 36 | 306 |
| src\components\Dashboard | 23 | 2,776 | 166 | 392 | 3,334 |
| src\components\Dashboard\Inventory | 3 | 640 | 48 | 66 | 754 |
| src\components\Dashboard\Lister | 14 | 1,728 | 99 | 246 | 2,073 |
| src\components\Dashboard\Lister\AddItem | 2 | 284 | 30 | 37 | 351 |
| src\components\Dashboard\Lister\Item | 10 | 1,125 | 40 | 148 | 1,313 |
| src\components\Dashboard\Lister\Item\Categories | 1 | 112 | 2 | 19 | 133 |
| src\components\Dashboard\Overview | 2 | 172 | 7 | 32 | 211 |
| src\components\Dashboard\Settings | 2 | 159 | 9 | 27 | 195 |
| src\components\Icons | 1 | 15 | 0 | 3 | 18 |
| src\components\Landing | 24 | 1,323 | 68 | 196 | 1,587 |
| src\components\Landing\Footer | 2 | 135 | 3 | 26 | 164 |
| src\components\Landing\HowItWokrs | 2 | 48 | 4 | 13 | 65 |
| src\components\Landing\LandingHeader | 2 | 92 | 14 | 30 | 136 |
| src\components\Landing\LandingServices | 5 | 208 | 12 | 23 | 243 |
| src\components\Landing\LandingServices\ServicesCard | 2 | 83 | 7 | 13 | 103 |
| src\components\Landing\LandingSupliers | 4 | 166 | 4 | 29 | 199 |
| src\components\Landing\LandingSupliers\Countries | 2 | 126 | 2 | 19 | 147 |
| src\components\Landing\Partners | 2 | 45 | 0 | 9 | 54 |
| src\components\Landing\Plans | 4 | 235 | 28 | 40 | 303 |
| src\components\Landing\Plans\PricingSlider | 2 | 141 | 28 | 22 | 191 |
| src\components\Landing\TermsOfUse | 2 | 355 | 0 | 19 | 374 |
| src\components\Layouts | 9 | 702 | 20 | 97 | 819 |
| src\components\Layouts\Footer | 2 | 33 | 0 | 7 | 40 |
| src\components\Layouts\Header | 4 | 303 | 11 | 61 | 375 |
| src\components\Layouts\Header\LoginButton | 2 | 82 | 2 | 25 | 109 |
| src\components\Layouts\SidebarUi | 3 | 366 | 9 | 29 | 404 |
| src\components\Sales | 1 | 18 | 4 | 5 | 27 |
| src\components\SideBar | 1 | 47 | 2 | 10 | 59 |
| src\components\Stats | 2 | 134 | 9 | 31 | 174 |
| src\components\Stats\Plan | 2 | 134 | 9 | 31 | 174 |
| src\components\UI | 9 | 736 | 106 | 126 | 968 |
| src\components\UI\Buttons | 2 | 174 | 48 | 40 | 262 |
| src\components\UI\ErrorBoundary | 1 | 21 | 0 | 6 | 27 |
| src\components\UI\Inputs | 2 | 212 | 19 | 29 | 260 |
| src\components\UI\Modal | 1 | 16 | 0 | 3 | 19 |
| src\components\UI\Notifications | 1 | 113 | 24 | 20 | 157 |
| src\config | 1 | 18 | 1 | 1 | 20 |
| src\dataFiles | 1 | 173 | 3 | 17 | 193 |
| src\hoc | 3 | 72 | 6 | 17 | 95 |
| src\hooks | 2 | 47 | 15 | 13 | 75 |
| src\reducers | 11 | 212 | 12 | 30 | 254 |
| src\sass | 8 | 274 | 153 | 104 | 531 |
| src\sass\base | 6 | 229 | 152 | 97 | 478 |
| src\sass\mixins | 2 | 45 | 1 | 7 | 53 |
| src\styles | 20 | 11,906 | 5 | 17 | 11,928 |
| src\styles\fonts | 19 | 11,786 | 0 | 10 | 11,796 |
| src\styles\theme | 1 | 120 | 5 | 7 | 132 |
| src\utils | 14 | 97 | 56 | 29 | 182 |
| src\validators | 2 | 23 | 0 | 2 | 25 |

[details](details.md)