var express = require('express');
var proxy = require('http-proxy-middleware');
var PORT = 3001;
var app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST, PATCH, DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  if ('OPTIONS' === req.method) {
    res.sendStatus(200);
  }
  next();
});

app.use(
  '/api',
  proxy({ target: 'http://sandbox.torpec.com', changeOrigin: true })
);

app.listen(PORT, () => {
  console.log(`proxy listening on port ${ PORT }...`);
});
