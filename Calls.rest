@protocol = http
@hostname = localhost:8081/DropShip.WorkLoad
#@hostname = sandbox.torpec.com

# @name login
POST {{protocol}}://{{hostname}}/api/v1/Account/ApiLogin
Content-Type: application/json

{
    "username": "{{restUser}}",
    "password": "{{restPass}}",
    "appid":"Dropshie_com_web_app"
}
###

# @name register
POST {{protocol}}://{{hostname}}/api/v1/Account/Register
Content-Type: application/json

{
    "Email": "pan4@dropshie.com",
    "Password": "123456",
    "ConfirmPassword": "123456",
    "Name":"test",
    "Surname":"123456666",
    "TermsAccepted": true,
    "AffiliateCode": ""
}

###

# @name TEST123
GET {{protocol}}://{{hostname}}/api/v1/Test123/Test/test

###

@authToken = {{login.response.body.access_token}}

# @name search 
POST {{protocol}}://{{hostname}}/api/v2/InventoryGrid/Search
Content-Type: application/json
Authorization: Bearer {{authToken}}

{"ItemProperties":null,"Supplier":null,"DateAddedFrom":null,"DateAddedTo":null,"CurrentStockQtyEquals":null,"CurrentStockQtyFrom":null,"CurrentStockQtyTo":null,"ListPriceFrom":null,"ListPriceTo":null,"NetProfitFrom":null,"NetProfitTo":null,"SourcePriceFrom":null,"SourcePriceTo":null,"PaypalFeesFrom":null,"PaypalFeesTo":null,"EbayFeesFrom":null,"EbayFeesTo":null,"OtherFeesPersFrom":null,"OtherFeesPersTo":null,"FixedCostFrom":null,"FixedCostTo":null,"TaxFrom":null,"TaxTo":null,"FixedProfitFrom":null,"FixedProfitTo":null,"DesiredQtyEquals":null,"DesiredQtyFrom":null,"DesiredQtyTo":null,"DaysListedFrom":null,"DaysListedTo":null,"CompetitorsPriceDiffFrom":null,"CompetitorsPriceDiffTo":null,"HandlingProtectionDaysEquals":null,"HandlingProtectionDaysFrom":null,"HandlingProtectionDaysTo":null,"NumberViewsEquals":null,"NumberViewsFrom":null,"NumberViewsTo":null,"NumberWatchingEquals":null,"NumberWatchingFrom":null,"NumberWatchingTo":null,"NumberSoldEquals":null,"NumberSoldFrom":null,"NumberSoldTo":null,"ConversionRatioFrom":null,"ConversionRatioTo":null,"PrimeProtection":null,"CompetitorsCheck":null,"SellerSelectionPolicy":null,"AddOnProtection":null,"BackOrderProtection":null,"ShippingDaysProtection":null,"HandlingProtectionDDL":null,"UpdateListingQuantity":null,"EbayUsername":null,"RepriceState":null,"SupplierName":null,"HasSaleFrom":null,"HasSaleTo":null,"HasNotSaleFrom":null,"HasNotSaleTo":null,"CurrentPage":1,"PageSize":50,"SortColumn":"SubmitionDate","SortColumnDirection":1}

###

# @name get_account_v1_UserInfo
GET {{protocol}}://{{hostname}}/api/v1/Account/UserInfo
Content-Type: application/json
Authorization: Bearer {{authToken}}

###

# @name get_user_settings
GET {{protocol}}://{{hostname}}/api/v1/Settings/UserSettings
Content-Type: application/json
Authorization: Bearer {{authToken}}


###

# @name update_user_seller_profiles

POST {{protocol}}://{{hostname}}/api/v2/UserSellerPoliciesSelections
Content-Type: application/json
Authorization: Bearer {{authToken}}

{
  "id": 1,
  "userId": "1E319CFB-CB65-460E-900C-D12E84986C65",
  "ebayUsername": "testuser_profitrooper",
  "selingMarket": 1,
  "selectedReturnPolicyId": null,
  "selectedPaymentPolicyId": 2,
  "selectedShippingPolicyId": 7
}

###

# @name get_paged
GET {{protocol}}://{{hostname}}/api/v2/InventoryGrid?page=1&take=100
Content-Type: application/json
Authorization: Bearer {{authToken}}

###
# [Obsolete("This call will soon be deprecated use <<submit_to_fetch_V2>>")]
# @name submit_to_fetch 
POST {{protocol}}://{{hostname}}/api/v1/ListManager/GetAmazonListingPropertiesV2
Content-Type: application/json
Authorization: Bearer {{authToken}}

{
    "ProductIdInput": "https://www.amazon.co.uk/GACHI-Portable-26800mAh-Capacity-Powerbank/dp/B07T2CLXD3/ref=pd_sim_23_3/257-2467857-9199561?_encoding=UTF8&pd_rd_i=B07T2CLXD3&pd_rd_r=aa0219d9-4f77-4128-a938-3704f7c9ebbe&pd_rd_w=82pQr&pd_rd_wg=LvIUb&pf_rd_p=e323aefa-0fb8-4e75-9bef-479c25fd70eb&pf_rd_r=97QS5SKB4HGMXMX4657R&psc=1&refRID=97QS5SKB4HGMXMX4657R",
    "WebGateOriginMarketId": "amazon uk",
    "EbayUserName": "testuser_nakata",
    "TemplateId": "aac1e610-256e-4c20-8cff-113ab54f8e92",
}

###

# @name submit_to_fetch_V2
POST {{protocol}}://{{hostname}}/api/v2/ItemFetch/
Content-Type: application/json
Authorization: Bearer {{authToken}}

{
    "ProductUrl": "https://www.amazon.com/gp/product/B073WJMKHN",
    "CountryId": 1,
    "EbayUserName": "testuser_nakata",
    "IsVariation": true
}

###

# @name get_user_setting 
GET {{protocol}}://{{hostname}}/api/v2/UserSettings/1E319CFB-CB65-460E-900C-D12E84986C65
Content-Type: application/json
Authorization: Bearer {{authToken}}


###

# @name set_user_setting 
POST {{protocol}}://{{hostname}}/api/v2/UserSettings/1E319CFB-CB65-460E-900C-D12E84986C65
Content-Type: application/json
Authorization: Bearer {{authToken}}

{
    "Id": "1E319CFB-CB65-460E-900C-D12E84986C65",
    "ConsentForTermsOfUse": true,
    "ConsentForEmails": false,
    "PaypalFeesPers": 1.01,
    "EbayFeesPers": 1.02,
    "BreakEvenPers": 1.03,
    "Country": "Greece",
    "DefaultItemLocation": "New York",
    "DefaultItemQty": 2,
    "FirstName": "Pan ",
    "FixedProfit": 1.06,
    "FixedCost": 1.04,
    "SourceTax": 1.07,
    "GlobalShipping": 0,
    "LastName": "Dev",
    "ListingDuration": {
      "Token": "GTC"
    },
    "Location": null,
    "PrivateListing": 2,
    "ProfitPers": 1.05,
    "ShouldUpdateListingQty": 1,
    "UserId": null,
    "PricingCalculationOption": {
      "Token": "ProfitPersentageOnFinalValue"
    },
    "PrimeProtectionType": 0,
    "PreferedOfferSelectionPolicy": 3,
    "ProtectionAddon": true,
    "ProtectionBackOrder": true,
    "ProtectionShippingDays": true,
    "ProtectionMaxShippingDays": 1,
    "ProtectionHandlingDays": true,
    "ProtectionMaxHandlingDays": 1,
    "DropshiePasteKey": "1640dba86cd34c1d85dab1306ab48d60",
    "DefaultWebGateOriginMarketId": "amazon us",
    "DefaultCountryId": "2",
    "DefaultTemplateId": "aac1e610-256e-4c20-8cff-113ab54f8e92",
    "DefaultEbayUserName": "testuser_nakata",
    "PayPalEmailAddress": "pan.lefas-facilitator@gmail.com",
    "ChinaBrandsClientSecret": "bbc2e8388e5fdce09243aa82efed04de",
    "ChinaBrandsClientEmail": "mpitzios.c@gmail.com",
    "ChinaBrandsClientId": "1825763431",
    "ChinaBrandsClientPassword": "6U6saZMrHjSjXLWiaqRtpuncy2cG39gqwugd7+JVoWM="
}

###

# @name get_user_templates 
GET {{protocol}}://{{hostname}}/api/v1/Settings/GetUserListingTemplates
Content-Type: application/json
Authorization: Bearer {{authToken}}

###

# @name get_origin_marketplaces 
GET {{protocol}}://{{hostname}}/api/v2/StaticData/OriginMarketPlaces
Content-Type: application/json
Authorization: Bearer {{authToken}}

###

# @name get_selling_marketplaces 
GET {{protocol}}://{{hostname}}/api/v2/StaticData/SellingMarketPlaces
Content-Type: application/json
Authorization: Bearer {{authToken}}

###

# @name get_all_user_messages

GET {{protocol}}://{{hostname}}/api/v2/UserMessage?rowstatus=1&daysfrom=8
Content-Type: application/json
Authorization: Bearer {{authToken}}

###

# @name add_user_message

POST {{protocol}}://{{hostname}}/api/v2/UserMessage
Content-Type: application/json
Authorization: Bearer {{authToken}}
x-user: 1E319CFB-CB65-460E-900C-D12E84986C65

{"Header":"Multi Listing Inventory Completed","Message":"Your multi listing has completed. variation from https://www.amazon.com/Womens-Walking-Shoes-Sock-Sneakers/dp/B07PW8V84P, error: The request has invalid data with message: java.lang.NullPointerException","ExpireDate":"2020-04-10T11:26:10.5416453Z","LinkUrl":null}

###

# @name get_user_program_state

GET {{protocol}}://{{hostname}}/api/v2/UserProgramState/1E319CFB-CB65-460E-900C-D12E84986C65
Content-Type: application/json
Authorization: Bearer {{authToken}}


###

# @name get_user_seller_profiles

GET {{protocol}}://{{hostname}}/api/v2/UserSellerProfiles/1E319CFB-CB65-460E-900C-D12E84986C65
Content-Type: application/json
Authorization: Bearer {{authToken}}

###

# @name get_user_seller_profiles_ebay_user

GET {{protocol}}://{{hostname}}/api/v2/UserSellerProfiles/1E319CFB-CB65-460E-900C-D12E84986C65/ebayuser/testuser_profitrooper
Content-Type: application/json
Authorization: Bearer {{authToken}}

###

# @name get_inventory_item_reprice_cache
GET {{protocol}}://{{hostname}}/api/v2/InventoryItem/reprice-cache
Content-Type: application/json
Authorization: Bearer {{authToken}}

###

# @name get_v2_test 
GET {{protocol}}://{{hostname}}/api/v2/test
Content-Type: application/json
