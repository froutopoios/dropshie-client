#!/bin/bash

echo "building for production"
# npm run build
echo "building completed"
tar cvzf build.tar.gz build
scp build.tar.gz killerwhale@138.68.90.69:~/dropshie-build
echo 'compression completed. file sent to the server'
rm -rf build.tar.gz

ssh killerwhale@138.68.90.69  << 'ENDSSH'
echo "connection established"
BUILD=/home/killerwhale/dropshie-build/build/
cd /home/killerwhale/dropshie-build
if [ -d "$BUILD" ]; then
   echo 'old build found.. \n deleting'
   rm -rf build
   echo 'old build deleted'
fi

mkdir build
tar xf build.tar.gz
echo "unpack build file..."
rm build.tar.gz
echo "removing compressed archives..."
echo "Operation completed. build folder updated at $PWD"
ENDSSH
