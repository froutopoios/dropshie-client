import map from 'lodash/map';

/**
 * check the properties of an object and if there is no value or value is 0 in replaces the value with "Does not apply"
 * @param {*} obj 
 */
export const checkValidity = obj => {
  const copy = { ...obj };

  map(copy, (value, key) => {
    if (value === undefined || value.length === 0) {
      copy[key] = 'Does not Apply'
    }
  })
  return copy
}