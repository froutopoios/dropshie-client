export default file => {
  const acceptedTypes = ['image/gif', 'image/jpeg', 'image/png'];

  return file && acceptedTypes.includes(file['type'])
}