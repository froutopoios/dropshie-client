export const has = (obj, level, ...rest) => {
  if (obj === undefined) return 'Does not apply'
  if (rest.length === 0 && obj.hasOwnProperty(level)) {
    return obj[level]
  }
  return has(obj[level], ...rest)
}