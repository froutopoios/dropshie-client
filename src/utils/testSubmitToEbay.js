export default {
  "EbayProperties": {
    "Country": 1,
    "IsPrime": 1, // obsolete test without this property
    "DestinationMarket": 1,
    "SellMarketUserName": "testuser_nakata",
    "ItemCondition": 1000,
    "AllowBuyersToRemainAnonymous": 2,
    "ShippingCostPaidBy": "Buyer",
    "SellerProfiles": {
      "sellerPaymentProfileId": "5583444000",
      "sellerPaymentProfileName": "PROFNAME NO NEED",
      "sellerReturnProfileId": "5583439000",
      "sellerReturnProfileName": "PROFNAME NO NEED",
      "sellerShippingProfileId": "5583441000",
      "sellerShippingProfileName": "PROFNAME NO NEED"
    },
    "SelectedCategory": {
      "title": "Controllers & Attachments",
      "id": "117042"
    },
    "ItemLocation": "Athens",
    "RequireImmediatePayment": 1,
    "ProductReferenceId": null,
    "ListingDuration": "GTC",
    "PrivateListing": 2,
    "UseEbayCompetitors": false,
    "CompetitorsPriceDiff": 0,
    "CompetitorEbayItemId": null,
    "CompetitorsEbayItemId": '3455353'
  },
  "GenericProperties": {
    "ItemId": "00000000-0000-0000-0000-000000000000",
    "OriginMarket": 1,
    "OriginProductCode": "B01MD19OI2",
    "OriginProductSecondaryCode": null,
    "CurrentListingQuantity": 6,
    "ItemSpecifics": {
      "ItemsSpecifics": [
        {
          "Title": "Connectivity",
          "SpecificValues": [
            {
              "Value": "Bluetooth",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Type",
          "SpecificValues": [
            {
              "Value": "Wireless controller",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Features",
          "SpecificValues": [
            {
              "Value": "Built-in Speaker and Stereo Headset",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Compatible Model",
          "SpecificValues": [
            {
              "Value": "For Sony PlayStation 4",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Compatible Product",
          "SpecificValues": [
            {
              "Value": "For Controller",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Color",
          "SpecificValues": [
            {
              "Value": "Magma Red",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Platform",
          "SpecificValues": [
            {
              "Value": "Sony PlayStation 4",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Material",
          "SpecificValues": [
            {
              "Value": "Plastic",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Wireless Range",
          "SpecificValues": [
            {
              "Value": "More Than 10 m",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Manufacturer Color",
          "SpecificValues": [
            {
              "Value": "Red",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Manufacture",
          "SpecificValues": [
            {
              "Value": "Sony",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Maximum Number of Players",
          "SpecificValues": [
            {
              "Value": "4",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Number of Fans",
          "SpecificValues": [
            {
              "Value": "1",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Product Type",
          "SpecificValues": [
            {
              "Value": "Video Game Gamepads",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Manufacturer",
          "SpecificValues": [
            {
              "Value": "PlayStation",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Accessory Type",
          "SpecificValues": [
            {
              "Value": "Controllers",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Cable Length",
          "SpecificValues": [
            {
              "Value": "28 in",
              "IsSelected": true
            }
          ]
        },
        {
          "Title": "Function",
          "SpecificValues": [
            {
              "Value": "Wireless Game controller",
              "IsSelected": true
            }
          ]
        }
      ]
    },
    // "AppBaseUrl": "https://profitize.qwerty/DropShip.Web",
    "AppBaseUrl": "https://profitize.qwerty/DropShip.Web/App/Inventory",
    "UserId": "BDBEC804-B02B-490E-A71E-E6E1FD17678F"
  },
  "UserPreferences": {
    "DispatchTimeMax": 2,
    "ProtectionAddon": true,
    "ProtectionBackOrder": false,
    "ProtectionShippingDays": true,
    "ProtectionHandlingDays": false,
    "DesiredListingQuantity": 6,
    "PreferedOfferSelectionPolicy": 0,
    "PrimeProtectionType": 2,
    "ShouldUpdateListingQty": 1,
    "ProtectionMaxShippingDays": 5
  },
  "ItemProperties": {
    "DestinationDescriptionHtml": "<div>boing</div>",
    "EAN": "",
    "ISBN": "",
    "UPC": "711719504405",
    "ProductTitle": "DualShock 4 Wireless Controller for PlayStation 4 - Magma Red",
    "OriginProductUrl": "https://www.amazon.com/DualShock-Wireless-Controller-PlayStation-Magma-4/dp/B01MD19OI2",
    "UserProvidedOriginProductUrl": "https://www.amazon.com/DualShock-Wireless-Controller-PlayStation-Magma-4/dp/B01MD19OI2",
    "Images": [
      {
        "isThumbnail": false,
        "width": 0,
        "height": 0,
        "url": "https://images-na.ssl-images-amazon.com/images/I/81L9%2B4dTIgL._SL1500_.jpg",
        "IsMain": true,
        "Selected": false,
        // "ImageId": "gpafdq4za6dko05n4d31s",
        "IsSelected": true
      },
      {
        "isThumbnail": false,
        "width": 0,
        "height": 0,
        "url": "https://images-na.ssl-images-amazon.com/images/I/61Na4U9J8%2BL._SL1500_.jpg",
        "IsMain": false,
        "Selected": false,
        // "ImageId": "uk9au9ivc9m515rxcy68",
        "IsSelected": true
      },
      {
        "isThumbnail": false,
        "width": 0,
        "height": 0,
        "url": "https://images-na.ssl-images-amazon.com/images/I/71U7CvkhFuL._SL1500_.jpg",
        "IsMain": false,
        "Selected": false,
        // "ImageId": "hccgsyuzvt7ktek5qk959",
        "IsSelected": true
      },
      {
        "isThumbnail": false,
        "width": 0,
        "height": 0,
        "url": "https://images-na.ssl-images-amazon.com/images/I/81P9dXpJbDL._SL1500_.jpg",
        "IsMain": false,
        "Selected": false,
        // "ImageId": "rijkxvzj29icl3rq6ctu6n",
        "IsSelected": true
      }
    ]
  },
  "PricingProperties": {
    "CurrentListingPrice": 55.07,
    "BreakEvenPers": 1.1,
    "PaypalFeesPers": 1.08,
    "EbayFeesPers": 1.04,
    "ProfitPers": 1.11,
    "FixedCost": 1.09,
    "FixedProfit": 1.14,
    "SourceTax": 1.11,
    "SourcePrice": 49.9,
    "NetProfit": 1.75
  },
  "VariationProperties": {
    variationSku: '',
    variationNameValues: ''
  }
}