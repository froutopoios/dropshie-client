import React, { Component } from 'react';


export default class Decorator extends Component {
  state = {
    foo: '',
    bar: ''
  }

  baz = () => {
    console.log('baz')
  }

  render() {
    return (
      <div>
        { this.props.children(this.state) }
      </div>
    )
  }
}

