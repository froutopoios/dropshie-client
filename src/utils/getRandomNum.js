/**
 * Returns a random int with the max and min inclusive
 * @param {*} min 
 * @param {*} max 
 */
export const getRandomNum = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);

  return Math.floor(Math.random() * (max - min + 1)) + min;
}