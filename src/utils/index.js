import to from './to';
import urlEncoder from './urlEncoder';
import errorLogger from './errorLogger';
import cherryPick from './cherryPick';
import history from './history';
import isFileImage from './isFileImage';
import getToken from './getToken';
import validateStr from './validateStr';
import refFactory from './refFactory';
import marketPlaceByValue from './mapMarketPlaces';

// For unit testing
export const findByTestAttribute = (component, attr) => {
  const wrapper = component.find(`[data-test='${ attr }']`);
  return wrapper
}

// Gemeral utils

/**
 * Extracts the last segment of a path
 * @param {*} path 
 */
export const getLastSegment = path => {
  return path.substring(path.lastIndexOf('/') + 1)
}

export {
  validateStr,
  to,
  urlEncoder,
  errorLogger,
  cherryPick,
  history,
  isFileImage,
  getToken,
  refFactory,
  marketPlaceByValue
}