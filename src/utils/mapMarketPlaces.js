const arrOfMarkets = [
  {value: 1, name: 'ebay.com'},
  {value: 2, name: 'ebay.co.uk'},
  {value: 3, name: 'ebay.ca'},
  {value: 4, name: 'ebay.au'},
  {value: 5, name: 'ebay.de'}
]

export default arrOfMarkets;