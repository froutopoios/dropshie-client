import { createBrowserHistory } from 'history';

const history = createBrowserHistory({ basename: '/' })
// export default createBrowserHistory({ basename: '/' });
export default history;