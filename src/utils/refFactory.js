import React from 'react'

/**
 * creates refs for a number of elements
 * @param {number} - number of elements
 */
export default numOfrefs => {
  const arrayOfRefs = [];
  for (let i = 1; i <= numOfrefs; i++) {
    arrayOfRefs.push(React.createRef())
  }

  return arrayOfRefs
}