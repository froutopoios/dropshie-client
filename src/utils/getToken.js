export default getState => {
  return getState().auth.userToken;
};