/**
 * Keep certain properties inside an array
 * @param { function }
 * (input Array)(callback)
 * callback -- ({ arg, arg }) => ({ arg, arg })
 */
export default ( arr ) => {
  return (callback) => { // this is the cherypick function 
    // console.log(arr);
    const transformedArray = arr.reduce((acc, obj) => {
      const cherrypicked = (callback)(obj);

      return [ ...acc, cherrypicked ]
    }, [])
    return transformedArray;
  }
}
