import { ACCOUNT } from './types'

export const fetchEmail = () => ({ type: ACCOUNT.FETCH_EMAIL });
export const sendPassChangeRequest = payload => ({ type: ACCOUNT.CHANGE_PASS, payload })
export const getUserPlan = () => ({type: ACCOUNT.GET_USER_PLAN});

export const fetchTemplates = () => ({type: ACCOUNT.FETCH_TEMPLATES});