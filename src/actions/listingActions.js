import axios from '../api/axiosConf';
import itemSubmitJson from '../dataFiles/itemSubmitJson';
import { TEMPLATES_FOR_ADD_FETCHED,
         FETCH_ORIGIN_MARKETPLACES,
         FETCH_EBAY_USERS,
         IS_LOADING,
         STOP_FETCHING,
         STOP_LOADING,
         SYSTEM_ERRORS,
         SYSTEM_MESSAGES,
         SUBMIT_TO_FETCH_START,
         CLEAR_ITEM,
         FILE_ERRORS,
         SUBMIT_TO_FETCH_SUCCESS,
         SUBMIT_TO_FETCH_FAILURE,
         LISTINGS,
         EDITOR} from './types';

import { to, errorLogger, cherryPick, history, isFileImage, getToken } from '../utils';

// --*--- version2 --*--
export const sendInitValues = payload => ({ type: LISTINGS.SEND_INIT_VALUES, payload });
export const sendValueToStore = payload => ({ type: LISTINGS.SEND_PER_CHANGE_TO_STORE, payload })
export const submitToEbay = payload => ({ type: LISTINGS.SUBMIT_TO_EBAY, payload });
export const sendSellAndNetToStore = payload => ({ type: LISTINGS.SEND_SELL_AND_NET_TO_STORE, payload })

// GET ORIGIN MARKETPLACES
export const fetchOriginMarketplaces = () => async (dispatch, getState) => {
  // console.log('inside fetch origin marketplaces')
  const token = getToken(getState);
  const url = '/api/v2/StaticData/OriginMarketPlaces';

  const [error, response] = await to(axios({
    method: 'get',
    headers: {
      'Authorization': `bearer ${ token }`
    },
    url
  }))
  if (error) {
    errorLogger(error)
  } else {
    const { Data } = response.data;
    console.log(Data);
    const picked = cherryPick(Data)(({ DomainName, WebGateOriginId }) => ({ DomainName, WebGateOriginId }));

    dispatch({ type: FETCH_ORIGIN_MARKETPLACES, payload: picked })
  }
}

// TEMPLATES ACTION
export const fetchTemplates = () => async (dispatch, getState) => {
  const token = getToken(getState);
  const url = 'api/v1/Settings/GetUserListingTemplates';

  const [error, response ] = await to(axios({
    method: 'get',
    headers: {
      'Authorization': `bearer ${ token }`
    },
    url
  }))
  if (error) {
    errorLogger(error);
  } else {
    // properties from temnplates are removed from the final array, no indexing.
    // TODO: have to change this action to fetch the all the properties of the obj and apply indexing to it.
    const { Data } = response.data;
    const pickedData = cherryPick(Data)(({ TemplateId, Name }) => ({ TemplateId, Name }));
    
    dispatch({
      type: TEMPLATES_FOR_ADD_FETCHED,
      payload: pickedData
    })
  }
}

// GET EBAY USER NAMES
export const fetchEbayUsers = () => async(dispatch, getState) => {
  const token = getToken(getState);
  const url = '/api/v1/Settings/UserSettings'

  const [ error, response ] = await to(axios({
    method: 'get',
    headers: {
      'Authorization': `bearer ${ token }`
    },
    url
  }))
  if (error) {
    errorLogger(error);
  } else {
    const data = response.data.Data.EbayUsersSettings;
    console.log('feth ebay users motha fuckaaaaa');
    console.log(response.data.Data.EbayUsersSettings);
    const payload = cherryPick(data)(({ EbayUserName }) => ({ EbayUserName }));

    dispatch({ type: FETCH_EBAY_USERS, payload })
  }
}

// **
// SUBMIT FOR FETCH
export const submitForFetch = item => async (dispatch, getState) => {
  const token = getToken(getState);
  const url = '/api/v2/ItemFetch';
  
  // Here is the template of the item
  dispatch({ type: EDITOR.SET_CURRENT_TEMPLATE_ID, payload: item.TemplateId })
  // sTore it to the redux state to use inside the inventory submit
  const newItem = (({CountryId, EbayUserName, ProductIdInput}) => ({CountryId, EbayUserName, ProductUrl: ProductIdInput}))(item)

  dispatch({ type: SUBMIT_TO_FETCH_START });
  dispatch({ type: EDITOR.GET_TEMPLATE_BY_ID, payload: item.TemplateId });
  dispatch({ type: IS_LOADING });  
  try {
    const response = await axios({
      method: 'post',
      headers: {
        'Authorization': `bearer ${ token }`
      },
      url,
      data: newItem
    });

    // TODO: use the errors from response.data to show information to the user
    const rawData = response.data.Data;
    
    //check the errors array
    const { SystemErrors, SystemMessages } = rawData;
    if (SystemErrors !== undefined && SystemErrors.length !== 0) {
      dispatch({ type: SYSTEM_ERRORS,  payload: SystemErrors }) // NOT IMPLEMENTED
    }
    
    if ( SystemMessages !== undefined && SystemMessages.length !== 0) {
      dispatch({ type: SYSTEM_MESSAGES, payload: SystemMessages }) // NOT IMPLEMENTED
    }

    console.log('inside listings Response')
    console.log(rawData);

    const payload = rawData.FetchedItems[0] // also there are system and error messages objecs
    dispatch({type: SUBMIT_TO_FETCH_SUCCESS, payload })
    dispatch({ type: STOP_LOADING });

    history.push('/dashboard/InventorySubmit');
  } catch(error) {

    const { status } = error.response ? error.response : 'cannot get status'

    dispatch({ type: SUBMIT_TO_FETCH_FAILURE, payload: status })
    dispatch({ type: STOP_LOADING });
  }
}

// IMAGE UPLOAD
export const uploadImage = imageData => async (dispatch, getState) => {
  const token = getToken(getState);
  const url = '/api/v2/FileManager/UpladImage';

  const formdata = new FormData();
  formdata.append('image', imageData);
  formdata.append('name', imageData.name);
  
  // validation 
  const validate = isFileImage(imageData);

  if (!validate) {
    const message = 'There is No valid Image Uploaded'
    dispatch({ type: FILE_ERRORS, payload: message })

    return;
  }

  const config = {
    headers: {
      'Authorization': `bearer ${ token }`,
      // 'Content-Type': imageData.type
      'Content-Type': 'multipart/form-data'
    },
    onUploadProgress: progressEvent => {
      console.log('proggress', Math.round((progressEvent.loaded / progressEvent.total) * 100) + '%')
    }
  }

  const [err, res] = await to(axios.post(url, formdata, config))

  if (err) {
    errorLogger(err);
  } else {
    if (res.data.StatusCode === 500) {
      errorLogger(res.data.Message);
    } else if (res.data.StatusCode === 200) {
      console.log('Image Uploaded Done');
      // console.log(res.data);
      console.log(res);
      /**
       * TODO: send an action create entry on state with the imageId from the response
       */
    }
  }
}

// STOP FETCHING ACTiON
export const stopFetchingItem = () => {
  return {
    type: STOP_FETCHING
  }
}

/**
 * SUBMIT ITEM
 * @param {*} data  the payload
 * @param {*} source The identifier of the component which triggers the action 
 */
export const submitItem = (data, source) => async (dispatch, getState) => {
  const fromFetcher = getState().fetchedItem;
  const token = getToken(getState);
  const url = '/api/v2/ItemSubmit/kolos';
  const config = {
    headers: {
      'Authorization': `bearer ${ token }`
    }
  }
  // MAYBE HAVE TO GRUB INFO FROM SOME ENDPOINT FROM SETTINGS ALSO!!
  
  // TODO: move this!! only clear if the response is ok and the item is added to the inventory.
  dispatch({
    type: CLEAR_ITEM
  })
  // clear storage
  // saveState()

  const finalJson = itemSubmitJson(data, fromFetcher, source);
  const [err, response] = await to(axios.post(url, finalJson, config))
  
  if (err) {
    errorLogger(err)
  } else {
    console.log("Response from addItem", response);
  }
}
