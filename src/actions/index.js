import { USER_LISTINGS_FETCHED, CRED_FETCHED, USER_ID_FETCHED } from './types';
import { to, errorLogger } from '../utils';
import axios from '../api/axiosConf';



// OBSOLETE TODO: remove the action
export const credAction = () => async (dispatch, getState) => {
  // TODO: have to dry the following. retreive the token one time put the urls of all actions to a specific place
  const token = getState().auth.userToken;
  const url = '/api/v1/Account/BasicApplicationUserInfo'

  const [ error, response ] = await to(axios({
    method: 'get',
    headers: {
      'Authorization': `bearer ${ token }`
    },
    url
  }));
  if (response) {
    try {
      const userId = response.data.Data.Id
      const { UserSettings } = response.data.Data; 
      const info = JSON.parse(UserSettings);
  
      dispatch({ type: USER_ID_FETCHED, payload: userId});
      dispatch({ type: CRED_FETCHED, payload: info && info.FirstName });
    } catch (error) {
      console.log(error)
    }
  } else {
    console.log(error);
  }
} 


// FETCH LISTINGS
// TODO: remove on production
let t0, t1;
export const fetchInventory = () => async (dispatch, getState) => {
  const token = getState().auth.userToken;
  const inventory = getState().inventory;

  let url = '/api/v1/ListManager/UserListingItems'

  t0 = performance.now();
  const [ error, response ] = await to(axios({
    method: 'get',
    headers: {
      'Authorization': `bearer ${ token }`
    },
    url
  }));

  if (error) {
    errorLogger(error);
  }

  // console.log(typeof(inventory)); 
  console.log(response.data);
  // console.log(response.length)
  t1 = performance.now();
  console.log('fetch listings took ' + ( (t1 - t0) / 1000 ) + " seconds")

  dispatch({ type: USER_LISTINGS_FETCHED, payload: response.data.Data })

  if ( inventory !== undefined && inventory.length === 0) {
    // console.log('empty inventory')
    // dispatch({ type: USER_LISTINGS_FETCHED, payload: response.data })
  }
  else if ( inventory !== undefined && inventory.length !== response.data.length) {
    // TODO: dispatch update action
  }  
}