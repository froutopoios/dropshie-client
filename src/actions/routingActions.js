import { CHANGE_PATH } from './types';

export const changePath = path => {
  return {
    type: CHANGE_PATH,
    payload: path
  }
}