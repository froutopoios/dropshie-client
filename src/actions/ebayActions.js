import { ACCOUNT, EBAY } from './types';

export const fetchPolicies = () => ({type: ACCOUNT.GET_EBAY_POLICIES});
export const refreshPolicy = payload => ({type: ACCOUNT.REFRESH_POLICY, payload});
export const clearRefreshedPolicies = () => ({type: ACCOUNT.CLEAR_REFRESHED_POLICIES})
export const setPolicy = payload => ({type: ACCOUNT.SET_EBAY_POLICY, payload})
export const addNewPolicy = payload => ({type: ACCOUNT.ADD_POLICIES, payload})
export const deleteEbayPolicies = payload => ({type: ACCOUNT.DELETE_POLICIES, payload})
export const getEbayUsers = () => ({type: ACCOUNT.GET_EBAY_USERS});
export const loginEbay = () => ({type: ACCOUNT.LOGIN_EBAY});
export const authEbayUserToDropshie = payload => ({type: EBAY.AUTH_EBAY_USER_TO_DROPSHIE, payload })
export const removeEbayUser = payload => ({type: EBAY.REMOVE_EBAY_USER, payload})