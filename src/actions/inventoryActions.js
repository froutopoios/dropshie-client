import { USER_LISTINGS_FETCHED, 
         CLEAR_INVENTORY,
         INVENTORY, 
         IS_LOADING, 
         STOP_LOADING,
         NET_ERROR,
         ERRORS, 
         UI} from './types';
import axios from '../api/axiosConf';
import { to, errorLogger } from '../utils';


/**
 * 
 * @param {*} payload needs an object [name of the prop]: value 
 */
export const searchInventory = payload => ({type: INVENTORY.SEARCH_ITEMS, payload}); 
export const getItems = payload => ({type: INVENTORY.GET_PAGED, payload});
export const resetFilters = () => ({type: UI.DISABLE_FILTER});



// FETCH LISTINGS
// TODO: remove on production
let t0, t1;
export const fetchInventory = () => async (dispatch, getState) => {
  const token = getState().auth.userToken;

  let url = '/api/v1/ListManager/UserListingItems'

  t0 = performance.now();
  dispatch({ type: IS_LOADING })
  const [ error, response ] = await to(axios({
    method: 'get',
    headers: {
      'Authorization': `bearer ${ token }`
    },
    url
  }));

  if (error) {
    errorLogger(error);
    dispatch({ type: STOP_LOADING });
    console.log(error);
    dispatch({ type: NET_ERROR, payload: 'cannot fetch inventory' });
    
    setTimeout(() => {
      dispatch({type: ERRORS.CLEAR_NET_ERROR})
    }, 3000)

    // TODO: dispath an action to show a message
  } else {
    dispatch({ type: STOP_LOADING });

    t1 = performance.now();
    console.log('fetch listings took ' + ( (t1 - t0) / 1000 ) + " seconds")
  
    dispatch({ type: USER_LISTINGS_FETCHED, payload: response.data.Data })
  }
}

// CLEARS THE INVENTORY **  not implemented  **
export const clearInventory = () => {
  return {
    type: CLEAR_INVENTORY,
  }
}

