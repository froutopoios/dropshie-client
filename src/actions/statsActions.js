// import { GET_MAX_ITEMS, GET_CURRENT_ITEMS_COUNT } from '../actions/types';
// import axios from '../api/axiosConf';
// import { to, errorLogger } from '../utils';

// export const getCurrentItemsCount = () => async (dispatch, getState) => {
//   const token = getState().auth.userToken;
//   const getOneItemUrl = '/api/v2/InventoryGrid'

//   const [ error, response ] = await to(axios({
//     method: 'get',
//     headers: {
//       'Authorization': `bearer ${ token }`
//     },
//     params: {
//       page: 1,
//       take: 1
//     },
//     url: getOneItemUrl
//   }))

//   if (error) {
//     errorLogger(error + ' at ' + getOneItemUrl);
//   } else {
//     dispatch({
//       type: GET_CURRENT_ITEMS_COUNT,
//       payload: response.data.Data.TotalResults
//     })
//   }

// }

// export const getMaxItemsCount = () => async (dispatch, getState) => {
//   const token = getState().auth.userToken;
//   const url = '/api/v1/Account/UserClaims'

//   const [ error, response ] = await to(axios({
//     method: 'get',
//     headers: {
//       'Authorization': `bearer ${ token }`
//     },
//     url 
//   }))

//   if (error) {
//     errorLogger(error)
//   } else {
//     const data = response.data.Data.reduce((acc, item) => {
//       // console.log(item);
//       if (item.Type === 'MaxInventory') {
//         acc = item.Value
//         return acc
//       } else {
//         return acc;
//       }
//     }, 0)
//     dispatch({
//       type: GET_MAX_ITEMS,
//       payload: parseInt(data) // find the current item count in the user info
//     })
//   }

// }

