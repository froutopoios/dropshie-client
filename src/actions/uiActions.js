import { HANDLE_SHOW_ADD_ITEM_FORM, 
         CHANGE_MENU,
         CHANGE_SETTINGS_TAB,
         CHANGE_SIDEBAR_STATE,
         UI
        } from './types';
import { history } from '../utils';


export const stopLoading = () => { 
  return { type: UI.STOP_LOADING } 
}

// maybe deprecated
export const handleShowAdd = value  => (dispatch, getState) => {
  const isLoading = getState().ui.isLoading;
  
  if (!isLoading) {
    dispatch({
      type: HANDLE_SHOW_ADD_ITEM_FORM,
      payload: value
    })
  } else {
    history.push('/dashboard/addItem')
  }
}

export const keepSidebarCollapsed = value => {
  return {
    type: CHANGE_SIDEBAR_STATE,
    payload: value
  }
}

export const changeSettingsTab = value => {
  let tabName = value;
  const mapToValue = {
    0: 'account',
    1: 'ebayAccounts',
    2: 'list',
    3: 'scan',
    4: 'extensions',
    5: 'ui'
  }

  if (!isNaN(value)) {
    tabName = mapToValue[value]
  }

  return {
    type: CHANGE_SETTINGS_TAB,
    payload: tabName
  }
}