import { AFFILIATION } from './types'

export const getItems = () => ({type: AFFILIATION.GET_AFFILIATIOM_ITEMS});
export const postAffiliationEmail = payload => ({type: AFFILIATION.POST_AFFILIATION_EMAIL, payload})