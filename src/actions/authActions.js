// import axios from 'axios';
import { AUTH, ERRORS } from './types';

// SIGN IN ACTION
export const login = credentials => {
  return {
    type: AUTH.LOGIN,
    payload: credentials
  }
}

export const verify = () => ({type: AUTH.VERIFY})
export const setVerifyError = () => ({type: ERRORS.VERIFY_ERROR})
export const stopVerifyError = () => ({type: ERRORS.STOP_VERIFY_ERROR})

export const register = payload => {
  return {
    type: AUTH.REGISTER,
    payload
  } 
}

export const postFacebookData = payload => ({type: AUTH.POST_FACEBOOK_DATA, payload})
export const logout = () => ({type: AUTH.LOG_OUT})




