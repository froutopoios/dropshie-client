import { EDITOR, ACCOUNT } from './types';

export const fetchTemplates = () => ({type: ACCOUNT.FETCH_TEMPLATES});
export const fetchTemplate = id => ({type: EDITOR.GET_TEMPLATE_BY_ID, payload: id});
export const saveTemplate = payload => ({ type: EDITOR.SAVE_TEMPLATE, payload });
export const saveTemplateSucces = payload => ({ type: EDITOR.SAVE_TEMPLATE_SUCCESS, payload });