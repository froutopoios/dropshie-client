import { SETTINGS, ACCOUNT } from './types'


export const refreshPasteToken = () => ({type: ACCOUNT.REFRESH_PASTE_TOKEN})
export const getUserInfo = () => ({type: ACCOUNT.GET_USER_INFO})

// profile
export const fetchProfile = value => async (dispatch, getState) => {
  const userId = getState().acount.userId;
  console.log(userId);
}

// POST SETTINGS
export const postSettings = data => ({type: SETTINGS.POST, payload: data});

// For saga
// TODO: change the name (gets uset settings and templates)
export const fetchSettings = () => {
  return { type: SETTINGS.FETCH }
}

export const fetchUserSettings = () => ({ type: SETTINGS.FETCH_USER_SETTINGS });

// export const getTemplateById = id => ({type: SETTINGS.GET_TEMPLATE_BY_ID, payload: id})