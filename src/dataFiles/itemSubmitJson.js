import { json } from 'd3';
import omit from 'lodash/omit';

export default (mods, fromFetcher) => {
  const data = {
    EbayProperties: {
      Country: 1, // fetcher -- DONE -- 
      DestinationMarket: 1, // fetcher -- DONE -- 
      SellMarketUserName: "testuser_nakata", // fetcher -- DONE -- 
      ItemCondition: 1000, // fetcher -- DONE -- 
      AllowBuyersToRemainAnonymous: 2, // -- done -- from payload 
      ShippingCostPaidBy: "Buyer",
      SellerProfiles: { // notFound
        sellerPaymentProfileId: "5583454000", // -- DONE - 
        sellerPaymentProfileName: "PROFNAME NO NEED", // -- DONE -- 
        sellerReturnProfileId: "5767206000", // -- DONE -- 
        sellerReturnProfileName: "PROFNAME NO NEED", // -- DONE --
        sellerShippingProfileId: "5524618000", // -- DONE -- 
        sellerShippingProfileName: "PROFNAME NO NEED" // -- DONE --
      },
      SelectedCategory: { // mod -- DONE -- 
        title: "Hats", // -- DONE -- 
        id: "52365" // -- DONE -- 
      },
      ItemLocation: "USA", //  -- DONE -- 
      RequireImmediatePayment: 1, // as it is -- DONE -- 
      ShippingCostPaidBy: "Buyer", // as it is  --- DONE --
      // IsPrime: 1, // notFound // OBSOLETE TODO: check if working without it.
      ProductReferenceId: null, // as it is -- DONE 
      ListingDuration: null, // -- DONE -- 
      PrivateListing: 2, // mods - settings - Done
      UseEbayCompetitors: false, // notFound  ***
      CompetitorsPriceDiff: null, // notFound ***
      CompetitorEbayItemId: null // notFound ***
    },
    GenericProperties: {
      ItemId:"00000000-0000-0000-0000-000000000000",
      OriginMarket: 1, // mods fether -- DONE -- 
      OriginProductCode: "", // mod -- DONE --
      OriginProductSecondaryCode: null, // mod -- DONE -- 
      CurrentListingQuantity: 2, // from settings -- DONE -- 
      ItemSpecifics: { // mod -- DONE -- 
        ItemsSpecifics: []
      },
      AppBaseUrl: "https://profitize.qwerty/DropShip.Web" // -- DONE -- karfoto
    },
    UserPreferences: {
      ProtectionAddon: true, // from settings  -- DONE
      ProtectionBackOrder: true, // from settings -- DONE
      ProtectionShippingDays: true, // from settings -- DONE
      ProtectionHandlingDays: true, // from settings -- DONE
      DesiredListingQuantity: 2, //  -- From submit -- DONE --
      PreferedOfferSelectionPolicy: 0, // from settings -- DONE
      PrimeProtectionType: 2, // from settings -- DONE
      // DispatchTimeMax: 2, // notFound  ***
      ShouldUpdateListingQty: 1 // from settings -- DONE -- 
    },
    ItemProperties: {
      DestinationDescriptionHtml: "<p>HTML</p>", // notFound originMainDescriptionHtml ?  -- DONE -- WITH CAUTION
      EAN: "Does Not Apply", // mod -- DONE --
      ISBN: null, // mod -- DONE -- 
      UPC: "Does Not Apply", // mod -- DONE -- 
      MPN: "Does Not Apply", // mod -- DONE --
      Brand: "ANT EXPEDITION", // mod -- DONE --
      ProductTitle: // mod -- DONE -- 
        "ANT EXPEDITION Winter Kint Hat Wool Watch Cap Fur Skull Beanie Cold Weather Warm",
      OriginProductUrl: "", // from fetcher -- DONE --
      UserProvidedOriginProductUrl: "https://www.amazon.com/dp/B07J6667Z7", // mod -- DONE -- 
      Images: [ // mod -- DONE -- 
        {
          isThumbnail: false,
          width: 0,
          height: 0,
          url:
            "https://images-na.ssl-images-amazon.com/images/I/51OGCyonaxL.jpg",
          IsMain: true,
          Selected: true,
          ImageId: 0,
          IsSelected: true
        }
      ]
    },
    PricingProperties: {
      CurrentListingPrice: 18.5, //mod  -- DONE --
      BreakEvenPers: 1.1, // have to check! -- DONE -- 
      PaypalFeesPers: 1.3, // mod -- DONE --
      EbayFeesPers: 1.2, // mod -- DONE -- 
      ProfitPers: 0.2, // mod -- DONE --
      FixedCost: 2.1, // mod -- DONE --
      FixedProfit: 1.2, //mod  -- DONE --
      SourceTax: 0.2, // mod -- DONE --
      SourcePrice: 14.5, // mod -- DONE --
      NetProfit: 1.2 // mod -- DONE -- 
    }
  };

  console.log('from fetcher....', fromFetcher);
  console.log('mods', mods);

  const { originPrice, paypalFeesPers, ebayFeesPers, fixedCost, feesPers, sellPrice, sourceTax, fixedProfit, profitPers, netProfit, sellingMarket, selectedCategory } = mods;

  const { 
      GenericData: { 
        ItemCondition, 
        OriginMarket, 
        OriginProductCode,
        OriginProductSecondaryCode, 
        UserProvidedOriginProductUrl
      },
      EbayData: {
        Country,
        DestinationMarket,
        SellMarketUserName
      }
    } = fromFetcher;


  // -- POLICIES -- 
  const { selectedPaymentPolicy, 
          selectedReturnPolicy, 
          selectedShippingPolicy,
          returnPolicies,
          shippingPolicies,
          paymentPolicies } = mods;

  // findName and Id helper
  const findProfileName = (policies, selectedPolicyId) => {
    const policy =  policies.filter(policy => policy.ProfileId === selectedPolicyId);
    return policy.ProfileName
  }

  // sanitize originUrl
  let originUrlArr = UserProvidedOriginProductUrl.split('ref');
  let originUrl = originUrlArr[0];
  console.log(originUrl);

  // remove image id
  const images = mods.images.map(image => omit(image, 'ImageId'));
  console.log('images...', images)

  const sellerPaymentProfileName = data.EbayProperties.SellerProfiles.sellerPaymentProfileName
  const sellerReturnProfileName = data.EbayProperties.SellerProfiles.sellerReturnProfileName
  const sellerShippingProfileName = data.EbayProperties.SellerProfiles.sellerShippingProfileName;

  data.EbayProperties = {
    ...data.EbayProperties,
    Country,
    DestinationMarket,
    SellMarketUserName,
    ItemCondition,
    PrivateListing: mods.PrivateListing,
    ItemLocation: mods.defaultItemLocation,
    SellerProfiles: {
      sellerPaymentProfileId: selectedPaymentPolicy,
      sellerPaymentProfileName,
      sellerReturnProfileId: selectedReturnPolicy,
      sellerReturnProfileName,
      sellerShippingProfileId: selectedShippingPolicy,
      sellerShippingProfileName
    },
    UseEbayCompetitors: mods.ebayCompetitors,
    CompetitorsPriceDiff: mods.competitorsPriceDifference,
    CompetitorsEbayItemId: mods.ebayItemId,
    SelectedCategory: mods.selectedCategory,
    ListingDuration: mods.listingDuration
  }

  // specifics
  data.GenericProperties = {
    ...data.GenericProperties,
    ItemSpecifics: {
      ...data.GenericProperties.ItemSpecifics,
      ItemsSpecifics: [
        ...data.GenericProperties.ItemSpecifics.ItemsSpecifics,
        ...mods.itemSpecifics
      ]
    }
  }

  // ---*--- USER PREFFERENCES ---*----
  data.UserPreferences = {
    ...data.UserPreferences,
    ProtectionAddon: mods.ProtectionAddon,
    ProtectionBackOrder: mods.ProtectionBackOrder,
    ProtectionShippingDays: mods.ProtectionShippingDays,
    ProtectionHandlingDays: mods.ProtectionHandlingDays,
    PreferedOfferSelectionPolicy: mods.PreferedOfferSelectionPolicy,
    PrimeProtectionType: mods.PrimeProtectionType,
    DesiredListingQuantity: mods.desiredItemQty,
    ShouldUpdateListingQty: mods.ShouldUpdateListingQty
    
  }
  
  // handle if protection is off
  if (mods.ProtectionHandlingDays) {
    data.UserPreferences = {
      ...data.UserPreferences,
      ProtectionMaxHandlingDays: mods.ProtectionMaxHandlingDays, // what if its false
    }
  }
  
  if (mods.ProtectionShippingDays) {
    data.UserPreferences = {
      ...data.UserPreferences,
      ProtectionMaxShippingDays: mods.ProtectionMaxShippingDays, // what if tis false
    }
  }

  // *** ----------------  PRiCING ----***

  data.PricingProperties = {
      ...data.PricingProperties,
      SourcePrice: originPrice,
      CurrentListingPrice: parseFloat(sellPrice),
      PaypalFeesPers: paypalFeesPers,
      EbayFeesPers: ebayFeesPers,
      BreakEvenPers: feesPers,
      FixedCost: fixedCost,
      FixedProfit: fixedProfit,
      SourceTax: sourceTax,
      ProfitPers: profitPers,
      NetProfit: parseFloat(netProfit)
  }

  data.ItemProperties = {
    ...data.ItemProperties,
    OriginProductUrl: originUrl, // TODO: check that
    UserProvidedOriginProductUrl: UserProvidedOriginProductUrl,
    DestinationDescriptionHtml: mods.template,
    EAN: fromFetcher.ItemProperties.EAN,
    ISBN: fromFetcher.ItemProperties.ISBN,
    UPC: fromFetcher.ItemProperties.UPC,
    MPN: fromFetcher.MPN,
    Brand: fromFetcher.Brand,
    ProductTitle: mods.productTitle,
    Images: mods.images
  }


  data.GenericProperties = {
    ...data.GenericProperties,
    UserId: mods.UserId,
    CurrentListingQuantity: mods.DefaultItemQty,
    OriginMarket,
    OriginProductCode: fromFetcher.GenericData.OriginProductCode,
    OriginProductSecondaryCode,
  }

  console.log('------------')
  console.log('final Obj', data);

  const strippedForTesting = JSON.parse(JSON.stringify(data))
  strippedForTesting.ItemProperties.DestinationDescriptionHtml = '<div>boing</div>'
  console.log('stripped', strippedForTesting);

  return data;
};
