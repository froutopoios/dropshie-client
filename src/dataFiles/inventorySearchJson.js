/**
 * 
 */
export const inventoryParams = {
  getParams: function () {
    return data;
  }
};


const data = {
  ItemProperties: null,
  Supplier: null,
  DateAddedFrom: null,
  DateAddedTo: null,
  CurrentStockQtyEquals: null,
  CurrentStockQtyFrom: null,
  CurrentStockQtyTo: null,
  ListPriceFrom: null,
  ListPriceTo: null,
  NetProfitFrom: null,
  NetProfitTo: null,
  SourcePriceFrom: null,
  SourcePriceTo: null,
  PaypalFeesFrom: null,
  PaypalFeesTo: null,
  EbayFeesFrom: null,
  EbayFeesTo: null,
  OtherFeesPersFrom: null,
  OtherFeesPersTo: null,
  FixedCostFrom: null,
  FixedCostTo: null,
  TaxFrom: null,
  TaxTo: null,
  FixedProfitFrom: null,
  FixedProfitTo: null,
  DesiredQtyEquals: null,
  DesiredQtyFrom: null,
  DesiredQtyTo: null,
  DaysListedFrom: null,
  DaysListedTo: null,
  CompetitorsPriceDiffFrom: null,
  CompetitorsPriceDiffTo: null,
  HandlingProtectionDaysEquals: null,
  HandlingProtectionDaysFrom: null,
  HandlingProtectionDaysTo: null,
  NumberViewsEquals: null,
  NumberViewsFrom: null,
  NumberViewsTo: null,
  NumberWatchingEquals: null,
  NumberWatchingFrom: null,
  NumberWatchingTo: null,
  NumberSoldEquals: null,
  NumberSoldFrom: null,
  NumberSoldTo: null,
  ConversionRatioFrom: null,
  ConversionRatioTo: null,
  PrimeProtection: null,
  CompetitorsCheck: null,
  SellerSelectionPolicy: null,
  AddOnProtection: null,
  BackOrderProtection: null,
  ShippingDaysProtection: null,
  HandlingProtectionDDL: null,
  UpdateListingQuantity: null,
  EbayUsername: null,
  RepriceState: null,
  SupplierName: null,
  HasSaleFrom: null,
  HasSaleTo: null,
  HasNotSaleFrom: null,
  HasNotSaleTo: null,
  CurrentPage: 1,
  PageSize: 50,
  SortColumn: "SubmitionDate",
  SortColumnDirection: 1,
}


