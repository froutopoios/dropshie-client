import React from "react";
import ReactDOM from "react-dom";
import { Router } from 'react-router-dom';
import { history } from './utils';
import App from "./components/App";
import * as serviceWorker from "./serviceWorker";
import "./index.scss";

import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducers';
import { loadState, saveState } from './config/localStorage';

import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './styles/theme/theme';

import rootSaga from './sagas/rootSaga';


const sagaMiddleware = createSagaMiddleware();


const composeEnhancers =
typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
: compose;



const enhancer = composeEnhancers(
  applyMiddleware(thunk, sagaMiddleware)
  );
  const initialState = loadState();
  
  const store = createStore(
    rootReducer,
    initialState,
    enhancer
    );
    
sagaMiddleware.run(rootSaga);

// TODO: subscribe only when the token becomes null during refresh
// store.subscribe(throttle(() => {
//   console.log('subcribe to store...')
//   const ebay = store.getState().ebay;
//   saveState('ebay', ebay)
// }), 1000);

// PERSIST STATE
// console.log('INDEX IS RUNNINGGG')

window.onbeforeunload = () => {
  console.log('save to storage..')
  // const { auth, fetchedItem, ui: { collapse } } = store.getState();
  const state = store.getState();
  Object.keys(state).forEach(key => {
    if (key === 'auth' || key === 'fetchedItem' || key === 'ui' || key === 'ebayAuth' || key === 'editor') {
      saveState(key, state[key])
    }
  })
}

ReactDOM.render(
  <Provider store={ store }>
    <MuiThemeProvider theme={ theme }>
      <Router history={ history }>
        <App />
      </Router>
    </MuiThemeProvider>
  </Provider>, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
