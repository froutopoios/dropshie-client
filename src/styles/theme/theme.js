import { createMuiTheme } from '@material-ui/core/styles';
import orange from '@material-ui/core/colors/orange';
import teal from '@material-ui/core/colors/teal';
import { blueGrey, amber, red } from '@material-ui/core/colors'

const defaultTheme = createMuiTheme();

const theme = createMuiTheme({
  typography: {
    // "fontFamily": "\"Roboto\", \"Helvetica\", \"Arial\", sans-serif",
    fontFamily: '\'Quicksand\', sans-serif',
    // "fontSize": 14,
    // "fontWeightLight": 300,
    "fontWeightRegular": 500,
    "fontWeightMedium": 500
   },
  palette: {
    primary: { 
      light: teal[200], 
      main: '#2DB4B3',
      // main: teal[400],
      // main: '#007F7F',
      dark: teal[600], // TODO: if the original main color is used have to change the light value
      // font color on dark background
      // contrastText: '#888'
     },
     secondary: {
       light: red[400],
       main: red[500],
       dark: red[600]
     },
     customButton: amber,
     text: {
       primary: blueGrey[500]
     },
    lightBlue: '#76AEED'
  },
  root: {
    primary: { 
      color: orange
    },
    dropshie: {
      color: teal
    }
  },
  Label: {
    primary: {
      marginRight: '15px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      // width: '140px',
      textAlign: 'right',
      fontSize: '1.1rem',
      transition: 'fontSize .2s ease', // not working
      [defaultTheme.breakpoints.down('sm')]: {
        fontSize: '0.8rem'
      },
      fontWeight: '900',
      verticalAlign: 'middle'
    },
    secondary: {
      fontSize: '0.9rem'
    }
  },
  link: {
    color: '#007bff'
  },
  FormControl: {
    left: {
      marginTop: defaultTheme.spacing(2),
      display: 'flex',
      justifyContent: 'space-between'
    }
  },
  status: {
    danger: orange[500]
  },
  marginTop: {
    medium: defaultTheme.spacing(3),
    large: defaultTheme.spacing(6),
    xl: defaultTheme.spacing(8)
  },
  iconBtn: {
    color: teal[400],
    '&:hover': { 
      // boxShadow: '1px 1px 1px rgba(0, 0, 0, .5)'
      filter: 'drop-shadow(1px 1px 1px rgba(0, 0, 0, .5))'
    },
    '&:active': {
      filter: 'none',
      opacity: '.8',
      userSelect: 'none'
    }
  }, 
  overrides: {
    MuiButton: {
      root: {
        fontFamily: '\'Quicksand\', sans-serif',
      },
    },
    MuiTypography: {
      root: {
        color: '#777'
      }
    }
  },
  switch: {
    root: {
      width: {
        small: 50,
        medium: 50
      },
      height: {
        small: 20,
        medium: 50
      },
    },
    thumb: {
      width: {
        small: 23,
        medium: 23
      },
      height: {
        small: 12,
        medium: 23
      }
    },
    track: {
      borderRadius: {
        small: 23 / 2,
        medium: 26 /2
      },
      height: {
        small: '23px',
        medium: '26px'
      },
      width: {
        small: '38px',
        medium: '45px'
      }
    },
    switchBase: {
      transform: {
        small: '14px'
      }
    }
  }
})

export default theme;


