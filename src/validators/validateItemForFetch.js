export default values => {
  let errors = {}
  if (!values.ProductIdInput) {
    errors.ProductIdInput = 'Product url is required'
  }
  return errors; 
}