
export default function validatePass(values) {
  let errors = {};
  if (!values.currentPass) {
    errors.currentPass = 'Password is required'
  }
  // new Pass
  if (values.newPass.length < 6) {
    errors.newPass = 'Must be more than 6 chars'
  }
  if (values.newPass === values.currentPass) {
    errors.newPass = 'New password is the same as the old one'
  }

  if (values.newPass !== values.confirmPass) {
    errors.confirmPass = 'Passwords are not the Same'
  }

  return errors;
}