export default function validateRegister(values) {
  let errors = {};
  
  if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i.test(values.email)) {
    errors.email = 'Email adress is invalid'
  }
  if (!values.name) {
    errors.name = 'Name must not be empty'
  }
  if (!values.sureName) {
    errors.sureName = 'SureName must not be empty'
  }
  if (values.pass.length < 6) {
    errors.pass = 'Password must be more than 6 characters'
  }
  if (values.pass !== values.confirmPass) {
    errors.confirmPass = 'Password is not the same'
  }
  if (!values.terms) {
    errors.terms = 'Have to accept the terms of use'
  }

  return errors;
}