export const saveState = (key, data) => {
  try {
    const serializedData = JSON.stringify(data);
    localStorage.setItem(key, serializedData);
  } catch (error) {
    console.log(error);
  }
}

export const loadState = () => {
  try {
    const auth = JSON.parse(validate(localStorage.getItem('auth')));
    const ui = JSON.parse(validate(localStorage.getItem('ui')));
    const fetchedItem = JSON.parse(validate(localStorage.getItem('fetchedItem')));
    const ebayAuth = JSON.parse(validate(localStorage.getItem('ebayAuth')));
    const editor = JSON.parse(validate(localStorage.getItem('editor')));
    const state = { auth, ui, fetchedItem, ebayAuth, editor }

    // console.log('get_storage', state);

    return state
  } catch (err) {
    console.log('get_storage_error', err)
    return undefined;
  }
};

const validate = val => {
  if (val === null) {
    return undefined
  } else if (Object.keys(val).length === 0 && val.constructor === Object) {
    return {}
  } else {
    return val
  }
}
