import React from "react";

const SvgDecorator = ({
  children,
  width,
  style,
  viewBox,
  className
}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fillRule="evenodd"
      width={width}
      style={style}
      viewBox={viewBox}
      className={className}>
        { children }
      </svg>
  );
};

export default SvgDecorator;
