import React from 'react'

import Amazon from './supliers/Amazon';
import Ebay from './supliers/Ebay';
import HomeDepot from './supliers/HomeDepot';
import Target from './supliers/Target';
import Walmart from './supliers/Walmart';
import Costco from './supliers/Costco';
import Overstock from './supliers/Overstock';
import Vidaxl from './supliers/Vidaxl';
import Thinkgeek from './supliers/Thinkgeek';
import Aliexress from './supliers/Aliexpress';
import Lettopia from './supliers/Lettopia';
import Petplanet from './supliers/PetPlanet';
import Chinabrands from './supliers/ChinaBrands';
import Aosom from './supliers/Aosom';

import Usa from './countryFlags/Usa'
import Canada from './countryFlags/Canada';
import Germany from './countryFlags/Germany';
import Uk from './countryFlags/Uk';
import Australia from './countryFlags/Australia';

const Icon = props => {
  const style = {
    style: {},
    fill: '',
    width: '100%',
    className: '',
    viewBox: '0 0 120 50',
  }
  // order of the passing props is important overriding occurs from right to left
  switch(props.name) {
    case "amazon":
      return <Amazon { ...style } { ...props }/>
    case "ebay":
      return <Ebay { ...style } { ...props }/>
    case "homedepot":
        return <HomeDepot { ...style } { ...props } viewBox='0 0 230 230'/>
    case "target":
      return <Target { ...style } { ...props } viewBox='0 0 432 574'/>
    case "walmart":
      return <Walmart { ...style } { ...props } viewBox='0 0 190 50'/>
    case "costco":
      return <Costco { ...style } { ...props } viewBox='208.5 290.5 201 72'/>
    case "overstock":
      return <Overstock { ...style } { ...props } viewBox='0 0 1225 215'/>
    case "vidaxl":
      return <Vidaxl { ...style } { ...props } viewBox='-1.54861884 -1.54861884 115.55031768 54.71786568'/>
    case "thinkgeek":
      return <Thinkgeek { ...style } { ...props } viewBox='0 0 594 83'/>
    case "aliexpress":
      return <Aliexress { ...style } { ...props } viewBox="0 0 57.573334 13.387917"/>
    case "lettopia":
      return <Lettopia { ...style } { ...props } viewBox="0 0 112 109"/>
    case 'usa':
      return <Usa { ...style } { ...props } viewBox='0 0 7410 3900' />
    case 'canada':
      return <Canada { ...style } { ...props } viewBox='0 0 9600 4800' />
    case 'germany':
      return <Germany { ...style } { ...props } viewBox='0 0 5 3' />
      case 'uk':
          return <Uk { ...style } { ...props } viewBox='0 0 60 30' />
      case 'aus':
          return <Australia { ...style } { ...props } viewBox='0 0 10080 5040' />
      case 'petplanet':
        return <Petplanet { ...style } { ...props } viewBox='0 0 1024 475' />
      case 'chinabrands':
        return <Chinabrands { ...style } { ...props } viewBox='0 0 764.08643 106.71193' />
      case 'aosom':
        return <Aosom { ...style } { ...props } viewBox='0 0 1628 370' />
    default: 
      return;
  }
}

export default Icon;