const proxy = require('http-proxy-middleware');
module.exports = function(app) {
  app.use(proxy('/api/.*', { target: 'http://sandbox.torpec.com' }))
  // app.use(proxy('/sandbox/v1', { target: 'http://sandbox.torpec.com/api/v1' }));
  // app.use(proxy('/sandbox/v2', { target: 'http://sandbox.torpec.com/api/v2' }))
  // app.use(proxy('/test/.*', { target: 'http://sandbox.torpec.com/api/v2' }))
  // app.use(proxy('/api/.*', { target: 'http://localhost:8081/DropShip.WorkLoad' }))
}