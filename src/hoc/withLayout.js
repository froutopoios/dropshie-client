import React, { Fragment } from 'react'
import Header from '../components/Layouts/Header';
import Footer from '../components/Layouts/Footer';


export const withLayout = WrappedCompoonent => {
  return (props) => {
    return (
    <Fragment>
      <Header/>
        <div className='layout__container'>
          <WrappedCompoonent { ...props }/>
        </div>
      <Footer/>
    </Fragment>
    )
  }
}

