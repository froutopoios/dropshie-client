import React, { Component } from 'react';

const withCounter = WrappedComponent =>  class extends Component {
    state = {
      counter: 0
    }

    incrementCounter = () => {
      this.setState(prevState => {
        return { counter: prevState.counter + 1}
      })
    }

    render() {
      console.log(this.props);
      return <WrappedComponent increment={ this.incrementCounter }
                               counter={ this.state.counter }
                               { ...this.props } />
    }
  }

  export default withCounter;
