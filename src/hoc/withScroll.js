import React, { Component } from 'react';


export const withScroll = WrappedComponent => {
  return class extends Component {
    state = {
      isInScroll: false,
      positionY: 0,
      directionIsUp: true
    }

    componentDidMount() {
      window.addEventListener('scroll', this.handleScroll, { passive: true })
    }

    componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
    }

    // TODO: handle the offset to trigger the directionDown
    handleScroll = event => {
      // setTimeout(() => {
        // console.log(window.scrollY);
        let isInScroll = window.scrollY > 20 ? true : false;
        let position = window.scrollY;
        let directionIsUp = false;
        const offset = 500; // distance from the top to keep the nav visible
  
        this.setState( prevState => {
          // console.log(position, prevState.positionY);
          if (position < prevState.positionY ||  window.scrollY < offset) {
            directionIsUp = true;
          }
          // console.log(directionIsDown);
  
          return {
            isInScroll,
            positionY: position,
            directionIsUp
          }
        })
      // }, 500)
    }

    render() {
      const { isInScroll, directionIsUp } = this.state;
      return <WrappedComponent isInScroll={ isInScroll }
                               directionIsUp={ directionIsUp }
                               { ...this.props }/>
    }
  }
}