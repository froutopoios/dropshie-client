import { useState, useEffect } from 'react'

/**
 * a name attribute has to be provided to the field that is used for the validation and this is used inside the handle change method of the hook to identify the current field
 * @param {*}  initialState  accepts the state of the current component 
 * @param {*} validate the validation method 
 */
function useFormValidation(initialState, validate) {
  const [values, setValues] = useState(initialState);
  const [errors, setErrors] = useState({})
  const [isSubmiting, setSubmitting] = useState(false);
  const [counter, setCounter] = useState(0)

  const InitStr = JSON.stringify(initialState);
  const valuesForCompare = JSON.stringify(values);

  useEffect(() => {
    setValues(initialState);
    setCounter(counter + 1)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [InitStr])

  useEffect(() => {
    if (isSubmiting) {
      const valid = Object.keys(errors).length === 0
      if (valid) {
        console.log('authenticated');
        setSubmitting(false);
      } else {
        setSubmitting(false); // if there are errors will be returned from the hook
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errors])

  // trigger the errors only if user typed something onChange
  useEffect(() => {
    if (counter > 1) {
      const validationErrors = validate(values);
      setErrors(validationErrors);
    }
  }, [valuesForCompare])
  
  const handleChange = name => event => {
    setCounter(counter + 1)
    // console.log('inside hook validation', event.target.value, name);

    setValues({
      ...values,
      [name]: event.target.value
    })
  }

  // give the name of the checkbox. must be the same name in the init state
  const handleCheckBoxChange = name => () => {
    setValues({
      ...values,
      [name]: !values[name] 
    })
  }
 
  const handleBlur = () => {
    const validationErrors = validate(values);
    // console.log(validationErrors);

    setErrors(validationErrors);
  }

  const handleSubmit = event => {
    event.preventDefault();
    const validationErrors = validate(values);
    setErrors(validationErrors);
    setSubmitting(true);
  }

  // returns the methods so these can be restructured 
  return { handleChange, handleSubmit, handleBlur, values, errors, isSubmiting, counter, handleCheckBoxChange }
}
 
export default useFormValidation;