import axios, { tokenConfig } from './axiosConf';
import keys from '../keys/ebayKeys';

const baseUrl = '/api/v1/Settings'

export const getEbayUsers = async (userToken) => {
  const url = `${baseUrl}/GetAuthAndAuthEbayUsernames`;
  const res = await axios.get(url, tokenConfig(userToken));

  return res;
}

export const getEbayPolicies = async (userToken, userId) => {
  const url = `${baseUrl}/EbayPolicies/${ userId }`
  const res = await axios.get(url, tokenConfig(userToken));

  return res;
}

export const refreshPolicy = async (userToken, { user, market }) => {
  const url = `${baseUrl}/RefreshAndMergePoliciesFromEbay`
  const data = {
    EbayUserName: user,
    EbayStore: market
  }
  const res = await axios.post(url, data, tokenConfig(userToken));

  return res
}
 
export const setEbayPolicy = async (userToken, { user, market, SelectedPaymentPolicy, SelectedShippingPolicy, SelectedReturnPolicy }) => {
  const url = '/api/v1/Settings/SetEbayPoliciesSelections'
  const data = {
    EbayUserName: user,
    EbayStore: market,
    PaymentSelection: SelectedPaymentPolicy,
    ShippingSelection: SelectedShippingPolicy,
    ReturnsSelection: SelectedReturnPolicy
  }
  const res = await axios.post(url, data, tokenConfig(userToken))

  return res;
}

export const deleteEbayPolicies = async (userToken, { ebayUser, market }) => {
  const url = `${baseUrl}/DeleteCountryEbayPolicies`;
  const data = {
    EbayUserName: ebayUser,
    EbayStore:market
  }
  const res = await axios.post(url, data, tokenConfig(userToken))

  return res;
}

export const loginEbayUser = async (userToken) => {
  const url = 'api/External/v2/ebay-auth-n-auth/session'
  const data = {
    runame: keys.runame
  }
  const res = await axios.post(url, data, tokenConfig(userToken))

  return res
}

export const authEbayUserToDropshie = async (userToken, session, username) => {
  const url = 'api/External/v2/ebay-auth-n-auth/success';
  const payload = {
    session: encodeURI(session),
    username
  }

  const res = await axios.post(url, payload, tokenConfig(userToken))

  return res
}

export const removeEbayUser = async (userToken, userName) => {
  const url = `${ baseUrl }/RemoveEbayUser`;
  const payload = {
    UserName: userName
  }

  const res = await axios.post(url, payload, tokenConfig(userToken))

  return res;
}