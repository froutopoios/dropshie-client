import axios from './axiosConf';
import { tokenConfig } from './axiosConf'

const handleError = (res) => {
  if (res.status > 200) {
    throw new Error('net error')
  }
}

export const fetchSettings = async (userToken, userId) => {
  const res =  await axios.get(`/api/v2/UserSettings/${userId}`, tokenConfig(userToken));
  handleError(res);
  return res
}

export const postSettings = async (userToken, userId, data) => {
  const config = {
    headers: {
      Authorization: `Bearer ${userToken}`,
     'Content-Type': 'application/json' 
    }
  }
  const res = await axios.post(`/api/v2/UserSettings/kolos${userId}`, data, config)

  return res
}

export const fetchTemplateById = async (token, id) => {
  const res = await axios.get(`api/v1/Settings/GetUserTemplateById/${id}`, tokenConfig(token))

  return res
}