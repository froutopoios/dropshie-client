## Login
```
api/v1/Account/ApiLogin'
```
## Register
```
api/v1/Account/Register
```
## BasicApplicationUserInfo
```
api/v1/Account/BasicApplicationUserInfo
```
## Change password
```
/api/v1/Account/ChangePassword
```
## REFRESH TOKEN DROPSHIE PASTE
```
/api/v1/Settings/RefreshDropshiePasteKey
```
---

## fetch templates
```
/api/v1/Settings/GetUserListingTemplates
```
