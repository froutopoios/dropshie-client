import axios, { tokenConfig } from './axiosConf';

// LOGIN
export const loginUser = async creds => {
  const url = '/api/v1/Account/ApiLogin'
  const res = await axios.post(url , { ...creds, appid: 'Dropshie_com_web_app' })

  return res
}

export const registerUser = async ({ email, pass, confirmPass, name, sureName, terms }) => {
  const data = {
    Email: email,
    Password: pass,
    ConfirmPassword: confirmPass,
    Name: name,
    Surname: sureName,
    TermsAccepted: terms.toString()
  }

  const url = '/api/v1/Account/Register';
  const res = await axios.post(url, data);

  return res
}

export const getUserProgramState = async (userId, token) => {
  const url = `api/v2/UserProgramState/${userId}`
  const res = await axios.get(url, tokenConfig(token));

  return res;
}

export const postFacebook = async data => {
  const payload = {
    token: data.token,
    username: data.name,
    appid: data.appId,
    AffiliateCode: null // need to implement
  }

  console.log('facebook api', payload)

  const url = '/api/v1/Account/FacebookLogin'
  const res = await axios.post(url, payload)

  return res
}

// is obsolete
export const getBasicApplicationUserInfo = async token => {
  const url = 'api/v1/Account/BasicApplicationUserInfo'
  const res = await axios.get(url, tokenConfig(token));

  // const { UserSettings } = res.data.Data;
  // const info = JSON.parse(UserSettings);

  return res
}

export const fetchTemplates = async token => {
  const url = '/api/v1/Settings/GetUserListingTemplates'
  const res = await axios.get(url, tokenConfig(token))

  return res
}

export const fetchOriginMarketplaces = async token => {
  const url = '/api/v2/StaticData/OriginMarketPlaces'
  const res = await axios.get(url, tokenConfig(token))

  if (res.data.StatusCode > 200) {
    throw new Error(`failed whit ${res.data.StatusCode}`)
  }

  return res
}

export const refreshToken = async token => {
  const url = '/api/v1/Settings/RefreshDropshiePasteKey'
  const res = await axios.post(url, { "Value": null }, tokenConfig(token))

  return res
}

export const sendPassChangeRequest = async (token, payload) => {
  const url = '/api/v1/Account/ChangePassword'
  const res = await axios.post(url, payload, tokenConfig(token));

  return res;
}

// REGISTER
 