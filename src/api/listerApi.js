import axios, { tokenConfig } from "./axiosConf";

export const submitToEbay = async (payload, token) => {
  const url = "/api/v2/ItemSubmit/";
  console.log("inside the api", payload);
  const payloadv2 = {
    EbayProperties: {
      Country: 1,
      DestinationMarket: 1,
      SellMarketUserName: "testuser_nakata",
      ItemCondition: 1000,
      AllowBuyersToRemainAnonymous: 2,
      ShippingCostPaidBy: "Buyer",
      ItemLocation: "Athens",
      RequireImmediatePayment: 1,
      ProductReferenceId: null,
      ListingDuration: "GTC",
      PrivateListing: 2,
      UseEbayCompetitors: false,
      CompetitorsPriceDiff: 0,
      CompetitorEbayItemId: null,
      SellerProfiles: {
        sellerPaymentProfileId: "5583444000",
        sellerPaymentProfileName: "PROFNAME NO NEED",
        sellerReturnProfileId: "5583439000",
        sellerReturnProfileName: "PROFNAME NO NEED",
        sellerShippingProfileId: "5583441000",
      },
      SelectedCategory: {
        title: "Controllers & Attachments",
        id: "117042",
      },
      CompetitorEbayItemId: "123",
    },
    GenericProperties: {
      "ItemId": "00000000-0000-0000-0000-000000000000",
      "UserId": "BDBEC804-B02B-490E-A71E-E6E1FD17678F",
      // "UserId": "BDBEC804-B02B-490E-A71E-E6E1FD17678F",      
      // "OriginMarket": 1,
      // "OriginProductCode": "B01MD19OI2",
      // "OriginProductSecondaryCode": null,
      // "CurrentListingQuantity": 6,
      // "ItemSpecifics": {
      //   "ItemsSpecifics": [
      //     {
      //       "Title": "Connectivity",
      //       "SpecificValues": [
      //         {
      //           "Value": "Bluetooth",
      //           "IsSelected": true
      //         }
      //       ]
      //     },
      //     {
      //       "Title": "Type",
      //       "SpecificValues": [
      //         {
      //           "Value": "Wireless controller",
      //           "IsSelected": true
      //         }
      //       ]
      //     }
      //   ]
      // },
      // "AppBaseUrl": "https://profitize.qwerty/DropShip.Web",
    },
    UserPreferences: {
    },
    ItemProperties: {

    },
    PricingProperties: {},
  };
  const res = await axios.post(url, payloadv2, tokenConfig(token));

  return res;
};
