import axios, { tokenConfig } from './axiosConf';

export const getAffiliationData = async token => {
  const url ='api/v2/Affiliations/Current';
  const res = await axios.get(url, tokenConfig(token));

  return res;
}

export const postAffiliationEmail = async (token, { AffiliateCode, Id, PaypalEmail }) => {
  const url = `api/v2/Affiliations/${ Id }`
  const data = {
    Id,
    PaypalEmail,
    HasPaypalEmail: true
  }
  const res = await axios.patch(url, data, tokenConfig(token));

  return res;
} 