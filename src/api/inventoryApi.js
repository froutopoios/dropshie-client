import axios, { tokenConfig, addParams } from './axiosConf';
import { inventoryParams } from '../dataFiles/inventorySearchJson';
import map from 'lodash/map';

// get Paged
export const getItemsPaged = async (userToken, payload) => {
  const url = '/api/v2/InventoryGrid'
  const params = addParams(userToken, payload)
  console.log('parrams', params)
  
  const res = await axios.get(url, params)

  return res;
}

export const searchGrid = async (userToken, ...params) => {
  const url = 'api/v2/InventoryGrid/Search';
  const initParams = inventoryParams.getParams();
  console.log(params);

  for (let obj of params) {
    for (let key in initParams) {
      if (obj[key]) {
        initParams[key] = obj[key]
      }
    }
  }

  console.log(initParams);

  const res = await axios.post(url, initParams, tokenConfig(userToken));

  return res
}