import axios from 'axios';

// sandbox
export const enviroment = 'sandbox'

export const tokenConfig = token => ({
  headers: { Authorization: `Bearer ${ token }` }
})

const ext = tokenConfig => (token, paramsFrom) => {
  const tokenObj = tokenConfig(token);
  return Object.assign(tokenObj, { params: { ...paramsFrom }})
}

/**
 * 
 * @param {String} token [takes the token]
 * @param {object} paramsObj
 */
export const addParams = ext(tokenConfig);


// const url = urlConfig(enviroment);
let baseURL = '/'
if (process.env.NODE_ENV === 'development') {
  baseURL = 'http://localhost:3001'
}

export default axios.create({
  baseURL
})

