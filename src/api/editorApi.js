import axios, { tokenConfig } from './axiosConf';

export const fetchTemplate = async (id, token) => {
  const url = `/api/v1/Settings/GetUserTemplateById/${id}`
  const res = await axios.get(url, tokenConfig(token))

  return res
}

export const saveTemplate = async (payload, token) => {
  console.log('payload for save', payload);
  const url = `/api/v1/Settings/SaveUserTemplate`;
  const res = await axios.post(url, payload, tokenConfig(token));

  return res;
}

// {
// PropertiesJson: {…}, 
// Status: 1, 
// Name: "Space shop-copy-2", 
// UserId: "BDBEC804-B02B-490E-A71E-E6E1FD17678F", 
// Html: " <meta charset="utf-8"> <meta http-equiv="X-UA-Com…>Thank 
// Name: "Space shop-copy-2"
// PropertiesJson: {}
// Status: 1
// TemplateId: "d99bf838-8301-4b8c-ac3e-27f14a043dd1"
// UserId: "BDBEC804-B02B-490E-A71E-E6E1FD17678F"

// payload = { 
//   Html: "<table align=\"center\" style=\"border-spacing: 0px...",
//   Name: "Just List It-Copy-copy-Copy",
//   PropertiesJson: {},
//   Status: 1,
//   TemplateId: "e56e5803-0215-494f-a61f-09636d939235",
//   UserId: "BDBEC804-B02B-490E-A71E-E6E1FD17678F",
//   AccessLevel: 1024
// }
