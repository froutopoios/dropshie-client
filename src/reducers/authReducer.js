import { AUTH } from '../actions/types';

const INITIAL_STATE = {
  isSignedIn: false,
  userToken: '',
  userId: '',
  emailConfirmed: false,
  registered: false
}

export default (state= INITIAL_STATE, { type, payload }) => {

  switch (type) {
    case AUTH.LOGIN_SUCCESS:
      console.log('inside login success', payload)
      const { access_token, userId } = payload;
      return { ...state, userToken: access_token, userId, isSignedIn: true }
    case AUTH.VERIFY_RESPONSE:
      console.log(payload)
      return { ...state, emailConfirmed: payload }
    case AUTH.REGISTER_SUCCESS:
      return { ...state, registered: true }
    case AUTH.CLEAR_REGISTER:
      return { ...state, registered: false }
    case AUTH.LOG_OUT:
      return { ...state, isSignedIn: false, userToken: null, emailConfirmed: false, userId: '' }
    default:
      return state;
  }
}