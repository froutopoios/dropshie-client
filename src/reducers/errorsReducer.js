import {  AUTH, ERRORS, SUBMIT_TO_FETCH_FAILURE, SUBMIT_TO_FETCH_START, NET_ERROR, FILE_ERRORS } from '../actions/types';

const INITIAL_STATE = {
  submitForFetchError: '',
  netError: '',
  fileError: '',
  credsError: '',
  registerError: '',
  verifyError: false
}

export default (state= INITIAL_STATE, { type, payload }) => {
  switch(type) {
    case AUTH.CREDS_ERROR:
      return { ...state, credsError: payload }
    case AUTH.LOGIN_ERROR:
      return { ...state, netError: payload }
    case AUTH.REGISTER_ERROR:
      return { ...state, registerError: payload }
    case NET_ERROR:
      return { ...state, netError: payload }
    case ERRORS.CLEAR_NET_ERROR:
      return { ...state, netError: ''}
    case SUBMIT_TO_FETCH_START:
      return { ...state, submitForFetchError: '' } 
    case SUBMIT_TO_FETCH_FAILURE:
      return { ...state, submitForFetchError: payload }
    case FILE_ERRORS:
      return { ...state, fileError: payload }
    case AUTH.REGISTER_SUCCESS:
      return {  ...state, registerError: false }
    case ERRORS.NET_ERROR:
      return { ...state, netError: payload }
    case ERRORS.VERIFY_ERROR:
      return { ...state, verifyError: true }
    case ERRORS.STOP_VERIFY_ERROR:
      return { ...state, verifyError: false }
    default:
      return state
  }
}