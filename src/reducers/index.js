import { combineReducers } from 'redux';
import AuthReducer from './authReducer';
import RoutingReducer from './routingReducer';
import UiReducer from './uiReucer';
import InventoryReducer from './inventoryReducer';
import AccountReducer from './acountReducer';
import AddItemReducer from './addItemReducer';
import ItemReducer from './itemReducer';
import FetchedItemReducer from './fetchedItemReducer';
import ErrorsReducer from './errorsReducer';
import settingsReducer from './settingsReducer'
import ebayReducer from './ebayReducer';
import ebayAuthReducer from './ebayAuthReducer';
import affiliationReducer from './affiliationReducer';
import editorReducer from './editorReducer';
import listerReducer from './listerReducer';

export default combineReducers({
  auth: AuthReducer,
  routing: RoutingReducer,
  ui: UiReducer,
  inventory: InventoryReducer,
  acount: AccountReducer,
  addItem: AddItemReducer,
  selectedTemplate: ItemReducer,
  fetchedItem: FetchedItemReducer,
  errors: ErrorsReducer,
  settings: settingsReducer,
  ebay: ebayReducer,
  ebayAuth: ebayAuthReducer,
  affiliation: affiliationReducer,
  editor: editorReducer,
  lister: listerReducer
})