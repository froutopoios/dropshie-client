import { UI, AUTH_ERROR, HANDLE_SHOW_ADD_ITEM_FORM, CHANGE_MENU, CHANGE_SIDEBAR_STATE, CHANGE_SETTINGS_TAB } from '../actions/types';

const INITIAL_STATE = {
  isLoading: false,
  showAddItemForm: false,
  changeMenu: '',
  collapse: false,
  settingsTab: 'acount',
  success: false,
  successMessage: '',
  fetchingToken: false,
  facebookPhoto: '',
  spinner: false,
  loadingMessage: '',
  disabled: false,
  filter: false
}

export default (state= INITIAL_STATE, { type, payload }) => {
  switch(type) {
    case UI.IS_LOADING:
      return { ...state, isLoading: true };
    case UI.STOP_LOADING:
      return { ...state, isLoading: false };
    case AUTH_ERROR:
      return { ...state, isLoading: false };
    case HANDLE_SHOW_ADD_ITEM_FORM:
      return { ...state, showAddItemForm: payload }
    case CHANGE_SIDEBAR_STATE:
      return { ...state, collapse: payload }
    case CHANGE_SETTINGS_TAB:
      return { ...state, settingsTab: payload }
    case UI.SUCCESS:
      return { ...state, success: true, successMessage: payload }
    case UI.STOP_SUCCESS:
      return { ...state, success:false, successMessage: '' }
    case UI.FETCHING_TOKEN:
      return { ...state, fetchingToken: true }
    case UI.STOP_FETCHING_TOKEN:
      return { ...state, fetchingToken: false }
    case UI.SET_SPINNER_ON:
      return { ...state, spinner: true, loadingMessage: payload}
    case UI.SET_SPINNER_OFF:
      return { ...state, spinner: false}
    case UI.DISABLE_BTN:
      return { ...state,  disabled: true}
    case UI.ENABLE_BTN:
      return { ...state, disabled: false }
    case UI.ENABLE_FILTER:
      return { ...state, filter: true }
    case UI.DISABLE_FILTER:
      return { ...state, filter: false }
    default:
      return state
  }
}