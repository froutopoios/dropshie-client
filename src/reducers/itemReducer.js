import { SET_ITEM_TEMPLATE } from '../actions/types';

export default (state = '', { type, payload }) => {
  switch(type) {
    case SET_ITEM_TEMPLATE:
      return payload
    default:
      return state
  }
}