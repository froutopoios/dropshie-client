const INITIAL_STATE = {
  paypalFeesPer: 3.40,
  ebayFeesPer: 9,
  feesPer: 0,
  profitPer: 0,
  fixedProfit: 0,
  fixedFees: 0.20,
  tax: 0
}


export default (state = INITIAL_STATE, { type, payload }) => {
  switch(type) {
    default:
      return state;
  }
}