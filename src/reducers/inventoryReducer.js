import { USER_LISTINGS_FETCHED, 
         USER_LISTINGS_ERROR,
         CLEAR_INVENTORY,
         INVENTORY,
         UPDATE_INVENTORY } from '../actions/types';


const INITIAL_STATE = {
  items: [],
  totalresults: 0,
  currentPage: 1,
  PageSize: 0
}

export default (state= INITIAL_STATE, { type, payload }) => {
  
  switch (type) {
    case USER_LISTINGS_FETCHED:
      return { ...state, items: [ ...state.items, ...payload ]  };
    case INVENTORY.GET_PAGED_SUCCESS:

      return { ...state, 
               totalresults: payload.TotalResults,
               currentPage: payload.CurrentPage,
               pageSize: payload.PageSize,
               items: [ ...payload['Results'] ] }
    case INVENTORY.SEARCH_ITEMS_SUCCESS:

      return { ...state,
               totalresults: payload.totalresults,
               currentPage: payload.CurrentPage,
               pageSize: payload.PageSize,
               items: [ ...payload['Results'] ] }
    case USER_LISTINGS_ERROR:
      return [ ...state, ...payload ];
    // case UPDATE_INVENTORY:
    //   const items = [ ...payload ]
    //   return { ...state, items } // TODO: check if this mutates the state in any fashion
    case CLEAR_INVENTORY:
      return { ...state, items: []}
    default:
      return state;
  }
}