import { EDITOR } from '../actions/types';

const INITIAL_STATE = {
  template: '',
  templateName: '',
  accessLevel: '',
  status: '',
  propertiesJson: {},
  currentTemplateId: ''
}

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case EDITOR.SET_CURRENT_TEMPLATE_ID:
      return {
        ...state,
        currentTemplateId: payload
      }
    case EDITOR.GET_TEMPLATE_BY_ID_SUCCESS:
      return { ...state, 
               template: payload.template,
               templateName: payload.name,
               accessLevel: payload.accessLevel,
               status: payload.status,
                }
    case EDITOR.SAVE_TEMPLATE_SUCCESS:
      console.log(payload);
    default:
      return state
  }
}