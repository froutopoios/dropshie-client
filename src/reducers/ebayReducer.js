import { ACCOUNT, EBAY } from '../actions/types';

const INITIAL_STATE = {
  users: [],
  policies: [],
  refreshedPolicies: {},
  ebaySuccess: false,
  ebaySession: '',
  runame: '',
  ebayUserName: ''
}

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case ACCOUNT.GET_EBAY_USERS_SUCCESS:
      return { ...state, users: [ ...payload ] }
    case ACCOUNT.GET_EBAY_POLICIES_SUCCESS:
      return { ...state, policies: [ ...payload ] }
    case ACCOUNT.REFRESH_POLICY_SUCCESS:
      return { ...state, refreshedPolicies: { ...payload }}
    case ACCOUNT.CLEAR_REFRESHED_POLICIES:
      return { ...state, refreshedPolicies: {}}
    case ACCOUNT.LOGIN_EBAY_SUCCESS:
      return { ...state, 
               ebaySession: payload.ebaySession,
               runame: payload.runame, 
               ebaySuccess: true }
    case EBAY.GET_USERNAME:
      return { ...state,  ebayUserName: payload }
    default:
      return state
  }
}