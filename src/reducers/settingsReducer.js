import { SETTINGS, ACCOUNT } from '../actions/types';

const modify = (data, payload) => {
  console.log(data)
  console.log('-------')
  console.log(payload)
  Object.keys(payload).forEach(propName => {
    // const capitalized = propName.charAt(0).toUpperCase() + propName.slice(1);
    data[propName] = payload[propName];
  })
 }

export default (state= {}, { type, payload }) => {
  switch(type){
    case SETTINGS.FETCH_SUCCESS:
      return { ...state, ...payload }
    case SETTINGS.MODIFY:
      const data = { ...state };
      modify(data, payload)

      return data;
    case ACCOUNT.REFRESH_PASTE_TOKEN_SUCCESS:
      return { ...state, DropshiePasteKey: payload }
    case SETTINGS.GET_TEMPLATE_BY_ID_SUCCESS:
      return { ...state, currentTemplate: payload }
    default:
      return state
  }
}