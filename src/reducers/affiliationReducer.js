import { AFFILIATION } from '../actions/types';

export default (state = {}, { type, payload }) => {
  switch (type) {
    case AFFILIATION.GET_AFFILIATIOM_ITEMS_SUCCESS:
      return { ...state, ...payload }
    default:
      return state
  }
}

