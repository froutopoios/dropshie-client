import { CRED_FETCHED, GET_MAX_ITEMS, GET_CURRENT_ITEMS_COUNT, USER_ID_FETCHED, ACCOUNT } from '../actions/types';


// TODO: maybe fill the rest of  the user properties as init to have all the properties together
const INITIAL_STATE = {
  user: '',
  firstName: '',
  plan: {
   current: 0,
   max: 0,
   unused: 0,
  },
  userId: '',
  email: '',
  facebookPhoto: '',
  templates: ''
}

// console.log(INITIAL_STATE);

export default (state= INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case CRED_FETCHED:
      return { ...state, firstName:payload };
    // FROM index actions maybe NOT NEEDED
    case USER_ID_FETCHED:
      return {
        ...state,
        userId: payload
      }
    case ACCOUNT.GET_USER_INFO_SUCCESS:
      return {
        ...state,
        user: payload
      }
    case ACCOUNT.FETCH_EMAIL_SUCCESS:
      return {
        ...state,
        email: payload
      }
    case ACCOUNT.FETCH_TEMPLATES_SUCCESS:
      return {
        ...state,
        templates: payload
      }
    case ACCOUNT.FETCH_ORIGINMARKETPLACES_SUCCESS:
      return {
        ...state,
        originMarketPlaces: payload
      }
    case ACCOUNT.GET_USER_PLAN_SUCCESS:
      return {
        ...state,
        plan: {
          ...state.plan,
          max: payload.MaxAllowInventory,
          current: payload.CurrentActiveInventory,
          unused: payload.UnusedInventory 
        }
      }
    case ACCOUNT.CLEAR_ACCOUNT:
      return { ...state, 
               plan: { 
                 ...state.plan, 
                 max: 0, 
                 current: 0 }, 
               user: '', 
               firstName: '',
               facebookPhoto: '', 
               userId: '' }
    case ACCOUNT.SET_FACEBOOK_PICTURE:
      return { ...state, facebookPhoto: payload}
    default:
      return state;
  }
}