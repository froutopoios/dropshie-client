import { EBAY } from '../actions/types';

const INITIAL_STATE = {
  ebaySession: ''   
}

export default (state = INITIAL_STATE, { type, payload}) => {
  switch (type) {
    case EBAY.SAVE_SESSION:
      return { ...state, ebaySession: payload }
    case EBAY.REMOVE_SESSION:
      return { ...state, ebaySession: '' }
   default:
     return state 
  }
}

