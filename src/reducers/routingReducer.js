import { CHANGE_PATH } from '../actions/types';

const INITIAL_STATE = {
  path: ''
}

export default (state=INITIAL_STATE, { type, payload }) => {
  switch(type) {
    case CHANGE_PATH:
      return { ...state, path: payload }
    default:
      return state
  }
}

