import { LISTINGS } from '../actions/types';

const INITIAL_STATE = {
  paypalFeesPers: 0,
  ebayFeesPers: 0,
  feesPers: 0,
  fixedProfit: 0,
  fixedCost: 0,
  sourceTax: 0,
  profitPers: 0,
  netProfit: 0,
  sellPrice: 0,
  breakEven: 0
}

export default (state=INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case LISTINGS.SEND_INIT_VALUES:
      return {
        ...state,
        ...payload
      }
    case LISTINGS.SEND_PER_CHANGE_TO_STORE:
      const { name, value } = payload;
      return {
        ...state,
        [name]: value
      }
    case LISTINGS.SEND_SELL_AND_NET_TO_STORE:
      const { sellPrice, netProfit } = payload;
      return {
        ...state,
        sellPrice,
        netProfit
      }
    default:
      return state
  }
}