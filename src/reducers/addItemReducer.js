import { TEMPLATES_FOR_ADD_FETCHED, 
         FETCH_ORIGIN_MARKETPLACES,
         FETCH_EBAY_USERS,
         SUBMIT_TO_FETCH_START,
         STOP_FETCHING} from '../actions/types';

const INITIAL_STATE = {
  templates: [],
  originMarketplaces: [],
  ebayUsers: [],
  fetchSuccess: false,
  fetchingItem: false
}

export default (state= INITIAL_STATE, { type, payload }) => {
  switch(type) {
    case TEMPLATES_FOR_ADD_FETCHED:
      return { ...state, templates: payload };
    case FETCH_ORIGIN_MARKETPLACES:
      return { ...state, originMarketplaces: payload }
    case FETCH_EBAY_USERS:
      return { ...state, ebayUsers: payload }
    case SUBMIT_TO_FETCH_START:
       return { ...state, fetchingItem: true }
    case STOP_FETCHING:
       return { ...state, fetchingItem: false }
      default:
        return state;
  }
}