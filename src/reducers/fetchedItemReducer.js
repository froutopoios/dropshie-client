import { SUBMIT_TO_FETCH_SUCCESS } from '../actions/types';
import { CLEAR_ITEM } from '../actions/types';


export default (state={}, { type, payload }) => {
  switch(type) {
    case SUBMIT_TO_FETCH_SUCCESS:
      return { ...state, ...payload };
    case CLEAR_ITEM:
      console.log(state);
      console.log('clear from reducer');
      return Object.assign({})
    default:
      return state
  }
}