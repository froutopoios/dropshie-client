import { takeEvery, put, call, select } from 'redux-saga/effects';
import { getUserToken } from './sagaHelpers/sagaHelpers';
import { AFFILIATION } from '../actions/types';
import { getAffiliationData, postAffiliationEmail } from '../api/affiliationsApi';
import { successSaga } from './uiSagas' 
import { netError } from './errorSagas';

function *getItems() {
  const token = yield select(getUserToken);
  try {
    const res = yield call(getAffiliationData, token)
    console.log(res);
    if (res.data.Success) {
      yield put({type: AFFILIATION.GET_AFFILIATIOM_ITEMS_SUCCESS, payload: res.data.Data})
    }
  } catch (error) {
    yield call(netError, error.message, 3000);
  }
}

function *postEmail({ payload }) {
  const token = yield select(getUserToken);

  try {
    const res = yield call(postAffiliationEmail, token, payload)
    console.log(res)
    if (res.data.Success) {
      yield call(successSaga, 'email updated', 2000)
      yield put({type: AFFILIATION.POST_AFFILIATION_EMAIL_SUCCESS});
    }
    // show a success message
    yield call(getItems); // to render the new email
  } catch (error) {
    yield call(netError, 'cannot set new Affiliation email', 3000)
  }
} 

export function *affiliationSagas() {
  yield takeEvery(AFFILIATION.GET_AFFILIATIOM_ITEMS, getItems);
  yield takeEvery(AFFILIATION.POST_AFFILIATION_EMAIL, postEmail)
}
