import { takeEvery, call, put, select, takeLatest } from 'redux-saga/effects';
import { getBasicApplicationUserInfo, sendPassChangeRequest, getUserProgramState } from '../api/acountApi';
import { ACCOUNT } from '../actions/types';
import { netError } from './errorSagas';
import { getUserId, getUserToken } from './sagaHelpers/sagaHelpers'


function *fetchEmail() {
  const token = yield select(getUserToken);

  try {
    const res = yield call(getBasicApplicationUserInfo, token)
    yield put({type: ACCOUNT.FETCH_EMAIL_SUCCESS, payload: res.data.Data.Email})

  } catch (error) {
    console.log(error)
  }
}

function *changePass(token, { payload: { currentPass, newPass, confirmPass } }) {
  const data = {
    OldPassword: currentPass,
    NewPassword: newPass,
    ConfirmPassword: confirmPass
  }

  try {
    const res = yield call(sendPassChangeRequest, token, data);
    console.log(res);
  } catch (error) {
    console.error('pass error', error)
  }
}

function *getUSerPlan() {
  const token = yield select(getUserToken);
  const userId = yield select(getUserId);

  try {
    const res = yield call(getUserProgramState, userId, token)

    yield put({type: ACCOUNT.GET_USER_PLAN_SUCCESS, payload: res.data.Data})
  }  catch (error) {
    console.log(error)
    yield call(netError, 'cannot fetch user plan')
  }
}

export function* accountSaga() {
  const token = yield select(getUserToken);

  yield takeEvery(ACCOUNT.FETCH_EMAIL, fetchEmail, token);
  yield takeEvery(ACCOUNT.CHANGE_PASS, changePass, token);
  yield takeEvery(ACCOUNT.GET_USER_PLAN, getUSerPlan);
}