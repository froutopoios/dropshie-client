import { takeEvery, delay, put } from 'redux-saga/effects';
import { UI } from '../actions/types';

function *stopLoading() {
  // yield put({type: })
}

export function *uiSaga() {
  yield takeEvery(UI.STOP_LOADING, stopLoading)
}

/**
 * shows a success snackbar to the user, duration is optional
 * use call effect to envoke the generator
 * @param {*} message 
 * @param {*} duration 
 */
export function *successSaga(message, duration) {
  yield put({type: UI.SUCCESS, payload: message })
  yield delay(duration || 3000)
  yield put({type: UI.STOP_SUCCESS})
}