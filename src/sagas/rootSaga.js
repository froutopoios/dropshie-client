import { all } from 'redux-saga/effects'

import { authSaga } from './authSagas';
import { accountSaga } from './accountSaga';
import { settingsSaga } from './settingsSaga';
import { uiSaga } from './uiSagas';
import { ebaySagas } from './ebaySagas';
import { inventorySagas } from './inventorySagas';
import { affiliationSagas } from './affiliationSagas';
import { editorSagas } from './editorSagas';
import { listerSagas } from './listerSagas';
//  consider using fork for parallel

export default function *rootSaga() {
  yield all([
    authSaga(),
    inventorySagas(),
    settingsSaga(),
    accountSaga(),
    uiSaga(),
    ebaySagas(),
    affiliationSagas(),
    editorSagas(),
    listerSagas()
  ])
}