import { takeEvery, select, call, put, delay } from 'redux-saga/effects'
import history from '../utils/history';
import keys from '../keys/ebayKeys';
import { ACCOUNT, UI, ERRORS, EBAY } from '../actions/types';
import { getEbayPolicies, 
         deleteEbayPolicies, 
         refreshPolicy, 
         setEbayPolicy,
         loginEbayUser,
         authEbayUserToDropshie,
         removeEbayUser,
         getEbayUsers } from '../api/ebayApi';

const getUserToken = state => state.auth.userToken
const getUserId = state => state.auth.userId;
const getSession = state => state.ebayAuth.ebaySession;
const redirect = location => history.push(location);

function *netError(message) {
  yield put({type: ERRORS.NET_ERROR, payload: message});
  yield delay(2000)
  yield put({type: ERRORS.CLEAR_NET_ERROR}) 
}

function *fetchEbayUsers() {
  const userToken = yield select(getUserToken);

  try {
    const res = yield call(getEbayUsers, userToken);

    yield put({type: ACCOUNT.GET_EBAY_USERS_SUCCESS, payload: res.data.Data})
  } catch (error) {
    // TODO: implement the array of errors to show a message about the error
    yield put({type: ERRORS.NET_ERROR})
  }
}

function *fetchEbayPolicies() {
  const userToken = yield select(getUserToken)
  const userId = yield select(getUserId); 

  try {
    const res = yield call(getEbayPolicies, userToken, userId);

    if (res.status === 200) {
      yield put({type: ACCOUNT.GET_EBAY_POLICIES_SUCCESS, payload: res.data.Data})
    }
  } catch (error) {
    yield put({type: ERRORS.NET_ERROR})
  }
}

function *refreshEbayPolicy({ payload }) {
  const userToken = yield select(getUserToken);


  yield put({type: UI.SET_SPINNER_ON, payload: 'fetching policies from ebay'})
  try {
    const res = yield call(refreshPolicy, userToken, payload);
    if (res.status > 200) {
      yield put({type: ERRORS.NET_ERROR, payload: 'Something went wrong, please try again'})
    }
    if (res.data.HasErrors) {
      yield put({type: ERRORS.NET_ERROR, payload: res.data.Errors})
    } else {
      yield put({type: ACCOUNT.REFRESH_POLICY_SUCCESS, payload: res.data.Data}) // to rerender poolicies
    } 
    yield put({ type: UI.SET_SPINNER_OFF })
  } catch (error) {
    yield put({type: ERRORS.NET_ERROR, payload: 'Something went wrong, please try again'})
    yield put({ type: UI.SET_SPINNER_OFF })
  }
}

function *saveEbayPolicy({ payload }) {
  const userToken = yield select(getUserToken);

  try {
    const res = yield call(setEbayPolicy, userToken, payload);
    if (res.data.HasErrors) {
      yield put({type: ERRORS.NET_ERROR, payload: res.data.Errors})
    } else {
      yield put({type: ACCOUNT.SET_EBAY_POLICY_SUCCESS})
      yield put({type: UI.SUCCESS, payload: 'ebay policy saved'}) // trigger a success snack 
      yield delay(2000)
      yield put({type: UI.STOP_SUCCESS})
      yield call(fetchEbayPolicies);
    }
  } catch (error) {
    yield put({type: ERRORS.NET_ERROR, payload: 'Something went wrong, please try again'})
  }
}

function *addPolicies({ payload }) {
  const userToken = yield select(getUserToken);
  yield put({type: UI.SET_SPINNER_ON, payload: 'adding new market..'})
  try {
    const res = yield call(refreshPolicy, userToken, payload);
    console.log(res)
    yield put({type: UI.SET_SPINNER_OFF})
    if (res.data.HasErrors) {
      yield put({type: ERRORS.NET_ERROR, payload: res.data.Errors[0].Description})
      yield delay(2000)
      yield put(({type: ERRORS.CLEAR_NET_ERROR})) 
    } else {
      yield put({type: UI.SUCCESS, payload: 'Marketplace added'})
      yield call(fetchEbayPolicies)
      yield delay(3000)
      yield put({type: UI.STOP_SUCCESS})

    }
  } catch (error) {
    yield put({type: ERRORS.NET_ERROR, payload: 'Could not contact ebay.Try again later'})
    yield delay(1000)
    yield put(({type: ERRORS.CLEAR_NET_ERROR}))
  }
}

function *deletePolicies({payload}) {
  const userToken = yield select(getUserToken);

  try {
    const res = yield call(deleteEbayPolicies, userToken, payload)

    if (res.data.HasErrors) {
      yield put({type: ERRORS.NET_ERROR, payload: res.data.Errors[0].Description})
      yield delay(1000)
      yield put(({type: ERRORS.CLEAR_NET_ERROR}))
    } else {
      yield put({type: UI.SUCCESS, payload: 'Market deleted successfully'})
      yield call(fetchEbayPolicies); // to rerender the page
      yield delay(2000)
      yield put({type: UI.STOP_SUCCESS})
    }
  } catch (error) {
    yield put({type: ERRORS.NET_ERROR, payload: 'couldn\'t delete market. Try again later'});
    yield delay(2000)
    yield put({type: ERRORS.CLEAR_NET_ERROR})
  }
} 

function *loginEbay() {
  const token = yield select(getUserToken);
  
  try {
    const res = yield call(loginEbayUser, token)
    console.log(res);
    if (res.data.Data.success) {
      const payload = {
        runame: keys.runame,
        ebaySession: res.data.Data.session
      }

      yield put({type: EBAY.SAVE_SESSION, payload: res.data.Data.session }) // save to local storage
      yield put({type: ACCOUNT.LOGIN_EBAY_SUCCESS, payload})
    }
  } catch (error) {
    yield put({type: ERRORS.NET_ERROR, payload: 'Cannot fetch session from ebay'});
    yield delay(4000)
    yield put({type: ERRORS.CLEAR_NET_ERROR}) 
  }
}

function *authEbayUserToDropshieSaga({ payload: userName }) {
  const token = yield select(getUserToken)

  yield put({type: EBAY.GET_USERNAME, userName})
  const session = yield select(getSession)

  try {
    const res = yield call(authEbayUserToDropshie, token, session, userName)
    //  TODO: handle the errors
    console.log(res)
    // TODO: if success redirect to settings and clear the session and ebay data
    yield call(fetchEbayUsers);
    yield call(redirect, '/dashboard/settings')

  } catch (error) {
    yield call(netError, 'Couldnt Auth the user to dropshie')
  }
}

function *deleteUser({ payload: user }) {
  const token = yield select(getUserToken);

  try {
    const res = yield call(removeEbayUser, token, user)

    if (res.data.HasErrors) {
      const error = res.data.Errors[0]
      yield call(netError, error)
    } else {
      yield call(fetchEbayUsers)
      yield call(redirect, '/dashboard/settings')
    }

  } catch (error) {
    yield call(netError, 'Couldn\'t remove EbayUser')
  } 
}

export function *ebaySagas() {
  yield takeEvery (ACCOUNT.GET_EBAY_POLICIES, fetchEbayPolicies)
  yield takeEvery (ACCOUNT.GET_EBAY_USERS, fetchEbayUsers);
  yield takeEvery (ACCOUNT.SET_EBAY_POLICY, saveEbayPolicy);
  yield takeEvery (ACCOUNT.REFRESH_POLICY, refreshEbayPolicy);
  yield takeEvery (ACCOUNT.DELETE_POLICIES, deletePolicies);
  yield takeEvery (ACCOUNT.ADD_POLICIES, addPolicies)
  yield takeEvery (ACCOUNT.LOGIN_EBAY, loginEbay);
  yield takeEvery (EBAY.AUTH_EBAY_USER_TO_DROPSHIE, authEbayUserToDropshieSaga);
  yield takeEvery (EBAY.REMOVE_EBAY_USER, deleteUser)
}