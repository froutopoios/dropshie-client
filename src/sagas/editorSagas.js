import { takeEvery, select, call, put, delay } from 'redux-saga/effects';
import { getUserToken } from './sagaHelpers/sagaHelpers';
import { EDITOR, ERRORS, ACCOUNT } from '../actions/types';
import { fetchTemplates } from '../actions/accountActions';
import { fetchTemplate, saveTemplate } from '../api/editorApi';
import { handleError } from './sagaHelpers';

function *getTemplate({ payload }) {
  const id = payload;
  const userToken = yield select(getUserToken);

  try {
    const res = yield call(fetchTemplate, id, userToken);

    if (res.status > 200) {
      yield call(handleError);
    }

    const { data: { Data, HasErrors }} = res;
    if (HasErrors) {
      yield call(handleError, 'Couldn\'t get the template')
    } else {
      console.log('Data from back', Data);
      const template = Data.Html;
      const name = Data.Name;
      const accessLevel = Data.AccessLevel
      const status = Data.Status;
      const propertiesJson = Data.PropertiesJson
      // USER ID sometimes is null so is retrieved from auth / redux state

      yield put({type: EDITOR.GET_TEMPLATE_BY_ID_SUCCESS, payload: {
        template,
        name,
        accessLevel,
        status,
        propertiesJson 
      }})
      // console.log(template);
    }


  } catch (error) {
    yield call(handleError, 'Oh my god it\'s happening again');
  }
}

function *saveTemplateSaga ({ payload: RawPayload }) {
  const userToken = yield select(getUserToken);

  const getUserId = state => state.auth.userId;
  const userId = yield select(getUserId);

  const getTemplateProps = state => state.editor;
  const templateProps = yield select(getTemplateProps);

  const originalTemplateName = templateProps.templateName;
  let modifiedName = RawPayload.Name;

  console.log('names', originalTemplateName, modifiedName);

  if (originalTemplateName === modifiedName) {
    modifiedName = modifiedName + '-copy';
  }

  console.log('props of template', templateProps, userId);
  // console.log('templateProps', templateProps);
  console.log('RawPalyload', RawPayload)

  const convertObjectToString = obj => {
    if (obj.length === 0) {
      return '{}'
    } else {
      return JSON.stringify(obj)
    }
  }

  const propertiesJson = convertObjectToString(templateProps.propertiesJson);

  const payload = {
    PropertiesJson: propertiesJson,
    Status: templateProps.status,
    Name: modifiedName,
    UserId: userId,
    Html: RawPayload.Html,
    TemplateId: RawPayload.TemplateId,
    AccessLevel: templateProps.accessLevel
  }

  

  console.log('PropertiesJson', payload.PropertiesJson ? payload.PropertiesJson : 'udne');
  console.log('properties_to_string', payload.PropertiesJson ? payload.PropertiesJson.toString : 'unde')

  console.log('__FINAL PAYLOAD__', payload)
  // from state
  // console.log('from the editor component', payload);
  console.log(modifiedName);

  try {
    const res = yield call(saveTemplate, payload, userToken);
    console.log(res);

    yield put({type: ACCOUNT.FETCH_TEMPLATES});
  } catch (error) {
    console.log(error);
  }
}

export function *editorSagas() {
  yield takeEvery (EDITOR.GET_TEMPLATE_BY_ID, getTemplate);
  yield takeEvery (EDITOR.SAVE_TEMPLATE, saveTemplateSaga);
}