import history from '../../utils/history';
import { put, delay } from 'redux-saga/effects';
import { ERRORS } from '../../actions/types';

/**
 * have to use select effect in order to call the method
 * @param {*} state 
 */
export const getUserToken = state => state.auth.userToken;
export const getUserId = state => state.auth.userId;
export const redirect = location => history.push(location);

export function *handleError (payload) {
  console.log('handel teh error')
  yield put({
    type: ERRORS.NET_ERROR,
    payload: payload ? payload : 'Something went wrong'
  })

  yield delay(700)
  yield put({type:ERRORS.CLEAR_NET_ERROR})
}