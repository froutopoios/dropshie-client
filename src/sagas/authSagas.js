import { takeEvery, call, put, select, delay, all } from  'redux-saga/effects';
import { AUTH, UI, EBAY, ACCOUNT } from '../actions/types';
import { loginUser, registerUser, getBasicApplicationUserInfo, getUserProgramState, postFacebook } from '../api/acountApi';
import { netError } from './errorSagas';
import { getUserId, getUserToken, redirect } from './sagaHelpers/sagaHelpers';


function *login(creds) {
  const { payload } = creds 
  yield put({ type: UI.IS_LOADING })

  try {
    const res = yield call(loginUser, payload)
    if (res.data.hasError) {
      const errorMessage = res.data.message;
      yield call(netError, errorMessage);

      yield put({ type: UI.STOP_LOADING })
    } else {
      yield put({ type: AUTH.LOGIN_SUCCESS, payload: res.data }) // sets the id and the token
      yield call(checkEmailVerification)  // verifies the email
      yield call(redirect, '/dashboard')

      yield call(getUserInfo);
      yield put({ type: UI.STOP_LOADING })
    }
  } catch (error) {
    yield put({type: UI.DISABLE_BTN})
    yield call(netError, error.message, 3000)
    yield put({type: UI.ENABLE_BTN})
    yield put({type: UI.STOP_LOADING})
  }
}

function *getUserInfo() {
  const token = yield select(getUserToken);
  const userId = yield select(getUserId);

  const [userInfo, userProgram] = yield all([
    call(getBasicApplicationUserInfo, token), // from accountApi
    call(getUserProgramState, userId, token) 
  ])

  yield put({type: ACCOUNT.GET_USER_PLAN_SUCCESS, payload: userProgram.data.Data})
}

function *checkEmailVerification() {
  const token = yield select(getUserToken)

  try {
    const { data: { Data: { EmailConfirmed:emailConfirmed }}} = yield call(getBasicApplicationUserInfo, token)
    yield put({type: AUTH.VERIFY_RESPONSE, payload: emailConfirmed})

  } catch (error) {
    console.log(error)
  }
}

function *register({ payload }) {
  yield put({ type: UI.IS_LOADING })

  try {
    const res = yield call(registerUser, payload)
    console.log(res);
    if (res.status === 200) {
      console.log('register success!')
      yield put({type: UI.SUCCESS})
      yield put({type: AUTH.REGISTER_SUCCESS})
      yield put({ type: UI.STOP_LOADING });
      yield delay(500)
      yield put ({type: UI.STOP_SUCCESS})
      yield delay(1000)
      yield put({type: AUTH.CLEAR_REGISTER})
    }
  } catch (error) {
    yield put({ type: UI.STOP_LOADING })
    yield put({type: AUTH.REGISTER_ERROR, payload: error.message})
    // implement action to inform the user with an alert in register page
  }
}

function *logout() {
  yield put({type: EBAY.REMOVE_SESSION})
  yield put({type: ACCOUNT.CLEAR_ACCOUNT});
  yield localStorage.removeItem('auth');
  yield localStorage.removeItem('ebayAuth');
}

function *facebookLogin({ payload }) {
  try {
    // from facebook api 
    yield put({type: ACCOUNT.SET_FACEBOOK_PICTURE, payload: payload.picture.data.url })
    // call to our api
    const res = yield call(postFacebook, payload);
    if (res.status === 200) {
      const payload = {
        access_token: res.data.access_token,
        userId: res.data.userId || ''
      }
      yield put({type: AUTH.LOGIN_SUCCESS, payload })

      // yield call(checkEmailVerification)  // verifies the email
      yield call(redirect, '/dashboard')
    }
    console.log(res)
  } catch (error) {
    console.log(error)
  }
}

export function *authSaga() {

  yield takeEvery(AUTH.LOGIN, login)
  yield takeEvery(AUTH.LOG_OUT, logout);
  yield takeEvery(AUTH.REGISTER, register);
  yield takeEvery(AUTH.VERIFY, checkEmailVerification)
  yield takeEvery(AUTH.POST_FACEBOOK_DATA, facebookLogin)
}