import { put, delay } from 'redux-saga/effects';
import { ERRORS } from '../actions/types';

/**
 * triggers an error snack with a messsage, define duration is optional
 * use the call effect to trigger the error
 * @param {string} message 
 * @param {number} duration 
 */
export function *netError(message, duration) {
  yield put({type: ERRORS.NET_ERROR, payload: message})
  yield delay(duration || 2000);
  yield put({type: ERRORS.CLEAR_NET_ERROR}) 
}
