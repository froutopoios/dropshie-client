import { takeEvery, call, select, put } from 'redux-saga/effects';
import { INVENTORY, UI } from '../actions/types';
import { getItemsPaged, searchGrid } from '../api/inventoryApi';
import { getUserToken } from './sagaHelpers/sagaHelpers';

import { netError } from './errorSagas';

function *getItems({ payload }) {
  const token = yield select(getUserToken);

  try {
    const res = yield call(getItemsPaged, token, payload)
    if (res.status === 200) {
      yield put({type: INVENTORY.GET_PAGED_SUCCESS, payload: res.data.Data})
    }
  } catch (error) {
    console.log(error.message)
    yield call(netError, error.message, 4000)
  }
}

function *searchInventoryGrid({ payload }) {
  const token = yield select(getUserToken);

  try {
    const res = yield call(searchGrid, token, payload);
    console.log('response from search', res);
    if (res.status === 200) {
      yield put({type: INVENTORY.SEARCH_ITEMS_SUCCESS, payload: res.data.Data})
      yield put({type: UI.ENABLE_FILTER});
    }

  } catch (error) {
    console.log(error)
    yield call(netError, 'einai kala ayto?')
  }
}

export function *inventorySagas() {
  yield takeEvery(INVENTORY.GET_PAGED, getItems);
  yield takeEvery(INVENTORY.SEARCH_ITEMS, searchInventoryGrid)
}