 import { call, select } from 'redux-saga/effects';
import { takeEvery, fork, put } from 'redux-saga/effects';
import { SETTINGS, ACCOUNT, UI } from '../actions/types';
import { fetchSettings, postSettings } from '../api/settingsApi';
import { refreshToken } from '../api/acountApi';
import { fetchTemplates, fetchOriginMarketplaces, getBasicApplicationUserInfo } from '../api/acountApi';
import { fetchTemplateById } from '../api/settingsApi'

import { cherryPick } from '../utils';
import { netError } from './errorSagas';
import { successSaga } from './uiSagas';

const getUserToken = state => state.auth.userToken;
const getUserId = state => state.auth.userId;
const getSettingsFromStore = state => state.settings;

function *getUserCreds() {
  const token = yield select(getUserToken);
  const userId = yield select(getUserId);
  
  return { token, userId }
}

function *getSettings(userToken) {
  const userId = yield select(getUserId);
  const token = yield select(getUserToken);

  try {
    const data = yield call(fetchSettings, token, userId)
    const { Data } = data.data
    
    yield put({type: SETTINGS.FETCH_SUCCESS, payload: Data})
  } catch (error) {
    console.log(error)
    // TODO: send action
  }
}

function *getTemplates(userToken) {
  const { token } = yield call(getUserCreds)

  try {
    const { data: { Data } } = yield call(fetchTemplates, token)
    const data = cherryPick(Data)(({ TemplateId, Name }) => ({ templateId: TemplateId, name: Name }))

    yield put({type: ACCOUNT.FETCH_TEMPLATES_SUCCESS, payload: data})
  } catch (error) {
    console.log(error)
    // TODO: send action  
  }
}

function *getTemplateById({ payload: id }) {
  const { token } = yield call(getUserCreds);
  console.log('ID__', id)

  try {
    const { data: { Data: { Html: templateHtml } } } = yield call(fetchTemplateById, token, id);

    yield put({type: SETTINGS.GET_TEMPLATE_BY_ID_SUCCESS, payload: templateHtml})
  } catch (error) {
    console.log(error)
  }
}

function *getOriginMarketplaces(userToken) {
  try {
    const res = yield call(fetchOriginMarketplaces, userToken)
    const originMarketPlaces = cherryPick(res.data.Data)(({ OriginMarketPlaceId, DomainName, WebGateOriginId }) => ({id: OriginMarketPlaceId, domainName: DomainName, webgateId: WebGateOriginId}));
    
    yield put({type: ACCOUNT.FETCH_ORIGINMARKETPLACES_SUCCESS, payload: originMarketPlaces})

  } catch (error) {
    console.log(error);
  }
}

function* getUserInfo() {
  const { token } = yield call(getUserCreds)

  try {
    const res = yield call(getBasicApplicationUserInfo, token);

    yield put({type: ACCOUNT.GET_USER_INFO_SUCCESS, payload: res.data.Data.UserName})
  } catch (error) {
    console.log(error)
    // TODO: send action  
  }
} 

function* getNewPasteToken() {
  const { token } = yield call(getUserCreds)

  yield put({ type: UI.FETCHING_TOKEN })
  try {
    const res = yield call(refreshToken, token)
    const newToken = res.data.Data;

    yield put({ type: UI.STOP_FETCHING_TOKEN })
    yield put({ type: ACCOUNT.REFRESH_PASTE_TOKEN_SUCCESS, payload: newToken })
  } catch (error) {
    yield put({ type: UI.STOP_FETCHING_TOKEN })
    console.log(error)
  }
}

function* getAllSettings() {
  const { token, userId } = yield call(getUserCreds)

    yield fork(getSettings, token, userId);
    yield fork(getTemplates, token);
    yield fork(getOriginMarketplaces, token);
}

// SUBMIT
function* postSettingsWorker({ payload }) {
  console.log('POST SETTINGS WORKNER IS RUNNING')
  const token = yield select(getUserToken);
  const userId = yield select(getUserId);

  yield put({type: SETTINGS.MODIFY, payload: { ...payload, userId: userId  }})
  const settings = yield select(getSettingsFromStore);

  try {
    const res = yield call(postSettings, token, userId, settings)
    console.log(res)
    if(res.data.StatusCode === 200) {
      yield call(successSaga, 'Settings saved')
    }
    
    // send succes snack 
  } catch(error) {
    console.log('error settings post "have to implementet to the snack"', error)
    yield call(netError, 'cannot submit the settings')
  }
}

export function* settingsSaga() {
  yield takeEvery(SETTINGS.FETCH_USER_SETTINGS, getSettings)
  yield takeEvery(SETTINGS.FETCH, getAllSettings)
  yield takeEvery(ACCOUNT.GET_USER_INFO, getUserInfo)
  yield takeEvery(SETTINGS.POST, postSettingsWorker)
  yield takeEvery(ACCOUNT.REFRESH_PASTE_TOKEN, getNewPasteToken)
  yield takeEvery(ACCOUNT.FETCH_TEMPLATES, getTemplates);
  yield takeEvery(SETTINGS.GET_TEMPLATE_BY_ID, getTemplateById)
}