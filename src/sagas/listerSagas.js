import { takeEvery, select, call } from 'redux-saga/effects';
import { LISTINGS } from '../actions/types';
import { netError } from './errorSagas';
import { getUserToken } from './sagaHelpers/sagaHelpers';
import createPayload from '../dataFiles/itemSubmitJson'
import { submitToEbay as postToebay } from '../api/listerApi';
import { delay } from 'lodash';
import testForSubmit from '../utils/testSubmitToEbay';

const getPriceValues = state => state.lister;
const getValuesFromFetcher = state => state.fetchedItem;
const getValuesFromSettings = state => state.settings;

function *submitToEbay({ payload }) {
  const token = yield select(getUserToken);
  const priceValues = yield select(getPriceValues);
  const valuesFromFetcher = yield select(getValuesFromFetcher);
  const valuesFromSettings = yield select(getValuesFromSettings);

  

  const mods = {
    ...payload,
    ...priceValues,
    ...valuesFromSettings
  }

  // console.log(mods);
  const finalObject = createPayload(mods, valuesFromFetcher);
  // console.log('test the request fk the rest', finalObject, token)
  try {
    // const res = yield call(postToebay, finalObject, token);
    console.log('test for submit', testForSubmit);
    const res = yield call(postToebay, testForSubmit, token);
    console.log('res from ebay submision', res);
    for (let error of res.data.Data.ErrorMessages) {
      yield call(netError, error, 2500);
      // yield delay(400)
    }
  } catch (error) {
    yield call(netError, error.message, 4000)
  }
}

export function *listerSagas() {
  yield takeEvery(LISTINGS.SUBMIT_TO_EBAY, submitToEbay)
}