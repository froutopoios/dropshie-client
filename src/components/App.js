import React from 'react'
import { Route, Switch, withRouter } from 'react-router-dom';

// import Header from './Layouts/Header';
import Footer from './Layouts/Footer';
import Landing from './Landing/Landing';
import Dashboard from './Dashboard/Dashboard';
import Login from './Auth/Login/Login';
import Register from './Auth/Register/Register';
import Terms from './Landing/TermsOfUse/TermsOfUse';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faUpload, 
         faDownload, 
         faEye, 
         faList,
         faToiletPaper,
         faCircle } from '@fortawesome/free-solid-svg-icons';

// import notFound1 from '../assets/images/PNP_DETECTED_FATAL_ERROR-600x374.png';
// import notFound2 from '../assets/images/404-1.png';
import notFound3 from '../assets/images/404-hello.png';


const style = {
  height: '100vh',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover'
}

library.add(faUpload, faDownload, faEye, faList, faToiletPaper, faCircle);

// TODO: remove this crap 404 craps
const NotFound = props => (
  <div style={{...style, backgroundImage: `url('${ notFound3 }')` }}></div>
)


//TODO: maybe withRouter triggers rerendering of the app
const  App = ({ location }) => {
  
  // console.log('__inside app')
  return (
        <div className="App">
          {/* { (location.pathname !== '/' && location.pathname !== '/login' && location.pathname !== '/dashboard') && <Header />  } */}
          {/* { location.pathname === '/' && <Header/>} */}

          <Switch>
            <Route exact path='/' component={ Landing }/>
            <Route path='/login' component={ Login }/>
            <Route path='/register' component={ Register }/>
            <Route path='/dashboard' component={ Dashboard }/>
            <Route path='/repricer' component={ NotFound }/>
            <Route path='/termsofuse' component={ Terms }/>
            <Route component={ NotFound }/>
          </Switch>
          {/* <Route exact path='/' component={ Landing }/> */}

          {/* <div style={{ display: 'flex', marginLeft: '1.2rem', fontSize: '.8rem' }}>
            <Link to='/box' style={ sandStyle }>box</Link>
            <Link to='/landing' style={ sandStyle }>landing remake</Link>
            <Link to='/sandbox' style={ sandStyle }>sandbox</Link>
          </div> */}

        { location.pathname !== '/' && <Footer />  }
        </div>
  );
}

export default withRouter(App);

// CssBaseline provides normalization rules to the css of the app.