import React, { Component, Fragment } from 'react';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import { handleShowAdd, changeSettingsTab } from '../../actions/uiActions';
import { withRouter, Redirect } from 'react-router-dom';

import SideBarUI from '../Layouts/SidebarUi/SidebarUi';


class SideBar extends Component {
  state= {
    redirect: false,
    path: ''
  }
  
  componentDidUpdate(prevProps, prevState) {
    if (prevState.redirect === true ) {
      this.setState({ redirect: false })
    }
  }

  handleSettingsChange = value => () => {
    this.props.changeSettingsTab(value);
  }

  redirect = path => {
    this.setState({ redirect: true, path })
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to={ `/dashboard/${ this.state.path }` } />
    }
  }

  expandItemMenu = () => {
    // console.log('he he ');
    this.setState({
      redirect: true,
      path: 'InventorySubmit'
    })
  }

  redirectToAdd = () => {
    console.log(this.props)
    this.props.history.push('/dashboard/addItem')
  }

  render() {
    // TODO: PERFORMANCE renders many times
    return (
      <Fragment>
        { this.renderRedirect() }
        <SideBarUI 
          redirect={ this.redirect }
          collapse={ this.props.collapse }
          path={ this.props.path }
          handleShowAdd={ this.redirectToAdd }
          isFetchedItemActive={ this.props.isFetchedItemActive }
          expandItemMenu={ this.expandItemMenu }
          handleSettingsChange={ this.handleSettingsChange }
          tab={this.props.tab}
          >
          { this.props.children }
        </SideBarUI>
      </Fragment>
    )
  }
}

const mapStateToProps = ({ ui: { collapse, settingsTab }, routing: { path }, fetchedItem}) => {
  const isFetchedItemActive = (!isEmpty(fetchedItem));
  // TODO: convenrt to indexes inside the reducer
  const tabIndexes = {
    'account': 0,
    'ebayAccounts': 1,
    'list': 2,
    'scan': 3,
    'extensions': 4,
    'ui': 5
  }

  const tab = tabIndexes[settingsTab];

  return { path, 
           collapse,
           isFetchedItemActive,
           tab
              }
}
 
const wrapWithRouter = withRouter(SideBar);
export default connect(mapStateToProps, { handleShowAdd, changeSettingsTab })(wrapWithRouter);