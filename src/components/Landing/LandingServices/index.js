import React, { Component } from "react";
import ServicesCard from './ServicesCard/ServicesCard';
import { servicesData } from './servicesData.js'

class Services extends Component {
  state = {
    services: null
  };

  // TODO: if the router is fixed not in need of that
  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.services === nextState.services) {
      return false;
    } else {
      return true;
    }
  }

  componentDidMount() {
    this.setState({
      services: servicesData
    })
  }

  render() {
    if (!this.state.services) {
      return <div>loading</div>
    }
    const { services } = this.state;

    return (
      <section className="landing-services" id="services">
        <div className="landing-services__title">SERVICES</div>
        <div className="landing-services__body">
          { services.map(service => {
            const { title, body, img } = service;
            return <ServicesCard title={ title } 
                                 body={ body }
                                 img={ img }
                                 key={ title }/>
                              })}
        </div>
      </section>
    );
  }
}

export default Services;
