import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const ServicesCard = ({ title, body, img }) => {
  // console.log(props.title);
  return (
    <div className={`services-card__container services-card__container--${ title.toLowerCase() }`}>
      <div className='services-card__img-container'>
        <span className='fa-layers fa-fw'
              style={{ fontSize: '50px' }}>
          <FontAwesomeIcon icon='circle' 
                           color='#2ab4b2'
                           size='lg'
                           style={{ transform: 'translateX(-2px)' }}
                           />
          <FontAwesomeIcon icon={ img }
                           style={{ fontSize: '30px', color: 'white' }}
                          />
        </span>
      </div>
      <h1 className='services-card__title'>{ title }</h1>
      <div className="services-card__body">
        <ul className='services-card__body__list'>
          { body.map(content => {
            return (
              <li key={ content }
                  className='services-card__body__list__item'>{ content }</li>
            ) 
          }) }
        </ul>
      </div>
    </div>
  );
};


export default ServicesCard;