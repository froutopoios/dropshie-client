export const servicesData = [ 
  {
    title: 'Fetch',
    body: [
      "Smart Fetcher / Rich Content Fetching",
      "Price/Seller Selection Policies",
      'Description Optimisation',
      'Multiple Suppliers',
      'Multi List'
      ],
    img: 'upload'
  },
  {
    title: 'List',
    body: [
      "Product Catalogue compliant",
      "Vero Protection",
      'Duplicates Protection',
      '20 Listing Templates',
      'Templates Editor',
      'Optimised Specifics Autofill'
      ],
    img: 'download'
  },
  {
    title: 'Scan',
    body: [
      "Accurate Price/Qty/StockManager",
      "Check Trigger System",
      'Price/Seller Selection Policies',
      'Shipping/Handling days protection',
      'Rules for Competition base auto-pricing',
      'Recent Changes Record'
      ],
    img: 'eye'
  },
  {
    title: 'Inventory',
    body: [
      "Multiple Accounts",
      "Active Listings/Sales Grids",
      'Business Policies synchronisation',
      'Import from eBay/CSV',
      'Quick Access links',
      'Important Metrics'
      ],
    img: 'list'
  },
  {
    title: 'Chrome Extensions',
    body: [
      "Copy/Paste Addresses for order Fulfillment",
      "One click Item Grap n' Fetch",
      'Bulk Grab Items for Multi Listing'
      ],
    img: 'toilet-paper'
  }
]