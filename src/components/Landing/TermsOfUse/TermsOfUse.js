import React from "react";

export default () => (
  <div className="terms">
    <h4 >Summary</h4>
    <p>
      By agreeing to the terms of use you allow Dropshie to connect into your
      private eBay account and perform changes, including creating new, delete
      or modify listings, change listing prices, quantity, and any other
      information and function allowed by eBay API. Listings are subjected to
      fees by eBay Inc.
    </p>
    <p>
      You have to use your own eBay account with this Service. Your eBay
      username and password will not be shared with anyone and will not be used
      for any other reason beyond the scope of usage of Dropshie. You have to
      agree to all eBay agreements that apply to listings you are managing. eBay
      listings are the sole responsibility of the individual sellers. You
      understand that such listings can be charged for a fee on a one off or on
      a monthly basis when listed as GTC.
    </p>
    <p>
      Dropshie does not accept any liability for any damage of any kind or money
      loss that could happen to you either in a direct or indirect usage of
      Dropshie.
    </p>
    <p>
      Dropshie is an automated system that in order to perform its purpose is
      working with multiple 3rd party APIs that can provide inaccurate data from
      time to time.
    </p>
    <p>
      EBay® is a registered trademark of eBay, Inc. This service is not a
      subsidiary of eBay, Inc.
    </p>
    <p>
      By agreeing to the terms of use you are accepting full responsibility and
      taking the risk of using Dropshie for managing your data, knowing it can
      fail to update, or update wrong data and you agree that Dropshie or its
      owners will not be held responsible at any moment, for damages, data loss,
      loss of profits or any other kind of loss or damage.
    </p>
    <p>
      Access to the Service is logged for security reasons. Anonymous
      statistical information is gathered for future internal improvements. All
      source code, including add-ons and other related plug-ins, is copyrighted
      material to Dropshie. Modification of the source code is not permitted
      without prior written approval.
    </p>
    <p>
      We reserve the right to deny the service to anyone at any time for any
      reason.
    </p>
    <p />
    <p>
      <b>
        This Agreement replaces and supersedes any prior verbal understandings,
        written communications and representations.
      </b>
    </p>

    <h4>Terms and Conditions </h4>
    <p>
      By signing up for the Dropshie.com, henceforward “Dropshie”, service,
      henceforward, “Service” or any of the extra services included or sold
      separately, henceforward, “Extra Services” you are agreeing to the
      following terms and conditions henceforward, “Agreement”. Any new features
      or Extra Services which will be added to the Service will be also subject
      to the Agreement unless stated otherwise. Dropshie reserves the right to
      update or change the Agreement without any prior notice to its signed-up
      users henceforward “Users”. You are advised to check the Agreement from
      time to time for any updates or changes that may impact you.
    </p>
    <p>
      By signing up to the Service or any Extra Service and activating your
      account, henceforward “Account” you acknowledge that you have read, agreed
      and accepted to all of the terms and conditions contained in the
      Agreement, including without limitations the Privacy Policy included
      within.{" "}
    </p>

    <h4>Limitation of Liability</h4>
    <p>
      Dropshie shall not be liable for any special, consequential, incidental,
      punitive, direct or indirect damages or loses of profit arising from or
      relating to but not limited to any breach of this agreement and/or use of
      the Service regardless of any notice of the possibility of such damages.
      Notwithstanding the above, nothing in this Agreement is intended to limit
      or restrict the rights of Dropshie to defend its interests.
    </p>

    <p>
      You hereby agree to indemnify Dropshie and any of its parent companies,
      subsidiaries, affiliates, partners, officers, directors, agents,
      employees, and suppliers from any claim or demand, including but not
      limited to reasonable attorneys’ fees. Your use of the Service is at your
      sole risk. The Service is provided on an “as is” and “as available” basis
      without any warranty or condition, express, implied or statutory.{" "}
    </p>
    <p>
      Dropshie does not warrant that the Service will be uninterrupted, timely,
      secure, or error-free nor that the results obtained from the use of the
      Service will be accurate or reliable. Dropshie does not warrant that any
      errors that exist or may occur in the Service will be corrected.
    </p>
    <p>
      It is hereby agreed, understood and clarified that any listings made by
      your Account through the Service are yours alone and that you are fully
      and solely responsible and liable for such listings and their content.
    </p>

    <h4>Dropshie Rights</h4>
    <ul>
      <li>
        Dropshie reserves the right to revise, adjust, modify or terminate the
        Service for any reason, without any prior notice of any type at any
        time.
      </li>
      <li>
        Dropshie may, but have no obligation to, remove User Content and
        Accounts containing content that it determines in its sole discretion to
        be unlawful, offensive, threatening, explicit or violates any party’s
        intellectual property or these Agreement.
      </li>
      <li>
        Dropshie reserves the right for its employees and/or contractors to use
        the Service as merchants themselves and might compete with you, although
        they may not use your confidential information in doing so.
      </li>
      <li>
        Dropshie does not provide exclusive access to any of its Users and
        reserves the right to provide the Service to competing parties of any
        kind.
      </li>
      <li>
        In the event of a dispute regarding Account ownership, we reserve the
        right to request official documentation to determine or confirm Account
        ownership.{" "}
      </li>
    </ul>

    <h4>Account Terms</h4>
    <ul>
      <li>You must be at least 18 years old</li>
      <li>
        To access and use the Service or the Extra Services, you must register
        for an Account by providing your full legal name, a valid email address,
        and/or any other information indicated as required.{" "}
      </li>
      <li>
        Dropshie may reject your application for an Account, or cancel an
        existing Account, for any reason, in its sole discretion.
      </li>
      <li>
        By connecting the Service with any third-party accounts owned by you or
        on your behalf, you hereby grant Dropshie to access such accounts via
        applicable APIs, for the sole purpose of the functionality of the
        Service.
      </li>
      <li>
        You are responsible for keeping your password secure. Dropshie cannot
        and will not be liable for any loss or damage from your failure to
        maintain the security of your Account and password.
      </li>
      <li>
        You are responsible for all activity and content that is posted using
        your Dropshie Account (“User Content”).{" "}
      </li>
    </ul>

    <h4>Term</h4>
    <p>
      This Agreement shall become effective once checked and agreed by User with
      that date set as the “Effective Date” and shall remain in full force on a
      month-by-month basis until terminated by either party.{" "}
    </p>

    <h4>Modifications to the Service and Prices</h4>
    <p>
      Prices for using the Service and Extra Services are subject to change upon
      prior notice and will only apply to future Term invoices. Such notice may
      be provided at any time by posting the changes to drophie.com. Dropshie
      reserves the right at any time, and from time to time, to modify or
      discontinue, the Service or any part of the Service or any Extra Services
      with or without prior notice. Dropshie shall not be liable to you or to
      any third party for any modification, price change, suspension or
      discontinuance of the Service.
    </p>

    <h4>Cancellation and Termination</h4>

    <p>
      You may cancel your Account at any time by cancelling payments via PayPal.
    </p>
    <p>
      We reserve the right to modify or terminate the Service and/or your
      Account for any reason, without notice and at any time.
    </p>
    <p>Upon termination of the Services by either party for any reason:</p>
    <ul>
      <li>You will no longer be able to access your Account</li>
      <li>You will not be entitled to any refunds of any Fees </li>
      <li>All of your data will be deleted from Dropshie database</li>
    </ul>

    <h4>Third Parties Services</h4>
    <p>
      In addition to these Terms, you also agree to be bound by additional third
      parties’ terms of services applicable to your usage of the Service,
      including without limitation, any listings made by you via the Service.
    </p>
    <p>
      Dropshie does not provide any warranties with respect to interoperability
      with third parties services or accounts, has no control over third
      parties’ services or accounts, and shall not be responsible or liable to
      anyone for such third parties’ services or accounts.{" "}
    </p>
    <p>
      If you provide Dropshie access to a third-party account for the use of the
      Service, you grant Dropshie permission to access any applicable data and
      to take any other actions as required for the interoperation of the
      third-party services with the Service, solely on your behalf. Dropshie is
      not responsible for any disclosure, modification or deletion of your data
      or User Content, or for any corresponding losses or damages you may
      suffer, as a result of actions of the Service to a third-party service or
      account.
    </p>
    <p>
      You agree to indemnify and hold us and/or our parent, subsidiaries,
      affiliates, partners, officers, directors, agents, employees, and
      suppliers harmless from any claim or demand, including reasonable
      attorneys’ fees, arising out of your use of a third-party service or your
      relationship with a third-party provider.
    </p>

    <h4>Privacy Policy</h4>
    <ul>
      <li>
        Dropshie uses cookies; these are stored on your local machine and are
        used to remember searches and passwords that you may have entered. This
        enables you to use the site without having to enter your username and
        password each time. Only cookies that are ‘strictly necessary’ for the
        above-mentioned reasons are used. Most browsers allow you to refuse to
        accept cookies and to delete cookies. The methods for doing so vary from
        browser to browser, and from version to version.
      </li>
      <li>
        When you register on Dropshie you are required to provide your name and
        surname, an email address, username and password, we do not share this
        data with any third parties. We may process your account data "account
        data” for the purposes of operating our website, providing our services,
        ensuring the security of our website and services, maintaining back-ups
        of our databases and communicating with you. The legal basis for this
        processing is consent or our legitimate interests, namely, the proper
        administration of our website and business. Our site contains links to
        other websites; we are not responsible for the privacy practices and/or
        content of any 3rd party web sites.
      </li>
      <li>
        We may process any of your personal data identified in this policy where
        necessary for the establishment, exercise or defense of legal claims,
        whether in court proceedings or in an administrative or out-of-court
        procedure. The legal basis for this processing is our legitimate
        interests, namely, the protection and assertion of our legal rights,
        your legal rights and the legal rights of others.
      </li>
      <li>
        Financial transactions relating to our website and services are handled
        by our payment services provider, PayPal. We will share transaction data
        with our payment services providers only to the extent necessary for the
        purposes of processing your payments, refunding such payments and
        dealing with complaints and queries relating to such payments and
        refunds. You can find information about the payment services providers'
        privacy policies and practices at (www.paypal.com).
      </li>
      <li>
        Personal data that we process for any purpose or purposes shall not be
        kept for longer than is necessary for that purpose or those purposes
      </li>
      <li>
        We collect non-personally-identifiable information such as but not
        limited to, your IP address, the type of browser software you use when
        using the Service or Extra Services, the date and time you visit our Web
        site and/or utilize our Services and what pages you viewed when visiting
        our Web site and/or utilizing our Services as part of our analytics
        program.
      </li>
      <li>
        Dropshie may store non-identifying information which does not identify
        you and was provided to us through your use of the Service and the
        Registration of your Account, including without limitation, any of your
        third party’s accounts.
      </li>
      <li>
        Dropshie may share your User Content and non-identifiable information
        with its affiliates. Their use of information shall be limited to these
        Terms.
      </li>
    </ul>
    <p>Your principal rights under data protection law are:</p>
    <ol>
      <li>the right to access;</li>
      <li>the right to rectification</li>
      <li>the right to erasure</li>
      <li>the right to restrict processing</li>
      <li>the right to object to processing</li>
      <li>the right to data portability</li>
      <li>the right to complain to a supervisory authority</li>
      <li>the right to withdraw consent.</li>
    </ol>
    <p>
      For any inquiries or complaints concerning your data please contact us at
      dropshie@outlook.com
    </p>

    <h4>Miscellaneous</h4>
    <p>
      Technical support is only provided to paying Account holders and is only
      available via email to the support email.
    </p>
    <p>
      You may not use the Service for any illegal or unauthorized purpose nor
      may you, in the use of the Service, violate any laws in your jurisdiction,
      including but not limited to, copyright laws and laws applicable to you in
      your customer’s jurisdiction. You will comply with all applicable laws,
      rules and regulations while using the Service.
    </p>
    <p>
      The failure of Dropshie to exercise or enforce any right or provision of
      the Terms shall not constitute a waiver of such right or provision. The
      Terms constitutes the entire agreement between you and Dropshie and govern
      your use of the Service, superseding any prior agreements including but
      not limited to, verbal or written communications.{" "}
    </p>
    <p>
      In the event that any provision of these Terms is held to be invalid or
      unenforceable, the remaining provisions of these Terms will remain in full
      force and effect.
    </p>
    <p>
      You agree not to reproduce, reverse engineer, duplicate, copy, sell,
      resell or exploit any portion of the Service, use of the Service, or
      access to the Service without prior written permission by Dropshie.
    </p>
  </div>
);
