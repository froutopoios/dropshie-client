import React from 'react'
import Icon from '../../../assets/vectors';
import dropSource from '../../../assets/images/logos/drop_source.png';


const Partners = () => (
  <section className='partners'>
    <h1 className='partners__title'>PARTNERS</h1>
    <div className="partners__logo__container">
      <img src={ dropSource } 
           alt="drop-source logo"
           className="partners__logo--drop-source"
           />
    </div>
    <Icon key="lettopia" 
          name='lettopia'
          className='partners__svg--lettopia'
          />
  </section>
)

export default Partners;