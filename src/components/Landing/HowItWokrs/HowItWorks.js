import React from 'react'


const HowItWorks = () => (
  <section className='how-it-works' id='how it works'>
  <h1 className="how-it-works__title">HOW IT WORKS</h1>
  <div className="how-it-works__container">
    <iframe max-width="1140"
            max-height="641"
            title="how it works"
            src="https://www.youtube.com/embed/LsdSUegajRI" 
            frameBorder="0" 
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen
            className='how-it-works__iframe'></iframe>" 
  </div>
</section>
)

export default HowItWorks;