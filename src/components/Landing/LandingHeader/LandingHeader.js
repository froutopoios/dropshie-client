import React from 'react'
import Header from '../../Layouts/Header';
import transLogo from '../../../assets/images/dropshipping_trasparent.png';


const LandingHeader = props => {

  return (
  <header className='landing-header'>
    <Header/>

    <div className="landing-header__hero container">
      <div className="landing-header__trans-logo-container">
        <img src={ transLogo } alt="dropshie logo" className="landing-header__trans-logo-container__img"/>
      </div>
    </div>
  </header>
  )
}

export default LandingHeader;
