import React, { useState, Fragment } from 'react'
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/core/Slider';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid'; 


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    padding:  24
  },
  margin: {
    height: theme.spacing(2)
  },
  greyed: {
    color: '#d3d3d3',
    textShadow: '0px 3px 0px rgba(255,255,255,.3), 0px -1px 0px rgba(0,0,0,.4)'
  },
  grid: {
    root: {
      flexGrow: 1,
    },
    paper: {
      height: 140,
      width: 100
    },
    control: {
      padding: theme.spacing(2)
    }
  }
}));


const iOSBoxShadow = '0 3px 1px rgba(0, 0, 0, .1), 0 4px 8px rgba(0, 0, 0, .13), 0 0 0 1px rgba(0, 0, 0, .02)';
const marks = [];



const IOSSlider = withStyles({
  root: {
    display: 'block',
    color: '#2BBFBC',
    height: 2,
    padding: '15px 0',
    maxWidth: '500px',
    margin: '0 auto'
  },
  thumb: {
    height: 20,
    width: 20,
    backgroundColor: '#555',
    boxShadow: iOSBoxShadow,
    marginTop: -9,
    marginLeft: -11,
    '&:hover,&$active': {
      boxShadow: '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.3),0 0 0 1px rgba(0,0,0,0.02)',
      // Reset on touch devices, it doesn't add specificity
      '@media (hover: none)': {
        boxShadow: iOSBoxShadow,
      },
    }
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 11px)',
    top: -22,
    '& *': {
      background: 'transparent',
      color: '#000'
    }
  },
  track: {
    height: 4
  },
  rail: {
    height: 2,
    opacity: 0.5,
    backgroundColor: '#bfbfbf'
  },
  mark: {
    backgroundColor: '#bfbfbf',
    height: 8,
    width: 2,
    marginTop: -3
  },
  markActive: {
    backgroundColor: 'currentColor'
  }
})(Slider)


// function ValueLabelComponent(props) {
//   const { children, open, value } = props;

//   const popperRef = React.useRef(null);
//   React.useEffect(() => {
//     if (popperRef.current) {
//       popperRef.current.update();
//     }
//   });

//   return (
//     <Tooltip
//       PopperProps={{
//         popperRef,
//       }}
//       open={open}
//       enterDelay={ 2 }
//       enterTouchDelay={0}
//       leaveDelay={ 100 }
//       placement="top"
//       title={ `${value} items` }
//     >
//       {children}
//     </Tooltip>
//   );
// }

const ValueLabel = ({ value }) => (
  <div style={{ marginBottom: '5px',
                width: '150px',
                textAlign: 'center',
                fontSize: '1.2rem'
         }}><b>{ value }</b> <span style={{ color: '#2bb4b3' }}>items</span>
          <div></div>
         </div>
)


export default function CustomizedSlider() {
  const classes = useStyles();
  
  const [value, setValue] = useState(20);
  const [price, setPrice] = useState('Free')
  
  
  const onChangeHanlder = (e, value) => {
    const items = [ 20, 50, 120, 250, 350, 500, 750, 1200, 1500, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]
    const prices = [ 'Free', 4.99, 8.49, 15.99, 22.99, 28.5, 39.5, 54.99, 64.99, 74.99, 99.99, 133.99, 159.99, 191,99, 222.99, 249.99, 274.99, 287.99 ]

    setValue(items[value]);
    setPrice(prices[value]);
  }

  return (
      <Fragment>
         {/* <Paper className={classes.root}> */}
          <Typography id='discrete-slider-always' gutterBottom>Availabe Plans</Typography>
          <div className={ classes.margin }/>
          <div className={ classes.margin }></div>
          <IOSSlider aria-label="iOS slider" 
                     getAriaValueText={ (value) => `${ value } einai` }
                     defaultValue={0}
                     marks={marks}
                     aria-labelledby="discrete-slider-always"
                     step={ 1 }
                     valueLabelDisplay="auto"
                    //  ThumbComponent={ props => <span { ...props }>valies</span> }
                    valueLabelFormat={ () => <ValueLabel value={ value } />}
                    //  valueLabelFormat={ val => `${ value } items` } 
                    //  ValueLabelComponent= { props => <ValueLabelComponent { ...props } value={ value }/>  }
                     min={ 0 }
                     max={ 17 }
                     onChange={ onChangeHanlder }
          />
          <div className={`${ classes.margin } pricing-slider__body__container` } />
          <Grid container className={ classes.grid.root } spacing={ 2 }>
            <Grid item xs={ 6 } md={ 6 }>
              <Typography><b>{ value }</b> virtual inventory entries</Typography>
              <Typography className={ value === 20 ? classes.greyed : null }>{ value === 20 ? 'Does not include' : 'Includes' } DropshiePaste</Typography>
              <Typography>Pricing and quantity automation</Typography>
              <Typography className={ value === 20 ? classes.greyed : null }>{ value === 20 ? 'Limited access to' : 'Access to all listing' } templates</Typography>
            </Grid>
            <Grid item xs={ 6 } md={ 6 }>
              <div style={{ display: 'flex' }}>
                <div>Price:</div>
                <div style={{ marginLeft: '20px', position: 'relative', top: '-5px' }}>
                  <h2>{ price === 'Free' ? price : `${ price } $` }</h2>
                  <p>{ price !== 'Free' ? `Per month` : '' }</p>
                </div>
              </div>
            </Grid>
          </Grid>
        {/* </Paper> */}
      </Fragment>
  );
}

