import React from "react";
import { Link } from 'react-router-dom';
import Slider from './PricingSlider/PricingSlider';


const Plans = () => (
  <section className="pricing-plans" id='pricing-plans'>
    <h1 className='pricing-plans__title'>PRICING PLANS</h1>
    <div className="pricing-plans__hero">
      <h3 className='pricing-plans__hero__title'>SCROLL THE BAR TO SEE ALL AVAILABLE SUBSCRIPTIONS</h3>
    </div>
    <div className="pricing-plans__bar__title">20 items Free</div>
    <div className="pricing-plans__bar">
      <Slider/>
    </div>
    <div className="pricing-plans__hero">
      <h3 className='pricing-plans__hero__title'>
        WANT MORE THAN 10000 ITEMS?<span>please contact us</span>
      </h3>
    </div>
    <div className="subscription">
      <h3 className="subscription__title">Try <span>DROP</span>shie now for free</h3>
      <button className="subscription__button--container">
        <Link to="/register" className="subscription__button">Join Now!</Link>
      </button>
    </div>
  </section>
);
export default Plans;
