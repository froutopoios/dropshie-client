import React, { Fragment } from 'react'
import Icon from '../../../../assets/vectors';
import Flag from '../../../../assets/vectors';
import zooplusImg from '../../../../assets/images/logos/zooplus.png'
import factoryFastImg from '../../../../assets/images/logos/factoryFast.jpg';

const Countries = ({ country, title }) => {
  const commonSuppliers = [
    'amazon',
    'ebay',
    'lettopia',
    'chinabrands',
    'aliexpress'
  ]

  const countries = {
      usa: [
        ...commonSuppliers,
        'homedepot',
        'walmart',
        'target',
        'costco',
        'overstock',
        'vidaxl',
        'thinkgeek',
      ],
      canada: [
        ...commonSuppliers,
        'costco',
        'overstock',
      ],
      germany: [
        ...commonSuppliers,
        'vidaxl',
      ],
      uk: [
        ...commonSuppliers,
        'costco',
        'vidaxl',
        'aosom',
        'petplanet',
        'zooplus',
      ],
      aus: [
        ...commonSuppliers,
        'vidaxl',
        'target',
        'factoryFast'
      ]
  }

  const ShowSupliers = countries[country]
    .map(logo => {
      // TODO: convert zoopplus to svg
      if (logo === 'zooplus') {
        return (
          <div key={ logo } className={`landing-logos__svg-container--style ${ logo }`}>
            <img src={ zooplusImg } 
                 alt="zooplus logo"
                 style={{ width: '100%' }} />
          </div>
        )
      } else if (logo === 'factoryFast') {
        return (
          <div key={ logo } className={ `landing-logos__svg-container--style ${ logo }` }>
            <img src={ factoryFastImg }
                 style={{ width: '100%' }} 
                 alt="factoryfast logo"/>
          </div>
        )
      }

      return <Icon key={ logo }
            name={ logo }
            className={`landing-logos__svg-container--style ${ logo }`}
            />
    })

  return (
    <Fragment>
      <div className="landing-logos__container">
        <Flag name={ country } className='landing-logos__flag'/>
        <h3 className="landing-logos__title">{ title }</h3>        
      </div>
      <div className='landing-logos__svg-container'>
        { ShowSupliers }
      </div>
    </Fragment>
  )
}

export default Countries;

