import React from 'react'
import SuplierLogos from './Countries/SuplierLogos';


const LandingSuppliers = () => {  
  return (
    <div className='container landing-suppliers' id='supliers'>
      <h1 className="landing-supliers__title">SUPPORTED SUPLIERS
        <span className='landing-supliers__title__span'>(More coming soon...)</span>
      </h1>
        <SuplierLogos country='usa' title='UNITED STATES'/>
        <SuplierLogos country='uk' title='UNITED KINGDOM'/>
        <SuplierLogos country='aus' title='AUSTRALIA'/>
        <SuplierLogos country='canada' title='CANADA'/>
        <SuplierLogos country='germany' title='GERMANY'/>
    </div>
  )
}

export default LandingSuppliers
