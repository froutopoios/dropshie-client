import React, { Component, Fragment } from 'react'
import LandingHeader from './LandingHeader/LandingHeader';
import Services from './LandingServices';
import Supliers from './LandingSupliers';
import HowItWorks from './HowItWokrs/HowItWorks';
import Plans from './Plans/Plans';
import Partners from './Partners/Partners';
import Footer from './Footer/LandingFooter';

import { connect } from 'react-redux';

// testi
class Landing extends Component {

  componentDidMount() {
    // console.log(this.props);
    const { isSignedIn, history } = this.props;
    if ( isSignedIn) {
      history.push('/dashboard');
    }
  }

  render() {
    // console.log('__inside landing')
    return (
      <Fragment>
        <div id="top"></div>
        <LandingHeader/>
          <div className="container">
            <Services/>
            <Supliers/>
            <HowItWorks/>
            <Plans/>
            <Partners/>
            <Footer/>
          </div>
      </Fragment>
    )
  }
}

const mapStateToProps = ({ auth: { isSignedIn } }) => {
  return {
    isSignedIn
  }
}

export default connect(mapStateToProps, null)(Landing);
