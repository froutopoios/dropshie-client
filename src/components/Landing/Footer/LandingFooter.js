import React from 'react'
import { faYoutube, faFacebookF, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Link } from 'react-router-dom';

const LandingFooter = () => (
  <div className="landing__footer" data-test='landing-footer'>
    <div className="landing__footer__copywrite">Copyright &copy; dropshie.com 2019</div>
    <div className="landing__footer__terms__container">
      <Link className="landing__footer__terms" to='/termsofuse'>Terms of Use</Link>
    </div>
    <div className="landing__footer__social">
      <a className='landing__footer__social--facebook'
         href="https://www.facebook.com/dropshie">
      <FontAwesomeIcon icon={ faFacebookF }
                       size='lg'
                       color='#3b5998'
                       className='landing__footer__social__logo landing__footer__social__logo--facebook'
      />
      <span>F</span>acebook</a>
      <a className='landing__footer__social--youtube'
            href='https://www.youtube.com/channel/UCNKe9VQCIrkmyJ2kL6RdNGQ/featured'>
      <FontAwesomeIcon icon={ faYoutube }
                       size='lg'
                       color='#c4302b'
                       className='landing__footer__social__logo landing__footer__social__logo--youtube'

      />
      <span>You</span>tube</a>
      <a className='landing__footer__social--instagram'
         href="https://www.instagram.com/dropshie/">
      <FontAwesomeIcon icon={ faInstagram }
                       className='landing__footer__social__logo  landing__footer__social__logo--instagram'
      />
      <span>Insta</span>gram</a>
    </div>
  </div>
)

export default LandingFooter;