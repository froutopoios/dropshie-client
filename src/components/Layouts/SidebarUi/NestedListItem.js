import React from "react";
import { Link as ScrollLing } from "react-scroll";
import { makeStyles } from '@material-ui/styles';

import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const useStyles = makeStyles(theme => ({
  root: {
    transform: 'translateZ(0px)',
    flexGrow: 1,
  },
  active: {
    '& > div': {
      // backgroundColor: '#aaaaaa',
      '& div': {
        color: 'teal',
        position: 'relative',
        '&:after': {
          content: '',
          position: 'absolute',
          borderBottom: '1px solid red',
          top: '50%',
          // height: '100%',
          height: '10px',
          width: '100%',
          color: 'red'
        }
      }
    }
  },
  wrapper: {
    position: 'relative',
  },
  speedDial: {
    position: 'absolute',
    '&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft': {
      bottom: theme.spacing(2),
      right: theme.spacing(2),
    },
    '&.MuiSpeedDial-directionDown, &.MuiSpeedDial-directionRight': {
      top: theme.spacing(2),
      left: theme.spacing(2),
    },
  },
}));

const NestedListItem = props => {
  const classes = useStyles();

  return (
    <ScrollLing to={ props.section } activeClass={classes.active} offset={ props.offset } smooth spy>
      <ListItem button>
        <ListItemIcon>
          <FontAwesomeIcon icon={ props.icon } size="1x" />
        </ListItemIcon>
        <ListItemText primary={ props.label } />
      </ListItem>
    </ScrollLing>
  );
};

export default NestedListItem;
