import React from "react";
import ToolTip from "../../UI/Notifications/Tooltip";

export default ({ open, title, ...props }) => {
  return ( !open ? 
    <ToolTip
      title={title}
      placement='right'
      arrow={true}
    >
      {props.children}
    </ToolTip> : props.children
  ) 
};
