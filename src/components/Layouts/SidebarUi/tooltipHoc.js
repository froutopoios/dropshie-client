import React from "react";

export const withTooltip = WrappedComponent => ({ children, ...props }) => {
  console.log(props);
  return (
    <WrappedComponent {...props}>{...children}</WrappedComponent>
  );
};
