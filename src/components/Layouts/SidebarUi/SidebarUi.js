import React, { useEffect, useState } from 'react'
import clsx from 'clsx';
import { useWindowSize } from '../../../hooks/useWiindowSize';

import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { Collapse } from '@material-ui/core';

import SidebarSettingsUi from './SidebarSettingsUi';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDollyFlatbed, 
         faPlus, 
         faBoxOpen,
         faPencilRuler,
         faDollarSign,
         faImages,
         faPalette,
         faBox,
         faPaintRoller,
         faListUl } from '@fortawesome/free-solid-svg-icons';

import Header from '../Header';
import UserPlan from '../../Stats/Plan';
import NestedListItem from './NestedListItem';



// TODO: move to a separate component
const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  insetShadow: {
    boxShadow: 'inset 0 0 5px #000000',
    backgroundColor: '#74D0B9'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    // width: 100,
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
    position: 'relative'
  },
  drawerOpen: {
    width: drawerWidth,
    [theme.breakpoints.down('sm')]: {
    width: `calc(${drawerWidth}px - 50px)`
    },
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  icon: {
    '&:hover': {
      boxShadow: 'none'
    }
  },
  grow: {
    flexGrow: 1
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  nestedLister: {
    backgroundColor: '#B2B2B2'
  },
  fab: {
    margin: theme.spacing(2)
  },
  sideHeader: {
    textTransform: 'capitalize',
    fontWeight: 'bold',
    paddingRigh: '10px'
  },
  planContainer: {
      borderRadius: '3px',
      height: '200px', width: '20px', 
      left: '10px', 
      bottom: '50px', 
      position: 'absolute',
      backgroundColor: 'tomato'
  },
  active: {
    '& > div': {
      backgroundColor: '#aaaaaa',
      // color: 'red',
      // borderBottom: '2px solid teal'
    }
  }
}));

const DrawerTest = props =>  {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = useState(true);
  const [InventoryOpen, setInventoryOpen] = useState(false);
  const [SettingsOpen, setSettingsOpen] = useState(false);
  const [listerOpen, setListerOpen] = useState(false);
  const [editorOpen, setEditorOpen] = useState(false);

  const [width] = useWindowSize()

  useEffect(() => {
    if (width <= 780) {
      setOpen(false)
    } else if (props.collapse !== true) {
      setOpen(true);
    }
  }, [width])

  useEffect(() => {
    // console.log(props.path);
    setSettingsOpen(false)
    setInventoryOpen(false);
    setListerOpen(false);
    setEditorOpen(false);

    switch(props.path) {
      case 'inventory':
        setInventoryOpen(true);
        break;
      case 'settings':
        setSettingsOpen(true);
        break;
      case 'InventorySubmit':
        setListerOpen(true);
        break;
      case 'editor':
        setEditorOpen(true)
        break;
      default:
        return;
    }
  }, [props.path])

  useEffect(() => {
    props.collapse ? setOpen(false) : setOpen(true)
  }, [props.collapse])
  
  function handleDrawerClose() {
    open === true ? setOpen(false) : setOpen(true);
  }

  // Open and redirect 
  const handleNested = (value) => () => {
    switch(value) {
      case ('inventory'):
        setInventoryOpen(!InventoryOpen);
        props.redirect(value)
        
        break;
      case ('settings'):
        setSettingsOpen(!SettingsOpen);
        props.redirect(value)
        break;
      case ('lister'):
        setListerOpen(!listerOpen);
        break;
      case ('templates'):
        setEditorOpen(!editorOpen)
        props.redirect('editor')
        break;
      default:
        return;
    }
  }

  return (
    <div className={classes.root}>
      {/* <CssBaseline /> */}
      <Drawer
        // elevation='15'
        variant="permanent"
        PaperProps={{
          elevation: 4
        }}
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({ 
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
        open={open}
      >
        <div className={ clsx(classes.toolbar, classes.sideHeader )}>
          { open ? props.path : null }
          <IconButton onClick={handleDrawerClose} className={ classes.icon }>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />
        <List component="nav"
              className={ classes.list }
              aria-labelledby="nested-list-subheader"
              >

            {/* INVENTORY */}
            <ListItem button onClick={ handleNested('inventory') }>
              <ListItemIcon>
                <FontAwesomeIcon
                  icon={ faDollyFlatbed }
                  size='1x'
                />
              </ListItemIcon>
              <ListItemText primary='Inventory' />
              {InventoryOpen ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={ InventoryOpen } timeout="auto" unmountOnExit>
              <List component='div' disablePadding  className={ clsx(classes.nested, classes.insetShadow) }>

                {/**  ADD ITEM NESTED */}
                <ListItem button onClick={ () => props.handleShowAdd(true) } >
                  <ListItemIcon>
                    <FontAwesomeIcon
                      icon={ faPlus }
                      size='1x'
                    />
                  </ListItemIcon>
                  <ListItemText primary='Add Item'/>
                </ListItem>
              </List>
            </Collapse>
            <Divider/>

            {/* Lister */}
            <ListItem 
              button 
              disabled={!props.isFetchedItemActive ? true : false}
              onClick={ props.expandItemMenu }
              >
              <ListItemIcon>
                <FontAwesomeIcon
                  icon={ faBoxOpen }
                  size='1x'
                />
              </ListItemIcon>
              <ListItemText primary='Lister'/>
              { listerOpen? <ExpandLess/> : <ExpandMore/> }
            </ListItem>
            <Collapse in={ listerOpen } timeout="auto" unmountOnExit>
              <List component='div' disablePadding className={clsx(classes.nested, classes.insetShadow, classes.nestedLister)}>
                <NestedListItem section='main properties' icon={ faPencilRuler } label='Main Properties' offset={ -170 }/>
                <NestedListItem section='pricing' icon={ faDollarSign } label='Pricing'/>
                <NestedListItem section='specifics' icon={ faListUl } label='Specifics'/>
                <NestedListItem section='images' icon={ faImages } label='Images'/>
                <NestedListItem section='editor' icon={ faPalette } label='Template Editor'/>
                <NestedListItem section='submit' icon={ faBox } label='Submit' offset={ -710 }/>
              </List>
            </Collapse>
            <Divider/>

            {/* EDITOR */}
            <ListItem 
              button 
              onClick={ handleNested('templates') }
              >
              <ListItemIcon>
                <FontAwesomeIcon
                  icon={ faPaintRoller }
                  size='1x'
                />
              </ListItemIcon>
              <ListItemText primary='Templates'/>
              { editorOpen? <ExpandLess/> : <ExpandMore/> }
            </ListItem>
            <Collapse in={ editorOpen } timeout="auto" unmountOnExit>
              <List component='div' disablePadding className={clsx(classes.nested, classes.insetShadow, classes.nestedLister)}>
                <NestedListItem section='main properties' icon={ faPencilRuler } label='Main Properties' offset={ -170 }/>
                <NestedListItem section='pricing' icon={ faDollarSign } label='Pricing'/>
                <NestedListItem section='specifics' icon={ faListUl } label='Specifics'/>
                <NestedListItem section='images' icon={ faImages } label='Images'/>
                <NestedListItem section='editor' icon={ faPalette } label='Template Editor'/>
              </List>
            </Collapse>
            <Divider/>

            {/* SETTINGS*/}
            <SidebarSettingsUi
              tab={props.tab}
              handleNested={handleNested}
              SettingsOpen={SettingsOpen}
              handleSettingsChange={props.handleSettingsChange}
              handleMenuItemChange={props.handleMenuItemChange}
              openFromUser={open}              
            />
        </List>

        {/* Plan Indicator */}
        <UserPlan/>
      </Drawer>
      <div className={`right-container ${ classes.grow }`}>
        <Header/>
        { props.children }
      </div>
    </div>
  );
}

export default DrawerTest;
