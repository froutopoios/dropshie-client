import React, { Fragment } from "react";
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';

import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import List from '@material-ui/core/List';

import ToolTipWrapper from './ToolTipWrapper';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPaintBrush,
  faCog,
  faDolly,
  faIdCard,
  faParachuteBox,
} from "@fortawesome/free-solid-svg-icons";

import { faChrome } from '@fortawesome/free-brands-svg-icons'

const useStyles = makeStyles(theme => ({
  insetShadow: {
    boxShadow: 'inset 0 0 5px #000000',
    backgroundColor: '#74D0B9'
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  nestedSettings: {
    backgroundColor: '#E2EE5A'
  },
  active: {
    color: '#F3FEFD',
    '& svg': {
      filter: 'drop-shadow(0 0 2px rgba(0, 0, 0, .7))'
    }
  }
}));

// Decorate if the sidebar is collpsed



export default props => {
  const classes = useStyles()

  let tab;
  if (props.tab !== undefined || props.tab === 0) {
    tab = props.tab
  } 

  return (

  <Fragment>
    <ListItem button onClick={props.handleNested("settings")}>
      <ListItemIcon>
        <FontAwesomeIcon icon={faCog} size="1x" />
      </ListItemIcon>
      <ListItemText primary="Settings" />
      {props.SettingsOpen ? <ExpandLess /> : <ExpandMore />}
    </ListItem>
    <Collapse in={props.SettingsOpen} timeout="auto" unmountOnExit>
      <List
        component="div"
        disablePadding
        className={clsx(
          classes.nested,
          classes.insetShadow,
          classes.nestedSettings
        )}
      >
        <ToolTipWrapper
            title='Profile'
            open={props.openFromUser}
          >
          <ListItem button onClick={props.handleSettingsChange("account")}>
            <ListItemIcon className={tab === 0 ? classes.active : null}>
              <FontAwesomeIcon icon={faIdCard} size="1x" />
            </ListItemIcon>
            <ListItemText primary="Acount" />
          </ListItem>
        </ToolTipWrapper>

        <ToolTipWrapper
            title='Ebay Accounts'
            open={props.openFromUser}
          >
          <ListItem button onClick={props.handleSettingsChange("ebayAccounts")}>
            <ListItemIcon className={tab === 1 ? classes.active : null}>
              <FontAwesomeIcon icon={faDolly} size="1x" />
            </ListItemIcon>
            <ListItemText primary="Ebay Accounts" />
          </ListItem>
        </ToolTipWrapper>

        <ToolTipWrapper
          title='List'
          open={props.openFromUser}
        >
          <ListItem button onClick={props.handleSettingsChange('list')}>
            <ListItemIcon className={ tab === 2 ? classes.active : null }>
              <FontAwesomeIcon icon={faParachuteBox} size="1x" />
            </ListItemIcon>
            <ListItemText primary="List" />
          </ListItem>
        </ToolTipWrapper>

        <ToolTipWrapper
          title='Scan'
          open={props.openFromUser}
        >
          <ListItem button onClick={props.handleSettingsChange('scan')}>
            <ListItemIcon className={ tab === 3 ? classes.active : null }>
              <FontAwesomeIcon icon="eye" size="1x" />
            </ListItemIcon>
            <ListItemText primary="Scan" />
          </ListItem>
        </ToolTipWrapper>

        <ToolTipWrapper
          title='Extensions'
          open={props.openFromUser}
        >
          <ListItem button onClick={props.handleSettingsChange("extensions")}>
            <ListItemIcon className={ tab === 4 ? classes.active : null }>
              <FontAwesomeIcon icon={faChrome} size="1x" />
            </ListItemIcon>
            <ListItemText primary="Extensions" />
          </ListItem>
        </ToolTipWrapper>

        <ToolTipWrapper
          title='Ui'
          open={props.openFromUser}
        >
          <ListItem button onClick={props.handleSettingsChange("ui")}>
            <ListItemIcon className={ tab === 5 ? classes.active : null }>
              <FontAwesomeIcon icon={faPaintBrush} size="1x" />
            </ListItemIcon>
            <ListItemText primary="ui" />
          </ListItem>
        </ToolTipWrapper>
      </List>
    </Collapse>
  </Fragment>
)};
