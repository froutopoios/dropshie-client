import React, { Fragment, useState } from 'react';
import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import avatar from '../../../../assets/images/avatars/avatar_256_white_back.png';

import Avatar from '@material-ui/core/Avatar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem'
import { makeStyles } from '@material-ui/styles';
import SlideTransition from '../../../UI/UtilComponents/SlideTransition'
import Account from '../../../Dashboard/Account';


const useStyles = makeStyles(theme => ({
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    margin: 10,
    width: 60,
    height: 60,
  },
  link: {
    color: 'inherit',
    textDecoration: 'none'
  }
}))


const menuId = 'acount-menu'

// Menu UI
const RenderMenu = props =>  {
  // opens the account settings modal
  const [modalOpen, setModalOpen] = useState(false);

  const openModal = () => {
    setModalOpen(true)
  };

  const closeModal = () => {
    setModalOpen(false)
  };

  const handleOpenModal = () => {
    props.handleMenuClose(); // 
    openModal();
  }

  return (
    <Fragment>
      <Menu
        anchorEl={props.anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        id={menuId}
        keepMounted
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={props.isMenuOpen}
        onClose={props.handleMenuClose}
        TransitionComponent={SlideTransition}
        TransitionProps={{ transition: 'left' }}
        transitionDuration={200}
      >
        <MenuItem onClick={handleOpenModal}>
          Account Settings
        </MenuItem>
        <MenuItem onClick={props.logout}>Log Out</MenuItem>
      </Menu>
      <Account
            modalOpen={modalOpen}
            closeModal={closeModal}
          />
    </Fragment>
  )
}


// Bdutton ui
const LoginButton = ({ userName, isSignedIn, isLoading, logout, facebookPhoto }) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const isMenuOpen = Boolean(anchorEl);

    const handleMenuOpen = e => {
      setAnchorEl(e.currentTarget)
    };
  
    const handleMenuClose = () => {
      setAnchorEl(null)
    };


    if (isLoading) {
      return (
        <div className='auth__login-btn auth__login-btn--loading'>Loading</div>
      )
    }
    if (!isSignedIn) {
      return (
        <Fragment>
            <Link className='auth__reg-btn auth__reg-btn--logged-out' to='/register'>Register</Link>
            <Link className='auth__login-btn auth__login-btn--logged-out' to='/login'>Login</Link>
        </Fragment>
      )
    }

    return (
        <div className='auth__button-container'>
          <div>
            <div className='auth__title'>Welcome { userName }</div>
            {/* <div onClick={ logout } className='auth__login-btn auth__login-btn--logged'>Logout</div> */}
          </div>
          <div className='auth__image-container'>
            <IconButton
              onClick={handleMenuOpen}
            >
              <Avatar 
                src={ facebookPhoto || avatar}
                className={ classes.avatar }/>
            </IconButton>
          </div>
          <RenderMenu
            handleMenuClose={handleMenuClose}
            anchorEl={anchorEl}
            isMenuOpen={isMenuOpen}
            logout={logout}
          />
        </div>
    )
}

export default LoginButton;
