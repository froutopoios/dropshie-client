import React, { Component } from "react";
import { Link as ScrollLing , animateScroll as scroll } from 'react-scroll';
import { getLastSegment } from '../../../utils';

import dropshieLogo from "../../../assets/images/logo_dropshie.png";
import LoginButton from "./LoginButton/LoginButton";
import { Link, NavLink } from "react-router-dom";

import { logout } from "../../../actions/authActions";
import { credAction } from "../../../actions";
import { changePath } from '../../../actions/routingActions';
import { connect } from "react-redux";
import { withScroll } from "../../../hoc/withScroll";

import { withRouter } from 'react-router-dom';

class Header extends Component {
  componentDidMount() {
    if (this.props.isSignedIn) {
      this.props.credAction();

      // TODO: method in utils to extract the last element of the path
      const path = this.props.location.pathname;
      const lastSegment = getLastSegment(path);
      // const lastSegment = path.substring(path.lastIndexOf('/') + 1);

      this.props.changePath(lastSegment);
    }
  }


  
  componentDidUpdate(prevProps) {
    const prevPath = prevProps.location.pathname;
    const thisPath = this.props.location.pathname;

    // console.log(this.props);
    // console.log(prevPath, thisPath);

    if (prevPath !== thisPath) {
      const segment = getLastSegment(thisPath)
      this.props.changePath(segment);
    }
  }

  handleLogout = () => {
    this.props.logout();
    this.props.history.push('/');
  };

  scrollToTop = () => {
    scroll.scrollToTop();
  }

  renderNav = () => {
    const { isSignedIn } = this.props;
    
    if (isSignedIn) {
      return (
        <ul className="header__navbar__list">
          <li className="header__navbar__list__item">
            <NavLink to="/dashboard/overview"
                     activeClassName='active'
                     className='header__link header__link--logged-in'>overview</NavLink>
          </li>
          <li className="header__navbar__list__item">
            <NavLink to="/dashboard/inventory"
                     activeClassName='active'
                    //  activeStyle={{ color: 'teal' }}
                  className='header__link header__link--logged-in'>inventory</NavLink>
          </li>
          <li className="header__navbar__list__item">
            <NavLink to="/Dashboard/Sales"
                     activeClassName='active'
                     className='header__link header__link--logged-in'>sales</NavLink>
          </li>
        </ul>
      );
    } else {
      // logged out
      return (
        <ul className="header__navbar__list">
          <li className="header__navbar__list__item">
            <ScrollLing to="services" 
                        className='header__link'
                        activeClass="active"
                        spy
                        smooth>
              Services
            </ScrollLing>
          </li>
          <li className="header__navbar__list__item">
            <ScrollLing to="supliers" 
                        smooth
                        activeClass="active"
                        spy
                        className='header__link'>
              Supliers
            </ScrollLing>
          </li>
          <li className="header__navbar__list__item">
            <ScrollLing to="how it works" 
                        smooth="easeOutQuart"
                        activeClass="active"
                        spy
                        className='header__link'>
              How It Works
            </ScrollLing>
          </li>
          <li className="header__navbar__list__item">
            <ScrollLing to="pricing-plans"
                        smooth
                        spy
                        activeClass="active"
                        className='header__link'>
              Pricing Plans
            </ScrollLing>
          </li>
        </ul>
      );
    }
  };

  // TODO: transition fade only on exit
  render() {
    const { firstName, isSignedIn, isLoading, isInScroll, directionIsUp } = this.props;

    return (
      <header className={
        `header ${ isInScroll && isSignedIn ? " header__floating" : ""}
                ${!isSignedIn ? " header--logged-out" : " "}
                ${!isSignedIn && isInScroll ? "  header__floating--logged-out" : " "}
                ${ !directionIsUp ? " header__hidden" : " " }`}>
        <div className="header__logo">
          <Link to="/" className='header__link' onClick={ this.scrollToTop }>
              <img
                src={dropshieLogo}
                alt="header logo"
                className="header__logo__img"
              />
          </Link>
        </div>
        <nav className="header__navabar">
          { this.renderNav() }
        </nav>
        <div className="header__login">
          <LoginButton
            userName={firstName}
            isSignedIn={isSignedIn}
            facebookPhoto={this.props.facebookPhoto}
            isLoading={isLoading}
            logout={this.handleLogout}
          />
        </div>
      </header>
    );
  }
}

const mapStateToProps = ({ auth: { isSignedIn }, ui: { isLoading }, acount: { firstName, facebookPhoto }} ) => {
  return {
    firstName, 
    isSignedIn,
    isLoading,
    facebookPhoto
  };
};

export default connect(
  mapStateToProps,
  { logout, credAction, changePath }
)(withScroll(withRouter(Header)));
