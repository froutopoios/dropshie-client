import React from 'react'

const Footer = props => {
  return (
    <div className='footer'>
      <ul className='footer__list'>
        <li className='footer__list-item'>Advertise</li>
        <li className='footer__list-item'>Privacy</li>
        <li className='footer__list-item'>Roules</li>
      </ul>
    </div>
  )
}

export default Footer
