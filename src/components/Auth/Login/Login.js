import React, { Component } from 'react'
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import FacebookLogin from './FacebookLogin';

import { login, verify, logout, stopVerifyError } from '../../../actions/authActions';
import { stopLoading } from '../../../actions/uiActions';
import Snack from '../../UI/Notifications/Snackv2';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import LoopIcon from '@material-ui/icons/Loop';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignInAlt } from '@fortawesome/free-solid-svg-icons';

import { Alert, AlertTitle } from '@material-ui/lab';



const styles = theme => ({
  root: {
    position: 'relative'
  },
  facebookLogin: {
    position: 'absolute',
    top: '10px',
    right: '10px'
  },
  loginContainer: {
    maxWidth: '500px',
    margin: '0 auto',
    display: 'flex',
    flexDirection: 'column' 
  },
  formElement: {
    padding: '10px 20px',
    display: 'flex',
    justifyContent: 'flex-end'
  },
  formTitle: {
    marginLeft: '20px'
  },
  btnBack: {
    marginRigh: '10px'
  },
  loginLabel: {
    flex: 1,
    marginRight: '50px'
  },
  input: {
    width: '300px',
    padding: '5px',
    fontSize: '14px'
  },
  '@keyframes spin': {
    from: {
      transform: 'rotate(0deg)'
    },
    to: {
      transform: 'rotate(360deg)'
    }
  },
  spinner: {
    animationName: '$spin',
    animationDuration: '1000ms',
    animationIterationCount: 'infinite',
    animationTimingFunction: 'linear'  
  },
  backBtn: {
    marginRight: '10px'
  }
})

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      Password: '',
      errorSnack: false,
      errorMessage: ''
    }

    this.inputRef = React.createRef()
  }

  componentDidMount() {
    this.props.logout() // clear the auth on store
    this.inputRef.current.focus();
  }

  componentDidUpdate(prevProps) {    
    if ((this.props.netError || this.props.credsError) && this.props.isLoading) {
      this.props.stopLoading();
    } 
  }

  signInHandler = e => {
    e.preventDefault();
    const { username, Password } = this.state;
    this.props.login({ username, Password })
  }

  handleInputChange = ({ target: { id, value } }) => {
    this.setState({
      [id]: value
    })
  }

  handleBack  = () => {
    this.props.stopVerifyError()
    this.props.history.push('/')
  }

  render() {
    const { username, Password, errorSnack, errorMessage } = this.state;
    const { classes, isLoading } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.facebookLogin}>
          <FacebookLogin/>
        </div>
        <div className={classes.loginContainer}>
          {/* TODO: provive the error message from login res  */}
          <Snack
            open={ Boolean(this.props.netError)}
            // vertical='bottom'
            // horizontal='right'
            icon='error'
            variant='error'
            transition='up'
            message={this.props.netError}
          />
          <h1 className={classes.formTitle}>Please Login</h1>
          <form onSubmit={ this.signInHandler } className='form-container'>
            <div className={classes.formElement}>
              <label htmlFor="username" className={classes.loginLabel}>Email</label>
              <input id='username' 
                    type="text"
                    className={classes.input}
                    ref={ this.inputRef } 
                    onChange={ this.handleInputChange } 
                    value={ username }/> 
            </div>
            <div className={classes.formElement}>
              <label htmlFor="Password" className='login__label login__label--password'>Password</label>
              <input id='Password' 
                    type="password" 
                    className={classes.input}
                    onChange={ this.handleInputChange } 
                    value={ Password }/>
            </div>
            <div className={classes.formElement}
                >
              <Button
                className={classes.backBtn}
                variant='outlined'
                color='secondary'
                onClick={ this.handleBack}
              >
                Back
              </Button>
              <Button
                disabled={this.props.disabled}
                type='submit'
                variant='contained'
                endIcon={
                  isLoading ? 
                  <LoopIcon className={classes.spinner}/> :
                  <FontAwesomeIcon
                    icon={faSignInAlt}
                    size='1x'
                  />
                }
              >{ isLoading ? 'Cancel' : 'LOGIN' }</Button>
            </div>
            <div>
              { this.props.verifyError ?
                  <Alert severity="error">
                    <AlertTitle>Error</AlertTitle>
                    Email is not Verified - <strong>Please verify your email</strong>
                  </Alert>
                   :
                  null
              } 
              </div>
          </form>
        </div>

      </div>
    )
  }
}


const mapStateToProps = ({ auth: { isSignedIn, userToken, emailConfirmed }, errors: { credsError, netError, stopVerifyError, verifyError }, ui: { isLoading, disabled }}) => {
  return { isSignedIn, 
           credsError, 
           netError,
           verifyError, 
           isLoading,
           userToken, 
           emailConfirmed, 
           disabled }
}

const enhance = compose(
  withRouter,
  connect(mapStateToProps, { login, logout, stopLoading, verify, stopVerifyError }),
  withStyles(styles)
)

export default enhance(Login);
