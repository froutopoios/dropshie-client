import React, { Fragment } from 'react';
import FacebookLogin from 'react-facebook-login';
import { appId } from '../../../keys/facebookKeys';

import { connect } from 'react-redux';
import { postFacebookData } from '../../../actions/authActions';

class FacebookLoginBtn extends React.Component  {
  state = {
    faceBookUserId: '',
    name: '',
    email: '',
    picture: ''
  }

  componentClicked = data => {
    console.log('clicked', data)
  }

  responseFacebook = res => {
    console.log('hallo', res)
    try {
      this.props.postFacebookData({
        name: res.name,
        email: res.email,
        picture: res.picture,
        appId: appId,
        token: res.accessToken
      })
      this.setState({
        name: res.name,
        email: res.email,
        picture: res.picture.data.url
      })
    } catch (error) {
      // send to reducers an error could not receive data from fb
      console.log(error)
    }

  }

  render() {
    return (
      <Fragment>
        <FacebookLogin
          appId={appId}
          autoLoad={false}
          fields="name,email, picture"
          onClick={this.componentClicked}
          callback={this.responseFacebook}
        />
      </Fragment>
    )
  }
}

export default connect(null, { postFacebookData })(FacebookLoginBtn);