import React from 'react'

class Recaptcha extends React.Component {
  state = {
    isReady: false,
    token: undefined
  }

  componentDidMount() {
    window.captchaOnLoad = this.onLoad;

    const url = 'https://www.google.com/recaptcha/api.js';
    const queryString = '?onload=captchaOnLoad&render=explicit';
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url + queryString
    script.async = true;
    script.defer = true;

    this.script = document.body.appendChild(script);
  }

  componentWillUnmount() {
    document.body.removeChild(this.widget);
    document.body.removeChild(this.script);
  }

  onLoad = () => {
    const widget = document.createElement('div');
    widget.id = 'g-recaptcha';
    this.widget = document.body.appendChild(widget);

    window.grecaptcha.render('g-recaptcha', {
      sitekey: '6LdprK4UAAAAAOqzOCZCus4JZWA11vgzVpPFypKY',
      size: 'invisible'
    })

    window.grecaptcha.ready(() => this.setState({
      isReady: true
    }))
  }

  executeCaptcha = () => {
    // console.log(this.state);
    const { isReady } = this.state;
    const { action } = this.props;
    console.log(this.props.action);
    return new Promise((resolve, reject) => {
      // console.log(this.state.ready);
      if (!isReady) {
        reject(new Error('Captcha can be executed only when it\'s ready'))
      } else {
        resolve(window.grecaptcha.execute({ action }))
      }
    }
  )}

  render() {
    const { children } = this.props;
    return (
        children({
        isReady: this.state.isReady,
        execute: this.executeCaptcha
      })
    )
  } 
}

export default Recaptcha;