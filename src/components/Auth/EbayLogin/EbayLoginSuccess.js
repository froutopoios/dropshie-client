import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

const EbayLoginSuccess = props => {

  const { search } = useLocation();
  const userName = search.split('username=')[1]

  useEffect(() => {
    props.authEbayUserToDropshie(userName);    
  }, [userName])

  return (
    <div>Login Success!!</div>
  )
}

export default EbayLoginSuccess;