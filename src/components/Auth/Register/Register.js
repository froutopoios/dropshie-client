import React, { Component } from 'react'
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

import RegisterUi from './RegisterUi';

import { withStyles } from '@material-ui/core/styles';
import { register } from '../../../actions/authActions';


// Style Customization
const styles = theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  textField: {
    color: theme.status.danger, 
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 400
  },
  margin: {
    margin: theme.spacing(1)
  },
  icon: {
    '&:hover': {
      boxShadow: 'none'
    }
  },
  button: {
    width: '200px'
  }
})

class Registration extends Component {
  componentDidUpdate(prevProps) {
    console.log('registereRROR', this.props)
    if (this.props.registered && !this.props.registerError) {
      console.log('register error', this.props.registerError)
      setTimeout(() => {
        this.props.history.push('/login')
        // TODO: have to find a better workouround to that
        
      }, 1500)
    }
  }

  handleSubmit = formValues => {
    this.props.register(formValues)
  }

  handleBack = () => {
    this.props.history.push('/')
  }

  // getCaptchaTokenHandler = captcha => async () => {
  //   const token = await captcha.execute()
  //   console.log(token);
  // }

  render() {
 
    return (
      <RegisterUi
        handleSubmit={this.handleSubmit}
        handleBack={this.handleBack}
        isLoading={this.props.isLoading}
        registered={this.props.registered}
        success={this.props.success}
        registerError={this.props.registerError}
        // getCaptchaTokenHandler={this.getCaptchaTokenHandler}
      />
    )
  }
}

const mapStateToProps = ({ auth: { registered }, ui: { isLoading, success }, errors: { registerError }}) => {
  return {
    registered,
    isLoading,
    success,
    registerError
  }
}

const styledAndConnected = compose(
  connect(mapStateToProps, { register }),
  withStyles(styles),
  withRouter
)

export default styledAndConnected(Registration);