import React, { useState, useRef } from 'react';
import clsx from 'clsx';
import { refFactory } from '../../../utils';
import { Link } from 'react-router-dom';
import useFormValidation from '../../../hooks/useFormValidation';
import validateRegister from '../../../validators/validateRegister';

import Recaptcha from '../Recaptcha';
import GeneralTextField from '../../UI/Inputs/GeneralTextField'

import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import teal from '@material-ui/core/colors/teal';
import Button from '@material-ui/core/Button'
import LoopIcon from '@material-ui/icons/Loop';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { Alert, AlertTitle } from '@material-ui/lab';

import balloonRotateSixty from '../../../assets/images/balloonRotate60.png'
import BalloonLeft from './BalloonLeft';

const styles = theme => ({
  root: {
    position: 'relative',
    height: '100%'
  },
  form: {
    position: 'relative',
    margin: 60,
    width: '60%'
  },
  list: {
    listStyle: 'none',
    marginTop: 30,
    marginLeft: 30
  },
  terms: {
    marginTop: 20
  },
  submit: {
    marginLeft: 10
  },
  buttonContainer: {
    // border: '1px solid red',
    display: 'flex',
    justifyContent: 'flex-end'
  },
  link: {
    color: '#007bff',
    textDecoration: 'none'
  },
  textField: {
    width: 300,
    // '& input': {
      // width: 500
    // }
    // '& '
  },
  successfullBtn: {
    backgroundColor: theme.palette.success.main
  },
  balloonContainer: {
    width: '200px'
  },
  balloon: {
    opacity: '.9',
    width: '100%',
    filter: 'drop-shadow(6px 4px 2px rgba(0, 0, 0, .8))',
  },
  svgContainer: {
    width: '200px',
    height: '200px',
    position: 'absolute',
    bottom: 0,
    right: 0
  },
  svg: {
    filter: 'drop-shadow(6px 4px 6px rgba(0, 0, 0, .8))'
  }
})

const DropshieCheckbox = withStyles({ 
  root: {
    color: teal[400],
    '&$checked': {
      color: teal[500]
    }
  },
  checked: {}
})(props => <Checkbox color="default" { ...props }/>);

const INITIALSTATE = {
  email: '',
  name: '',
  sureName: '',
  pass: '',
  confirmPass: '',
  terms: false
}

const fields = [
  {
    name: 'email',
    label: 'Email',
    id: 1
  },
  {
    name: 'name',
    label: 'Name',
    id: 2
  },
  {
    name: 'sureName',
    label: 'SureName',
    id: 3
  }
]

const passFields = [
  {
    name: 'pass',
    label: 'Password',
    id: 4
  },
  {
    name: 'confirmPass',
    label: 'Confirm Password',
    id: 5
  }
]

const checkEmptyKeys = obj => {
  for (let el in obj) {
    if (!obj[el]) {
      return true
    } else {
      return false
    }
  }
}

const RegisterUi = props => {
  const { classes } = props;
  const inputRefs= useRef(refFactory(5));
  const [showPass, setShowPass] = useState({
    pass: false,
    confirmPass: false
  })

  const style = {
    style: {},
    fill: '',
    width: '100%',
    className: '',
    viewBox: '0 0 170 200',
  }

  const { handleChange, handleBlur, handleCheckBoxChange, values, errors } = useFormValidation(INITIALSTATE, validateRegister)

  const handleShowPass = name => () => {
    setShowPass({
      ...showPass,
      [name]: !showPass[name]
    })
  }

  const handleKeyPress = id => ({ key }) => {
    if (key === 'Enter') {
      if (inputRefs.current[id + 1] !== undefined) {
        inputRefs.current[id + 1].current.focus()
      } else {
        inputRefs.current[0].current.focus()
      }
    }
  }

  const handleSubmit = e => {
    e.preventDefault();
    if (Object.keys(errors).length === 0) {
      props.handleSubmit(values)
    } else {
      console.error('not validated')
    }
  }

  const renderSubmitIcon = () => {
    if (props.isLoading) {
      return (
        <LoopIcon className={classes.spinner}/>     
      ) 
    } else if (props.success) {
      return (<FontAwesomeIcon
                icon={faCheck}
                size='1x'
              />
      )
    } else {
      return null
    }
  }
  
  // TODO CHECK THE TYPE IT EXPOSES THE CREDS TO THE URL
  return (
    <div className={classes.root}>
      <form
        className={classes.form}
        onSubmit={handleSubmit}> 
        <h2>Create a <span className='register__span'>DROP</span>shie Account</h2>
        <p>Or <Link className={classes.link} to='/login'>sign in</Link> to your acount</p>
        <ul className={classes.list}>
          {fields.map(field => (
            <li key={field.name}>
              <GeneralTextField
                name={field.name}
                className={classes.textField}
                label={field.label}
                onChange={handleChange(field.name)}
                onBlur={handleBlur}
                value={values[field.name]}
                error={Boolean(errors[field.name])}
                helperText={errors[field.name]}
                onKeyPress={handleKeyPress(field.id - 1)}
                ref={inputRefs.current[field.id - 1]}
              />
            </li>
          ))}
          {passFields.map(field => (
            <li key={field.name}>
              <GeneralTextField
                name={field.name}
                label={field.label}
                onChange={handleChange(field.name)}
                onBlur={handleBlur}
                value={values[field.name]}
                error={ Boolean(errors[field.name])}
                helperText={errors[field.name]}
                isPass
                handleShowPass={handleShowPass(field.name)}
                showPass={showPass}
                onKeyPress={handleKeyPress(field.id - 1)}
                ref={inputRefs.current[field.id - 1]}
            />
            </li>
          ))}
        </ul>
        <div className={clsx(classes.terms, 'register__terms__container')}>Please confirm that you accept the
          <Link className='register__terms__link' to='termsofuse'> Terms Of Use</Link>
        </div>
        <div>
          <FormControlLabel
            className={classes.checkBox}
            control={
              <DropshieCheckbox checked={ values.terms || false }
                                onChange={ handleCheckBoxChange('terms') }
                                />
            }
            label='I Agree with the terms of use'
          />

        </div>
        {/* <Recaptcha action='register'>
                  { capsProps => (
                    <button style={{ marginTop: '20px' }} 
                            onClick={ props.getCaptchaTokenHandler(capsProps) }
                            >Register</button>
                    )
                  }
        </Recaptcha> */}
        <div className={classes.buttonContainer}>
          <Button
            variant='contained'
            color='secondary'
            onClick={props.handleBack}
            >
            Back
          </Button>
          <Button
            classes={{
              root: props.success ? classes.successfullBtn : null
            }}
            className={classes.submit}
            type='submit'
            variant='contained'
            color='primary'
            disabled={ Boolean(Object.keys(errors).length) || checkEmptyKeys(values)}
            endIcon={
              renderSubmitIcon()
            }
            onClick={handleSubmit}>Submit</Button>
        </div>
        { props.registerError ?
          <Alert severity="error">
            <AlertTitle>Error</AlertTitle>
            Cannot proceed to registration  - <strong>Maybe email is in use</strong>
          </Alert> : null }
          {/* <div className={classes.balloonContainer}>
            <img
              className={classes.balloon}        
              src={balloonRotateSixty} alt="balloon-logo"/>
          </div> */}
      </form>
        <div className={classes.svgContainer}>
          <BalloonLeft { ...style } className={ classes.svg }/>
        </div>
    </div>
  )
}

export default withStyles(styles)(RegisterUi)