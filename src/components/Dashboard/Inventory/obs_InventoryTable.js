import React, { Component, useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import AddItem from '../Lister/AddItem';
import IOSSwitch from '../../UI/Buttons/IOSSwitch';

import clsx from 'clsx';
import { lighten, makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import TextField from '@material-ui/core/TextField'

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add'

import Slide from '@material-ui/core/Slide'

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const headRows = [
  { id: 'photo', numeric: false, disablePadding: true, label: 'Photo' },
  { id: 'title', numeric: false, disablePadding: true, label: 'Title' },
  { id: 'sourceLink', numeric: false, disablePadding: true, label: 'Wholesale' },
  { id: 'targetLink', numeric: false, disablePadding: true, label: 'Retail' },
  { id: 'sourcePrice', numeric: false, disablePadding: true, label: 'Source Price' },
  { id: 'sellPrice', numeric: false, disablePadding: true, label: 'Retail Price' },
  // { id: 'desired_quantity', numeric: true, disablePadding: false, label: 'Desired Quantity' },
  // { id: 'profit', numeric: true, disablePadding: false, label: 'Profit' },
  // { id: 'nr_of_views', numeric: true, disablePadding: false, label: 'Nr of Views' },
  // { id: 'nr_watching', numeric: true, disablePadding: false, label: 'Nr Watching' },
  // { id: 'nr_sold', numeric: true, disablePadding: false, label: 'Nr Sold' },
  // { id: 'conversion_rate', numeric: true, disablePadding: false, label: 'Convertion Rate' },
  { id: 'insertDay', numeric: false, disablePadding: true, label: 'Added' },
];


// Table Head

// transfered to its own component
function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;

  const createSortHandler = property => event => {
    // property is the id 
    // debugger;
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'select all desserts' }}
          />
        </TableCell>
        {headRows.map(row => (
          <TableCell
            key={row.id}
            align={row.numeric ? 'right' : 'left'}
            padding={row.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === row.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === row.id}
              direction={order}
              onClick={createSortHandler(row.id)}
            >
              {row.label}
              {orderBy === row.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
  icon: {
    '&:hover': {
      boxShadow: 'none'
    }
  },
  fab: {
    margin: theme.spacing(2),
  }
}));

// Overrides 
const CustomSearchField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: '#2AB4B2',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#2AB4B2',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        // borderColor: '#007F7F',
      },
      '&:hover fieldset': {
        borderColor: '#007F7F',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#007F7F',
      },
    },
  },
})(TextField);

const CustomExpansionPanel = withStyles({
  root: {
    boxShadow: 'none',
    width: '100%'
  }
})(ExpansionPanel)


// TRANSFERED
// TODO add to seperate component
/**
 *  TOOLBAR
 * @param {*} props 
 */
const EnhancedTableToolbar = props => {
  const [expanse, setExpanse] = useState("");
  const classes = useToolbarStyles();
  const { numSelected } = props;

  // TODO: different uses for filter and add item
  const handleExpanseButton = panel => () => {
    console.log(panel);
    if (panel === 'filter')
    setExpanse(expanse !== 'filter' ? 'filter' : "")
    else if (panel === 'add') {
      setExpanse(expanse !== 'add' ? 'add' : '')
    }
  }

  // closes the expansion panel 
  const handleExpanseMain = panel => (e, expanded) => {
    if (expanse) {
      setExpanse('')
    }
  }

  // console.log(expanse);
  return (
    <Toolbar
      className={ classes.root }
    >
      <CustomExpansionPanel
        onChange={ handleExpanseMain('filter') }
        expanded={ expanse === 'filter' || expanse === 'add' }
        className={ clsx( {[classes.highlight]: numSelected > 0 })}>
        <ExpansionPanelSummary
        >
          <div className={classes.title}>
            {numSelected > 0 ? 
              ( <Typography color="inherit" variant="subtitle1">{numSelected} selected</Typography> )
                            : 
              ( <Typography variant="h6" id="tableTitle">Inventory</Typography>)
            }
          </div>
          <div className={classes.spacer}/>
          <div className={classes.actions}>
            {numSelected > 0 ? (
              <Tooltip title="Delete">
                <IconButton aria-label="delete">
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            ) : (
              <Fragment>
                <Tooltip title="Filter inventory">
                  <IconButton aria-label="filter inventory" 
                              className={ classes.icon }
                              onClick={ handleExpanseButton('filter') }>
                    <FilterListIcon />
                  </IconButton>
                </Tooltip>
              </Fragment>
            )}
          </div>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
            <h3>holla</h3>
        </ExpansionPanelDetails>
      </CustomExpansionPanel>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};



// table styles
const styles = theme => ({
  root: {
    width: '100%',
    overflowX: 'hidden',
    marginTop: theme.spacing(3),
    padding: '20px'
  },
  paper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: theme.spacing(2),
  },
  table: {
    overflowX: 'hide',
    width: '100%',
    // minWidth: 500
  },
  tableCell: {
    paddingLeft: 0,
    fontSize: '10px',
    // for testing
    // border: '1px solid tomato'
  },
  links: {
    wordBreak: 'break-all'
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  icon: {
    '&:hover': {
      boxShadow: 'none'
    }
  },
  cover: {
    width: 55,
  },
  searchField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  pagination: {
    '&:hover': {
      boxShadow: 'none'
    },
    '& button': {
      color: theme.palette,
      '&:hover': {
        boxShadow: 'none'
      }
    }
  },
  fab: {
    color: 'white'
  }
});

// TABLE CLASS
class EnhancedTable extends Component {
  state = {
    tableData: [{ name: 'test' }],
    order: 'asc',
    orderBy: 'id',
    selected: [], // to its own state
    page: 0,
    dense: false,
    rowsPerPage: 5,
    searchQuery: '',
    filteredItems: [], // to its own state
    showTable: true // to show add item form
  }

  componentDidMount() {
    // TODO HAVE TO CHECK THE INVENTORY ON UPDATE ALSO PUT LOGIC TO COMPONENT DID UPDATE ALSO
    const { inventory } = this.props;

    const tableData = inventory.reduce((acc, item) => {
      const filteredProps = {
        id: item.Id,
        photo: item.ImageUrl,
        title: item.Title,
        sourceLink: item.SourceLink,
        targetLink: item.TargetLink,
        sourcePrice: item.SourcePrice,
        sellPrice: item.SellPrice,
        desiredQty: item.DesiredQty,
        profit: item.NetProfit,
        views: item.Views,
        watching: item.Watch,
        sold: item.Sold,
        conversionRate: item.ConversionRate,
        added: item.DateInsertStr ? item.DateInsertStr.replace(/<br\/>/g, ' ') : 'does not apply',
        insertDay: item.DateInsert
      }
      acc.push(filteredProps);
  
      return acc
    }, [])
    this.setState({ tableData })
  }

  componentDidUpdate(prevProps) {
    if (prevProps.showAddItemForm !== this.props.showAddItemForm) {
      this.setState({ showTable: !this.props.showAddItemForm })
    }
  }

  // Shorting
  handleRequestSort = (event, property) => {
    const isDesc = this.state.orderBy === property && this.state.order === 'desc';

    this.setState({ order:  isDesc ? 'asc' : 'desc', orderBy: property });
  }

  handleSelectAllClick = event => {
    if (event.target.checked) {
      //TODO: if is inSearch select the filtered items
      const newSelected = this.state.tableData.map(n => n.id);
      this.setState({ selected: newSelected })
      return;
    }
    this.setState({ selected: [] })
  }

  handleClick(event, name) {
    const { selected } = this.state
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    this.setState({ selected: newSelected })
  }

  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage })
  }

  handleChangeRowsPerPage = event => {
    this.setState({
      rowsPerPage: +event.target.value,
      page: 0
    })
  }

  handleChangeDense = event => {
    this.setState({ dense: event.target.checked })
  }

  handleSearchArray = async value => {
    const items = async searchQuery => {
      let t0 = performance.now()
      const items = this.state.tableData.filter(item => {
        const withoutPhoto = _.omit(item, 'photo');
        const arrayOfItems = Object.values(withoutPhoto);
        const lowerArrOfItems = arrayOfItems.map(item => {
          return String(item).toLowerCase();
        }).join('');
  
        return lowerArrOfItems.includes( searchQuery.toLowerCase());
      })
      let t1 = performance.now();
      console.log('search performance', t1 - t0);
      // TODO improve the functionality (day and numbers search creteria) remove photo values from the filter
      return items
    }
    const filteredItems = await items(value);
    this.setState({ filteredItems })
  }

  handleSearch = ({ target: { value } }) => {
    if (this.state.page !== 0) {
      this.setState({
        page: 0
      })
    }

    let searchQuery = value;
    this.setState({ searchQuery })
    this.handleSearchArray(searchQuery);
  }

  handleAddItem = () => {
    
    this.setState((prevState) => {
      return {
          showTable: !prevState.showTable
        }
      }
    )
  }

  isSelected = name => this.state.selected.indexOf(name) !== -1;

  
  render() {
    const { tableData, selected, rowsPerPage, page, searchQuery, filteredItems, dense, order, orderBy } = this.state;
    const { classes } = this.props;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, tableData.length - page * rowsPerPage);

    let finalItems = tableData;
    if ((searchQuery) !== null && searchQuery !== '') {
      finalItems = filteredItems;
    }
    // console.log(classes);
    // console.log(this.state.slide);
    return (
            <div className={ classes.root }>

              {/* ADD ITEM PANEL */}
              <Slide direction='left' in={ !this.state.showTable } mountOnEnter unmountOnExit>
                <div style={{ position: 'fixed' }}>
                  <AddItem />
                </div>
              </Slide>
              {/* TABLE PANEL */}
              <Slide direction='right' in={ this.state.showTable }>
                <div>
                  <Paper className={ classes.paper }>
                    { searchQuery }
                    <CustomSearchField
                      className={ classes.searchField }
                      variant="outlined"
                      label='Search'
                      margin="normal"
                      onChange={ this.handleSearch }
                    />
                  </Paper>
                  <Paper className={classes.paper}>
                    <EnhancedTableToolbar numSelected={selected.length}
                                          addItem={ this.props.addItem }/>
                          <Table
                            className={classes.table}
                            aria-labelledby="tableTitle"
                            size={dense ? 'small' : 'medium'}
                          >
                            {/* <colgroup>
                              <col/>
                              <col/>
                              <col/>
                              <col/>
                              <col/>
                              <col/>
                              <col/>
                              <col/>
                            </colgroup> */}
                            <EnhancedTableHead
                              classes={classes}
                              numSelected={selected.length}
                              order={order}
                              orderBy={orderBy}
                              onSelectAllClick={this.handleSelectAllClick}
                              onRequestSort={this.handleRequestSort}
                              rowCount={tableData.length}
                            />
                            <TableBody>
                              {stableSort(finalItems, getSorting(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, id) => {
                                  const isItemSelected = this.isSelected(row.id);
                                  const labelId = `enhanced-table-checkbox-${row.id}`;
                                  // console.log(row); // check the object
                                  return (
                                    <TableRow
                                      key={id}
                                      hover
                                      role="checkbox"
                                      aria-checked={isItemSelected}
                                      tabIndex={-1}
                                      selected={isItemSelected}>
                                      {/* checkbox */}
                                      <TableCell padding="checkbox">
                                        <Checkbox checked={isItemSelected}
                                                  onClick={event => this.handleClick(event, row.id)}
                                                  inputProps={{ 'aria-labelledby': labelId }}/>
                                      </TableCell>
                                      {/* Photo */}
                                      <TableCell id={labelId} comoponent='th' scope='row' padding='none'>
                                        <a href={ row.photo } target="_blank" rel="noopener noreferrer">
                                          <img src={ row.photo }
                                              alt="item"
                                              className={ classes.cover }/>
                                        </a>
                                      </TableCell>
                                      {/* Title */}
                                      <TableCell className={ classes.tableCell }>{row.title}</TableCell>
                                      {/* SourceLink */}
                                      <TableCell className={ clsx('global__link', classes.tableCell, classes.links) }
                                                    style={{  wordBreak: 'break-all' }} 
                                                >{row.sourceLink}</TableCell>
                                      {/* TargetLink */}
                                      <TableCell className={ clsx('global__link', classes.tableCell, classes.links) } style={{ wordBreak: 'break-all' }}>{row.targetLink}</TableCell>
                                      {/*  SourcePrice */}
                                      <TableCell align="left" className={ classes.tableCell} >{row.sourcePrice}</TableCell>
                                      {/* Retail Price */}
                                      <TableCell align="left" className={ classes.tableCell }>{row.sellPrice}</TableCell>
                                      {/* <TableCell align="right">{row.desiredQty}</TableCell> */}
                                      {/* <TableCell align="right">{row.profit}</TableCell> */}
                                      {/* <TableCell align="right">{row.views}</TableCell> */}
                                      {/* <TableCell align="right">{row.watching}</TableCell> */}
                                      {/* <TableCell align="right">{row.sold}</TableCell> */}
                                      {/* <TableCell align="right">{row.conversionRate}</TableCell> */}
                                      <TableCell align="right" className={ classes.tableCell }>{row.added}</TableCell>
                                    </TableRow>
                                  );
                                })}
                              {emptyRows > 0 && (
                                <TableRow style={{ height: 49 * emptyRows }}>
                                  <TableCell colSpan={6} />
                                </TableRow>
                              )}
                            </TableBody>
                            {/* <TableFooter> */}
                              {/* <TableRow> */}
                              {/* </TableRow> */}
                            {/* </TableFooter> */}
                          </Table>
                    {/* PAGINATION */}
                    <TablePagination
                      rowsPerPageOptions={[5, 10, 25]}
                      component="div"
                      className={ classes.pagination }
                      count={finalItems.length}
                      rowsPerPage={rowsPerPage}
                      page={page}
                      backIconButtonProps={{
                        'aria-label': 'previous page',
                        disableRipple: true
                      }}
                      nextIconButtonProps={{
                        'aria-label': 'next page',
                      }}
                      onChangePage={this.handleChangePage}
                      onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    />
                  </Paper>
                    {/* CONTROLS UNDER THE TABLE  */}
                    <div style={{ display: 'flex', padding: '2px' }}>
                      <FormControlLabel
                        control={<IOSSwitch checked={ dense } onChange={ this.handleChangeDense } />}
                        label="Dense padding"
                      />
                      {this.state.showTable ? 
                          <Tooltip title="Add item to inventory" style={{ marginLeft: 'auto', padding: '5px' }}>
                            <Fab className={ classes.fab }
                                color='primary'
                                size='small'
                                comoponent='button'
                                onClick={ () => this.props.handleAddItem(true) } >
                                <AddIcon/>
                            </Fab>
                          </Tooltip>
                        : <Fragment></Fragment> }
                    </div>
                    <button onClick={ this.props.clear }>clear</button>
                </div>
              </Slide>

          </div>
    );
  }
}

export default withStyles(styles)(EnhancedTable);
