import React, { Component } from 'react';
import { compose } from 'redux';  
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';

import { getItems, fetchInventory, clearInventory, searchInventory, resetFilters } from '../../../actions/inventoryActions';
import { getEbayUsers } from '../../../actions/ebayActions';
import { handleShowAdd } from '../../../actions/uiActions';

import {AntTabs, AntTab} from '../../UI/Styles/AntTabs';

import InventoryTable from './obs_InventoryTable';
import InventoryUi from './InventoryUi/InventoryUi';

const styles = theme => ({
  paper: {
    outline: 0,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5]
  },
  tabContainer: {

  }
})

// deprecated
const getArrayOfItems = inventory => inventory.length !== 0 ? inventory[0].Results : []


class Inventory extends Component {
  state = {
    items: [],
    ebayUsers: [],
    tabValue: 0,
    selector: '',
  }

  componentDidMount() {
    const { items } = this.props; // only if there is inventory in the localstorage else is useless

    if (items === undefined || items.length === 0) {
      console.log('__FETCHING ITEMS__')
      this.props.getItems({page: 1, take: 2000 })
    }

    // if the user was on settings ebayUsers is populated
    if (this.props.ebayUsers.length === 0) {
      this.props.getEbayUsers();
    } else {
      this.setState({
        ebayUsers: this.props.ebayUsers
      })
    }

    if (this.props.filter) {
      this.props.resetFilters()
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { items } = this.props;
    if (prevProps.items !== items) {
      this.setState({
        items
      })
    }

    if (prevState.items !== this.props.items) {
      this.setState({
        items
      })
    }

    if (prevProps.ebayUsers !== this.props.ebayUsers) {
      this.setState({
        ebayUsers: this.props.ebayUsers
      })
    }
  }

  componentWillUnmount() {
    // clear inventory state
    // console.log('clear the inventory now')
    // this.props.clearInventory();
  }

  // for testing
  handleSearch = () => {
    const param = {
      EbayUsername: 'testuser_nakata'
    }
    this.props.searchInventory(param)
  }

  handleTabChange = (e, value) => {
    this.setState({
      tabValue: value,
      selector: e.target.textContent
    })
  }

  handleSubmitFilters = payload => {
    console.log('Submit Filters', payload)

    this.props.searchInventory(payload);
  }

  handleResetFilters = () => {
    this.props.getItems({page: 1, take: 2000 })
    this.props.resetFilters();
  }

  renderTable = () => {
    const items = this.state.items;
    const { showAddItemForm,
            handleShowAdd } = this.props
     
    if ( this.props.netError ) {
      return (
          <h2>Network Error.Try again later</h2>
          )
    } else if ( items === undefined || items.length === 0) {
      return <div>Is loading</div>
    } else {
          return <InventoryUi inventory={items}
                              showAddItemForm={showAddItemForm}
                              clear={this.props.clearInventory}
                              tabValue={this.state.tabValue}
                              handleTabChange={this.handleTabChange}
                              ebayUsers={this.state.ebayUsers}
                              handleSubmitFilters={this.handleSubmitFilters}
                              handleResetFilters={this.handleResetFilters}
                              filter={this.props.filter}
                              selector={this.state.selector}
                              />
    }
  }


  // tab selector
  renderTables = (tab, classes) => {
    if (this.state.items.length > 0) {
      // console.log(this.state.inventory[0].Results);
    }
    const { ebayUsers } = this.state;
    const user = ebayUsers[tab - 1] !== undefined ? ebayUsers[tab -1].UserName : 'master' 
    // trigger search for the user
    if (user !== 'master') {
      const param = {
        EbayUsername: user
      }
      // this.props.searchInventory(param)
    }
    
    return (
      <React.Fragment>
        <div>{ tab }</div>
        <div>{user}</div>
      </React.Fragment>
    )
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        { this.renderTable() }
        <button onClick={this.handleSearch}>Test Search</button>
        <div className={classes.paper}>
          <div className={classes.container}>
            { this.renderTables(this.state.tabValue, classes)}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ inventory: { items }, ui: { isLoading, showAddItemForm, filter }, errors, ebay: { users } }) => {
  return {
    items: items,
    isLoading,
    showAddItemForm,
    submitForFetchError: errors.submitForFetchError,
    netError: errors.netError,
    ebayUsers: users,
    filter
  }
}

const styledAndConnected = compose(
  connect(mapStateToProps, { fetchInventory, 
                             handleShowAdd,
                             getItems,
                             searchInventory,
                             getEbayUsers,
                             resetFilters,
                             clearInventory }),
  withStyles(styles)
)

export default styledAndConnected(Inventory);
