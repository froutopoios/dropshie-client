// these are not all the fields of scan because some of theme have different internals and had to declared explicity

export default [
  {
    title: 'Competitors Price Diff',
    from: 'CompetitorsPriceDiffFrom',
    to: 'CompetitorsPriceDiffTo'
  },
  {
    title: 'Seller selection Policy',
    name: 'SellerSelectionPolicy',
    options: [
      {
        value: '',
        label: 'All'
      },
      {
        value: '0',
        label: 'Amazon'
      },
      {
        value: '1',
        label: 'Buy Box'
      },
      {
        value: '3',
        label: 'No specific policy'
      }
    ]
  },
  {
    title: 'Prime Protection',
    name: 'PrimeProtection',
    options: [
      {
        value: '0',
        label: 'No protection'
      },
      {
        value: '1',
        label: 'Only Prime Protection'
      },
      {
        value: '2',
        label: 'Exclude prime items'
      }
    ]
  },
  {
    title: 'Add-on protection',
    name: 'AddOnProtection',
    options: [
      {
        value: '',
        label: 'All'
      },
      {
        value: '1',
        label: 'Yes'
      },
      {
        value: '2',
        label: 'No'
      }
    ]
  },
  {
    title: 'Back Order Protection',
    name: 'BackOrderProtection',
    options: [
      {
        value: '',
        label: 'All'
      },
      {
        value: '1',
        label: 'Yes'
      },
      {
        value: '2',
        label: 'No'
      }
    ]
  },
  {
    title: 'Has shipping Protection',
    name: 'ShippingDaysProtection',
    options: [
      {
        value: '',
        label: 'All'
      },
      {
        value: '1',
        label: 'Yes'
      },
      {
        value: '2',
        label: 'No'
      }
    ]
  },
  {
    title: 'Shipping Days Protection',
    from: 'notAv',
    to: 'notAv' 
  },
  {
    title: 'Has Handling Protection',
    name: 'HandlingProtectionDDL',
    options: [
      {
        value: '',
        label: 'All'
      },
      {
        value: '1',
        label: 'Yes'
      },
      {
        value: '2',
        label: 'No'
      }
    ]
  },
  {
    title: 'Handling Protection Days',
    from: 'HandlingProtectionDaysFrom',
    to: 'HandlingProtectionDaysTo'
  },
  {
    title: 'Update Listing Quantity',
    name: 'UpdateListingQuantity',
    options: [
      {
        value: '',
        label: 'All'
      },
      {
        value: '1',
        label: 'Yes'
      },
      {
        value: '2',
        label: 'No'
      }
    ]
  }
]