export default [
  {
    title: 'Days Listed',
    from: 'DaysListedFrom',
    to: 'DaysListedTo'
  },
  {
    title: 'Number od Views',
    from: 'NumberViewsFrom',
    to: 'NumberViewsTo'
  },
  {
    title: 'Number Watching',
    from: 'NumberWatchingFrom',
    to: 'NumberWatchingTo'
  },
  {
    title: 'Number Sold',
    from: 'NumberSoldFrom',
    to: 'NumberSoldTo'
  },
  {
    title: 'Conversion ratio',
    from: 'ConversionRatioFrom',
    to: 'ConversionRatioTo'
  },
  {
    title: 'Current Stock Qty',
    from: 'CurrentStockQtyFrom',
    to: 'CurrentStockQtyTo'
  },
  {
    title: 'Desired Qty',
    from: 'DesiredQtyFrom',
    to: 'DesiredQtyTo'
  },
]