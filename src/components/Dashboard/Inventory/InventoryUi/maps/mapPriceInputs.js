export default [
  {
    title: 'Source Price',
    from: 'SourcePriceFrom',
    to: 'SourcePriceTo'
  },
  {
    title: 'List Price',
    from: 'ListPriceFrom',
    to: 'ListPriceTo'
  },
  {
    title: 'Net Profit',
    from: 'NetProfitFrom',
    to: 'NetProfitTo'
  },
  {
    title: 'Paypal Fees%',
    from: 'PaypalFeesFrom',
    to: 'PaypalFeesTo'
  },
  {
    title: 'Ebay Fees%',
    from: 'EbayFeesFrom',
    to: 'EbayFeesTo'
  },
  {
    title: 'Other Fees',
    from: 'OtherFeesPersFrom',
    to: 'OtherFeesPersTo'
  },
  {
    title: 'Fixed Cost',
    from: 'FixedCostFrom',
    to: 'FixedCostTo'
  },
  {
    title: 'Tax',
    from: 'TaxFrom',
    to: 'TaxTo'
  },
  {
    title: 'Fixed Profit',
    from: 'FixedProfitFrom',
    to: 'FixedProfitTo'
  }
]