import React, { useState, useEffect, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles, makeStyles, lighten } from '@material-ui/core/styles';
import clsx from 'clsx';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import FilterListIcon from '@material-ui/icons/FilterList';
import DeleteIcon from '@material-ui/icons/Delete';
import TextField from '@material-ui/core/TextField'
import Toolbar from '@material-ui/core/Toolbar';

import InventoryHeadPanel from './InventoryHeadPanel';
import InventoryEditItem from './IventoryEditItem';

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  actionsContainer: {
    position: 'relative',
    width: '250px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  search: {
    alignSelf: 'flex-start',
    marginRight: 20
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
  icon: {
    '&:hover': {
      boxShadow: 'none'
    }
  },
  fab: {
    margin: theme.spacing(2),
  }
}));





const CustomExpansionPanel = withStyles({
  root: {
    boxShadow: 'none',
    width: '100%'
  }
})(ExpansionPanel)

const EnhancedTableToolbar = props => {
  const [expanse, setExpanse] = useState(""); // to have the filter open by default put 'filter'
  const classes = useToolbarStyles();
  const { numSelected, editItem: { itemId, edit }} = props;

  useEffect(() => {
    edit ? setExpanse('edit') : setExpanse("") 
  }, [edit])

  // TODO: different uses for filter and add item
  const handleExpanseButton = panel => () => {
    console.log(panel);
    if (panel === 'filter')
    setExpanse(expanse !== 'filter' ? 'filter' : "")
    else if (panel === 'add') {
      setExpanse(expanse !== 'add' ? 'add' : '')
    }
  }

  // closes the expansion panel 
  const handleExpanseMain = panel => (e, expanded) => {
    if (expanse) {
      setExpanse('')
    }
  }

  // on search textField
  const handleFocus = e => {
    e.stopPropagation()
  }

  const handleSearchChange = ({ target: { value }}) => {
    props.handleSearch(value)
  }

  const handleClose = () => {
    setExpanse('')
  }

  console.log(expanse);
  return (
    <Toolbar
      className={ classes.root }
    >
      <CustomExpansionPanel
        onChange={ handleExpanseMain('filter') }
        expanded={ expanse === 'filter' || expanse === 'add' || expanse === 'edit' }
        className={ clsx( {[classes.highlight]: numSelected > 0 })}>
        <ExpansionPanelSummary
        >
          <div className={classes.title}>
            {numSelected > 0 ? 
              ( <Typography color="inherit" variant="subtitle1">{numSelected} selected</Typography> )
                            : 
              ( <Typography variant="h6" id="tableTitle">Inventory</Typography>)
            }
          </div>
          <div className={classes.spacer}/>
          <div className={classes.actions}>
            {numSelected > 0 ? (
              <Tooltip title="Delete">
                <IconButton aria-label="delete">
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            ) : (
              <Fragment>
                <div className={classes.actionsContainer}>
                  <TextField
                    className={classes.search}
                    size='small'
                    variant='outlined'
                    label='Search'
                    onFocus={handleFocus}
                    onChange={handleSearchChange}
                  />
                  <Tooltip title="Filter inventory">
                    <IconButton aria-label="filter inventory" 
                                className={ classes.icon }
                                onClick={ handleExpanseButton('filter') }>
                      <FilterListIcon />
                    </IconButton>
                  </Tooltip>
                </div>
              </Fragment>
            )}
          </div>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
            {(expanse === 'filter' || expanse === 'edit') && <InventoryHeadPanel
              editItem={props.editItem}
              handleSubmitFilters={props.handleSubmitFilters}
              handleResetFilters={props.handleResetFilters}
              handleClose={handleClose}
            />}
        </ExpansionPanelDetails>
      </CustomExpansionPanel>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

export default EnhancedTableToolbar;