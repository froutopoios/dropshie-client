import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

const StyledTabPanel = withStyles(theme => ({
  root: {
    width: '100%',
  },
  filterPanel: {
    // width: '80vw', 
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#f7f7f7',
    boxShadow: 'inset 0 0 4px rgba(0, 0, 0, .6)',
    marginLeft: 20,
    position: 'relative',
    minHeight: 200,
    padding: 20
  },
  submit: {
    marginTop: 20,
    alignSelf: 'flex-end',
    display: 'flex',
    bottom: 10,
    right: 10
  }
}))(({ classes, children, value, index, handleSubmit, handleResetFilter, ...other }) => {
  return (
    <div
      className={classes.root}
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Paper
          elevation={0}
          className={classes.filterPanel}>
         <Paper>
            <Box p={2}>
              {children}
            </Box>
         </Paper>
          <div className={classes.submit}>
            <Button
              onClick={handleResetFilter}
              color='secondary'
            >
              Reset Filters
            </Button>
            <Button
              onClick={handleSubmit}
              color='primary'
            >Submit Filters</Button>
          </div>
        </Paper>
      )}
    </div>
  )
})

export default StyledTabPanel;

