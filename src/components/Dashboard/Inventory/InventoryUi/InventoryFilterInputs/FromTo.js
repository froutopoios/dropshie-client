import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import clsx from 'clsx';
import { textAlign } from '@material-ui/system';

const useStyles = makeStyles(theme => ({
  root: {
    
  },
  paper: {
    display: 'flex',
    width: 270,
    padding: '5px 20px',
    alignItems: 'center'
  },
  title: {
    width: 70,
    marginRight: 20,
    textAlign: 'center'
  },
  container: {
    display: 'flex',
    justifyContent: 'space-between',
    width: 150
  },
  input: {
    width: 60,
    '& button': {
      '&:hover': {
        backgroundColor: 'transparent',
      },
    },
  },
}))

const FromTo = ({ title, from, to, values, handleChange, className, perc }) => {
  const classes = useStyles();

  return (
    <Paper className={clsx(classes.paper)}>
      <Typography 
        className={classes.title}
        variant='caption' >{title}</Typography>
      <div className={classes.container}>
        <TextField
          className={classes.input}
          type='number'
          margin='dense'
          label='from'
          inputProps={{
            step: perc ? .1 : 1
          }}
          onChange={handleChange(from)}
          value={values[from]}
          // startAdornment={perc ? <span>%</span> : null}
        />
        <TextField
          className={classes.input}
          type='number'
          margin='dense'
          label='to'
          inputProps={{
            step: perc ? .1 : 1
          }}
          onChange={handleChange(to)}
          value={values[to]}
        />
      </div>
    </Paper>
  )}

export default FromTo;