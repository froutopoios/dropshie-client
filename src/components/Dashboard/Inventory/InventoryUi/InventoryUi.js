import React, { useState, useEffect, Fragment } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles';
import omit from 'lodash/omit'
import clsx from 'clsx'
import history from '../../../../utils/history';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import Tooltip from '@material-ui/core/Tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add'
import EditIcon from '@material-ui/icons/Edit';
import Slide from '@material-ui/core/Slide'
import Filter from '@material-ui/icons/FilterList'
import IOSSwitch from '../../../UI/Buttons/IOSSwitch';

import EnhancedTableHead from './TableHead';
import TableToolbar from './TableToolBar';
import UserSelector from './UserSelector/UserSelector';
import { IconButton } from '@material-ui/core';

// Table styles
const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    overflowX: 'hidden',
    marginTop: theme.spacing(3),
    padding: '20px'
  },
  paper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: theme.spacing(2),
  },
  table: {
    overflowX: 'hide',
    width: '100%',
    // minWidth: 500
  },
  tableCell: {
    paddingLeft: 0,
    fontSize: '10px',
    // for testing
    // border: '1px solid tomato'
  },
  links: {
    wordBreak: 'break-all'
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  icon: {
    '&:hover': {
      boxShadow: 'none'
    }
  },
  cover: {
    width: 55,
  },
  searchField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  pagination: {
    '&:hover': {
      boxShadow: 'none'
    },
    '& button': {
      color: theme.palette,
      '&:hover': {
        boxShadow: 'none'
      }
    }
  },
  fab: {
    color: 'white'
  },
  resetFilter: {
    marginRight: 5
  },
  editIconBtn: {
    width: 40,
    height: 40
  }
}))

// HELPERS
// sort
function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const validateItems = (item, key) => item[key] ? item[key] : 'does not apply'  

// TABLE COMPONENT
const InventoryTable = props => {
  const classes = useStyles();
  const { inventory, selector } = props;
  const [tableData, setTableData] = useState([])
  const [selected, setSelected] = useState([]);
  const [filtered, setFiltered] = useState([]);
  const [showTable, setShowTable] = useState(true);
  const [tableProps, setTableProps] = useState({
    order: 'asc',
    orderBy: 'id',
    page: 0,
    dense: false,
    rowsPerPage: 5,
    searchQuery: '',
  })
  const [editItem, setEditItem] = useState({
    edit: false,
    itemId: ''
  })

  const [visibleColumns, setVisibleColumns] = useState({
    photo: true,
    title: true,
    sourceLink: true,
    targetLink: true,
    sourcePrice: true,
    sellPrice: true,
    insertDay: true,
    editItem: true
  })


  useEffect(() => {
    const data = inventory.reduce((acc, item) => {
      const mappedProps = {
        id: item.Id,
        photo: item.ImageUrl,
        title: item.Title,
        sourceLink: item.OriginProductUrl,
        targetLink: validateItems(item, 'TargetLink'), // have to change the name of the prop!
        sourcePrice: validateItems(item, 'CurrentOriginPrice'),
        sellPrice: validateItems(item, 'CurrentListingPrice'),
        desiredQty: validateItems(item, 'DesiredListingQuantity'),
        profit: item.NetProfit,
        views: item.Views,
        watching: item.Watch,
        sold: item.Sold,
        conversionRate: item.ConversionRate,
        added: item.DateInsertStr ? item.DateInsertStr.replace(/<br\/>/g, ' ') : 'does not apply',
        insertDay: item.DateInsert,
        sellMarketUserName: item.SellMarketUserName
      }
      acc.push(mappedProps);
  
      return acc
    }, [])

    if (selector && selector !== 'All Users') {
     const filteredData = data.filter(item => item.sellMarketUserName === selector);
     
     setTableData([
       ...filteredData
     ])
    } else {
      setTableData([
        // ...tableData,
        ...data
      ])
    } 
  }, [inventory, selector])

  //toggleColumnVisibility
  const handleColumnVisibility = name => ()  => {
    setVisibleColumns((prevState) => ({
      ...visibleColumns,
      [name] : !prevState[name]
    }))
  }

  // Sort
  const handleRequestSort = (event, property) => {
    const isDesc = tableProps.orderBy === property && tableProps.order === 'desc';

    setTableProps({
      ...tableProps,
      order: isDesc ? 'asc' : 'desc',
      orderBy: property
    })
  }

  //Select all items
  const handleSelectAllClick = event => {
    if (event.target.checked) {
      //TODO: if is inSearch select the filtered items
      const newSelected = tableData.map(n => n.id);
      setSelected([
        ...selected,
        ...newSelected
      ]);
      return;
    }
    setSelected([])
  }

  // select item
  const handleSelect = (event, name) => {
    const selectedCp = [ ...selected ];
    const selectedIndex = selectedCp.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selectedCp, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selectedCp.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selectedCp.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selectedCp.slice(0, selectedIndex),
        selectedCp.slice(selectedIndex + 1),
      );
    }

    setSelected([
      ...newSelected
    ])
  }


  const handleChangePage = (event, newPage) => {
    setTableProps({ 
      ...tableProps,
      page: newPage 
    })
  }

  const handleChangeRowsPerPage = event => {
    setTableProps({
      ...tableProps,
      rowsPerPage: +event.target.value,
      page: 0
    })
  }

  const handleChangeDense = event => {
    setTableProps({
      ...tableProps,
      dense: event.target.checked 
    })
  }

  // search table Data.
  const handleSearchArray = async value => {
    const items = async searchQuery => {
      let t0 = performance.now()
      const items = tableData.filter(item => {
        const withoutPhotoAndSourceLink = omit(item, 'photo', 'sourceLink'); // TODO: have to add targetLink 
        const arrayOfItems = Object.values(withoutPhotoAndSourceLink);
        const lowerArrOfItems = arrayOfItems.map(item => {
          return String(item).toLowerCase();
        }).join('');
  
        return lowerArrOfItems.includes( searchQuery.toLowerCase());
      })
      let t1 = performance.now();
      console.log('search performance', t1 - t0);
      return items
    }
    const filteredItems = await items(value);
    
    setFiltered([
      ...filteredItems 
    ])
  }

  const handleSearch = value => {
    if (tableProps.page !== 0) {
      setTableProps({
        ...tableProps,
        page: 0
      })
    }

    let searchQuery = value;
    setTableProps({
      ...tableProps,
       searchQuery
     })
    handleSearchArray(searchQuery);
  }

  // highligher
  const highlightValues  = (text, highlight) => {
    const parts = text.split(new RegExp(`(${highlight})`, 'gi'));
  return <span>{parts.map((part, i) => (
  <span key={i} style={part.toLowerCase() === highlight.toLowerCase() ? { fontWeight: 'bold'} : {} }>{ part }</span>
  ))}</span>
  }

  // show the table or the add item to inventory form 
  const handleAddItem = () => {
    console.log(history);
    history.push('/dashboard/addItem')
    // setShowTable(false)
    // setShowTable(prevState => !prevState.showTable)
  }

  const handleItemEdit = id => () => {
    setEditItem({
      ...editItem,
      edit: !editItem.edit,
      itemId: id
    })
  }

  const isSelected = name => selected.indexOf(name) !== -1;

  const emptyRows = tableProps.rowsPerPage - Math.min(tableProps.rowsPerPage, tableData.length - tableProps.page * tableProps.rowsPerPage);

  let finalItems = tableData;
  if ((tableProps.searchQuery) !== null && tableProps.searchQuery !== '') {
    finalItems = filtered;
  }

  console.log(editItem)  
  return (
    <div className={ classes.root }>

      {/* TABLE PANEL */}
      <Slide direction='right' in={ showTable }>
        <div>
          <UserSelector
            tabValue={props.tabValue}
            handleTabChange={props.handleTabChange}
            ebayUsers={props.ebayUsers}
            visibleColumns={visibleColumns}
            handleColumnVisibility={handleColumnVisibility}
          />
          <Paper className={classes.paper}>
            <TableToolbar numSelected={selected.length}
                          handleSearch={handleSearch}
                          editItem={editItem}
                          handleSubmitFilters={props.handleSubmitFilters}
                          handleResetFilters={props.handleResetFilters}
                          addItem={ props.addItem }
                          editItem={editItem}/>
                  <Table
                    className={classes.table}
                    aria-labelledby="tableTitle"
                    size={tableProps.dense ? 'small' : 'medium'}
                  >
                    <EnhancedTableHead
                      classes={classes}
                      numSelected={selected.length}
                      order={tableProps.order}
                      orderBy={tableProps.orderBy}
                      onSelectAllClick={handleSelectAllClick}
                      onRequestSort={handleRequestSort}
                      rowCount={tableData.length}
                      visibleColumns={visibleColumns}
                    />
                    <TableBody>
                      {stableSort(finalItems, getSorting(tableProps.order, tableProps.orderBy))
                        .slice(tableProps.page * tableProps.rowsPerPage, tableProps.page * tableProps.rowsPerPage +  tableProps.rowsPerPage)
                        .map((row, id) => {
                          const isItemSelected = isSelected(row.id);
                          const labelId = `enhanced-table-checkbox-${row.id}`;
                          // console.log(row); // check the object
                          return (
                            <TableRow
                              key={id}
                              hover
                              role="checkbox"
                              aria-checked={isItemSelected}
                              tabIndex={-1}
                              selected={isItemSelected}>
                              {/* checkbox */}
                              <TableCell padding="checkbox">
                                <Checkbox checked={isItemSelected}
                                          onClick={e => handleSelect(e, row.id)}
                                          inputProps={{ 'aria-labelledby': labelId }}/>
                              </TableCell>
                              {/* Photo */}
                              {visibleColumns.photo && <TableCell id={labelId} comoponent='th' scope='row' padding='none'>
                                <a
                                 href={ row.photo } target="_blank" rel="noopener noreferrer">
                                  <img src={ row.photo }
                                      alt="item"
                                      className={ classes.cover }/>
                                </a>
                              </TableCell>}
                              {/* Title */}
                              {visibleColumns.title && <TableCell className={ classes.tableCell }>{row.title}</TableCell>}
                              {/* SourceLink */}
                              {visibleColumns.sourceLink && <TableCell className={ clsx('global__link', classes.tableCell, classes.links) }
                                            style={{  wordBreak: 'break-all' }} 
                                        >
                                <a className={clsx('global__link')} href={row.sourceLink} target='_blank' rel='noopener noreferrer'>{row.sourceLink}</a>
                              </TableCell>}
                              {/* TargetLink */}
                              {visibleColumns.targetLink && <TableCell className={ clsx('global__link', classes.tableCell, classes.links) } style={{ wordBreak: 'break-all' }}>{row.targetLink}</TableCell>}
                              {/*  SourcePrice */}
                              {visibleColumns.sourcePrice && <TableCell align="left" className={ classes.tableCell} >{row.sourcePrice}</TableCell>}
                              {/* Retail Price */}
                              {visibleColumns.sellPrice && <TableCell align="left" className={ classes.tableCell }>{row.sellPrice}</TableCell>}
                              {/* Added */}
                              {visibleColumns.insertDay && <TableCell align="left" className={ classes.tableCell }>{row.added}</TableCell>}
                              <TableCell
                                padding='none'
                              >
                                <IconButton
                                  onClick={handleItemEdit(row.id)}
                                  classes={{
                                    sizeSmall: classes.editIconBtn
                                  }}
                                  size='small'
                                >
                                  <EditIcon
                                    fontSize='small'
                                  />
                                </IconButton>
                              </TableCell>
                            </TableRow>
                          );
                        })}
                      {emptyRows > 0 && (
                        <TableRow style={{ height: 49 * emptyRows }}>
                          <TableCell colSpan={6} />
                        </TableRow>
                      )}
                    </TableBody>
                    {/* <TableFooter> */}
                      {/* <TableRow> */}
                      {/* </TableRow> */}
                    {/* </TableFooter> */}
                  </Table>
            {/* PAGINATION */}
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              className={ classes.pagination }
              count={finalItems.length}
              rowsPerPage={tableProps.rowsPerPage}
              page={tableProps.page}
              backIconButtonProps={{
                'aria-label': 'previous page',
                disableRipple: true
              }}
              nextIconButtonProps={{
                'aria-label': 'next page',
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Paper>
            {/* CONTROLS UNDER THE TABLE  */}
            <div style={{ display: 'flex', padding: '2px' }}>
              <FormControlLabel
                control={<IOSSwitch checked={ tableProps.dense } onChange={ handleChangeDense } />}
                label="Dense padding"
              />
              {showTable ? 
                  <Fragment>
                    <div style={{ marginLeft: 'auto' }}>
                      {props.filter && <Fab 
                                         size='small'
                                         color='secondary'
                                         onClick={props.handleResetFilters}
                                         className={classes.resetFilter}
                                         ><Filter/></Fab>}
                      <Tooltip title="Add item to inventory" style={{ padding: '5px' }}>
                        <Fab className={ classes.fab }
                            color='primary'
                            size='small'
                            comoponent='button'
                            onClick={ () => handleAddItem(true) } >
                            <AddIcon/>
                        </Fab>
                      </Tooltip>
                    </div>
                  </Fragment>
                : <Fragment></Fragment> }
            </div>
            <button onClick={ props.clear }>clear</button>
            <button
              onClick={handleColumnVisibility('title')}
            >toggle title</button>
        </div>
      </Slide>
  </div>
  );
}

export default InventoryTable;

