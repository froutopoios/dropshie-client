import React, { Fragment, useState, useEffect, useMemo } from 'react';

import clsx from 'clsx';
import 'date-fns';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button';


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';

import StyledTabPanel from './StyledTabPanel';
import FromTo from './InventoryFilterInputs/FromTo';

import mapGeneralInputs from './maps/mapGeneralInputs';
import mapPriceInputs from './maps/mapPriceInputs';
import mapScanInputs from './maps/mapScanInputs';

const AntTabs = withStyles(theme => ({
  root: {
    // borderBottom: '1px solid #e8e8e8',
  },
  indicator: {
    // backgroundColor: '#1890ff',
    backgroundColor: theme.palette.primary.main,
  },
}))(Tabs);

const AntTab = withStyles((theme) => ({
  root: {
    textTransform: 'none',
    minWidth: 72,
    fontWeight: theme.typography.fontWeightRegular,
    marginRight: theme.spacing(4),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      // color: '#40a9ff',
      color: theme.palette.primary.main,
      opacity: 1,
    },
    '&$selected': {
      // color: '#1890ff',
      color: theme.palette.primary.main,
      fontWeight: theme.typography.fontWeightMedium,
    },
    '&:focus': {
      // color: '#40a9ff',
      color: theme.palette.primary.main,
    },
  },
  selected: {},
}))((props) => <Tab disableRipple {...props} />);

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex'
  },
  flex: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  rootPanel: {
    width: 400
  },
  tabPanel: {
    width: '100%',
    display: 'grid',
    gridTemplateColumns: 'repeat(4, 1fr)',
    gridGap: '.8rem .8rem',
    justifyContent: 'space-around'
  },
  ItemProperties: {
    gridColumn: '1/3'
  },
  supplier: {
    gridRow: '2/2'
  },
  userName: {
    gridRow: '2/2'
  },
  dateGridContainer: {
    display: 'grid',
    gridRow: '3',
    gridColumn: '1/5',
    gridTemplateColumns: 'repeat(auto-fit, 400px)',
    gridAutoRows: 'auto'
  },
  sold: {
    gridColumn: '1/2',
  },
  notSold: {
  },
  numericGrid: {
    gridRow: '4',
    gridColumn: '1/5',
    display: 'grid',
    gridTemplateColumns: 'repeat(auto-fit, minmax(250px, 1fr))',
    gridAutoRows: 'auto',
    gridGap: '.8rem'
  },
  dateTitle: {
    marginRight: 20
  },
  dateContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    width: 320
  },
  inputBig: {
    marginRight: 20,
    width: 'auto'
  }
}))

// Main Compoonent
const InventoryHeadPanel = props => {
  const [tab, setTab] = useState(2); // sets the default panel
  const currentDate = useMemo(() => {
    return new Date()
  }, [])
  const [dateValues, setDateValues] = useState({
    'sold': {
      from: currentDate,
      to: currentDate
    },
    'notSold': {
      from: currentDate,
      to: currentDate
    },
    'dateAdded': {
      from: currentDate,
      to: currentDate
    }
  })

  useEffect(() => {
    props.editItem && setTab(0)
  }, [])

  const [inputValues, setInputValues] = useState({
    DaysListedFrom: '',
    DaysListedTo: '',
    NumberViewsFrom: '',
    NumberViewsTo: '',
    NumberWatchingFrom: null,
    NumberWatchingTo: null,
    NumberSoldFrom: null,
    NumberSoldTo: null,
    ConversionRatioFrom: null,
    ConversionRatioTo: null,
    CurrentStockQtyFrom: null,
    CurrentStockQtyTo: null,
    DesiredQtyFrom: null,
    DesiredQtyTo: null,
    SourcePriceFrom: '',
    SourcePriceTo: '',
    ListPriceFrom: '',
    ListPriceTo: '',
    NetProfitFrom: '',
    NetProfitTo: '',
    PaypalFeesFrom: '',
    PaypalFeesTo: '',
    EbayFeesFrom: '',
    EbayFeesTo: '',
    OtherFeesPersFrom: '',
    OtherFeesPersTo: '',
    FixedCostFrom: '',
    FixedCostTo: '',
    TaxFrom: '',
    TaxTo: '',
    FixedProfitFrom: '',
    FixedProfitTo: '',
    RepriceState: '',
    CompetitorsCheck: '',
    CompetitorsPriceDiffFrom: '',
    CompetitorsPriceDiffTo: '',
    SellerSelectionPolicy: '',
    PrimeProtection: '',
    AddOnProtection: '',
    BackOrderProtection: '',
    ShippingDaysProtection: '',
    HandlingProtectionDDL: '',
    HandlingProtectionDaysFrom: '',
    HandlingProtectionDaysTo: '',
    UpdateListingQuantity: ''
  })

  const classes = useStyles();

  const handleTabChange = (e, value) => {
    setTab(value);
  }

  const handleDateChange = (prop, param) => date =>  {
    setDateValues({
      ...dateValues,
      [prop]: {
        ...dateValues[prop],
          [param]: date
      }
    })
  };

  const handleChange = param => ({target: {value}}) => {
    console.log(param, value)
    setInputValues(prevValues => ({
      ...prevValues,
      [param]: value
    }))
  }

  const handleSubmitFilters = () => {
    props.handleSubmitFilters(inputValues);
    props.handleClose();
    // close the panel
  }

  const handleResetFilters = () => {
    props.handleResetFilters();
  }

  const RenderDates = ({ name, param, classes: { date }}) => (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          className={date}
          disableToolbar
          variant="inline"
          format="MM/dd/yyyy"
          margin="dense"
          id="date-picker-inline"
          label={param}
          value={dateValues[name][param]}
          onChange={handleDateChange(name, param)}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
    </MuiPickersUtilsProvider>
  )

  const StyledDateComponent = withStyles(theme => ({
    paper: {
      display: 'flex',
      alignItems: 'baseline',
      // backgroundColor: theme.palette.background.paper
    },
    dateTitle: {
      width: 50,
      marginRight: 20,
      textAlign: 'center'
    },
    dateContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      width: 320
    },
    date: {
      width: 150,
      '& button': {
        '&:hover': {
          backgroundColor: 'transparent',
        },
      },
    },
  }))(({ classes, className, name, title, firstParam, secondParam }) => (
      <div className={clsx(className, classes.paper)}>
        <Typography
          className={classes.dateTitle}
          variant='caption'>{title}</Typography>
          <div className={classes.dateContainer}>
            <RenderDates
              classes={{
                date: classes.date
              }}
              name={name}
              param={firstParam}
            />
            <RenderDates
              classes={{
                date: classes.date
              }}
              name={name}
              param={secondParam}
            />
          </div>
      </div>
  ))
  
  const renderScanInputs = () => {
    return mapScanInputs.map(field => {
      if (field.from) {
        return (
          <FromTo
            key={field.title}
            title={field.title}
            from={field.from}
            to={field.to}
            values={inputValues}
            handleChange={handleChange}
          />
        )
      } else {
        return (
          <TextField
            key={field.title}
            select
            variant='outlined'
            size='small'
            value={inputValues[field.name]}
            label={field.title}
            onChange={handleChange(field.name)}
            InputLabelProps={{
              shrink: true
            }}
            SelectProps={{
              native: true
            }}>
              {field.options.map(option => (
                <option
                 key={option.value}
                 value={option.value}
              >{option.label}</option>
              ))}
            </TextField>
        )
      } 
    })
  }

  const renderEdit = () => {
    return (
      <Fragment>
        <AntTabs
          value={tab}
          orientation='vertical'
          onChange={handleTabChange}
        >
          <AntTab label='List Settings'/>
          <AntTab label='Scan Settings'/>
        </AntTabs>
        <StyledTabPanel
          value={tab}
          index={0}
        >
          Settings Screen
        </StyledTabPanel>
        <StyledTabPanel
          value={tab}
          index={1}
        >

        </StyledTabPanel>
      </Fragment>
    )
  }

  const renderFilter = () => {
    return (
      <Fragment>
        <AntTabs 
          value={tab}
          orientation='vertical' 
          onChange={handleTabChange}>
          <AntTab 
            label='General'
            />
          <AntTab label='Price Formula'/>
          <AntTab label='Scan'/>
        </AntTabs>
        <StyledTabPanel
          handleSubmit={handleSubmitFilters} 
          handleResetFilter={handleResetFilters}
          value={tab} 
          index={0}>
          <div className={classes.tabPanel}>
            <TextField
              className={clsx(classes.inputBig, classes.ItemProperties)}
              label='Item Properties'
              size='small'
              variant='outlined'
            />
            <TextField
              className={clsx(classes.inputBig, classes.supplier)}
              label='Supplier'
              variant='outlined'
              size='small'
              select
            />
            <TextField
              className={clsx(classes.inputBig, classes.userName)}
              label='UserName'
              variant='outlined'
              size='small'
              select
            />
            <div className={classes.dateGridContainer}>
              <StyledDateComponent
                className={classes.sold}
                title='Sold'
                name='sold'
                firstParam='from'
                secondParam='to'
              />
              <StyledDateComponent
                className={classes.notSold}
                title='Not sold'
                name='notSold'
                firstParam='from'
                secondParam='to'
              />
            </div>
            <div className={classes.numericGrid}>
              {mapGeneralInputs.map(field => (
                  <FromTo
                    key={field.title}
                    title={field.title}
                    values={inputValues}
                    handleChange={handleChange}
                    from={field.from}
                    to={field.to}
                />            
                ))}
            </div>
          </div>
        </StyledTabPanel>
        <StyledTabPanel
          handleSubmit={handleSubmitFilters}
          handleResetFilter={handleResetFilters}
          value={tab} 
          index={1}>
          <div className={classes.tabPanel}>
            <div className={classes.numericGrid}>
              {mapPriceInputs.map(field => (
                <FromTo
                  key={field.title}
                  title={field.title}
                  values={inputValues}
                  handleChange={handleChange}
                  from={field.from}
                  to={field.to}
                  perc
                />
              ))}
            </div>
          </div>
        </StyledTabPanel>
        <StyledTabPanel
          handleSubmit={handleSubmitFilters}
          handleResetFilter={handleResetFilters} 
          value={tab} 
          index={2}>
          <div className={classes.tabPanel}>
            <div className={classes.numericGrid}>
              <TextField
                value={inputValues.RepriceState}
                label='Reprice State'
                variant='outlined'
                size='small'
                onChange={handleChange('RepriceState')}
                InputLabelProps={{
                  shrink: true
                }}
                SelectProps={{
                  native: true
                }}
                // helperText='All'
                select
              >
                <option value=''>All</option>
                <option value='1'>Active</option>
                <option value='3'>Off</option>
              </TextField>

              <TextField
                value={inputValues.CompetitorsCheck}
                label='Competitors Check'
                variant='outlined'
                size='small'
                onChange={handleChange('CompetitorsCheck')}
                InputLabelProps={{
                  shrink: true
                }}
                SelectProps={{
                  native: true
                }}
                // helperText='All'
                select
              >
                <option value=''>All</option>
                <option value='1'>Yes</option>
                <option value='2'>No</option>
              </TextField>
              {renderScanInputs()}
            </div>
          </div>
        </StyledTabPanel>

      </Fragment>
    )
  }

  return (
    <Fragment>
      {props.editItem ? 
        renderEdit()  :  
        renderFilter()
      }
    </Fragment> 
  )
}

export default InventoryHeadPanel;
