import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Checkbox from '@material-ui/core/Checkbox';

import _map from 'lodash/map';


const useStyles = makeStyles(theme => ({
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  }
}))

const headRows = [
  { id: 'photo', numeric: false, disablePadding: true, label: 'Photo' },
  { id: 'title', numeric: false, disablePadding: true, label: 'Title' },
  { id: 'sourceLink', numeric: false, disablePadding: true, label: 'Wholesale' },
  { id: 'targetLink', numeric: false, disablePadding: true, label: 'Retail' },
  { id: 'sourcePrice', numeric: false, disablePadding: true, label: 'Source Price' },
  { id: 'sellPrice', numeric: false, disablePadding: true, label: 'Retail Price' },
  // { id: 'desired_quantity', numeric: true, disablePadding: false, label: 'Desired Quantity' },
  // { id: 'profit', numeric: true, disablePadding: false, label: 'Profit' },
  // { id: 'nr_of_views', numeric: true, disablePadding: false, label: 'Nr of Views' },
  // { id: 'nr_watching', numeric: true, disablePadding: false, label: 'Nr Watching' },
  // { id: 'nr_sold', numeric: true, disablePadding: false, label: 'Nr Sold' },
  // { id: 'conversion_rate', numeric: true, disablePadding: false, label: 'Convertion Rate' },
  { id: 'insertDay', numeric: false, disablePadding: true, label: 'Added' },
  { id: 'editItem', numeric: false, disablePadding: true, label: 'Edit' }
];


function EnhancedTableHead(props) {
  const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort, visibleColumns } = props;
  const classes = useStyles();
  const [visible, setVisible] = useState([])

  useEffect(() => {
    const columns = [];
    headRows.forEach(column => {
      return _map(visibleColumns,(value, key) => {
        if (column['id'] === key) {
          if (value) {
            columns.push(column)
          }
        }
      })
    })
    setVisible(columns)
  }, [visibleColumns])

  const createSortHandler = property => event => {
    // property is the id 
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'select all desserts' }}
          />
        </TableCell>
        {visible.map(row => (
          <TableCell
            key={row.id}
            align={row.numeric ? 'right' : 'left'}
            padding={row.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === row.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === row.id}
              direction={order}
              onClick={createSortHandler(row.id)}
            >
              {row.label}
              {orderBy === row.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

export default EnhancedTableHead;