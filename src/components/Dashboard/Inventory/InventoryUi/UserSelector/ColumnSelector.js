import React, { useMemo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'

import IOSswitch from '../../../../UI/Buttons/IOSSwitch';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '10px 30px'
  },
  gridContainer: {
    display: 'grid',
    gridTemplateColumns: 'repeat(auto-fit, 160px)',
    gridGap: '5px 30px'
  },

  toggleContainer: {
    
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '140px'
  }
}))

const ColumnSelector = props => {
  const classes = useStyles()
  const { visibleColumns, handleColumnVisibility } = props;

  return (
    <div className={classes.root}>
      <Typography variant='h5'>Set Visible Columns</Typography>
      <div className={classes.gridContainer}>
        {Object.keys(visibleColumns).map(column => (
          <div 
            className={classes.toggleContainer}
            key={column}
            >
            <Typography variant='overline'>{column}</Typography>
            <IOSswitch
              checked={visibleColumns[column]}
              onChange={handleColumnVisibility(column)}
            />
          </div>
        ))}
      </div>
    </div>
  )
}

export default ColumnSelector;