import React, { Fragment, useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import ColumnSelector from './ColumnSelector';

import Paper from '@material-ui/core/Paper';
import Collapse from '@material-ui/core/Collapse';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import IconButton from '@material-ui/core/IconButton';
import Wrench from '@material-ui/icons/Build'

const AntTabs = withStyles(theme => ({
  root: {
    // borderBottom: '1px solid #e8e8e8',
  },
  indicator: {
    // backgroundColor: '#1890ff',
    backgroundColor: theme.palette.primary.main,
  },
}))(Tabs);

const AntTab = withStyles((theme) => ({
  root: {
    textTransform: 'none',
    minWidth: 72,
    fontWeight: theme.typography.fontWeightRegular,
    marginRight: theme.spacing(4),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      // color: '#40a9ff',
      color: theme.palette.primary.main,
      opacity: 1,
    },
    '&$selected': {
      // color: '#1890ff',
      color: theme.palette.primary.main,
      fontWeight: theme.typography.fontWeightMedium,
    },
    '&:focus': {
      // color: '#40a9ff',
      color: theme.palette.primary.main,
    },
  },
  selected: {},
}))((props) => <Tab disableRipple {...props} />);

const useStyles = makeStyles(theme => ({
  root: {

  },
  paper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: theme.spacing(2),
  },
  toolbarContainer: {
    display: 'flex',
    alignItems: 'baseline'
  },
  tabs: {
    marginBottom: 20,
    marginLeft: 20
  },
  wrench: {
    marginLeft: 'auto'
  }
}))


const UserSelector = props => {
 const classes = useStyles();
 const { visibleColumns, handleColumnVisibility } = props;
 const [expanse, setExpanse] = useState(false); // opens the column settings

 const handleExpanse = () => {
   setExpanse(!expanse)
 }

 return (
  <Fragment>
      <Paper className={classes.paper}>
        <div className={classes.toolbarContainer}>
          <AntTabs
            className={classes.tabs}
            value={props.tabValue} 
            onChange={props.handleTabChange}>
            <AntTab label='All Users'/>
            {props.ebayUsers.map(ebayUser => (
              <AntTab label={ebayUser.UserName} key={ebayUser.Username}/>
            ))}
          </AntTabs>
            <IconButton
              onClick={handleExpanse}
              className={classes.wrench}>
              <Wrench/>
            </IconButton>
        </div>
        <Collapse in={expanse}>
          <ColumnSelector
            visibleColumns={visibleColumns}
            handleColumnVisibility={handleColumnVisibility}
          />
        </Collapse>
      </Paper>
  </Fragment>
 )
}

export default UserSelector;