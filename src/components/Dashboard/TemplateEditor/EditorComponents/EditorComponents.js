import React, { forwardRef, useRef } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';

import FormatBold from '@material-ui/icons/FormatBold';
import FormatItalic from '@material-ui/icons/FormatItalic';
import FormatUnderline from '@material-ui/icons/FormatUnderlined';
import Attachment from '@material-ui/icons/Attachment';
import FormatListBulleted from '@material-ui/icons/FormatListBulleted';
import FormatListNumbered from '@material-ui/icons/FormatListNumbered';
import FormatQuote from '@material-ui/icons/FormatQuote'
import FormatStrikeTrough from '@material-ui/icons/FormatStrikethrough';

const useStyles = makeStyles(theme => ({
  root: {

  },
  menu: {
    whiteSpace: 'pre-wrap',
    margin: '0 -20px 10px',
    padding: '10px 20px',
    fontSize: '14px',
    background: '#f8f8e8'
  },
  toolbar: {
    position: 'relative',
    padding: '1px 18px 17px',
    margin: '0 -20px',
    borderBottom: '2px solid #eee',
    marginBottom: '20px'  
  },
  button: {
    cursor: 'pointer',
    color: ({reversed, active}) => (
      reversed ? 
      active ?
      'white' : 
      '#aaa': 
      active ? 
      'black': 
      '#ccc'
    )
  },
  icon: {
    fontSize: '18px',
    verticalAlign: 'text-bottom'
  }
}))

export const Menu = forwardRef(({ className, ...props }, ref) => {
  const classes = useStyles()
  
  return (
  <div
    {...props}
    ref={ref}
    className={clsx(className, classes.menu)}
  >
  </div>
)})

export const Button = React.forwardRef(
  ({ className, active, reversed, ...props }, ref) => {
    const classes = useStyles({ active, reversed })
    
    
    return (
      <span
        {...props}
        ref={ref}
        className={clsx(className, classes.button)}
      />
  )
})

export const Icon = React.forwardRef(({ className, icon,  ...props }, ref) => {
  const classes = useStyles();

  return (
    <span
      {...props}
      ref={ref}
      className={clsx(
       className,
       classes.icon)}
    >
      { icon === 'format_block_quote' && <FormatQuote/>}
      { icon === 'strike_through' && <FormatStrikeTrough/>}
      { icon === 'bold' && <FormatBold/>}
      { icon === 'italic' && <FormatItalic/>}
      { icon === 'underline' && <FormatUnderline/>}
      { icon === 'attachement' && <Attachment/>}
      { icon === 'format_list_numbered' && <FormatListNumbered/>}
      { icon === 'format_list_bulleted' && <FormatListBulleted/>}
    </span>
)})


export const Toolbar = forwardRef(({ className, ...props }, ref) => {
  const classes = useStyles();

  return (
  <Menu
    { ...props }
    ref={ref}
    className={clsx(className, classes.toolbar)}
  />
)})


