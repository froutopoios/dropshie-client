import React, { useState, useMemo, useRef, useEffect } from 'react'
import { Slate, Editable, ReactEditor, withReact, useSlate } from 'slate-react'
import { Editor, Transforms, Text, createEditor } from 'slate'
import { clsx } from 'clsx'
import { makeStyles } from '@material-ui/styles';

import { Button, Icon, Menu } from './EditorComponents';
import { Portal } from './Portal';
import { Range } from 'slate';

const useStyles = makeStyles({
  menu: {
    padding: '8px 7px 6px',
    position: 'absolute',
    zIndex: 1,
    top: '-10000px',
    left: '-10000px',
    marginTop: '-6px',
    opacity: 0,
    backgroundColor: '#222',
    bordeRadius: '4px',
    transition: 'opacity 0.75s'
  }
})


const HoveringToolbar = ({ FormatButton }) => {
  const classes = useStyles();
  const ref = useRef();
  const editor = useSlate();

  useEffect(() => {
    const el = ref.current;
    const { selection } = editor;

    if (!el) {
      return;
    }

    if (
      !selection ||
      !ReactEditor.isFocused(editor) ||
      Range.isCollapsed(selection) ||
      Editor.string(editor, selection) === ''
    ) {
      el.removeAttribute('style');
      return;
    }

    const domSelection = window.getSelection();
    const domRange = domSelection.getRangeAt(0);
    const rect = domRange.getBoundingClientRect()
    el.style.opacity = 1;
    el.style.top = `${rect.top + window.pageYOffset - el.offsetHeight}px`;
    el.style.left = `${rect.left +
      window.pageXOffset -
      el.offsetWidth / 2 +
      rect.width / 2
    }px`
  })

  return (
    <Portal>
      <Menu
        ref={ref}
        className={classes.menu}
      >
        <FormatButton format='bold' icon='bold'/>
        <FormatButton format='italic' icon='italic'/>
        <FormatButton format='underline' icon='underline'/>
      </Menu>
    </Portal>
  )
}

export default HoveringToolbar;