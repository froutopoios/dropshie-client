import React from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
// import './test.scss';
import template from './EditorUtils/template';

class QuillEditor extends React.Component {
  state = {
    text: ''
  }

  handleChange = value => {
    this.setState({
      text: value
    })
  }

  modules = {
    toolbar: [
      [{ 'header': [1, 2, false] }],
      ['bold', 'italic', 'underline','strike', 'blockquote'],
      [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
      ['link', 'image'],
      ['clean']
    ], 
  }

  formats = [
    'header',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image'
  ]

  handleImport = () => {
    this.setState({
      text: template
    })
  }

  render() {
    return (
      <div className='editor'>
        <h2>Quill Editor</h2>
        <button
          onClick={this.handleImport}
        >import</button>
        <ReactQuill
          theme="snow"
          modules={this.modules}
          formats={this.formats}
          value={this.state.text}
          onChange={this.handleChange}
        />
      </div>
    )
  }
}

export default QuillEditor;

