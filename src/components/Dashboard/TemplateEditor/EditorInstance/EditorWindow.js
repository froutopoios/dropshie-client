import React, { Fragment, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Editor } from '@tinymce/tinymce-react';
import tinykey from '../../../../keys/tinyKey';


const EditorWindow = props => {
  const [template, setTemplate] = useState('');

  useEffect(() => {
    setTemplate(props.template)
  }, [props.template])

  const handleEditorChange = (content, editor) => {
    // console.log(content);
    props.handleEditorChange(content);
  }

  // console.log('indisde editor window', props.template)
  return (
    <Fragment>
      <Editor
        apiKey={tinykey}
        initialValue="write something down"
        selector='textarea#dropshie-editor'
        // inline={true}
        value={template || 'please load a template'}
        init={{
          setup: function (editor) {
            // console.log('SETUP IS TRIGGERED');
            // editor.focus()
            editor.addCommand('hop', () => {
              console.log('the new command triggered')
            })
            editor.addShortcut('ctrl+a', "the shortcut", function () { console.log('shortcut added') })
          },
          // extended_valid_elements : "img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name]",
          valid_elements: "*[*]",
          allow_unsafe_link_target: true,
          quickbars_selection_toolbar: 'bold italic | h2 h3',
          font_formats: 'Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva',
          // block_formats: 'Paragraph=p; Header 1=h1; Header 2=h2',
          height: 800,
          plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount emoticons template'
          ],
          toolbar:
            'undo redo | formatselect | fontselect | bold italic backcolor | \
            alignleft aligncenter alignright alignjustify | \
            bullist numlist outdent indent | removeformat | emoticons template | help',
          templates: [
            { title: 'one', description: 'Some template', content: 'template'},
            { title: 'blackFirday', description: 'Some template', content: 'hello'},
          ]
        }}
        onEditorChange={handleEditorChange}
                    
      />
    </Fragment>
  )
}

EditorWindow.propTypes = {
  template: PropTypes.string,
  handleEditorChange: PropTypes.func
}

export default EditorWindow;