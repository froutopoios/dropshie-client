import React from 'react';

import EditorWindow from './EditorInstance/EditorWindow';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';

import { Editor } from '@tinymce/tinymce-react';
// import tinymce from 'tinymce/tinymce';
// import 'tinymce/icons/default';
// import 'tinymce/themes/silver';
import tinykey from '../../../keys/tinyKey';

import template from './EditorUtils/template'; // temp for testing
import { fetchTemplates } from '../../../actions/accountActions';
import { fetchTemplate, saveTemplate } from '../../../actions/editorActions';
import isEqual from 'lodash/isEqual';

import { compose } from 'redux';
import { connect } from 'react-redux';
import { InputLabel } from '@material-ui/core';
import { display } from '@material-ui/system';


const styles = theme => ({
  root: '',
  editor: {
    marginTop: 100
  },
  controls: {
    padding: 20,
    display: 'flex',
    justifyContent: 'space-between',
    // alignItems: 'center'
  },
  templatesControl: {
    margin: 10
  },
  nameField: {
    marginTop: 20
  },
  saveContainer: {
    flex: '1 1 100',
    // height: 100,
    alignSelf: 'flex-end'
  },
  btnLabel: { 
    
    color: 'white'
  }
})

class TinyMce extends React.Component {
  constructor(props) {
    super(props);
    this.editorRef = React.createRef()
  }
  state = {
    templates: [],
    template: '',
    content: '',
    templateId: '',
    templateName: '',
    fetchTemplateLimit: 0
  }

  componentDidMount() {
    this.props.fetchTemplates();
    let editor = this.editorRef.current;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.template !== this.props.template) {

      this.setState({
        template: this.props.template,
        templateName: this.props.templateName
      })
    }

    const { templates } = this.props

    if (this.state.templates.length === 0 || !isEqual(prevProps.templates, templates)) {
      this.props.fetchTemplates();

      this.setState({ 
        templates,
        fetchTemplateLimit: this.state.fetchTemplateLimit + 1 
      }, () => {
        const templateId = this.state.templates[0].templateId;

        this.props.fetchTemplate(templateId); // load init template
      })
    }

    if (this.state.templates.length === 0 && this.state.fetchTemplatesLimit < 3) {
      this.props.fetchTemplates();

      this.setState({
        fetchTemplatesLimit: this.state.fetchTemplatesLimit + 1
      }) 
    }
  }

  // obselete
  showTemplate = () => {
    this.setState({
      template: template // from import
    })
  }


  handleEditor = () => {
    const editor = this.editorRef.current;
  }

  handleEditorChange = (content, editor) => {
    console.log('editor changed triggered')
    console.log('contentOnChange', content); 
    
    // this.setState({ template: this.props.template });
    

    // console.log('the contnet of the editor', content);
  }

  // for the lister
  changeSpecifics = () => {
    // console.log('change specificis');
    // console.log(this.state.template);
    const template = this.state.template;

    const parser = new DOMParser();
    const temp = parser.parseFromString(template, 'text/html');
    // console.log(temp)

    const title = temp.querySelector("[name='__TITLE__']")
    title.innerHTML = 'AMAZING CHAIR FOR YOU'
    // console.log(title);

    // description
    const description = temp.querySelector("[name='__DESCRIPTION__']");
    description.innerHTML = 'This is an amazing product that creates a vacum in space time'

    // features list
    const featuresList = temp.querySelector("[name='__FEATURES__']")
    featuresList.innerHTML = '';
    
    const features = [
      'the first specific',
      'awasome door',
      'amazing price',
      'build quality'
    ]
    
    features.forEach(feature => {
      const li = document.createElement('li');
      const text = document.createTextNode(feature);
      li.appendChild(text);

      featuresList.appendChild(li);
    })

    const modifiedTemplate = temp.body.innerHTML;    
    this.setState({ template: modifiedTemplate })
  }

  handleTemplateChange = ({ target }) => {
    // console.log(target.selectedIndex);
    const index = target.selectedIndex;
    const templateName = target[index].text
    const templateId = target.value;

    this.setState({
      templateId,
      templateName
    })

    // TODO: fetch the template with the selected value
    this.props.fetchTemplate(target.value)
  }

  handleTemplateNameChange = ({target: { value }}) => {
    this.setState({
      templateName: value
    })
  }

  handleSaveTemplate = () => {
    const payload = {
      Name: this.state.templateName,
      TemplateId: this.state.templateId,
      Html: this.state.template
    }

    // if the name is the same modify it inside sagas
    this.props.saveTemplate(payload)
  }

  render () {
    const { classes } = this.props;
    const { templates, templateId, templateName } = this.state;

    console.log('state tinymce', this.state)
    // console.log(this.state.content);
    // console.log(this.state.templates)
    return (
      <div> 
        <h2>TinyMce</h2>
        <div>
          <button onClick={this.showTemplate}>add template</button>
          <button onClick={this.handleEditor}>editorNodes</button>
          <button onClick={this.changeSpecifics}>changeSpecifics</button>
        </div>
        <div className={classes.controls}>
          <FormControl
            className={classes.templatesControl}
          >
            <InputLabel
              shrink
              htmlFor='templates'
            >Templates</InputLabel>
            <Select
              native
              // defaultValue={templateId && this.state.templateId}
              value={templateId && this.state.templateId || 'no'}
              // inputProps={{
              //   name: 'Templates'
              // }}
              onChange={this.handleTemplateChange}
            >
              {templates.length !== 0 && templates.map(template => {
                  return (
                  <option
                    key={template.templateId}
                    value={template.templateId}
                    >{template.name}</option>
                  )
              })}
            </Select>
            <TextField
              className={ classes.nameField }
              label='Name'
              variant='outlined'
              size='small'
              InputLabelProps={{
                shrink: true
              }}
              value={this.state.templateName}
              onChange={this.handleTemplateNameChange}
            />
          </FormControl>
          <div className={classes.saveContainer}>
            <Button
              classNmae={classes.saveBtn}
              classes={{
                label: classes.btnLabel
              }}
              onClick={this.handleSaveTemplate}
              variant='contained'
              color='primary'
              >Save Template</Button>
          </div>
        </div>
        <div className={ classes.editor }>
            <EditorWindow
              template={this.state.template}
              handleEditorChange={this.handleEditorChange}
            />
          {/* ----- old instance ----- */}
          {/* <Editor
            ref={this.editorRef}
            apiKey={tinykey}
            initialValue="write something down"
            selector='textarea#dropshie-editor'
            // inline={true}
            value={this.state.template}
            init={{
              setup: function (editor) {
                console.log('SETUP IS TRIGGERED');
                // editor.focus()
                editor.addCommand('hop', () => {
                  console.log('the new command triggered')
                })
                editor.addShortcut('ctrl+a', "the shortcut", function () { console.log('shortcut added') })
              },
              // extended_valid_elements : "img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name]",
              valid_elements: "*[*]",
              allow_unsafe_link_target: true,
              quickbars_selection_toolbar: 'bold italic | h2 h3',
              font_formats: 'Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva',
              // block_formats: 'Paragraph=p; Header 1=h1; Header 2=h2',
              height: 800,
              plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount emoticons template'
              ],
              toolbar:
                'undo redo | formatselect | fontselect | bold italic backcolor | \
                alignleft aligncenter alignright alignjustify | \
                bullist numlist outdent indent | removeformat | emoticons template | help',
              templates: [
                { title: 'one', description: 'Some template', content: template},
                { title: 'blackFirday', description: 'Some template', content: 'hello'},
              ]
            }}
            on
            onEditorChange={this.handleEditorChange}
            
          /> */}
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ acount: { templates }, editor: { template, templateName }}) => {

  return { 
    templates,
    template,
    templateName
   }
}

const styledAndConnected = compose(
  connect(mapStateToProps, { fetchTemplates, fetchTemplate, saveTemplate }),
  withStyles(styles)
)

export default styledAndConnected(TinyMce);