import { jsx } from 'slate-hyperscript';


const ELEMENT_TAGS = {
  A: el => ({ type: 'link', url: el.getAttribute('href') }),
  BLOCKQUOTE: () => ({ type: 'quote' }),
  H1: () => ({ type: 'heading-one' }),
  H2: () => ({ type: 'heading-two' }),
  H3: () => ({ type: 'heading-three' }),
  H4: () => ({ type: 'heading-four' }),
  H5: () => ({ type: 'heading-five' }),
  H6: () => ({ type: 'heading-six' }),
  IMG: el => ({ type: 'image', url: el.getAttribute('src') }),
  LI: () => ({ type: 'list-item' }),
  OL: () => ({ type: 'numbered-list' }),
  P: () => ({ type: 'paragraph' }),
  PRE: () => ({ type: 'code' }),
  UL: () => ({ type: 'bulleted-list' }),
}

// COMPAT: `B` is omitted here because Google Docs uses `<b>` in weird ways.
const TEXT_TAGS = {
  CODE: () => ({ code: true }),
  DEL: () => ({ strikethrough: true }),
  EM: () => ({ italic: true }),
  I: () => ({ italic: true }),
  S: () => ({ strikethrough: true }),
  STRONG: () => ({ bold: true }),
  U: () => ({ underline: true }),
}

// DESERIALIZE HTML
export const deserialize = el => {
  console.log('node', el);

  console.log(el.nodeType);
  if (el.nodeType === 3) {
    return el.textContent
  } else if (el.nodeType !== 1) {
    return null
  } else if (el.nodeName === 'BR') {
    return '\n'
  }

  const { nodeName } = el
  let parent = el

  if (
    nodeName === 'PRE' &&
    el.childNodes[0] &&
    el.childNodes[0].nodeName === 'CODE'
  ) {
    parent = el.childNodes[0]
  }
  
  const children = Array.from(parent.childNodes)
    .map(deserialize)
    .flat()

  if (el.nodeName === 'BODY') {
    return jsx('fragment', {}, children)
  }

  if (ELEMENT_TAGS[nodeName]) {
    const attrs = ELEMENT_TAGS[nodeName](el)
    return jsx('element', attrs, children)
  }
  
  if (TEXT_TAGS[nodeName]) {
    const attrs = TEXT_TAGS[nodeName](el)
    console.log(attrs);
    debugger;
    return children.map(child => jsx('text', attrs, child))
  }

  console.log('childreennn')
  console.log(children);
  return children
}


// export const deserialize = el => {
//   if (el.nodeType === 3) {
//     return el.textContent
//   } else if (el.nodeType !== 1) {
//     return null
//   } else if (el.nodeName === 'BR') {
//     return '\n'
//   }

//   const children = Array.from(el.childNodes).map(deserialize)
//   console.log('element from parser', el.nodeName);

//   switch (el.nodeName) {
//     case 'BODY':
//       return jsx('fragment', {}, children)
//     case 'LINK':
//       return jsx(
//         'element',
//         {
//           type: 'link',
//           rel: el.getAttribute('rel'),
//           url: el.getAttribute('href') 
//         },
//         children
//         )
//     case 'BR':
//       return '\n'
//     case 'BLOCKQUOTE':
//       return jsx('element', { type: 'quote' }, children)
//     case 'P':
//       return jsx('element', { type: 'paragraph' }, children)
//     case 'A':
//       return jsx(
//         'element',
//         { type: 'link', url: el.getAttribute('href') },
//         children
//       )
//     default:
//       return el.textContent
//   }
// } 