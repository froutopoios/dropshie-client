import React, { useState, useEffect } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { Editor, EditorState, ContentState, RichUtils, convertFromHTML } from 'draft-js';
import template from './EditorUtils/template';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles(theme => ({
  root: {
    padding: 10
  },
  editorWrapper: {
    marginTop: 20,
    border: '1px solid #ccc',
    minHeight: 200
  }
}))


function DraftEditor() {
  const [editorState, setEditorState] = useState(
    EditorState.createEmpty()
  )

  // const editor = React.useRef(null);
  const classes = useStyles();

  // const focusEditor = () => {
  //   editor.current.focus()
  // }

  // useEffect(() => {
  //   focusEditor();
  // }, [])

  const toggleInlineStyle = e => {
    e.preventDefault();
    let style = e.currentTarget.getAttribute('data-style');

    setEditorState(RichUtils.toggleInlineStyle(editorState, style))
  }

  const loadHtml = () => {
    // const data = `
    //   <head>
    //   <style>
    //     h1 {
    //       color: red;
    //     }
    //   </style>
    //   </head>
    //   <body>
    //     <div>
    //       <h1>HEllo there</h1>       
    //       <ul>
    //         <li style="color:red;" >one</li>
    //         <li>tow</li>
    //         <li>trhee</li>
    //       </ul>
    //     </div>
    //   </body>
    // `
    // const state = EditorState.createWithContent(stateFromHTML(data));
    // const state = stateFromElement(data);
    const blocksFromHtml = convertFromHTML(template);
    const content = ContentState.createFromBlockArray(
      blocksFromHtml.contentBlocks,
      blocksFromHtml.entityMap
    );
    const state = EditorState.createWithContent(content);

    setEditorState(state);
  }

  return (
    <div
      className={classes.root} 
      // onClick={focusEditor}
     >
      <Typography variant='h5'>Template Editor</Typography>
      <input 
        type="button"
        value="Bold"
        data-style="BOLD"
        onMouseDown={toggleInlineStyle}
      />
      <input 
        type="button"
        value="Italic"
        data-style="ITALIC"
        onMouseDown={toggleInlineStyle}
        />
        <input 
          type="button"
          value="InsertTemplate"
          onClick={loadHtml}
          />
       <div className={classes.editorWrapper}>
        <Editor
          // ref={editor}
          editorState={editorState}
          onChange={editorState => setEditorState(editorState)}
        />
       </div>
    </div>
  )  
}

export default DraftEditor;




// const styles = {
//   root: {
//     padding: 10
//   },
//   editorWrapper: {
//     marginTop: 20,
//     border: '1px solid #ccc',
//     minHeight: 200
//   }
// }

// class DraftEditor extends React.Component {
//   state = {
//     editorState: EditorState.createEmpty()
//   }

//   onChange = editorState => {
//     this.setState({ editorState })
//   }

//   toggleInlineStyle = e => {
//     e.preventDefault();
//     let style = e.currentTarget.getAttribute('data-style');

//     this.setState({
//       editorState: RichUtils.toggleInlineStyle(this.state.editorState, style) 
//     })
//   }

//   render() {
//     const { classes } = this.props;
    
//     return (
//       <div
//         className={classes.root} 
//         // onClick={focusEditor}
//       >
//         <Typography variant='h5'>Template Editor</Typography>
//         <input 
//           type="button"
//           value="Bold"
//           data-style="BOLD"
//           onMouseDown={this.toggleInlineStyle}
//         />
//         <input 
//           type="button"
//           value="Italic"
//           data-style="ITALIC"
//           onMouseDown={this.toggleInlineStyle}
//           />
//         <div className={classes.editorWrapper}>
//           <Editor
//             // ref={editor}
//             editorState={this.state.editorState}
//             onChange={this.onChange}
//           />
//         </div>
//       </div>
//     )
//   }


// }

// export default withStyles(styles)(DraftEditor);
