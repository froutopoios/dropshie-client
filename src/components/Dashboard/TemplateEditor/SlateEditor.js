import React, { Fragment, useCallback, useMemo, useState, useEffect } from 'react';
import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import { createEditor, Editor, Transforms, Text } from 'slate';
import { Slate, Editable, withReact, useSlate } from 'slate-react';
// import plain from 'slate-plain-serializer';
import { deserialize } from './EditorUtils/Serializers';
 
import { Toolbar, Button, Icon } from './EditorComponents/EditorComponents';
import { ACCOUNT, SETTINGS } from '../../../actions/types';

import HoveringToolbar from './EditorComponents/HoveringToolbar';

import { makeStyles } from '@material-ui/core/styles' 
import Typography from '@material-ui/core/Typography'
import { contours } from 'd3';

const useStyles = makeStyles(theme => ({
  root: {
    padding: 10
  },
  editorContainer: {
    margin: 50,
    '& span': {
      userSelect: 'none'
    }
  },
  blockquote: {
    borderLeft: '2px solid #ddd',
    marginLeft: 0,
    marginRight: 0,
    paddingLeft: '10px',
    color: '#aaa',
    fontStyle: 'italic',
    '& [dir=\'rtl\']': {
      borderLeft: 'none',
      paddingLeft: 0,
      paddingRight: '10px',
      borderRight: '2px solid #ddd'
    }
  }
}))

const LIST_TYPES = ['numbered-list', 'bulleted-list']


// Custom helpers . Not used for now
const CustomEditor = {
  isBoldMarkActive(editor) {
    const [match] = Editor.nodes(editor, {
      match: n => n.bold === 'true',
      universal: true
    })

    return !!match
  },

  toggleBoldMark(editor) {
    const isActive = CustomEditor.isBoldMarkActive(editor)
    Transforms.setNodes(
      editor,
      { bold: isActive ? null : true },
      { match: n => Text.isText(n), split: true }
    )
  }
}

const isImageElement = element => {
  return element.type === 'image' && typeof element.url === 'string'
}

// TODO: check if it's better to decorate withStyles instead of using classes as a prop
const Element = ({ classes, attributes, children, element}) => {
  switch (element.type) {
    case 'block-quote':
      return <blockquote className={classes.blockquote} {...attributes}>{children}</blockquote>
    case 'bulleted-list':
      return <ul {...attributes}>{children}</ul>
    case 'heading-one':
      return <h1 {...attributes}>{children}</h1>
    case 'heading-two':
      return <h2 {...attributes}>{children}</h2>
    case 'list-item':
      return <li {...attributes}>{children}</li>
    case 'numbered-list':
      return <ol {...attributes}>{children}</ol>
    default:
      return <p {...attributes}>{children}</p>
  }
}

const Leaf = ({ attributes, children, leaf }) => {
  // console.log('attributes', attributes);
  // console.log('leaf', leaf);
  // console.log('leaft', leaf.strikeTrough)
  if (leaf.strikeTrough) {
    children = <s>{children}</s>
  }
  if (leaf.red) {
    children = <span style={{ color: 'red' }}>{children}</span>
  }
  if (leaf.bold) {
    children = <strong>{children}</strong>
  }

  if (leaf.italic) {
    children = <em>{children}</em>
  }

  if (leaf.underline) {
    children = <u>{children}</u>
  }

  return <span {...attributes}>{children}</span>
}

const isMarkActive = (editor, format) => {
  const marks = Editor.marks(editor)
  return marks ? marks[format] === true : false
}

const isBlockActive = (editor, format) => {
  const [match] = Editor.nodes(editor, {
    match: n => n.type === format,
  })

  return !!match
}


const isFormatActive = (editor, format) => {
  const [match] = Editor.nodes(editor, {
    match: n => n[format] === true,
    mode: 'all',
  })
  return !!match
}

const toggleFormat = (editor, format) => {
  const isActive = isFormatActive(editor, format)
  Transforms.setNodes(
    editor,
    { [format]: isActive ? null : true },
    { match: Text.isText, split: true }
  )
}

const toggleBlock = (editor, format) => {
  const isActive = isBlockActive(editor, format)
  const isList = LIST_TYPES.includes(format)

  Transforms.unwrapNodes(editor, {
    match: n => LIST_TYPES.includes(n.type),
    split: true,
  })

  Transforms.setNodes(editor, {
    type: isActive ? 'paragraph' : isList ? 'list-item' : format,
  })

  if (!isActive && isList) {
    const block = { type: format, children: [] }
    Transforms.wrapNodes(editor, block)
  }
}

const toggleMark = (editor, format) => {
  const isActive = isMarkActive(editor, format)

  if (isActive) {
    Editor.removeMark(editor, format)
  } else {
    Editor.addMark(editor, format, true)
  }
}

const FormatButton = ({ format, icon }) => {
  const editor = useSlate()
  return (
    <Button
      reversed
      active={isFormatActive(editor, format)}
      onMouseDown={event => {
        event.preventDefault()
        toggleFormat(editor, format)
      }}
    >
      <Icon icon={icon}/>
    </Button>
  )
}

const MarkButton = ({ format, icon }) => {
  const editor = useSlate()
  return (
    <Button
      active={isMarkActive(editor, format)}
      onMouseDown={event => {
        event.preventDefault()
        toggleMark(editor, format)
      }}
    >
     <Icon icon={icon}/>
    </Button>
  )
}

const BlockButton = ({ format, icon }) => {
  const editor = useSlate()
  return (
    <Button
      active={isBlockActive(editor, format)}
      onMouseDown={event => {
        event.preventDefault()
        toggleBlock(editor, format)
      }}
    >
      <Icon icon={icon}/>
    </Button>
  )
}

/*
  EDITOR
*/
const SlateEditor = props => {
  const classes = useStyles();
  const editor = useMemo(() => withReact(createEditor()), [])
  const [value, setValue] = useState(INITIAL_STATE);
  const [templateHtml, setTemplateHtml] = useState('');

  const dispatch = useDispatch();
  const templates = useSelector(state => state.acount.templates);
  const template = useSelector(state => state.settings.currentTemplate);

  const renderElement = useCallback(props => {
    return <Element classes={classes} {...props}/>
  }, [])

  const renderLeaf = useCallback(props => {
    return <Leaf {...props}/>
  }, [])

  useEffect(() => {
    if (!templates) {
      dispatch({type: ACCOUNT.FETCH_TEMPLATES});
    }

  }, [templates])

  useEffect(() => {
    setTemplateHtml(template);
  }, [template])

  console.log('templates', templates);

  const getTemplate = () => {
    dispatch({type: SETTINGS.GET_TEMPLATE_BY_ID, payload: 'e56e5803-0215-494f-a61f-09636d939235'})
  }

  //  PARSER - CONVERT TO SLATE STATE
  const deserializeTemplate = editor => () => {
    const input = '<p style="color:red">hello</p>'

    const templateRaw = new DOMParser().parseFromString(input, 'text/html');
    // console.log(document)
    console.log(templateRaw)

    const value = deserialize(templateRaw.body);


    // const slateDoc = deserialize(templateRaw.body);
    // console.log('deserialized value', slateDoc);

    
    setValue([
      ...value,
      // ...slateDoc
    ])
    // return editor
  }


  const handleKeyDown = (e, editor) => {
    if (!e.ctrlKey) {
      return
    }

    switch(e.key) {
      case 'b':
        e.preventDefault();
        CustomEditor.toggleBoldMark(editor)
        break;
      default:
        return 
    }
  }

  const changeSpecifics = (value) => () => {
    const copyOfstate = [ ...value ];
    const specifics = copyOfstate.find(child => child.id === 'specifics');
    const indexOfSpecifics = copyOfstate.findIndex(child => child.id === 'specifics');
    
    // from fetcher
    const newSpecifics = [
      'awasome color',
      'great performance',
      'all sizes inlcuded',
      'quality materials'
    ]
    
    // map the spcecifics to the correct form
    const mapedSpecifics = newSpecifics.map(specific => {
      return {
        children: [
          {
            text: specific
          }
        ],
        type: 'list-item'
      }
    })
    
    console.log('maped specifics', mapedSpecifics);
    // insert to the parent
    const copyOfSpecifics = { ...specifics }
    copyOfSpecifics.children = mapedSpecifics;
    console.log('specifics', copyOfSpecifics) 
    copyOfstate.splice(indexOfSpecifics, 1, copyOfSpecifics);


    console.log('copy of state', copyOfstate);
    console.log('old state', value);

    setValue(copyOfstate);
   }

  console.log('slate state', value);
  return (
      <div className={classes.root}>
        <Typography variant='h4'>Template Editor</Typography>
        <button onClick={changeSpecifics(value)}>replace Specifics</button>
        <button onClick={getTemplate}>get Template</button>
        <button onClick={deserializeTemplate(editor)}>deserialize</button>
        <div className={classes.editorContainer}>
          <Slate
            editor={editor}
            value={value}
            onChange={value => setValue(value)}
          >
            <Toolbar>
              <MarkButton format='bold' icon='bold'/>
              <MarkButton format='italic' icon='italic'/>
              <MarkButton format='underline' icon='underline'/>
              <MarkButton format='strikeTrough' icon='strike_through'/>
              <MarkButton format='red' icon='attachement' />
              <BlockButton format="block-quote" icon="format_block_quote" />
              <BlockButton format="numbered-list" icon="format_list_numbered" />
              <BlockButton format="bulleted-list" icon="format_list_bulleted" />
            </Toolbar>
            {/* <button
              style={{ userSelect: 'none' }}
              onMouseDown={e => {
                e.preventDefault();
                CustomEditor.toggleBoldMark(editor)
              }}
            >Bold</button> */}
            <HoveringToolbar FormatButton={ FormatButton }/>
            <Editable
              renderElement={renderElement}
              renderLeaf={renderLeaf}
              onKeyDown={(e) => handleKeyDown(e, editor)}
              spellCheck
              autoFocus
            />
          </Slate>
        </div>
      </div>
  )
}

export default SlateEditor;


const withHtml = editor => {
  const { insertData, isInline, isVoid } = editor;

  editor.isInline = element => {
    return element.type === 'link' ? true : isInline(element)
  }

  editor.isVoid = element => {
    return element.type === 'image' ? true : isVoid(element)
  }

}

const INITIAL_STATE = [
  {
    type: 'paragraph',
    children: [{ text: 'So, the whole history of software development is the history of war against complexity. Structural programming and OOP, function libraries, and (very) high-level languages — just a very few examples of attempts to cope with this complexity. And once we discover any new way to reduce the complexity we immediately use it to build even more complex systems, so this war never ends.' }]
  },
  {
    type: 'paragraph',
    children: [{ text: 'Donald duck went to Limnoupoli to steal money.' }]
  },
  {
    type: 'block-quote',
    children: [
      {
        text: 'hello there'
      }
    ]
  },
  {
    type: 'bulleted-list',
    id: 'specifics',
    children: [
      {
        type: 'list-item',
        children: [
          {
            text: 'hey'
          }
        ]
      },
      {
        type: 'list-item',
        children: [
          {
            text: 'hop',
          }
        ]
      }
    ]
  }
]