import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { submitToEbay, stopFetchingItem } from '../../../actions/listingActions';
import { fetchTemplate } from '../../../actions/editorActions';
import { fetchPolicies } from '../../../actions/ebayActions'
import { fetchUserSettings } from '../../../actions/settingsActions';
import EditorWindow from '../TemplateEditor/EditorInstance/EditorWindow';

// import PriceCalculator from '../../../calculators/PriceCalculator';

import { styles } from './InventorySubmit.style';
import { withStyles } from '@material-ui/core/styles';
import NumberFormat from 'react-number-format';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add'
import Tooltip from '@material-ui/core/Tooltip';
import clsx from 'clsx';


import MainProperties from './Item/MainProperties';
import ProfitCalculator from './Item/ProfitCalculator';
import EbayPolicies from './Item/EbayPolicies';
import ItemSpecifics from './Item/ItemSpecifics';
import Images from './Item/Images.js';
import AccountBasedSelections from './Item/AccountBasedSelections';
import ConfirmDialog from '../../UI/Modal/ConfirmDialog';

import { has } from '../../../utils/has'; 
import { checkValidity } from '../../../utils/checkValidity'; 

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPallet } from '@fortawesome/free-solid-svg-icons'; 


const NumberFormatTextField = props => {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      { ...other }
      getInputRef={ inputRef }
      onValueChange={values => {
        onChange({
          target: {
            value: values.value
          }
        })
      }}
      thousandSeparator
      isNumericString
      prefix="$"
     />
  )
}

NumberFormatTextField.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
}

class InventorySubmit extends Component {
  state = {
    ebayUser: '',
    productTitle: '',
    titleCount: 0,
    focused: true,
    asin: '',
    originPrice: 0,
    activeStep: 0,
    selectedCategory: {},
    itemSpecifics: [],
    itemProperties: {},
    images: [],
    categories: [],
    showDialog: false,
    floatButton: false,
    redirect: null,
    template: 'Start Writing Down',
    selectedShippingPolicy: '',
    selectedReturnPolicy: '',
    selectedPaymentPolicy: '',
    listingDuration: null,
    itemCondition: null,
    allowBuyersToRemainAnonymous: 1,
    desiredItemQty: 0,
    defaultItemLocation: null,
    ebayCompetitors: false,
    competitorsPriceDifference: 0,
    ebayItemId: null
  }

  componentDidMount() {
    this.props.stopFetchingItem()
    this.props.fetchUserSettings();
    // console.log(this.props.fetchedItem);
    let  { GenericData, ItemProperties, EbayData } = this.props.fetchedItem;

    if ( (GenericData === undefined || GenericData.length === 0)) {
      // TODO: show a notification that something went wrong
      this.setState({redirect: '/dashboard/AddItem'})
    } else {
      const categories = has(EbayData, 'Categories', 'CategoriesFound');
      const originPrice = has(GenericData, 'CurrentOriginPrice');
      const itemSpecifics = has(GenericData, 'ItemSpecifics', 'ItemsSpecifics');
      let selectedCategory = has(EbayData, "Categories", "SelectedCategory");
      const itemCondition = has(GenericData, 'ItemCondition');

      // pricing
      // ebay 
      const ebayUser = has(EbayData, 'SellMarketUserName');
      const sellingMarket = has(EbayData, 'DestinationMarket');
      this.props.fetchPolicies(ebayUser);
  
      let { UPC, Brand, ISBN, EAN, MPN } = ItemProperties;
      const hashedProps = { UPC, EAN, Brand, ISBN, MPN};
  
  
      // Main ITEM properties
      const productTitle = ItemProperties.ProductTitle;
      const asin = GenericData.OriginProductCode;
      const description = ItemProperties.OriginMainDescriptionHtml;
      const checkedProperties = checkValidity(hashedProps);
      let bulletPoints = has(ItemProperties, 'BulletPoints');

      if (bulletPoints instanceof Array) {
        bulletPoints = bulletPoints.map(bulletpoint => {
          return `<li>${ bulletpoint }</li>`
        })
      }
    
      const features = `<ul>${ bulletPoints }</ul>`
  
      // Imnages
      const images = has(ItemProperties, 'Images');
  
      this.setState({
        ebayUser,
        sellingMarket,
        productTitle,
        description,
        features,
        images, 
        selectedCategory,
        asin,
        categories,
        titleCount: productTitle.length,
        originPrice,
        netProfit: 0, // done
        itemProperties: checkedProperties,
        itemSpecifics,
        template: this.props.template,
        itemCondition
      })
    }
  }

  componentDidUpdate(prevProps, prevState) {
    // handle Scroll
    window.onscroll = () => {
      if (window.pageYOffset >= 50 && this.state.floatButton === false) {
        this.setState({
          floatButton: true
        })
      } else if ( window.pageYOffset < 50 && this.state.floatButton === true  ){
        this.setState({
          floatButton: false
        })
      }
    }
  
    const { template } = this.props;

    if (template && prevProps.template !== template) {
      // this.handleLoadTemplate()
      this.setState({
        template
      })
    }
    
    if (prevState.productTitle !== this.state.productTitle || prevState.description !== this.state.description || prevState.features !== this.state.features ) {
      this.handleReplaceEditorValues();
    }

    // policies
    if (prevProps.policies !== this.props.policies) {
      const { policies } = this.props
      const currentUserPolicies = policies.filter(allUserPolicy => allUserPolicy.EbayUsername === this.state.ebayUser)
                                            .filter(policy => policy.SelingMarket === this.state.sellingMarket)
                                            .reduce((ac, item) => item,{})

      const { PaymentPolicies, ReturnPolicies, ShippingPolicies } = currentUserPolicies;
      const { SelectedPaymentPolicy, SelectedReturnPolicy, SelectedShippingPolicy } = currentUserPolicies;

      this.setState({
        paymentPolicies: PaymentPolicies,
        returnPolicies: ReturnPolicies,
        shippingPolicies: ShippingPolicies,
        selectedShippingPolicy: SelectedShippingPolicy,
        selectedReturnPolicy: SelectedReturnPolicy,
        selectedPaymentPolicy: SelectedPaymentPolicy 
      })
    }

    if (prevProps.listingDuration !== this.props.listingDuration) {
      this.setState({
        listingDuration: this.props.listingDuration
      })
    }

    if (prevProps.defaultItemLocation !== this.props.defaultItemLocation) {
      this.setState({
        defaultItemLocation: this.props.defaultItemLocation
      })
    }

    if (prevProps.defaultItemQty !== this.props.defaultItemQty) {
      const { defaultItemQty } = this.props
      this.setState({
        defaultItemQty,
        desiredItemQty: defaultItemQty
      })
    }
  }

  componentWillUnmount() {
    window.onscroll = null;
  }

  handleTitleChange = (name, value) => {
    const currentCount = value.length;
    const limit = 80;

    if (currentCount <= limit) {
      this.setState({
        [name]: value,
        titleCount: currentCount
      })
    }
  }

  handleSelectedCategory = e => {
    // console.log(this.state.categories);
    const id = e.target.id;
    // find the selected category
    const selectedCategory = this.state.categories[id].primaryCategory;
    
    this.setState({
      selectedCategory
    })
  }

  handleChange = name => ({ target: { value } }) => {

    // TODO: have to refactor that
    if (name === 'productTitle') {
      this.handleTitleChange(name, value)
      return;
    }

    if (value > 0) {
      this.setState({
        [name]: value
      })
    } else if (value === undefined || value === null) {
      this.setState({
        [name]: 0
      })
    }
  }

  handleFeeChange = name => value => {
    this.setState({
      [name]: value
    })
  }

  handleEbayCompetitors = () => {
    this.setState(prevState => ({
      ebayCompetitors: !prevState.ebayCompetitors
    }))
  }

  handleEbayCompetitorsValues = name => ({ target: { value }}) => {
    console.log('triger', name, value)
    this.setState({
      [name]: value
    })
  }

  // SPecifics Methods
  handleAddFeature = (value, id) => () => {
    const specificArr = [ ...this.state.itemSpecifics ];
    const valuesArray = specificArr[id]['SpecificValues'];

    const newFeature = {
      Value: value,
      IsSelected: true
    }
    valuesArray.push(newFeature);
    this.setState({
      itemSpecifics: specificArr
    })
  }

  
  handleDeleteFeature = id => {
    const features = [ ...this.state.itemSpecifics ];
    features.splice(id, 1);

    this.setState({
      itemSpecifics: features
    })
  }

  handleDeleteSpec = ( value, index ) => () => {
    const specificArr = [ ...this.state.itemSpecifics ];
    const SpecificValues = specificArr[index].SpecificValues;

    // find the specific in the array of specifics
    for (let i in SpecificValues) {
      if (SpecificValues[i].Value === value) {
        SpecificValues.splice(i, 1);
      }
    }

    this.setState({
      itemSpecifics: specificArr
    })
  }

  handleAddSpecific = (specific) => {
    const itemSpecifics = [ ...this.state.itemSpecifics ];
    itemSpecifics.push(specific);

    this.setState({
      itemSpecifics
    })
  }

  // Updates the array of images with the mods of the user
  handleImageArray = images => {
    this.setState({
      images
    })
  } 

  // --*-- EDITOR METHODS --*--
  // *** OBSELETE ***
  handleLoadTemplate = () => {
    const { templateId } = this.props;
    console.log('inside template load handler', templateId);

    if (templateId) {
      this.props.fetchTemplate(templateId)
      console.log('LOADING TEMPLATE...')
    } else {
      this.setState({
        template: 'Could not load the Selected Template'
      })
    }
  }

  handleReplaceEditorValues = () => {
    const { template } = this.state;
    
    if (!template) {
      return;
    }

    const parser = new DOMParser();
    const templateTree = parser.parseFromString(template, 'text/html');

    const title = templateTree.querySelector("[name='__TITLE__']");
    title.innerHTML = this.state.productTitle ? this.state.productTitle : 'DOES NOT APPLY';
    const description = templateTree.querySelector("[name='__DESCRIPTION__']");
    description.innerHTML = this.state.description;

    const features = templateTree.querySelector("[name='__FEATURES__']");
    features.innerHTML = this.state.features;

    // images
    // TODO: gather number of imageplaceholders with regex
    
    const firstImage = templateTree.querySelector("[name='__1_IMAGE__']")
    const secondImage = templateTree.querySelector("[name='__2_IMAGE__']")
    const thirdImage = templateTree.querySelector("[name='__3_IMAGE__']")
    const forthImage = templateTree.querySelector("[name='__4_IMAGE__']")

    // console.log(this.state.images);
    const imagesUrlArray = this.state.images.map(image => image.url);

    const checkIfImageExists = (imageArray, number) => {
      return imageArray[number] ? imageArray[number] : ''
    }
    // replace innerhtml of image placeholders
    firstImage.src = checkIfImageExists(imagesUrlArray, 0)
    secondImage.src = checkIfImageExists(imagesUrlArray, 1)
    thirdImage.src = checkIfImageExists(imagesUrlArray, 2)
    forthImage.src = checkIfImageExists(imagesUrlArray, 3)

    const modifiedTemplate = templateTree.body.innerHTML;

    this.setState({
      template: modifiedTemplate
    })
  }

  handleEditorChange = content => {
    this.setState({
      template: content
    })
  }

  handleChangeAccountValues = name => ({ target: { value }}) => {
    console.log(name, value);
    this.setState({
      [name]: value
    })
  }

  // TODO: dry that with the above
  handleQtyChange = name => ({ target: { value }}) => {
    this.setState({
      [name]: value
    })
  }

  handleShowConfirmDialog = () => {
    this.setState({
      showDialog: !this.state.showDialog
    })
  }

  //  --*-- SUBMIT --*--
  handleSubmit = () => {
    this.props.submitToEbay(this.state);

    if ( this.state.showDialog ) {
      this.setState({
        showDialog: false
      })
    }
  }
  
  render() {
    if (this.state.redirect)  {
      return (
        <Redirect to={this.state.redirect}/>
      )
    }
    // console.log("STATE OF THE TEMPLATE", this.state.template);

    const { classes } = this.props;

    const {
      productTitle,
      titleCount,
      itemProperties,
      selectedCategory,
      originPrice,
      asin,
      desiredQty,
      itemSpecifics,
      categories,
      images
     } = this.state


    return (
      <div className={ classes.container }>
        {/* <div style={{ backgroundColor: '#f2f2f2' }}> */}
          <MainProperties 
              titleCount={ titleCount }
              title={ productTitle }
              handleChange={ (value) => this.handleChange(value) }
              category={ selectedCategory } // category title
              categories={ categories }
              asin={ asin }
              handleSelected={ this.handleSelectedCategory }
              />

          <ProfitCalculator
            originPrice={originPrice}
            paypalFeesPers={this.props.paypalFeesPers}
            ebayFeesPers={this.props.ebayFeesPers}
            feesPers={this.props.feesPers}
            fixedCost={this.props.fixedCost}
            profitPers={this.props.profitPers}
            fixedProfit={this.props.fixedProfit}
            sourceTax={this.props.sourceTax}
            desiredItemQty={this.state.desiredItemQty}
            handleChange={this.handleQtyChange}
            ebayCompetitors={this.state.ebayCompetitors}
            competitorsPrice={this.state.competitorsPrice}
            handleEbayCompetitors={this.handleEbayCompetitors}
            handleEbayCompetitorsValues={this.handleEbayCompetitorsValues}
            competitorsPriceDifference={this.state.competitorsPriceDifference}
            ebayItemId={this.state.ebayItemId}
          />

          <div className={ classes.marginTop }>
            <Divider variant='inset'/>
            <EbayPolicies
              fetchPolicies={this.props.fetchPolicies}
              shippingPolicies={this.state.shippingPolicies}
              returnPolicies={this.state.returnPolicies}
              paymentPolicies={this.state.returnPolicies}
              selectedShippingPolicy={this.state.selectedShippingPolicy}
              selectedReturnPolicy={this.state.selectedReturnPolicy}
              selectedPaymentPolicy={this.state.selectedPaymentPolicy}
              handleChange={ (value) => this.handleChange(value) }/>
          </div>

          <div className={ classes.marginTop }>
            <Divider variant='inset'/>
            <ItemSpecifics itemProperties={ itemProperties }
                           specifics={ itemSpecifics }
                           addFeature={ this.handleAddFeature }
                           onDeleteSpec={ this.handleDeleteSpec }
                           handleDeleteFeature={this.handleDeleteFeature}
                           handleAddSpecific={ this.handleAddSpecific }
            />
          </div>

          <div className={ classes.marginTop }>
            <Divider variant='inset'/>
            {/* images component -- list of images + upload image func */}
            <Images images={ images }
                    handleSelected={ this.handleSelected }
                    handleUpdateImages = { this.handleUpdateImages }
                    handleImageArray = { this.handleImageArray }
            />
          </div>
          {/* EDITOR */}
          <div className={classes.marginTop}>
            <div className={classes.controls}>
              <Button
                variant='contained'
                onClick={this.handleReplaceEditorValues}
              >Change title</Button>
            </div>
            <EditorWindow
              template={this.state.template}
              handleEditorChange={this.handleEditorChange}
            />
          </div>    
          {/* -- ACCOUNT SPECIFITCS --- */}
          <div className={classes.marginTop}>
            <Divider variant='inset'/>
            <AccountBasedSelections
              defaultListingDuration={this.props.listingDuration}
              listingDuration={this.state.listingDuration}
              handleChange={this.handleChangeAccountValues}
              defaultItemCondition={this.props.fetchedItem.GenericData.ItemCondition}
              itemCondition={this.state.itemCondition}
              allowBuyersToRemainAnonymous={this.state.allowBuyersToRemainAnonymous}
              defaultItemLocation={this.state.defaultItemLocation}
            />
          </div>
          <div className={ classes.marginTop }>
            <section id='submit' className={ classes.submit }>
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Button variant='contained'
                        classes={{
                          label: classes.label
                        }}
                        // style={{ padding: '10px 20px' }}
                        color='primary'
                        endIcon={<FontAwesomeIcon icon={ faPallet } size='1x'/>}
                        onClick={ this.handleSubmit }>Add item to Inventory</Button>
              </div>
            </section>
          </div>

          <Tooltip
            title='Quick Submit'
            placement='right'
          >
            <Fab
              className={ clsx(classes.fab, this.state.floatButton ? classes.float : null) }
              color='primary'
              size='small'
              component='button'
              onClick={ this.handleShowConfirmDialog }
            >
              <AddIcon/>
            </Fab>          
          </Tooltip>
          {this.state.showDialog ? <ConfirmDialog 
                                      open={this.state.showDialog}
                                      content='Are you sure you want to add the item with the default settings?'
                                      title='Quick Add'
                                      close={this.handleShowConfirmDialog}
                                      confirm={this.handleSubmit}
                                      okBtn='Submit'
                                      /> : null}
      </div>
      )
    }
  }
  
  // TODO: get ebaypolicies from api/vi/Settings/UserSettings
  const mapSTateToProps = ({ addItem:  { fetchedItemProperties }, fetchedItem, selectedTemplate, editor: { template }, ebay: { policies }, settings: { PaypalFeesPers, EbayFeesPers, BreakEvenPers, ProfitPers, FixedProfit, FixedCost, SourceTax, ListingDuration, DefaultItemLocation, DefaultItemQty
  }}) => {
    return {
      itemProperties: fetchedItemProperties,
      fetchedItem: fetchedItem,
      templateId: selectedTemplate,
      template,
      policies,
      paypalFeesPers: PaypalFeesPers,
      ebayFeesPers: EbayFeesPers,
      feesPers: BreakEvenPers, // CAUTION. (breakeven from api is otherFeesPers in reality)
      profitPers: ProfitPers,
      fixedProfit: FixedProfit,
      fixedCost: FixedCost,
      sourceTax: SourceTax,
      listingDuration: ListingDuration,
      defaultItemLocation: DefaultItemLocation,
      defaultItemQty: DefaultItemQty
    }
  }
  
  const StyledInventorySubmit = withStyles(styles)(InventorySubmit)
  export default connect(mapSTateToProps, { submitToEbay, stopFetchingItem, fetchTemplate, fetchPolicies, fetchUserSettings })(StyledInventorySubmit);