import React, { Component } from 'react';
import { connect } from 'react-redux';
import { handleShowAdd, stopLoading } from '../../../../actions/uiActions';

import { withRouter } from 'react-router-dom';

import AddItem from './AddItem';
import Slide from '@material-ui/core/Slide'

// ** THis is a universal parent for the add item form and for the bulk item form in the future

import { submitForFetch, fetchEbayUsers, fetchTemplates, fetchOriginMarketplaces } from '../../../../actions/listingActions';
import { getEbayUsers } from '../../../../actions/ebayActions';

class Lister extends Component {
  state = {
    show: false,
    templateId: ''
  }

  componentDidMount() {
    this.setState({
      show: true
    })
  }

  handleClose = (value) => {
    this.props.handleShowAdd(false);
    this.props.stopLoading();
    // use the close method to redirect to inventory if the component is not loaded from the inventory
    if (this.props.location.pathname !== '/dashboard/inventory') {
      if (!this.props.fetchingItem) {
        this.props.history.push('/dashboard/inventory')
      }      
    }
  }

  // not used
  sendTemplateIdToState = templateId => {
    console.log('template id to parent',templateId);
  }


  render() {
    const { submitForFetch,
      getEbayUsers, 
      fetchTemplates, 
      templates, 
      fetchOriginMarketplaces, 
      originMarketplaces,
      isLoading,
      ebayUsers,
      submitForFetchError } = this.props;

  
    return (
        <Slide
          direction='left'
          in={this.state.show}
          mountOnEnter
          unmountOnExit
        >
          <div>
            <AddItem
              templates={ templates }
              submitForFetch={ submitForFetch }
              originMarketplaces={ originMarketplaces }
              ebayUsers={ ebayUsers }
              isLoading={ isLoading }
              submitForFetchError={ submitForFetchError }
              fetchEbayUsers={ getEbayUsers }
              fetchTemplates={ fetchTemplates }
              fetchOriginMarketplaces={ fetchOriginMarketplaces }
              handleFormClose={ this.handleClose } // false
              sendTemplateIdToState={ this.sendTemplateIdToState }
            />
          </div>
        </Slide>
    )
  }
}

const mapStateToProps = ({ ebay: { users }, addItem: { templates, originMarketplaces, fetchingItem }, ui: { isLoading, showAddItemForm, collapse }, errors }) => {
  console.log('Inventory -- error', errors);
  return {
    templates,
    ebayUsers: users,
    originMarketplaces,
    isLoading,
    showAddItemForm,
    fetchingItem,
    submitForFetchError: errors.submitForFetchError
  }
}

export default connect(mapStateToProps, {  
  submitForFetch, 
  fetchEbayUsers, 
  fetchTemplates,
  handleShowAdd,
  getEbayUsers,
  stopLoading,
  fetchOriginMarketplaces })(withRouter(Lister));
