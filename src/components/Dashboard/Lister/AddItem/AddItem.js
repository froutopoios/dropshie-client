import React, { useEffect, useState } from "react";
import { makeStyles } from '@material-ui/styles';
import clsx from 'clsx';

import useFormValidation from '../../../../hooks/useFormValidation';
import validateItemForFetch from '../../../../validators/validateItemForFetch';

import Snack from '../../../UI/Notifications/Snack';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    overflowX: 'hidden',
    marginTop: theme.spacing(3),
    padding: '20px'
  },
  paper: {
    padding: '20px 20px',
    width: '400px',
    zIndex: '1000'
    // textAlign: 'center'
  },
  heading: {
    // padding: '30px 50px'
  },
  button: {
    // margin: theme.spacing(1),
    '&:hover': {
      boxShadow: 'none'
    }
  },
  TextField: {
    // marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  buttonGroup: {
    marginTop: '20px', 
    // border: '1px solid green',
    display: 'flex',
    justifyContent: 'flex-end'
  },
  space: {
    marginTop: theme.spacing(3)
  },
  select: {
    '& option': {
      color: 'red',
      '&:selected': {
        color: 'green !important'
      },
    },
    '& inputSelect': {
      border: '1px solid red',
      color: 'blue !important'
    }
  },
  dropdownStyle: {
    transition: 'unset',
    color: theme.palette.primary.main,
    backgroundColor:'rgba(0, 0, 0, .8)',
    '&::-webkit-scrollbar': {
      width: '10px'
    } 
  },
  error: {
    border: '1px solid red'
  },
  test: {
    color: 'red',
    backgroundColor: 'blue',
    border: '1px solid yellow'
  },
  errorSnack: {
    backgroundColor: theme.palette.error.dark
  }
}))

const countries = [
  {
    name: 'United States',
    id: 1
  },
  {
    name: 'United Kingdom',
    id: 2
  },
  {
    name: 'Canada',
    id: 3
  },
  {
    name: 'Australia',
    id: 4
  },
  {
    name: 'Germany',
    id: 5
  },
]

// ** IS CHILD OF AN INDXE FILE IN THE SAME FOLDER
// TODO: select boxes do not set the STate on mount 
const AddItem = props => {
  const [state, setState] = useState({
    WebGateOriginMarketId: '',
    TemplateId: '',
    EbayUserName: '',
    ProductIdInput: '',
    "CountryId": 1
  })

  const [elevate, setElevate] = useState(1);
  const [error, setError ] = useState('');
  const [show, setShow] = useState(false)

  const classes = useStyles()
  const { templates, originMarketplaces, ebayUsers, isLoading, submitForFetchError, handleFormClose } = props;
  
  useEffect(() => {
    props.fetchEbayUsers();
    props.fetchTemplates();
    props.fetchOriginMarketplaces();

    setTimeout(() => {
      setElevate(5)
    }, 500);
    return () => {
      props.handleFormClose(false);
      props.sendTemplateIdToState(state.TemplateId);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    setShow(true)
  }, [])
  
  // POPULATE THE STATE FOR THE DEFAULT VALUE
  useEffect(() => {
    if (templates.length !== 0 ) {
      // console.log('templates from fetch',templates)

      setState(prevState => {
        const TemplateId =  templates[0].TemplateId;
        return { ...prevState, TemplateId }
      })
    }

    if (ebayUsers.length !== 0) {
      setState(prevState => {
        const EbayUserName = ebayUsers[0].UserName
        console.log('ebay user', EbayUserName);
        
        return { ...prevState, EbayUserName }
      })
    }

    if (originMarketplaces.length !== 0) {
      setState(prevState => {
        // console.log(originMarketplaces)
        const WebGateOriginMarketId = originMarketplaces[0].WebGateOriginId;
        
        return {...prevState, WebGateOriginMarketId} 
      })
    }
  }, [templates, originMarketplaces, ebayUsers])

  useEffect(() => {
    setError(submitForFetchError);
  }, [submitForFetchError])


  // VALIDATION  
  const { handleChange, handleBlur, values, errors } = useFormValidation( { ...state }, validateItemForFetch);

  // SUBMIT FOR FETCH
  const submitItem = (item) => () => {
    props.submitForFetch(item);
  }

  console.log('tmeplateID', state.TemplateId);

  return (
    <div className={ classes.root }>
        { error ?  
          <Snack open={ true }
              type='netError'
              vertical='bottom'
              horizontal='right'
              variant='error'
              transition='up'
              message="Could not fetch"/> : null }
          <Snack open={ isLoading } 
              vertical='bottom' 
              horizontal='right'
              //  variant='error'
              transition='left'
              message="fetching from Amazon Us"/>
          <Paper elevation={ elevate } className={ classes.paper }>
            <Typography variant='h5' className={clsx('font-primary-l1', classes.heading)}>Add Item to Inventory</Typography>
            <form className={ classes.form }>

              {/** PRODUCT URL */}
              <TextField
                  error={ errors.ProductIdInput ? true : false }
                  helperText= { errors.ProductIdInput ? errors.ProductIdInput : false }
                  label='Product Url'
                  type='text'
                  value={ values.ProductIdInput }
                  onChange={ handleChange('ProductIdInput') }
                  onBlur={ handleBlur }
                  className={ classes.TextField }/>

              {/* TODO: to seperate component one universal combo */}
              {/* ORIGIN */}
              <InputLabel htmlFor="WebGateOriginMarketId-select" className={ classes.space }>Country</InputLabel>
              <Select
                  displayEmpty
                  name='country'
                  value={ values.CountryId}
                  className={ classes.select }
                  MenuProps={{ classes: { paper: classes.dropdownStyle } }}
                  onChange={ handleChange('CountryId') }
                  inputProps={{
                    name: 'WebGateOriginMarketId',
                    id: 'WebGateOriginMarketId-select'
                  }}>
                  { countries.map(country => (
                    <MenuItem key={ country.id } value={ country.id }>{ country.name }</MenuItem>
                  )) }
              </Select>

              {/* TEMPLATE */}
              <InputLabel htmlFor="TemplateId-select" className={ classes.space }>Select Template</InputLabel>
              <Select
                  displayEmpty
                  name="TemplateId"
                  MenuProps={{ classes: { paper: classes.dropdownStyle } }}
                  // SelectDisplayProps={ classes.test }
                  className={ classes.select }
                  value={ values.TemplateId }
                  onChange={ handleChange('TemplateId') }
                  inputProps={{
                    name: 'TemplateId',
                    id: 'TemplateId-select'
                  }}>
                  { templates && templates.length !== 0 ? templates.map(TemplateId => (
                    <MenuItem key={ TemplateId.TemplateId} value={ TemplateId.TemplateId }>{ TemplateId.Name }</MenuItem>
                  )) : null }
              </Select>

              {/* EBAY USERS */}
              <InputLabel htmlFor="EbayUserName-select" className={ classes.space }>Select Ebay User</InputLabel>
              <Select
                  native
                  value={ values.EbayUserName }
                  onChange={ handleChange('EbayUserName') }
                  inputProps={{
                    name: 'EbayUserName',
                    id: 'EbayUserName-select'
                  }}>
                  { ebayUsers && ebayUsers.length !== 0 ? ebayUsers.map(usr => {
                    const { UserName } = usr;
                    return <option value={ UserName } key={ UserName }>{ UserName }</option>
                  }) : null}
              </Select>
            </form>
            <div className={ classes.buttonGroup }>
              <Button 
                  onClick={ () => handleFormClose(false) }
                  className={ classes.button }
                  color='secondary'
                  >back</Button>
              <Button 
              //  This is temp to test the Action
                  onClick={ submitItem(values) }
                  // disabled={ errors ? true : false }
                  className={ classes.button }
                  disabled={ isLoading || Object.keys(errors).length || !values.ProductIdInput.length  ? true : false }
                  color='primary'
                  >Add</Button>
            </div>
          </Paper>

        {/* </Slide> */}
    </div>
  )
};

export default AddItem;
