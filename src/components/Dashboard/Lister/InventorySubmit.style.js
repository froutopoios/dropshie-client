export const styles = theme =>  ({ 
  container: {
    padding: '50px 50px',
    [theme.breakpoints.up('lg')]: {
      maxWidth: '80%',
      margin: '50px auto'
    }
  },
  marginTop: {
    marginTop: '50px'
  },
  submit: {
    marginTop: '100px',
  },
  fab: {
    position: 'fixed',
    top: '80px',
    right: '20px',
    transition: 'top .6s ease-out'
  },
  float: {
    top: '30px'
  },
  label: {
    // color: 'red'
    padding: '5px 10px'
  }
})
 