import React, { Fragment, useState } from "react";
import { styles } from "./mainProperties.styles";

import { withStyles } from "@material-ui/core/styles";
import clsx from "clsx";

import Categories from "./Categories/Categories";

import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import Tooltip from "@material-ui/core/Tooltip";
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'

const CustomExpansionPanel = withStyles({
  root: {
    boxShadow: "none",
    border: "1px solid #ddd",
    width: "100%",
    "&:before": {
      backgroundColor: "rgba(0, 0, 0, 0)",
    },
  },
})(ExpansionPanel);

const MainProperties = (props) => {
  const { classes, title, titleCount, category, buyPrice } = props;
  const [state, setState] = useState({
    focused: true,
  });
  const [expanse, setExpanse] = useState(false);

  // console.log(itemProperties);
  const setFocus = () => {
    setState((prevState) => !prevState.focused);
  };

  const handleTitleEditExpanse = () => {
    setExpanse(expanse ? false : true);
  };

  return (
    <Fragment>
      <section id="main properties">
        <Typography variant="h4">Main Properties</Typography>
        {/* <Paper className={ classes.paper }> */}

        {/* TITLE */}
        <div className={classes.marginTop}>TITLE</div>
        <CustomExpansionPanel
          className={classes.expansionPanel}
          onChange={handleTitleEditExpanse}
          expanded={expanse}
        >
          <ExpansionPanelSummary className={classes.expansion}>
            <div style={{ flex: "1 1 auto" }}>
              <Typography className={clsx(classes.title)} variant="h6">
                {title}
              </Typography>
            </div>
            <div>
              <Tooltip
                className={classes.tooltip}
                title="Edit the Original Title"
              >
                <IconButton>
                  <EditIcon fontSize="default" style={{ fontSize: 20 }} />
                </IconButton>
              </Tooltip>
            </div>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            {/* <div className={ clsx(classes.formControl) } >  */}
            <label htmlFor="title" className={classes.label}>
              Title
            </label>
            {/* <Typography variant='h5'>Title</Typography> */}
            <TextField
              id="title"
              classes={{
                root:
                  state.focused || titleCount > 0
                    ? classes.focusedTextField
                    : null,
              }}
              onFocus={setFocus}
              onBlur={setFocus}
              className={clsx(classes.textField, classes.dense, classes.title)}
              label={`${titleCount}/80`}
              value={title}
              variant="outlined"
              margin="dense"
              onChange={props.handleChange("productTitle")}
              inputProps={{ "aria-label": "bare" }}
            />
            {/* </div> */}
          </ExpansionPanelDetails>
        </CustomExpansionPanel>
        {/* </Paper> */}

        {/* ASIN */}
        <div className={clsx(classes.borderBottom, classes.marginTop)}>
          <div>ASIN</div>
          <Typography classes={{ root: classes.root }} variant="h5">
            {props.asin}
          </Typography>
        </div>
        {/* </Paper> */}

        {/* **** CATEGORY */}
        <div className={clsx(classes.flex, classes.formControl)}>
          <label htmlFor="categories" className={classes.label}>
            Category
          </label>
          <TextField
            id="categories"
            className={classes.textField}
            value={category.title || "loading"}
            margin="dense"
            variant="outlined"
            InputProps={{
              endAdornment: (
                <Button
                  color="primary"
                  classes={{
                    root: classes.button,
                  }}
                >
                  search
                </Button>
              ),
            }}
          />
        </div>
        {/* Categories */}
        <Categories
          categories={props.categories}
          handleSelected={props.handleSelected}
        />
      </section>
    </Fragment>
  );
};

export default withStyles(styles)(MainProperties);
