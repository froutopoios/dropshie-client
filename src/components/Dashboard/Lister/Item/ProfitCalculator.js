import React, { Component } from "react";
import clsx from "clsx";
import NumberFormat from "react-number-format";
import PropTypes from "prop-types";

import { connect } from "react-redux";
import { compose } from "redux";

import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

import RegularInput from "../../../UI/Inputs/RegularInput";
import InputNumeric from "../../../UI/Inputs/InputNumeric";
import {
  sendInitValues,
  sendValueToStore,
  sendSellAndNetToStore,
} from "../../../../actions/listingActions";
import IosSwitch from "../../../UI/Buttons/IOSSwitch";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import AttachMoney from "@material-ui/icons/AttachMoney";

const styles = (theme) => ({
  root: {},
  flex: {
    display: "flex",
    justifyContent: "space-between",
    flex: 1,
  },
  formControl: {
    marginTop: theme.spacing(2),
    display: "flex",
    justifyContent: "space-between",
  },
  label: theme.Label.primary,
  numeric: {
    width: "130px",
  },
  mg2: {
    marginTop: theme.spacing(2),
  },
  flexAround: {
    display: "flex",
  },
  cardTitle: {
    fontSize: 16,
  },
  competitorslabel: {
    marginLeft: 0,
    "& > span:nth-child(2)": {
      color: "#607d8b",
      fontWeight: 900,
      fontSize: "1.1rem",
    },
  },
  competitorsContainer: {
    marginTop: 40,
  },
  competitors: {
    marginLeft: 20,
    transition: "all .2s ease-in",
  },
  hide: {
    opacity: 0,
    tranform: "translateY(30px)",
  },
  ebayItemId: {
    // margin: 0
  },
  competitorsValue: {
    width: 150,
  },
});

const fields = [
  {
    label: "Paypal Fees",
    name: "paypalFeesPers",
  },
  {
    label: "Ebay Fees",
    name: "ebayFeesPers",
  },
  {
    label: "Fees",
    name: "feesPers",
  },
  {
    label: "Fixed Cost",
    name: "fixedCost",
  },
  {
    label: "Profit",
    name: "profitPers",
  },
  {
    label: "Fixed Profit",
    name: "fixedProfit",
  },
];

const NumberFormatTextField = (props) => {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      thousandSeparator
      isNumericString
      prefix="$"
    />
  );
};

class ProfitCalculator extends Component {
  state = {
    paypalFeesPers: 0,
    ebayFeesPers: 0,
    feesPers: 0,
    fixedProfit: 0,
    fixedCost: 0,
    sourceTax: 0,
    profitPers: 0,
    netProfit: 0,
    sellPrice: 0,
    breakEven: 0,
    competitors: true, // for ui switch
  };

  componentWillMount() {
    const {
      paypalFeesPers,
      ebayFeesPers,
      feesPers,
      fixedProfit,
      fixedCost,
      sourceTax,
      profitPers,
    } = this.props;

    this.setState(
      {
        paypalFeesPers,
        ebayFeesPers,
        feesPers,
        profitPers,
        fixedProfit,
        fixedCost,
        sourceTax,
      },
      () => this.CalculateProfit()
    );
  }

  componentDidUpdate = (prevProps, prevState) => {
    const {
      paypalFeesPers,
      ebayFeesPers,
      feesPers,
      fixedProfit,
      fixedCost,
      sourceTax,
      profitPers,
    } = this.props;

    if (prevProps.originPrice !== this.props.originPrice) {
      this.CalculateProfit();
    }

    if (
      prevProps.paypalFeesPers !== paypalFeesPers ||
      prevProps.ebayFeesPers !== ebayFeesPers ||
      prevProps.feesPers !== feesPers ||
      prevProps.fixedProfit !== fixedProfit ||
      prevProps.fixedCost !== fixedCost ||
      prevProps.sourceTax !== sourceTax ||
      prevProps.profitPers !== profitPers
    ) {
      this.setState(
        {
          paypalFeesPers,
          ebayFeesPers,
          feesPers: feesPers, // other fees
          profitPers: profitPers,
          fixedProfit,
          fixedCost,
          sourceTax,
        },
        () => {
          const {
            paypalFeesPers,
            ebayFeesPers,
            feesPers,
            profitPers,
            fixedProfit,
            fixedCost,
            sourceTax,
          } = this.state;

          this.props.sendInitValues({
            paypalFeesPers,
            ebayFeesPers,
            feesPers,
            profitPers,
            fixedCost,
            fixedProfit,
            sourceTax,
          });
        }
      );
    }

    if (
      prevState.paypalFeesPers !== this.state.paypalFeesPers ||
      prevState.ebayFeesPers !== this.state.ebayFeesPers ||
      prevState.feesPers !== this.state.feesPers ||
      prevState.fixedProfit !== this.state.fixedProfit ||
      prevState.fixedCost !== this.state.fixedCost ||
      prevState.sourceTax !== this.state.sourceTax ||
      prevState.profitPers !== this.state.profitPers
    ) {
      this.CalculateProfit();
    }
  };

  handleChange = (name) => ({ target: { value } }) => {
    if (value > 0) {
      this.setState({
        [name]: value,
      });
      this.props.sendValueToStore({ name, value });
    }
  };

  handleArrowsChange = (name) => (value) => {
    this.setState({
      [name]: value,
    });
    this.props.sendValueToStore({ name, value });
  };

  // TODO: combine the methods beneath
  handleNetProfitChange = (name) => ({ target: { value } }) => {
    if (value > 0) {
      this.props.sendValueToStore({ name, value });
      this.setState(
        {
          [name]: value,
        },
        () => {
          this.CalculateProfitFromNetProfit();
        }
      );
    }
  };

  handleSellPriceChange = (name) => ({ target: { value } }) => {
    if (value > 0) {
      this.props.sendValueToStore({ name, value });
      this.setState(
        {
          [name]: value,
        },
        () => {
          this.CalculateProfitFromSellPrice();
        }
      );
    }
  };

  CalculateProfit = () => {
    const { originPrice } = this.props;
    const originPriceF = parseFloat(originPrice);
    const { sourceTax } = this.state;

    const {
      paypalFeesPers,
      ebayFeesPers,
      feesPers,
      fixedCost,
      profitPers,
      fixedProfit,
    } = this.getNumericalValues(
      "paypalFeesPers",
      "ebayFeesPers",
      "feesPers",
      "fixedCost",
      "profitPers",
      "fixedProfit"
    );

    const breakEven =
      paypalFeesPers / 100 + ebayFeesPers / 100 + feesPers / 100;

    let sellPrice =
      (originPriceF +
        fixedCost +
        fixedProfit +
        originPrice * (sourceTax / 100)) /
      (1 - breakEven - profitPers / 100);
    sellPrice = sellPrice.toFixed(2);

    let netProfit =
      sellPrice -
      (originPriceF +
        sellPrice * breakEven +
        fixedCost +
        originPriceF * (sourceTax / 100));
    netProfit = netProfit.toFixed(2);

    this.setState({
      sellPrice,
      netProfit,
      breakEven,
    });
    this.props.sendSellAndNetToStore({ sellPrice, netProfit });
  };

  CalculateProfitFromNetProfit = () => {
    const { netProfit, fixedCost, fixedProfit } = this.getNumericalValues(
      "netProfit",
      "fixedCost",
      "fixedProfit"
    );
    const { originPrice } = this.props;
    const sourceTaxPer = this.state.sourceTax / 100;
    const { breakEven } = this.state;

    const fp =
      netProfit +
      (originPrice +
        ((originPrice + fixedCost + netProfit + originPrice * sourceTaxPer) /
          (1 - breakEven)) *
          breakEven +
        fixedCost +
        originPrice * sourceTaxPer);

    const profit = -(
      (originPrice +
        fp * breakEven +
        fixedCost +
        fixedProfit +
        originPrice * sourceTaxPer -
        fp) /
      fp
    );

    const profitPers = (profit * 100).toFixed(2);

    this.setState({
      profitPers,
    });
    this.props.sendValueToStore({ name: "profitPers", value: profitPers });
  };

  CalculateProfitFromSellPrice = () => {
    const { sellPrice, fixedCost, fixedProfit } = this.getNumericalValues(
      "sellPrice",
      "fixedCost",
      "fixedProfit"
    );
    const { originPrice } = this.props;
    const sourceTaxPer = this.state.sourceTax;
    const { breakEven } = this.state;

    const profit = -(
      (originPrice +
        sellPrice * breakEven +
        fixedCost +
        fixedProfit +
        originPrice * (sourceTaxPer / 100) -
        sellPrice) /
      sellPrice
    );
    const profitPers = (profit * 100).toFixed(2);

    this.setState({
      profitPers,
    });
    this.props.sendValueToStore({ name: "profitPers", value: profitPers });
  };

  // ** helpers **
  getNumericalValues = (...values) => {
    const numerics = {};
    for (let value of values) {
      numerics[`${value}`] = parseFloat(this.state[value]);
    }

    return numerics;
  };

  handleCompetitors = () => {
    this.setState((prevState) => ({
      competitors: !prevState.competitors,
    }));
  };

  render() {
    const { classes, originPrice } = this.props;

    return (
      <section id="pricing">
        {/**** BUY PRICE */}
        <div className={clsx(classes.flex, classes.formControl)}>
          <label htmlFor="buy-price" className={classes.label}>
            Buy Price
          </label>
          <TextField
            style={{ fontWeight: 600, color: "#222" }}
            className={clsx(classes.textField, classes.numeric)}
            value={originPrice}
            margin="dense"
            onChange={this.handleChange("buyPrice")}
            id="buy-price"
            InputProps={{
              inputComponent: NumberFormatTextField,
            }}
          />
        </div>
        {/* FIRST ROW RIGHT */}
        <Divider variant="middle" className={classes.mg2} />

        <Grid
          container
          className={classes.mg2}
          spacing={5}
          justify="space-between"
          direction="row"
        >
          <Grid item xs={6}>
            <div className={classes.flexAround}>
              {/* FIRST COLUMN */}
              <div className={classes.column}>
                {fields.slice(0, 3).map(({ label, name }) => (
                  <InputNumeric
                    key={name}
                    label={label}
                    handleChange={(nam) => this.handleChange(nam)}
                    handleArrowsChange={(value) =>
                      this.handleArrowsChange(name)(value)
                    }
                    name={name}
                    value={this.state[name]}
                  />
                ))}
              </div>

              {/* SECOND COLUMN */}
              <div className={classes.column}>
                {fields.slice(3, 7).map(({ label, name }) => (
                  <InputNumeric
                    key={name}
                    label={label}
                    handleChange={(value) => this.handleChange(value)}
                    handleArrowsChange={(value) =>
                      this.handleArrowsChange(name)(value)
                    }
                    name={name}
                    notPercent={name === "fixedCost" || name === "fixedProfit"}
                    value={this.state[name]} // props.paypalFees for example to take the value from the parent
                  />
                ))}
              </div>
            </div>
          </Grid>

          {/* RESULTS */}
          <Grid item xs={6}>
            <Card className={classes.card}>
              <CardContent>
                <Typography className={classes.cardTitle} color="textSecondary">
                  Output
                </Typography>
                <div style={{ maxWidth: "90%", margin: "0 auto" }}>
                  <RegularInput
                    labelValue="Sell Price"
                    textFieldClass={classes.bold}
                    handleChange={this.handleSellPriceChange("sellPrice")}
                    value={this.state.sellPrice}
                    name="sellPrice"
                    width="100px"
                    withLabel
                  />
                  <RegularInput
                    labelValue="Net Profit"
                    handleChange={this.handleNetProfitChange("netProfit")}
                    value={this.state.netProfit}
                    name="netProfit"
                    width="100px"
                    withLabel
                  />
                  <RegularInput
                    labelValue="Desired qty"
                    inputProps={{
                      type: "number",
                    }}
                    handleChange={this.props.handleChange("desiredItemQty")}
                    value={this.props.desiredItemQty}
                    name="desiredQty"
                    width="100px"
                    withLabel
                  />
                </div>
              </CardContent>
            </Card>
          </Grid>
        </Grid>

        <FormControl
          className={classes.competitorsContainer}
          component="fieldset"
        >
          <FormGroup aria-label="position" row>
            <FormControlLabel
              className={classes.competitorslabel}
              value="start"
              control={
                <IosSwitch
                  checked={this.props.ebayCompetitors}
                  name="ebayCompetitors"
                  onChange={this.props.handleEbayCompetitors}
                />
              }
              label="Ebay Competitors"
              labelPlacement="start"
            />
            <div
              className={clsx(
                classes.competitors,
                !this.props.ebayCompetitors ? classes.hide : ""
              )}
            >
              <TextField
                className={classes.competitorsValue}
                label="Price Difference"
                value={this.props.competitorsPrice}
                onChange={this.props.handleEbayCompetitorsValues('competitorsPriceDifference')}
                inputProps={{
                  type: "number",
                  step: 0.1,
                }}
                InputProps={{
                  startAdornment: <AttachMoney />,
                }}
              />
              <TextField
                className={classes.ebayItemId}
                value={this.props.ebayItemId || ''}
                onChange={this.props.handleEbayCompetitorsValues('ebayItemId')}
                label="Ebay Item Id"
              />
              {/* </div> */}
            </div>
          </FormGroup>
        </FormControl>
      </section>
    );
  }
}

NumberFormatTextField.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

const styledAndConnected = compose(
  withStyles(styles),
  connect(null, { sendInitValues, sendValueToStore, sendSellAndNetToStore })
);

export default styledAndConnected(ProfitCalculator);
