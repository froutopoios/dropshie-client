import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles({
  marginTop: {
    marginTop: 30
  },
  inputContainer: {
    display: 'flex',
    flexDirection: 'column'
  },
  formControl: {
    marginTop: 20
  },
  select: {
    width: 300,
    // color: 'black'
  },
  input: {
    marginTop: 20,
    width: 300
  }
})

const listingDurationValues = [
  '1 Day',
  '3 Days',
  '5 Days',
  '7 Days',
  '10 Days',
  '14 Days',
  '21 Days',
  '30 Days',
  '60 Days',
  '90 Days',
  '120 Days',
]

const AccountBasedSelections = props => {
  const [listingDuration, setListingDuration] = useState(null);
  const [itemConditionValue, setitemConditionValue] = useState(null);

  React.useEffect(() => {
    setListingDuration(props.listingDuration)
  }, [])

  const { itemCondition } = props;

  React.useEffect(() => {
    setitemConditionValue(itemConditionValue)
  }, [itemCondition])

  const handleitemCondition = itemCondition => {
    return parseInt(itemCondition) === 1000 ? 'New' : 'Used'
  }

  const classes = useStyles()
  return (
    <div className={classes.marginTop}>
      <Typography variant='h4'>Account Based Selections</Typography>
      <div className={classes.inputContainer}>
        <FormControl className={classes.formControl} variant='outlined'>
          <InputLabel
            htmlFor="list-duration"
          >List Duration</InputLabel>
          <Select
            id='list-duration'
            className={classes.select}
            native
            margin='dense'
            name='listingDuration'
            label='List Duration'
            value={listingDuration}
            onChange={props.handleChange('listingDuration')}
          >
          <option value={props.defaultListingDuration}>{props.defaultListingDuration || 'loading'}</option>
          {listingDurationValues.map(duration => <option key={duration} value={duration}>{duration}</option>)}
          </Select>
        </FormControl>

        <FormControl className={classes.formControl} variant='outlined'>
          <InputLabel
            htmlFor="Item Condition"
          >Item Condition</InputLabel>
          <Select
            id='item-Condition'
            className={classes.select}
            native
            margin='dense'
            name='itemCondition'
            label='Item Condition'
            value={itemConditionValue}
            onChange={props.handleChange('ItemCondition')}
          >
          <option value={props.defaultItemCondition}>{handleitemCondition(props.defaultItemCondition) || 'loading...'}</option>
          <option value={0}>Used</option>
          </Select>
        </FormControl>

        <FormControl className={classes.formControl} variant='outlined'>
          <InputLabel
            htmlFor="allow-buyers-to-remain-anonymous"
          >Allow Buyers To Remain Anonymous</InputLabel>
          <Select
            id='allow-buyers-to-remain-anonymous'
            className={classes.select}
            native
            margin='dense'
            name='AllowBuyersToRemainAnonymous'
            label='Allow Buyers To Remain Anonymous'
            value={props.allowByersToRemainAnonymous}
            onChange={props.handleChange('allowByersToRemainAnonymous')}
          >
          <option value={1}>True</option>
          <option value={2}>False</option>)
          </Select>
        </FormControl>

        <TextField
          InputLabelProps={{
            shrink: true
          }}
          className={classes.input}
          label='Item Location'
          variant='outlined'
          margin='dense'
          value={props.defaultItemLocation}
          onChange={props.handleChange('defaultItemLocation')}
        />

      </div>
    </div>
  )
}

export default AccountBasedSelections;