import React, { useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    border: '1px dashed black',
    borderRadius: '10px',
    height: '70px',
    padding: '5px',
    marginBottom: '20px'
  }
})

const DropZone = props => {
  const onDrop = useCallback(file => {

    props.handleDropZone(file); 
  }, [props])

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop })
  const { classes } = props;


  return (
    <div { ...getRootProps() } className={ classes.root }>
      <input { ...getInputProps() }/>
      {
        isDragActive ?
          <p>Drop the files here ...</p> :
          <p>Drag 'n' drop some files here, or click to select file</p>
      }
    </div>
  )
}

export default withStyles(styles)(DropZone);


