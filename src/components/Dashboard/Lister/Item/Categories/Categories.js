import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { usePrevious } from '../../../../../hooks/usePrevious';
import cherryPick from '../../../../../utils/cherryPick';
import isEqual from 'lodash/isEqual';
import { withStyles } from '@material-ui/styles';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';

import Paper from '@material-ui/core/Paper'


const styles = {
  root: {
    marginTop: '50px',
    width: '100%',
    height: '250px',
    overflow: 'scroll',
    overflowX: 'hidden'
  },
  paper: {
    marginTop: '6px',
    padding: '6px 10px',
    display: 'inline-block',
  },
  button: {
    cursor: 'pointer',
    color: 'black',
    backgroundColor: '#ddd',
    '&:active': {
      dropShadow: 'none !important',
      userSelect: 'none'
    },
  }
}

const Categories = React.memo(props => {
  const { categories, classes } = props;
  const [categoriesList, setCategoriesList] = useState([]);
  const preValues = usePrevious(categories);
  // console.log(categories);

  useEffect(() => {
    if (!isEqual(preValues, categories)) {
      const categoryList = cherryPick(categories)(({ primaryCategory, parentCategories }) => ({ primaryCategory, parentCategories }))

      setCategoriesList(categoryList);
      // console.log('effect triggered')
    }
  }, [preValues, categories])
  
  const handleHover = value => e => {
    const id = e.target.id;
    
    if (id) {
      let selectedItem = { ...categoriesList[e.target.id] }
      const copyOfCategories = JSON.parse(JSON.stringify(categoriesList));
      let elevation;

      if (value === 'in' || value === 'up') {
        elevation = 3
      } else if ( value === 'down' ) {
        elevation = 0
      } else {
        elevation = 1
      }

      selectedItem = {
        ...selectedItem,
        elevation
      }

      copyOfCategories[id] = selectedItem
      setCategoriesList(copyOfCategories)      
    } 
  }



  const renderTitle = (element, elevation, index, handleSelected) => {
    return (
      <Paper id={ index }
             boing={ element }
             value={element}
             className={ clsx(classes.paper, `${ index || index === 0 ? classes.button : null  }`) } 
             elevation={ elevation } 
             onMouseOver={ handleHover('in') } 
             onMouseOut={ handleHover('out') } 
             onMouseDown={ index ? handleHover('down') : null } 
             onMouseUp={ handleHover('up') }
             onClick={ handleSelected }
             >
        { element }
      </Paper>
    )
  }
  // console.log(categoriesList);

  const forth = categoriesList.map((obj, index) => {
    return (
      <div key={ index }>
        { obj.parentCategories.map((parent, i) => {
          return (
            <div key={ i }>{ i !== 0 ? 
              (
              <div style={{ marginLeft: `${i * 25}px` }}>
                <FontAwesomeIcon
                  style={{ marginRight: '8px' }}
                  icon={ faAngleDown }
                  size='1x'
                />
                { renderTitle(parent.title) }
              </div>
             ) : 
             renderTitle(parent.title) }</div>
          )
        }) }
        <div style={{ marginLeft: `${ index * 20 }px` }}>{ renderTitle(obj.primaryCategory.title, obj.elevation, index, props.handleSelected) }</div>
        {/* <div>-------------</div> */}
      </div>
    )
  })

  
  return (
    <div className={ classes.root }>
        { forth }
    </div>
  )
})

export default withStyles(styles)(Categories);