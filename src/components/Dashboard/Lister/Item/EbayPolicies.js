import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import NativeSelect from '@material-ui/core/NativeSelect';
import Typography from '@material-ui/core/Typography'
import PolicySelectBox from '../../Settings/EbayUser/PolicySelectBox';


const useStyles = makeStyles(theme => ({
  formControl: theme.FormControl.left,
  label: theme.Label.primary,
  margin: {
    marginTop: '30px'
  }
}))



const EbayPolicies = React.memo(props => {
  const classes = useStyles();
  const { shippingPolicies, 
          returnPolicies, 
          paymentPolicies,
          selectedShippingPolicy,
          selectedReturnPolicy,
          selectedPaymentPolicy } = props;

  // TODO: put to separate component. map the 3 diffent policies to one component

  return (
    <div className={ classes.margin }>
      <Typography variant='h4'>Ebay Policies</Typography>
    {/* SHipping Policy */}
      <div className={ classes.formControl }>
        <label htmlFor="shippingPolicy" className={ classes.label }>Shiping Policy</label>
        <NativeSelect 
          value={selectedShippingPolicy}
          onChange={props.handleChange('selectedShippingPolicy')}
          inputProps={{
            name: 'policy',
            id: 'policy'
          }}>
          {shippingPolicies ? shippingPolicies.map(policy => {
            return <option key={policy.ProfileId} value={policy.ProfileId}>{policy.ProfileName}</option>
          }) : <option>Loading</option>}
        </NativeSelect>
      </div>

    {/* Return Policy */}
      <div className={ classes.formControl }>
        <label htmlFor="returnPolicy" className={ classes.label }>Return Policy</label>
        <NativeSelect 
          value={selectedReturnPolicy}
          onChange={props.handleChange('selectedReturnPolicy')}
          inputProps={{
            name: 'returnPolicy',
            id: 'returnPolicy'
          }}>
          {returnPolicies ? returnPolicies.map(policy => {
            return <option key={policy.ProfileId} value={policy.ProfileId}>{policy.ProfileName}</option>
          }) : <option>Loading</option>}
        </NativeSelect>
      </div>

    {/* Payment Policy */}
      <div className={ classes.formControl }>
        <label htmlFor="Payment Policy" className={ classes.label }>Payment Policy</label>
        <NativeSelect 
          value={selectedPaymentPolicy}
          onChange={props.handleChange('selectedPaymentPolicy')}
          inputProps={{
            name: 'paymentPolicy',
            id: 'paymentPolicy'
          }}>
          {paymentPolicies ? paymentPolicies.map(policy => (
            <option key={policy.ProfileId} value={policy.ProfileId}>{policy.ProfileName}</option>
          )) : <option>Loading</option>}
        </NativeSelect>
      </div>

    </div>
  )
})

export default EbayPolicies;