import React, { useState, useRef, useEffect } from 'react';
import isEqual from 'lodash/isEqual';
import { usePrevious } from '../../../../hooks/usePrevious';

import Chip from '@material-ui/core/Chip';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel'
import AddIcon from '@material-ui/icons/Add';

import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import Tooltip from '../../../UI/Notifications/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Grow from '@material-ui/core/Grow';

const styles = theme => ({
  root: {
    display: 'flex',
    // justifyContent: 'space-around',
    padding: '7px 5px'
  },
  delete: {
    // alignSelf: 'center',
    marginRight: 20
  },
  label: {
    marginRight: '1rem',
    flex: '0 1 250px'
  },
  chips: {
    flex: '1 1 250px'
  },
  addContainer: {
    flex: '1 1 200px'
  }, 
  input: {
    width: '200px',
    marginLeft: '10px'
  },
  chip: {
    maxWidth: '200px',
    marginRight: '3px',
    wordBreak: 'break-all',
    '& span': {
      wordBreak: 'break-all'
    }
  },
  chipLabel: {
    wordBreak: 'break-all',
    overflowWrap: 'break-all',
    color: '#555',
  },
  add: {
    '&:hover': {
      color: theme.palette.primary.main,
      cursor: 'pointer'
    }
  },
  error: {
    color: 'tomato',
    fontSize: '.7rem'
  }
})

const Details = props => {
  const [chips, setChips] = useState(['duck', 'bob', 'mike']);
  const [label, setLabel] = useState('Does not Apply')
  const [invalid, setInvalid] = useState(false);
  const [ feature, setFeature ] = useState('')
  const { classes } = props;

  const textInput = useRef(null);
  // const { Title, SpecificValues } = props.detail;

  const copyOfdetail = JSON.parse(JSON.stringify(props.detail));
  const { Title, SpecificValues: specifics } = copyOfdetail;
  const arrOfSpecifics = specifics.map(val => val['Value'])
  const prevValues = usePrevious(arrOfSpecifics);

  useEffect(() => {
    setLabel(Title)
  }, [Title])

  useEffect(() => {
    if ( !isEqual(prevValues, arrOfSpecifics)) {
      // console.log('USE EFFECT triggered --Details.js')
      setChips(arrOfSpecifics)
    }
  }, [prevValues, arrOfSpecifics])

  const addNew = (feature) => () => {
    if (!invalid && feature.length !== 0) {
      props.addFeature(feature)();
      textInput.current.value = ''
    }
  }

  const handleChange = e => {
    let invalid = chips.includes(e.target.value);
    invalid ? setInvalid(true) : setInvalid(false)

    setFeature(e.target.value);
  }

  const handlePress = e => {
    if (e.key === 'Enter') {
      addNew(feature)();
    }
  }

  const renderChips = (chips) => {
    return chips.map(chip => {
      return (
            <Tooltip placement='right-start'
                     title={ chip } 
                     key={ chip } 
                     arrow={true}>
              <Chip
                //  size='small'
                key={ chip }
                classes={{
                  label: classes.chipLabel
                }}
                className={ classes.chip }
                variant='outlined'
                label={ chip }
                onDelete={ props.handleDelete(chip) }
                />
            </Tooltip>
              )
    })
  }
  return (
    <div className={ classes.root }>
          {/* {props.edit && */}
          <Grow 
            in={props.edit}
            {...(props.edit ? { timeout: 200 /  (1 /(props.id + 1)) } : {})}
            >
            <IconButton 
              className={classes.delete}
              onClick={props.handleDeleteFeature}
              size='small'
              >
              <DeleteIcon color='secondary'/>
            </IconButton>
          </Grow>
          <Typography
            className={classes.label}
            variant='overline'
            >{ label }</Typography>
          <div className={ classes.chips }>
            { renderChips(chips) }
          </div>
          <div className={ classes.addContainer }>
            { invalid ? <InputLabel htmlFor={ `${props.id}` } className={ classes.error }>Allready Declared</InputLabel> : null }
            <Input
              error={invalid ? true : false}
              id={ `${props.id}` }
              onKeyPress={ handlePress }
              inputRef={ textInput }
              color='primary'
              placeholder='Add new property'
              className={ classes.input }
              onChange={ handleChange }
              endAdornment={
                <AddIcon 
                  className={ classes.add }
                  onClick={ props.addFeature(feature) }
                  />
              }
            />
          </div>
    </div>
  )
}

export default  withStyles(styles)(Details);