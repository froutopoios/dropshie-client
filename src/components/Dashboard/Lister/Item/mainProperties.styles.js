export const styles = theme => ({
  paper: {
    // width: '90%',
    margin: '0 auto',
    // padding: theme.spacing(5, 10)
  },
  header: {
    color: theme.palette.text.primary,
    width: '400px',
    fontWeight: '400'
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: theme.spacing(2)
  },
  // TODO: remove this style the fields inside a container, this was used when a label was in place along with the input
  formControl: {
    marginTop: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-between',
    // border: '1px solid #333'
  },
  focusedTextField: {
    '& legend': {
      width: '40px !important'
    },
  },
  input: {
    marginBottom: '10px'
  },
  dense: {
    // marginTop: theme.spacing(2)
  },
  label: theme.Label.primary,
  labelSecondary: {
    fontSize: '0.9rem'
  },
  flex: {
    display: 'flex',
    justifyContent: 'space-between',
    flex: 1
  },
  button: {
    '&:hover': {
      boxShadow: 'none'
    }
  },
  mg2: {
    marginTop: theme.spacing(2)
  },
  numeric: {
    width: '130px'
  },
  test: {
    color: theme.palette.primary.main,
    border: '2px solid green'
  },
  testLabel: theme.Label,
  bold: {
    fontWeight: 900,
    '& input': {
      color: '#444'
    }
  },
  btn: {
    height: '30px',
    width: '50px',
    '& button:first-child': {
      backgroundColor: 'green',
      color: 'blue !important'
    }
  },
  marginTop: {
    marginTop: theme.marginTop.medium
  },
  edit: theme.iconBtn,
  btnColor: theme.palette.primary.main,
  expansion: {
    display: 'flex'
  },
  title: {
    // maxWidth: '500px',
    flex: '1 1 auto'
  },
  spacer: {
    flex: '1 1 100%'
  },
  expansionPanel: {
    marginTop: '5px',
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
    // border: 'none'
  },
  summary: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  border: {
    padding: '5px 10px',
    display: 'inline-block',
    border: '1px solid #ddd',
    borderRadius: '5px'
  },
  borderBottom: {
    padding: '0 5px 0 0',
    display: 'inline-block',
    borderBottom: `1px solid ${ theme.palette.primary.main }` 
  },
  card: {
    marginLeft: 'auto',
    minWidth: '280px',
    maxWidth: '400px',
    padding: theme.spacing(3)
  },
  cardTitle: {
    fontSize: 16
  },
  flexAround: {
    display: 'flex'
  }
})