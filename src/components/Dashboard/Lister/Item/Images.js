import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import createRandomId from '../../../../utils/createRandomId';
import copyObj from '../../../../utils/copyObj';


import { connect } from 'react-redux';
import { uploadImage } from '../../../../actions/listingActions';

import DropZone from './DropZone';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUpload } from '@fortawesome/free-solid-svg-icons';

import Typography from '@material-ui/core/Typography';

import Paper from '@material-ui/core/Paper';
import Radio from '@material-ui/core/Radio';
import Checkbox from '@material-ui/core/Checkbox'

import Button from '@material-ui/core/Button';
import  Fab  from '@material-ui/core/Fab';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';



// TODO: renders three times
const styles = theme => ({
  root: {
    width: '90%',
    margin: '0 auto',
    // border: '1px solid black',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  imageContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper
  },
  paper: {
    minWidth: 275,
    width: '100px',
    margin: '10px 20px'
    // width: '400px'
  },
  marginTop: {
    marginTop: theme.marginTop.large
  },
  radio: {
    color: theme.palette.primary.main,
    '&$checked': {
      color: theme.palette.primary.dark
    }
  },
  checkbox: {
    color: theme.palette.primary.main,
    '&$checked': {
      color: theme.palette.primary.dark
    },
  },
  checked: {},
  image: {
    width: '100%'
  },
  fileInput: {
    display: 'none',
  },
  banner: {
    width: '500px'
  },
  lightBlue: {
    color: theme.palette.lightBlue,
    fontWeight: 'bold',
    fontSize: '38px'
  },
  card: {
    maxWidth: 345,
    padding: '10px'
  },
  uploadBtn: {
    marginLeft: '7px',
    paddingBottom: '2px' // changes the size of the icon (casuse of borderBox?)
  },
  hidden: {
    opacity: 0
  }
})

class Images extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      uploadedImage: null, // TODO: add the whole array,
      imageUrl: null,
      index: 0,
      // position: '0% 0%',
      imageState: {
        zoom: false,
        index: null,
        position: '0% 0%'
      }
    })
  }

  componentDidUpdate(prevProps) {
    // Change the value of the first image to "isMain"
    if (prevProps.images !== this.props.images && prevProps.images.length === 0) {

      // Attach random id to images
      const copyOfImages = copyObj(this.props.images);
      copyOfImages.forEach(image => {
        // console.log(image);
        image.ImageId = createRandomId();
        image.IsSelected = true;
      })

      // set the First image "IsMain"
      copyOfImages[0].IsMain = true;

      this.props.handleImageArray(copyOfImages);
    }
  }
  
  fileUploadHandler = () => {
    console.log('upload')
    this.props.uploadImage(this.state.uploadedImage)
  }

  handleDropZone = files => {
    console.log(files);
    const file = files[0];
    const imageUrl = URL.createObjectURL(file);

    this.setState({ uploadedImage: file, imageUrl })
  }

  cancelUpload = () => {
    this.setState({ uploadedImage: null, imageUrl: null })
  }

  // TODO: after the response have to add the uploaded image to the arrray of images
  // TODO: combine the methods 
  handleMainChange = index => () => {    
    const images = copyObj(this.props.images);
    images.forEach(image => image.IsMain = false);
    images[index].IsMain = true;

    this.props.handleImageArray(images);

    console.log(index, "re")
    this.setState({ index });
  }

  handleIsSelected = index => () => {
    const images = JSON.parse(JSON.stringify(this.props.images));
    images[index].IsSelected = !this.props.images[index].IsSelected;
   
    this.props.handleImageArray(images);
  }

  handleMouseMove = index => e => {
    const { left, top, width, height } = e.target.getBoundingClientRect();
    const x = (e.pageX - left) / width * 100;
    const y = (e.pageY - top) / height * 100;

    this.setState({
      imageState: { 
        position: `${x}% ${y}%`, 
        zoom: true,
        index 
      }
    })
  }

  handleMouseOut = () => {
    this.setState({
      imageState: {
        zoom: false,
      }
    })
  }
  
  render() {
    const { classes, images } = this.props;

    // console.log(images);
    // console.log("-----------", images.length !== 0);

    return (
      <section id='images'>
        <Typography className={ classes.marginTop } variant='h4'>Images</Typography>
        <div className={ classes.root }>
         { images.map((image, index) => (
           <Paper key={ index } 
                  className={ classes.paper }
                  elevation={ 1 }>
             <Radio checked={ this.state.index === index }
                    onChange={ this.handleMainChange(index) }
                    className={ classes.radio }
                    color='default'
             />
             <Checkbox className={ classes.checkbox }
                       checked={ image.IsSelected }
                       color='default'
                       onChange={ this.handleIsSelected(index) }
             />
             <figure 
              onMouseMove={ this.handleMouseMove(index) } 
              onMouseOut={ this.handleMouseOut } 
              style={{ backgroundImage: `url(${ image.url })`, backgroundPosition: this.state.imageState.index === index && this.state.imageState.position }}>
                <img 
                  className={ clsx(classes.image, this.state.imageState.zoom && this.state.imageState.index === index ? classes.hidden : null) }
                  src={ image.url }
                  alt='product' />
             </figure>
           </Paper>
         ))}
        </div>

        {/* UPLOADER */}
        <input type="file"
               className={ classes.fileInput }
               onChange={ this.fileSelectedHandler }
               ref={ this.fileUploadRef }/>

 
        <Card className={ classes.card }> 
          <CardHeader title='Upload Image'/>
          <DropZone handleDropZone={ this.handleDropZone }/>
          <CardActionArea>
            <CardMedia className={ classes.media }
                       component='img'
                       src={ this.state.imageUrl || 'image' }/>
          </CardActionArea>
          <CardActions style={{ display: 'flex', justifyContent: 'space-between' }}>
            <Button color='secondary' 
                    onClick={ this.cancelUpload }
                    className={ !this.state.uploadedImage ? classes.hidden : null }>cancel</Button>
            <Fab variant='extended'
                 color='primary'
                 size='small'
                 disabled={ !this.state.uploadedImage }
                 onClick={ this.fileUploadHandler }>
                Upload
              <FontAwesomeIcon icon={ faUpload } 
                               size='1x'
                               className={ classes.uploadBtn }/>
            </Fab>
          </CardActions>
        </Card>

        <div className={ classes.lightBlue }>hello</div>
      </section>
    )
  }
}

// try whith functional component otherwise modify the object inside the action
// functional component is better approatch to the problem 

const styledImages = withStyles(styles)(Images);
export default connect(null, { uploadImage })(styledImages);
