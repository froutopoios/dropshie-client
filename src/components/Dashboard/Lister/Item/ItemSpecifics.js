import React, { useState, useRef } from 'react';
import clsx from 'clsx';
import Input from '../../../UI/Inputs/RegularInput';
import InputBare from '@material-ui/core/Input'
import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add'
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import Details from './Details';
import EditIcon from '@material-ui/icons/Edit';

const styles = theme => ({
  margin: {
    marginTop: theme.marginTop.large
  },
  flex: {
    marginTop: theme.spacing(6),
    display: 'flex',
    // flexDirection: 'column'
  },
  paper: {
    position: 'relative',
    marginTop: theme.marginTop.medium,
    padding: '50px 20px',
  },
  addFab: {
    position: 'absolute',
    right: '20px',
    bottom: '20px',
    color: 'white'
  },
  editFab: {
    right: '70px',
    backgroundColor: 'lightGrey'
  },
  input: {
    opacity: '0',
    width: '200px',
    // marginLeft: '10px',
    color: theme.palette.primary.main,
    transform: 'translateX(-40px)',
    transition: theme.transitions.create(
      ['opacity', 'transform'],
      { duration: theme.transitions.duration.complex }
    )
  },
  show: {
    transform: 'translateX(0)',
    opacity: '1'  
  },
  add: {
    '&:hover': {
      color: theme.palette.primary.main,
      cursor: 'pointer'
    }
  }
})

const ItemSpecifics = React.memo(props => {
  const [specific, setSpecific] = useState({ Title: '', SpecificValues: []});
  const [showInput, setShowInput] = useState(false);
  const [edit, setEdit] = useState(false);
  const { classes, itemProperties, specifics } = props;
  const textInput = useRef(null);

  const renderInputs = (items, column) => {
    return Object.keys(items).map((key, i) => {
      if ((column === 1 && i < 3) || (column === 2 && i >= 3)) {
        return (
          <Input
                key={ i }
                width='300px'
                name={ key }
                label={ key.toUpperCase() }
                value={ items[key] }
                color={ items[key] === 'Does not apply' ? '#999' : undefined }
                handleChange={ () => console.log('oooppp') }/>
        )
      } else {
        return null
      }
    })
  }
  
  const renderDetails = (specifics) => {
    if(specifics.length === 0) {
      return <Typography variant='h5'>Loading Details</Typography>
    }
    return specifics.map((detail, id) => {
      return <Details key={ id } 
                      detail={ detail } 
                      id={ id }
                      edit={edit}
                      addFeature={(value) => props.addFeature(value, id)}
                      handleDelete={(value) => props.onDeleteSpec(value, id)}
                      handleDeleteFeature={() => props.handleDeleteFeature(id)}
                      /> 
    })
  }

  const handleAddSpecific = (speicfic) => () => {
    props.handleAddSpecific(specific);
    textInput.current.value = ""
  }

  const handlePress = e => {
    if (e.key === 'Enter') {
      handleAddSpecific(specific)();
    }
  }

  const handleEdit = () => {
    edit ? setEdit(false) : setEdit(true);
  }


  // Show the addNewSpecific
  const renderSpecific = () => {
    const handleChange = e => {
      setSpecific({
        SpecificValues: [],
        Title: e.target.value
      })
    }

    return (
      <InputBare
      inputRef={ textInput }
      color='primary'
      onKeyPress={ handlePress }
      placeholder='Add new specific'
      className={ clsx(classes.input, showInput ? classes.show : null ) }
      onChange={ handleChange }
      endAdornment={
        <AddIcon 
          className={ classes.add }
          onClick={ handleAddSpecific(specific) }
          />
        }
      />
    )
  }

  return (
    <section className={ classes.margin } id='specifics'>
      <Typography variant='h4'>Item Specifics</Typography>
      <div className={ classes.flex }>
        <div>
          { renderInputs(itemProperties, 1) }
        </div>
        <div>
          { renderInputs(itemProperties, 2) }
        </div>
      </div>
      <div className={ classes.margin }>
        <Typography variant='h5'>Details</Typography>
        <Paper className={ classes.paper }>
          { renderDetails(specifics) }
          <div style={{ marginTop: '20px' }}>
            { renderSpecific() }
          </div>
          <Fab
            className={clsx( classes.addFab, classes.editFab)}
            color='primary'
            size='small'
            component='button'
            onClick={ handleEdit }
          >
            <EditIcon fontSize='small'/>
          </Fab>
          <Fab
            className={ classes.addFab }
            color='primary'
            size='small'
            component='button'
            onClick={ () => setShowInput(true) }
          >
            <AddIcon/>
          </Fab>
        </Paper>
      </div>
    </section>
  )
})

export default withStyles(styles)(ItemSpecifics);