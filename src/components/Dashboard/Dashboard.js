import React, { Component, Fragment, lazy, Suspense } from 'react'
import { Route, Switch } from 'react-router-dom';

import InventorySubmit from './Lister/InventorySubmit';
import AddItem from './Lister/AddItem';

import { connect } from 'react-redux';
import { fetchInventory, credAction } from '../../actions';
import { setVerifyError, verify, stopVerifyError } from '../../actions/authActions'

import DrawerAndNavbar from '../SideBar/Sidebar';
import Overview from './Overview/Overview';
import Settings from './Settings';
import TemplateEditor from './TemplateEditor';
import Account from './Account';
import D3test from '../Sales/d3test';
import Snack from '../UI/Notifications/Snackv2'

import notFound from '../../assets/images/404-hello.png';

import Inventory from './Inventory/Inventory';
// const Inventory = lazy(() => import('../Dashboard/Inventory/Inventory'));


// TODO: move to seperate component
const style = {
  height: '100vh',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover'
}

const NotFound = props => (
  <div style={{...style, backgroundImage: `url('${ notFound }')` }}></div>
)

class Dashboard extends Component {
  componentDidMount() {
    if (!this.props.isSignedIn || !this.props.emailConfirmed) {
      this.props.setVerifyError()
      this.props.history.push('/login');
    } else {
      this.props.stopVerifyError()
    }

    // window.addEventListener("berforeunload", this.props.handleWidnowBeforeUnload);
  }

  componentDidUpdate(prevProps) {
    // TODO: if the plan is empty trigger an action to fetch again
    
    // console.log('INSDIE DASHBOROARD COMPONENT DID UPA')
    // if (!this.props.emailConfirmed) {
    //   this.props.setVerifyError()
    //   this.props.history.push('/login');
    // }
  }

  componentWillUnmount() {
    // window.removeEventListener("beforeunload", this.props.handleWidnowBeforeUnload)
  }
  
  render() {
    // console.log('inside dashboard')
    return (
      <Fragment>
        {/* { this.props.location.pathname !== '/dashboard/InventorySubmit' && */}
            {this.props.netError ? 
            (<Snack
              open={true}
              variant='error'
              message={this.props. netError}
            />) : 
            null}
            <DrawerAndNavbar>
              { this.props.location.pathname === '/dashboard' && <Overview /> }
                {/* <Suspense fallback={ <div>Loading...</div> }> */}
                  <Switch>
                    <Route path='/dashboard/overview' render={ () => <Overview /> }/>
                    <Route path='/dashboard/account' render={ () => <Account /> }/>
                    <Route path='/dashboard/editor' render={ () => <TemplateEditor/> }/>
                    <Route exact path='/dashboard/inventory' render={ () => {
                      return (
                        <div className='py2'>
                            <Inventory/>
                        </div>
                      )
                    }}/>
                    <Route path='/dashboard/settings' render={ () =>  {
                      return (
                        // <div style={{ height: '100%' }}>
                          <Settings />
                        // </div>
                      )}}/>
                    <Route path='/dashboard/additem' render={ () => {
                      return (
                        <AddItem close={ () => console.log('close') }/>
                        )}}/>

                      { /** LISTER  */ }
                    <Route path='/dashboard/InventorySubmit' render={ () => <InventorySubmit/> }/>
                    <Route path='/dashboard/Sales' render ={ () => <D3test/> }/>                    
                      { this.props.location.pathname !== '/dashboard' &&  
                    <Route component={ NotFound }/> }
                  </Switch>
                {/* </Suspense> */}
            </DrawerAndNavbar> 
        {/* } */}
      </Fragment>
    )
  }
}

const mapStateToProps = ({auth: { isSignedIn, emailConfirmed }, addItem: { fetchSuccess }, errors: { netError }}) => {
  return { isSignedIn, fetchSuccess, emailConfirmed, netError }
}

export default connect(mapStateToProps, { fetchInventory, credAction, setVerifyError, verify, stopVerifyError })(Dashboard);




