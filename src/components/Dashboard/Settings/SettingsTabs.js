import React, { useRef, useEffect, useState, forwardRef } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const useStyles = makeStyles(theme => ({
  root: {
    color: 'red',
    '&:hover': {
      color: '#333'
    }
  },
  tabs: {

  },
  tab: {
    width: '40px',
    minWidth: '40px',
    '&:hover': {
      color: 'red',
      opacity: 1
    }
  },
}))

const DropshieTabs = withStyles({
    root: {
      // fontFamily: '\'Montserrat\', sans-serif',
      // width: '800px'
    },
    indicator: {
      borderRadius: '10px 10px 0 0',
      height: '2px',
      backgroundColor: '#454545' 
    }
})(forwardRef((props, ref) => (
  <Tabs
    classes={ props.classes } 
    centered 
    ref={ref} 
    {...props} />
  )
))

const DropshieTab  = withStyles(theme => ({
  root: {
      fontSize: '1.2rem',
      fontWeight: 600,
      '&:active': {
        fontWeight: 'bold',
      },
      '&$selected': {
        color: '#333', 
        fontWeight: theme.typography.fontWeightBold
      },
      [theme.breakpoints.down('sm')]: {
        fontSize: '.8rem',
        minWidth: 70,
      },
      [theme.breakpoints.down('md')]: {
        fontSize: '.9rem',
        minWidth: 80
      },
      '&:hover': {
        color: '#3F3E3B'
      }
    },
  wrapper: {
    fontFamily: '\'Quicksand\', sans-serif',
  },
    selected: {},
  }))(props => <Tab  disableRipple {...props} />)
  

export default props => {
  const [update, setUpdate] = useState(false);
  const classes = useStyles();
  const tabsRef = useRef();
 
  const resizeObserver = new ResizeObserver(entries => setUpdate(entries[0].contentRect.width));
  const { tab } = props;

  useEffect(() => {
    const targetElement = tabsRef.current;

      resizeObserver.observe(targetElement);
    return () => {
      resizeObserver.unobserve(targetElement);
    }
  }, [])

  return (
    <Paper className={ classes.paper }>
      <DropshieTabs
        ref={ tabsRef }
        className={ classes.tabs }
        value={ props.value }
        onChange={ props.handleChange }
        textColor="primary"
        >
        <DropshieTab
            label='account'/>

        <DropshieTab label='ebay Accounts'/>
        <DropshieTab label='list'/>
        <DropshieTab label='scan'/>
        <DropshieTab label='extensions'/>
        <DropshieTab label='ui'/>
      </DropshieTabs>
  </Paper>
  )
}