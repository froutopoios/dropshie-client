import React from 'react'
import { makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';

import IOSSwitch from '../../UI/Buttons/IOSSwitch';

const useStyles = makeStyles(theme => ({
    root: {
      margin: theme.spacing(3)
    },
    title: {
      padding: theme.spacing(2)
    },
    container: {
      marginTop: theme.spacing(4),
      display: 'flex',
      justifyContent: 'flex-end'
    },
    leftItemn: {
      display: 'flex',
      justifyContent: 'flex-end',
      // width: '50%',
      marginRight: theme.spacing(2)
    },
    rightItem: {
      width: '600px',
      [theme.breakpoints.down('md')]: {
        width: '300px'
      },
    },
    flex: {
      display: 'flex',
      justifyContent: 'space-around'
    }
}))


const UiSettings = props => {
  const classes = useStyles();

  const handleChange = name => e => {
    props.keepSidebarCollapsed(e.target.checked)
  }

  return (
      <div className={ classes.root }>
        <Typography variant='h5' className={ classes.title }>Ui Settings</Typography>
        <Divider/>
        <div className={ classes.container }>
          <div className={ classes.leftItemn }>
            <FormControl component="fieldset">
              <FormGroup aria-label='position' row>
                <FormControlLabel
                  value='start'
                  control={ <IOSSwitch
                              checked={ props.collapse }
                              name='sideDrawerSwitch'
                              onChange={ handleChange('sidebar') }
                            /> }
                  label='Keep sidedrawer collapsed'
                  labelPlacement='start'
                />
              </FormGroup>
            </FormControl>
          </div>
          <div className={ classes.rightItem }>2</div>
        </div>
      </div>
  )
}

export default UiSettings;