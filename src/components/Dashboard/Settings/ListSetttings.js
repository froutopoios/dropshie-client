import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { validateStr } from '../../../utils';

import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGlobeAfrica, faAt } from '@fortawesome/free-solid-svg-icons';

import InputNumeric from '../../UI/Inputs/InputNumeric';


const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    padding: '30px'
  },
  paper: {
    maxWidth: '900px',
    display: 'flex',
    flexDirection: 'column',
    // padding: '50px 50px'
  },
  inputContainer: {
    // marginTop: '1.8rem',
    paddingLeft: '1.2rem',
    listStyleType: 'none',
  },
  title: {
    fontFamily: 'Modserrat, sans-serif',
    marginBottom: '30px'
  },
  submit: {
    alignSelf: 'flex-end',
    marginTop: '40px',
    width: '200px'
  },
  inputNumberic: {
    width: 400
  },
  affiliation: {
    width: 400
  },
  secondColumnPaper: {
    // paddingTop: '.5rem',
    // padding: '2.3rem 1.5rem',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 0
  },
  list: {
    listStyle: 'none'
  },
  formControl: {
    marginTop: '8px',
    marginBottom: '15px',
    width: '300px'
  },
  textField: {
    marginBottom: '15px'
  },
  marginTopFix: {
    marginTop: '8px'
  },
  paypalEmailAddress: {
    width: 300
  }
}))


const Profile = props => {
  const classes = useStyles();
  const [inputValues, setInputValues] = useState({})

  const { settings, ebayUsers, originMarketPlaces } = props;
  console.log('settings', settings);
  const inputLabel = React.useRef(null); 
  const { PaypalFeesPers, 
    EbayFeesPers, 
    BreakEvenPers,
    ProfitPers, 
    FixedCost, 
    FixedProfit, 
    SourceTax, 
    DefaultItemQty,
    PayPalEmailAddress,
    DefaultCountryId,
    DefaultTemplateId,
    DefaultEbayUserName,
    DefaultWebGateOriginMarketId,
    DefaultItemLocation } = settings;


  useEffect(() => {
      setInputValues({
      ...inputValues,
      PaypalFeesPers: validateStr(PaypalFeesPers),
      EbayFeesPers: validateStr(EbayFeesPers),
      BreakEvenPers: validateStr(BreakEvenPers),
      FixedCost: validateStr(FixedCost),
      ProfitPers: validateStr(ProfitPers),
      FixedProfit: validateStr(FixedProfit),
      SourceTax: validateStr(SourceTax),
      DefaultItemQty: validateStr(DefaultItemQty),
      DefaultItemLocation: validateStr(DefaultItemLocation),
      PayPalEmailAddress: validateStr(PayPalEmailAddress),
      DefaultCountryId: validateStr(DefaultCountryId),
      DefaultTemplateId: validateStr(DefaultTemplateId),
      DefaultWebGateOriginMarketId: validateStr(DefaultWebGateOriginMarketId),
      DefaultEbayUserName: validateStr(DefaultEbayUserName)
    })
  }, [
    EbayFeesPers,
    PaypalFeesPers,
    ProfitPers,
    FixedCost,
    FixedProfit,
    SourceTax,
    DefaultItemQty,
    PayPalEmailAddress,
    DefaultCountryId,
    DefaultItemLocation,
    DefaultTemplateId,
    DefaultEbayUserName,
    DefaultWebGateOriginMarketId
  ]);

  // For testing
  const listCountries = [
    {
      id: '1',
      name: 'United State'
    },
    {
      id: '2',
      name: 'United Kingdom'
    },
    {
      id: '3',
      name: 'Canada'
    },
    {
      id: '4',
      name: 'Australia'
    },
    {
      id: '5',
      name: 'Germany'
    }
  ]



  // TODO: move the methods to the parent class component and make the component presentational
  // the same method is inside th einventory submit . make it universal
  const handleChange = name => ({ target: { value } }) => {
    // values with % 
    if (name === 'PaypalFeesPers' || name === 'EbayFeesPers' || name === 'BreakEvenPersfees' || name === 'ProfitPers' || name === 'SourceTax') {
      if (value.length > 0 && value >= 0) {
        setInputValues({
          ...inputValues,
          [name]: value
        })
      }
    } else {
      console.log(value, name)
      setInputValues({
        ...inputValues,
        [name]: value
      })
    }
  }

  const handleSubmit = () => {
    const copyOfValues = { ...inputValues };
    props.handleSubmit(copyOfValues);
  }

  // from + - operators
  const handleFeeChange = name => value => {
    if (name === 'DefaultItemQty') {
      value = parseInt(value)
    }
    setInputValues({
      ...inputValues,
      [name]: value
    })
  }

  const fees = [
    {
      id: 'PaypalFeesPers',
      label: 'Paypal Fees',
    },
    {
      id: 'EbayFeesPers',
      label: 'Ebay Fees'
    },
    {
      id: 'BreakEvenPers',
      label: 'Other Fees'
    },
    {
      id: 'FixedCost',
      label: 'Fixed Cost'
    },
    {
      id: 'ProfitPers',
      label: 'Profit'
    },
    {
      id: 'FixedProfit',
      label: 'Fixed Profit'
    },
    {
      id: 'SourceTax',
      label: 'Tax'
    }
  ]

  // console.log('input Values', inputValues)
  return (
    <div className={ classes.root }>
      {/* <form className={ classes.form }> */}
        <div className={ classes.paper}>
          <Typography 
            variant='h3' 
            component='h1' 
            className={ classes.title }>List</Typography>
          <Grid container justify='space-around'>
            <Grid item xs={4}>
              <ul className={ clsx(classes.inputContainer) }>
                { fees.map(({ id, label }) => (
                  <li key={id}>
                  <InputNumeric
                    className={ classes.InputNumeric }
                    label={ label }
                    handleChange={ (name) => handleChange(name) }
                    handleFeeChange={ (value) => handleFeeChange(id)(value) }
                    name={ id }
                    value={ inputValues[id] }
                    notPercent={ id === 'FixedCost' || id === 'FixedProfit' }
                  />
                  </li>
                ))}
              </ul>
            </Grid>

            <Grid item xs={8}>
              {/* <Paper className={ classes.secondColumnPaper }> */}
                  <ul className={ clsx(classes.list)  }>
                    <li>
                      <TextField
                        className={clsx(classes.textField, classes.marginTopFix)}
                        size='small'
                        variant='outlined'
                        id='DefaultItemLocation'
                        label='Default Item Location'
                        value={ inputValues.DefaultItemLocation || '' }
                        onChange={ handleChange('DefaultItemLocation') }
                        InputProps={{
                          endAdornment: (
                            <InputAdornment>
                              <FontAwesomeIcon
                                icon={ faGlobeAfrica }
                                size='1x'
                              />
                            </InputAdornment>
                          )
                        }}
                      />
                    </li>
                    <li>
                      <InputNumeric
                        className={ clsx(classes.InputNumeric) }
                        label='Default Item Quantity'
                        handleChange={ (name) => handleChange(name) }
                        handleFeeChange={ (value) => handleFeeChange('DefaultItemQty')(value) }
                        name='DefaultItemQty'
                        value={ inputValues['DefaultItemQty'] }
                        notPercent
                        width='180px'
                        step='1'
                      />
                    </li>
                    <li>
                    <TextField
                        className={clsx(classes.textField, classes.marginTopFix, classes.PaypalEmailAddress)}
                        size='small'
                        variant='outlined'
                        label='Paypal Email'
                        onChange={ handleChange('PayPalEmailAddress') }
                        value={inputValues.PayPalEmailAddress || ''}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment>
                              <FontAwesomeIcon
                                icon={ faAt }
                                size='1x'
                              />
                            </InputAdornment>
                          )
                        }}
                      />
                    </li>
                    <li>
                      <FormControl
                        variant='outlined'
                        className={ classes.formControl }
                      >
                      <InputLabel 
                        htmlFor='list-country'
                        ref={ inputLabel }
                        >Prefered List Country</InputLabel>
                        <Select
                          id='list-country'
                          native
                          margin='dense'
                          label='Prefered List Country'                          
                          value={inputValues.DefaultCountryId}
                          onChange={handleChange('DefaultCountryId')}
                          >
                          { listCountries.map(({ name, id }) => (
                            <option key={ id } id={ id } value={id}>{ name }</option>
                          ))}
                        </Select>
                      </FormControl>
                    </li>
                    <li>
                    <FormControl
                        variant='outlined'
                        className={ classes.formControl }
                      >
                      <InputLabel 
                        htmlFor='origin-marketplace'
                        ref={ inputLabel }
                        >Origin MarketPlace</InputLabel>
                        <Select
                          id='origin-marketplace'
                          native
                          margin='dense'
                          label='Origin Marketplace'
                          value={inputValues.DefaultWebGateOriginMarketId || ''}
                          onChange={ handleChange('DefaultWebGateOriginMarketId') }
                          >
                          { originMarketPlaces ? originMarketPlaces.map(({ domainName, webgateId }) => (
                            <option key={webgateId} id={webgateId} value={webgateId}>{domainName}</option>
                          )) : null}
                        </Select>
                      </FormControl>
                    </li>
                    <li>
                    <FormControl
                        variant='outlined'
                        className={ classes.formControl }
                      >
                      <InputLabel 
                        htmlFor='template'
                        ref={ inputLabel }
                        >Select Template</InputLabel>
                        <Select
                          id='template'
                          native
                          margin='dense'
                          label='Default Template'
                          onChange={ handleChange('DefaultTemplateId')}                          
                          value={inputValues.DefaultTemplateId || ''}
                          >
                          { props.templates ? props.templates.map(({ templateId, name }) => (
                            <option key={ templateId } id={ templateId } value={templateId}>{name}</option>
                          )) : null}
                        </Select>
                      </FormControl>
                    </li>


                    <li style={{ width: '300px' }}>
                      <InputLabel htmlFor="EbayUserName-select" className={ classes.space }>Select Ebay User</InputLabel>
                      <Select
                        native
                        value={inputValues.DefaultEbayUserName || ''}
                        onChange={ handleChange('DefaultEbayUserName') }
                        inputProps={{
                          name: 'DefaultEbayUserName',
                          id: 'EbayUserName-select'
                        }}>
                          {ebayUsers.map(user => (
                            <option key={user.UserName} value={user.UserName}>{ user.UserName }</option>
                          ))}
                      </Select>
                    </li>
                  </ul>

              {/* </Paper> */}
            </Grid>
          </Grid>
          <Button
            className={ classes.submit }
            variant='contained'
            onClick={handleSubmit}
          >Submit</Button>
        </div>
      {/* </form> */}
    </div>
    )
}

export default Profile;