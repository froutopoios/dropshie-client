import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField'
import LoopIcon from '@material-ui/icons/Loop';
import IconButton from '@material-ui/core/IconButton';



import lightBlue from '@material-ui/core/colors/lightBlue';
import cyan from '@material-ui/core/colors/cyan';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    padding: '30px'
  },
  paper: {
    maxWidth: '900px',
    display: 'flex',
    flexDirection: 'column',
    padding: '30px 50px'
  },
  listStyle: {
    marginTop: '1rem',
    display: 'flex',
    flexDirection: 'column',
    listStyleType: 'none',
    '& li': {
      marginBottom: '.6rem'
    }    
  },
  textField: {
    width: 400
  },
  linksContainer: {
    marginTop: '1.5rem',
    marginLeft: '2rem'
  },
  linkBtn: {
    padding: '8px 20px',
    marginRight: '.5rem'
  },
  title: {
    fontFamily: 'Modserrat, sans-serif'
  },
  submit: {
    alignSelf: 'flex-end',
    marginTop: '40px',
    width: '200px'
  },
  inputNumberic: {
    width: 400
  },
  // TODO: dry is uded in snack also 
  '@keyframes spin': {
    from: {
      transform: 'rotate(0deg)'
    },
    to: {
      transform: 'rotate(360deg)'
    }
  },
  spinner: {
    animationName: '$spin',
    animationDuration: '500ms',
    animationIterationCount: 'infinite',
    animationTimingFunction: 'linear'
  },
  pasteContainer: {
    display: 'flex',
    position: 'relative'
  }
}))

const Extensions = props => {
  const classes = useStyles();
  const { settings: { DropshiePasteKey }, user} = props;
  const [inputValues, setInputValues] = useState({
    dropshiePasteKey: '',
    user: ''
  })

  useEffect(() => {
    setInputValues({
      ...inputValues,
      dropshiePasteKey: DropshiePasteKey,
      user
    })
  }, [DropshiePasteKey, user])

  useEffect(() => {
    props.getUserInfo()
  }, [])

  const handleRefresh = () => {
    props.refreshPasteToken()
  }
  
  const handleLink = link => () => {
    window.open(link, "_blank")
  }

  const LinkButton = withStyles((theme) => ({
    root: {
      color: 'white',
      // color: theme.palette.getContrastText(lightBlue[500]),
      backgroundColor: cyan[500],
      '&:hover': {
        backgroundColor: cyan[700]
      }
    }
  }))(Button)

  const RefreshBtn = ({ loading }) => {

    return (
      <div className={ clsx(classes.fabContainer, loading ? classes.spinner : '')}>
        <IconButton
          onClick={handleRefresh}
        >
          <LoopIcon/>
        </IconButton>
      </div>
    )
  }

  console.log('Inside Extensions')

  return (
    <div className={ classes.root }>
      {/* <form className={ classes.form }> */}
        <Paper className={ classes.paper}>
          <Typography variant='h4' component='h1' className={ classes.title }>Chrome Extentions</Typography>
              <div className={ classes.container }>
                <ul className={classes.listStyle}>
                  <li>
                    <TextField
                      className={classes.textField}
                      label='UserName'
                      value={inputValues.user}
                    />
                  </li>
                  <li className={classes.pasteContainer}>
                    <TextField
                      className={classes.textField}
                      label='Dropshie Paste Key'
                      value={inputValues.dropshiePasteKey || ''}
                    />
                    <RefreshBtn loading={props.isLoading}/>
                  </li>
                </ul>
                <div className={classes.linksContainer}>
                  <LinkButton
                    className={classes.linkBtn}
                    onClick={handleLink('https://chrome.google.com/webstore/detail/dropshiegrab/mpfpjbkkmogbgngbchnplkikkehocgjb?hl=en')}
                    size='small'
                    variant='contained'
                  >Get Dropshie Grab</LinkButton>
                  <LinkButton
                    className={classes.linkBtn}
                    size='small'
                    variant='contained'
                    onClick={handleLink('https://chrome.google.com/webstore/detail/dropshiepaste/maggmfgfhkjaapchnkpdkkchmchpnfjg')}
                  >Get Dropshie Paste</LinkButton>
                </div>
              </div>
        </Paper>
      {/* </form> */}
    </div>
    )
}

export default Extensions;