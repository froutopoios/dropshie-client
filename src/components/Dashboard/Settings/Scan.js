import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { validateStr } from '../../../utils';


import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';

import InputNumeric from '../../UI/Inputs/InputNumeric';
import IOSSwitch from '../../UI/Buttons/IOSSwitch';

import Info from '@material-ui/icons/InfoOutlined'
import ToolTip from '../../UI/Notifications/Tooltip'; 

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    padding: '30px'
  },
  flex: {
    display: 'flex'
  },
  paper: {
    maxWidth: '900px',
    display: 'flex',
    flexDirection: 'column',
    padding: '30px 50px'
  },
  container: {
    paddingTop: '3rem'
  },
  inputContainer: {
    marginTop: '1.8rem',
    paddingLeft: '1.2rem',
  },
  infoContainer: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '10px'
  },
  info: {
    fontSize: '20px',
    marginRight: '5px'
  },
  listStyle: {
    display: 'flex',
    flexDirection: 'column',
    listStyleType: 'none',
    '& li': {
      marginBottom: '1rem'
    }    
  },
  title: {
    fontFamily: 'Modserrat, sans-serif'
  },
  submit: {
    alignSelf: 'flex-end',
    marginTop: '40px',
    width: '200px'
  },
  inputNumberic: {
    width: 400
  },
  affiliation: {
    width: 400
  },
  formControl: {
    width: '300px',
    // marginBottom: '10px'
  },
  flexRow: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: 50
  },
  toggleContainer: {
    display: 'flex',
    marginBottom: 10
  },
  protection: {
    marginBottom: 0
  },
  secondToggleContainer: {
    marginBottom: 0
  },
  toggle: {
    display: 'flex',
    justifySelf: 'flex-start'
  },
  label: {
    flex: '0 0 300px',
    paddingBottom: '5px',
    alignSelf: 'center',
  },
  shippingCounter: {
    marginLeft: 40,
    transition: 'all .2s ease-in'
  },
  hide: {
    opacity: 0,
    transform: 'translateY(30px)'
  }
}))


const Scan = props => {
  const classes = useStyles();
  const [inputValues, setInputValues] = useState({})

  const {
    PreferedOfferSelectionPolicy,
    PrimeProtectionType,
    ProtectionAddon,
    ProtectionHandlingDays,
    ProtectionMaxHandlingDays,
    ProtectionShippingDays,
    ProtectionMaxShippingDays,
    UpdateListingQuantity,
  } = props.settings
  // maybe not needed (copied from another component. check it)
  useEffect(() => {
    setInputValues({
      ...inputValues,
      PreferedOfferSelectionPolicy: validateStr(PreferedOfferSelectionPolicy),
      PrimeProtectionType: PrimeProtectionType,
      ProtectionAddon: ProtectionAddon,
      ProtectionHandlingDays: ProtectionHandlingDays,
      ProtectionMaxHandlingDays: validateStr(ProtectionMaxHandlingDays),
      ProtectionShippingDays: ProtectionShippingDays,
      ProtectionMaxShippingDays: validateStr(ProtectionMaxShippingDays),
      UpdateListingQuantity: UpdateListingQuantity
    })
  }, [
    PreferedOfferSelectionPolicy,
    PrimeProtectionType,
    ProtectionAddon,
    ProtectionHandlingDays,
    ProtectionMaxHandlingDays,
    ProtectionMaxShippingDays,
    ProtectionMaxShippingDays
  ]);
  

  const handleChange = name => ({target: { value }}) => {
    setInputValues({
      ...inputValues,
      [name]: value
    })
  }

  const handleToggleChange = name => ({target: { checked }}) => {
    // api needs 0 or 1
    console.log(name, checked)
    if (name === 'updateListingQuantity') {
      checked = Number(checked)
    }

    setInputValues({
      ...inputValues,
      [name]: checked
    })
  }

  const handleNumericChange = name => ({ target: { value } }) => {
      setInputValues({
        ...inputValues,
        [name]: value,
      })
  }
  
  // from + - operators
  const handlePlusMinusChange = name => value => {
    value = parseInt(value)
    
    setInputValues({
      ...inputValues,
      [name]: value,
    })
  }

  const handleSubmit = () => {
    const copyOfValues = { ...inputValues }
    props.handleSubmit(copyOfValues)
  }



  const fields = [
    {
      label: 'Seller selection policy',
      id: '1',
      name: 'PreferedOfferSelectionPolicy',
      info: (
          <ul>
            <li>Choose <strong>No Specific Policy</strong> if you want Dropshie to deside the lowest price regardless the seller</li>
            <li>Choose <strong>Amazon Only</strong> to select Amazon as a seller, all others will considered OOS</li>
            <li>Choose  <strong>BuyBox Only</strong> if you want Dropshie to always choose the BuyBox offer</li>
          </ul>
      ),
      // info: 'Choose "No Specific Policy" if you want Dropshie to deside the lowest price regardless the seller \n Choose Amazon only to select only Amazon as a seller, all others offer will considered as OOS. Choose "BuyBox only" if you want Dropshie to always choose the BuyBox offer',
      options: [
        {
          value: 1,
          name: 'Amazon only'
        },
        {
          value: 2,
          name: 'Buy Box',
        },
        {
          value: 3,
          name: 'No specific policy'
        }
      ]
    },
    {
      label: 'Prime Protection',
      id: '2',
      name: 'PrimeProtectionType',
      info: (
        <ul>
        <li>• Choose <strong>Prime Only</strong> if you want Dropshie to treat not Prime eligible offers as OOS</li>
        <li>• Choose <strong>Non Prime Only</strong> if you want Dropshie to treat all Prime eligible offers as OOS</li>
        <li>• Choose  <strong>NO protection</strong> if you want Dropshie to consider all offers valid</li>
      </ul>
      ),
      options: [
        {
          value: 0,
          name: 'No protection'
        },
        {
          value: 1,
          name: 'Only prime protection',
        },
        {
          value: 2,
          name: 'Exclude prime items'
        }
      ]
    },
  ]

  // console.log(props.settings)
  console.log(inputValues);
  return (
    <div className={ classes.root }>
        <Paper className={ classes.paper}>
          <Typography variant='h3' component='h1' className={ classes.title }>Scan</Typography>
              <div className={ classes.container }>
                  <ul className={ clsx(classes.listStyle)  }>
                    { fields.map( field => (
                      <div className={classes.infoContainer} key={field.id}>
                        <ToolTip
                          placement='top'
                          arrow
                          title={field.info}
                        >
                          <Info
                            className={classes.info}
                          />
                        </ToolTip>
                        <FormControl
                            variant='outlined'
                            className={ classes.formControl }
                          >
                          <InputLabel 
                            htmlFor={field.name}
                            >{field.label}</InputLabel>
                            <Select
                              id={field.id}
                              native
                              margin='dense'
                              label={field.label}                          
                              value={inputValues[field.name]}
                              onChange={handleChange(field.name)}
                              >
                              { field.options.map(option => (
                                <option key={option.value}value={option.value}>{option.name}</option>
                              )) }
                            </Select>
                          </FormControl> 
                      </div>
                        ))}

                          <div className={classes.flexRow}>
                              <div className={clsx(classes.toggleContainer, classes.protection)}>
                                <div className={clsx(classes.label, classes.flex)}>
                                  <ToolTip
                                    placement='top'
                                    arrow
                                    title='Dropshie will traeat add-on item as OOS'
                                  >
                                    <Info className={classes.info}/>
                                  </ToolTip>
                                  <div>Add on protection</div>
                                </div>

                              <div className={classes.toggle}>
                                <IOSSwitch
                                    checked={ inputValues['ProtectionAddon'] || false }
                                    name={'ProtectionAddon'}
                                    onChange={ handleToggleChange('ProtectionAddon') }
                                    />
                              </div>
                            </div>

                          <div className={clsx(classes.toggleContainer, classes.secondToggleContainer)}>
                            <div className={clsx(classes.label, classes.flex)}>
                                <ToolTip
                                  placement='top'
                                  arrow
                                  title='Τhis protection compensates for the number of days an item ships within found on Amazon.A suggested value is checked and set to 0.'
                                >
                                  <Info className={classes.info}/>
                                </ToolTip>
                                <div>Handling protection</div>
                            </div>

                            <div className={classes.toggle}>
                              <IOSSwitch
                                  checked={ inputValues['ProtectionHandlingDays'] || false }
                                  name={'ProtectionHandlingDays'}
                                  onChange={ handleToggleChange('ProtectionHandlingDays') }
                                  />
                                  <div className={ clsx(classes.shippingCounter, !inputValues.ProtectionHandlingDays ? classes.hide : '') }>
                                    <InputNumeric
                                        label=''
                                        variant='standard'
                                        handleChange={ (name) => handleNumericChange(name) }
                                        handleFeeChange={ (value) => handlePlusMinusChange('ProtectionMaxHandlingDays')(value) }
                                        name='ProtectionMaxHandlingDays'
                                        value={ inputValues['ProtectionMaxHandlingDays'] }
                                        notPercent
                                        width='50px'
                                        step='1'
                                    />
                                  </div>
                            </div>
                          </div>

                          <div className={clsx(classes.toggleContainer, classes.secondToggleContainer)}>
                            <div className={clsx(classes.label, classes.flex)}>
                                <ToolTip
                                  placement='top'
                                  arrow
                                  title='Τhis protection compensates for the number of days an item "arrives between" found on Amazon. A suggested value is checked and set to 0.'
                                >
                                  <Info className={classes.info}/>
                                </ToolTip>
                                <div>Shipping protection</div>
                            </div>

                            <div className={classes.toggle}>
                              <IOSSwitch
                                  checked={ inputValues['ProtectionShippingDays'] || false }
                                  name={'ProtectionShippingDays'}
                                  onChange={ handleToggleChange('ProtectionShippingDays') }
                                  />
                                  <div className={ clsx(classes.shippingCounter, !inputValues.ProtectionShippingDays ? classes.hide : '') }>
                                    <InputNumeric
                                        label=''
                                        variant='standard'
                                        handleChange={ (name) => handleNumericChange(name) }
                                        handleFeeChange={ (value) => handlePlusMinusChange('ProtectionMaxShippingDays')(value) }
                                        name='ProtectionMaxShippingDays'
                                        value={ inputValues['ProtectionMaxShippingDays'] }
                                        notPercent
                                        width='50px'
                                        step='1'
                                    />
                                  </div>
                            </div>
                          </div>

                          <div className={classes.toggleContainer}>
                              <div className={clsx(classes.label, classes.flex)}>
                                <ToolTip
                                  placement='top'
                                  arrow
                                  title='By choosing yes you allow Dropshie to update the qty of a listing everytime a sale is made. Selecting No will let the item OOS  after all available qty is sold.'
                                >
                                  <Info className={classes.info}/>
                                </ToolTip>
                                <div>Update Listing Quantity</div>
                              </div>
                              <div className={classes.toggle}>
                              <IOSSwitch
                                  checked={ Boolean(inputValues['UpdateListingQuantity']) }
                                  name={'UpdateListingQuantity'}
                                  onChange={ handleToggleChange('UpdateListingQuantity') }
                                  />
                              </div>
                            </div>
                        </div>
                  </ul>
              </div>
          <Button
            className={ classes.submit }
            variant='contained'
            onClick={handleSubmit}
          >Submit</Button>
        </Paper>
      {/* </form> */}
    </div>
    )
}

export default Scan;