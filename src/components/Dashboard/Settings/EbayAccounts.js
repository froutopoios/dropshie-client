import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { useHistory, Route, Switch, useRouteMatch } from 'react-router-dom'; 
import { makeStyles, withStyles } from '@material-ui/core/styles';

import EbayUser from './EbayUser/Ebayuser';
import EbaySuccess from '../../Auth/EbayLogin/EbayLoginSuccess';

import { DropshieTableCell, DropshieTableRow } from '../../UI/Table'

import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import amber from '@material-ui/core/colors/amber'
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit'

import WarningDecorator from '../../UI/Notifications/warningDecorator';
import NotFound from '../../../assets/images/404-hello.png';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    padding: '30px',
    maxWidth: 1600,
    margin: 'auto'
  },
  title: {
    marginBottom: 60
  },
  label: {
    marginTop: 30,
    padding: 10
  },
  tableContainer: {

  },
  table: {
    minWidth: 700,
  },
  extendedIcon: {
    marginRight: 5
  },
}))

const AddAccountBtn = withStyles((theme) => ({
  root: {
    color: 'white',
    backgroundColor: amber[500],
    '&:hover': {
      backgroundColor: amber[700]
    }
  }
}))(Fab)

const EbayAccounts = props => {
  const classes = useStyles();
  const history = useHistory();
  const { path } = useRouteMatch();

  useEffect(() => {
    props.fetchPolicies();
  }, [])

  const handleManageEbayUser = slag => {
    history.push(`${ path }/ebayUser/${ slag }`)
  }

  const renderStatus = user => {
    switch(user.TokenState) {
      case 'Connected':
        return 'success'
      case 'ConnectionAboutToExpire':
        return 'warning'
      case 'ConnectionLost':
        return 'danger'
      default:
        return 'danger'
    }
  }

  return (
    <Switch>
      <Route exact path={ path } render={ () => (
        <div className={ classes.root }>
          <div className={classes.title}>Ebay Accounts</div>
          <div>
            <AddAccountBtn
              onClick={props.loginEbay}
              variant="extended">
              <AddIcon className={classes.extendedIcon} />
              Connect ebay account
            </AddAccountBtn>
          </div>
          <div className={classes.label}>
            <Typography variant='caption'>Connected Accounts</Typography>
          </div>
          <TableContainer
            className={classes.tableContainer}
            component={Paper}>
            <Table
              // size='small'
              className={classes.table}>
              <TableHead>
                <TableRow>
                  <DropshieTableCell>UserName</DropshieTableCell>
                  <DropshieTableCell align="right">Token Expiry</DropshieTableCell>
                  <DropshieTableCell align="right">Status</DropshieTableCell>
                  <DropshieTableCell align="right">Manage</DropshieTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
              {props.ebayUsers.length > 0 ? props.ebayUsers.map(user => (
                <DropshieTableRow
                  key={user.UserName}>
                  <DropshieTableCell component="th" scope="row">
                    {user.UserName}
                  </DropshieTableCell>
                  <DropshieTableCell align="right">{user.RefreshTokenExpiryDate}</DropshieTableCell>
                  <DropshieTableCell align="right">
                    <WarningDecorator type={ renderStatus(user) }>
                      {user.TokenState}
                    </WarningDecorator>
                   </DropshieTableCell>
                  <DropshieTableCell align="right" padding="none">
                    <IconButton
                      onClick={() => handleManageEbayUser(user.UserName)}
                    >
                      <EditIcon
                        fontSize='small'
                      />
                    </IconButton>
                  </DropshieTableCell>
                </DropshieTableRow>
              )) : 
              <DropshieTableRow>
                <DropshieTableCell>
                  Loading
                </DropshieTableCell>
              </DropshieTableRow> }
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      )}/>
      <Route path={ `${ path }/ebaysuccess`} render={() => <EbaySuccess
                                                              authEbayUserToDropshie={props.authEbayUserToDropshie}
                                                            />}/>
      <Route path={ `${ path }/ebayUser/:slug` } render={() => <EbayUser
                                                                  policies={props.policies}
                                                                  spinner={props.spinner}
                                                                  refreshPolicy={props.refreshPolicy}
                                                                  refreshedPolicies={props.refreshedPolicies}
                                                                  handleSetPolicy={props.handleSetPolicy}
                                                                  clearRefreshedPolicies={props.clearRefreshedPolicies}
                                                                  addNewPolicy={props.addNewPolicy}
                                                                  deleteEbayPolicies={props.deleteEbayPolicies}
                                                                  removeEbayUser={props.removeEbayUser}
                                                                  tab={props.tab}/>}/>
      <Route component={NotFound}/>
    </Switch>
    )
}

export default EbayAccounts;