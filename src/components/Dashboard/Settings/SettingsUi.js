import React, { useState, useEffect } from 'react'
import { makeStyles, useTheme } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';

import Profile from './Profile';
import EbayAccounts from './EbayAccounts';
import ListSettings from './ListSetttings';
import Scan from './Scan';
import Extentions from './Extentions';
import UISettings from './UiSettings';

import TabPanel from './TabPanel';
import SettingsTabs from './SettingsTabs';

import Snack from '../../UI/Notifications/Snackv2'

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    // width: 800,
    display: 'flex',
    flexDirection: 'column',
    height: 'calc(100vh - 100px)' // heaader 60 footer 40 
  },
  title: {
    padding: '25px'
  },
  panel: {
    height: '100%'
  },
  tabPanel: {
    flexGrow: 1,
  },
}))

const SettingsUi = props => {
  const classes = useStyles();
  const theme = useTheme();
  const { tab } = props;
  const [value, setValue] = useState(0);

  useEffect(() => {
    if (tab !== undefined) {
      setValue(tab)
    }
  }, [tab])

  const handleChange = (event, newValue) => {
    props.changeTab(newValue);
    setValue(newValue);
  }

  const handleChangeIndex = index => {
    setValue(index);
  }
  const showRefreshedSucces = Object.keys(props.refreshedPolicies).length > 0
  console.log(showRefreshedSucces);
  return (
    <div className={classes.root}>
      <Snack
        open={props.spinner}
        variant='loading'
        message={props.loadingMessage}
      />
      {props.netError &&
      <Snack
        open={true}
        variant='error'
        message={props.netError}
        icon='error'
      />
      }
      <Snack
        open={showRefreshedSucces || Boolean(props.successMessage)}
        variant='success'
        direction='up'
        vertical='bottom'
        horizontal='right'
        message={showRefreshedSucces ? 'Retrieved policies from Ebay' : props.successMessage}
        icon='success'
        />
        <SwipeableViews
          className={ classes.panel }
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={value}
          onChangeIndex={handleChangeIndex}
        >
          <TabPanel
            className={classes.tabPanel}
            value={value}
            index={0}
            dir={theme.direction}
          >
            <Profile
              settings={props.settings}
              handleSubmit={props.handleSubmit}
            />
          </TabPanel>

          <TabPanel
            className={classes.tabPanel}
            value={value}
            index={1}
            dir={theme.direction}
          >
            <EbayAccounts
              handleManageEbayUser={props.handleManageEbayUser}
              fetchPolicies={props.fetchPolicies}
              handleSetPolicy={props.handleSetPolicy}
              tab={props.tab}
              policies={props.policies}
              refreshPolicy={props.refreshPolicy}
              refreshedPolicies={props.refreshedPolicies}
              deleteEbayPolicies={props.deleteEbayPolicies}
              clearRefreshedPolicies={props.clearRefreshedPolicies}
              ebayUsers={props.ebayUsers}
              addNewPolicy={props.addNewPolicy}
              spinner={props.spinner}
              loginEbay={props.loginEbay}
              authEbayUserToDropshie={props.authEbayUserToDropshie}
              removeEbayUser={props.removeEbayUser}
            />
          </TabPanel>

          <TabPanel
            value={value}
            index={2}
            dir={theme.direction}
          >
            <ListSettings
              settings={props.settings}
              templates={props.templates}
              originMarketPlaces={props.originMarketPlaces}
              handleSubmit={props.handleSubmit}
              ebayUsers={props.ebayUsers}
            />
          </TabPanel>
          <TabPanel
            value={value}
            index={3}
            dir={theme.direction}
          >
            <Scan
              settings={props.settings}
              handleSubmit={props.handleSubmit}
            />
        </TabPanel>
          <TabPanel
            value={value}
            index={4}
            dir={theme.direction}
          >
            <Extentions
              getUserInfo={props.getUserInfo}
              refreshPasteToken={props.refreshPasteToken}
              user={props.user}
              settings={props.settings}
              isLoading={props.isLoading}
            />
          </TabPanel>
          <TabPanel
            value={value}
            index={5}
            dir={theme.direction}
          >
            <UISettings
              collapse={props.collapse}
              keepSidebarCollapsed={props.keepSidebarCollapsed}
            />
          </TabPanel> 
        </SwipeableViews>
        <SettingsTabs 
          value={value}
          tab={props.tab}
          handleChange={ handleChange }
          />    
    </div>
  )
}

export default SettingsUi