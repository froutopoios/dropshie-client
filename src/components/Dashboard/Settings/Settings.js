import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';
import { keepSidebarCollapsed } from '../../../actions/uiActions';
import { fetchSettings, postSettings, getUserInfo, refreshPasteToken } from '../../../actions/settingsActions';
import { fetchPolicies, 
         refreshPolicy, 
         setPolicy,
         deleteEbayPolicies, 
         getEbayUsers,
         addNewPolicy, 
         loginEbay,
         authEbayUserToDropshie,
         removeEbayUser,
         clearRefreshedPolicies } from '../../../actions/ebayActions';
import { changeSettingsTab } from '../../../actions/uiActions';
import { withRouter } from 'react-router-dom'

import SettingsUi from './SettingsUi';

class Settings extends Component {
  state = {
    settings: {}
  }
  // set intial tab 
  componentDidMount() {
    this.props.changeSettingsTab(2);
    if (Object.keys(this.props.settings).length === 0) {
      console.log('Fetching settings')
      this.props.fetchSettings();
    }
    this.props.getEbayUsers();
  }
  
  componentDidUpdate(prevProps) {
    if (prevProps.settings !== this.props.settings) {
      this.setState({
        settings: this.props.settings
      })
    }
    if (prevProps.tab !== this.props.tab) {
      this.props.history.push('/dashboard/settings')
    }
    if (this.props.ebaySuccess) {
      const signInUrl = `https://signin.sandbox.ebay.com/ws/eBayISAPI.dll?SignIn&runame=${ this.props.runame }&SessID=${ encodeURI(this.props.ebaySession) }` 
      window.location.replace(signInUrl)
    }
  }
  
  changeTab = value => {
    this.props.changeSettingsTab(value);
  }

  handleTest = () => {
    this.props.fetchSettings()
  }

  handleSubmit = data => {
    this.props.postSettings(data);
  }

  handleManageEbayUser = id => {
    console.log('inside manage ebay user', id);
  }

  handleSetPolicy = payload => {
    this.props.setPolicy(payload)
  }

  render() {
    const { settings } = this.state;
    const { templates, 
            originMarketPlaces,
            tab, 
            getUserInfo, 
            refreshPasteToken, 
            user, 
            keepSidebarCollapsed, 
            fetchingToken, 
            fetchPolicies, 
            ebayUsers, 
            refreshPolicy,
            refreshedPolicies,
            clearRefreshedPolicies,
            deleteEbayPolicies,
            addNewPolicy,
            loginEbay,
            authEbayUserToDropshie,
            removeEbayUser,
            policies } = this.props;
    return (
      <Fragment>
        <SettingsUi
          settings={settings}
          templates={templates}
          originMarketPlaces={originMarketPlaces}
          tab={tab}
          changeTab={this.changeTab}
          getUserInfo={getUserInfo}
          refreshPasteToken={refreshPasteToken}
          user={user}
          handleSubmit={this.handleSubmit}
          keepSidebarCollapsed={keepSidebarCollapsed}
          isLoading={fetchingToken}
          handleManageEbayUser={this.handleManageEbayUser}
          fetchPolicies={fetchPolicies}
          deleteEbayPolicies={deleteEbayPolicies}
          handleSetPolicy={this.handleSetPolicy}
          ebayUsers={ebayUsers}
          policies={policies}
          refreshPolicy={refreshPolicy}
          refreshedPolicies={refreshedPolicies}
          addNewPolicy={addNewPolicy}
          clearRefreshedPolicies={clearRefreshedPolicies}
          spinner={this.props.spinner}
          loadingMessage={this.props.loadingMessage}
          successMessage={this.props.successMessage}
          loginEbay={loginEbay}
          authEbayUserToDropshie={authEbayUserToDropshie}
          removeEbayUser={removeEbayUser}
          {...this.props}
          />
      </Fragment>
    )
  }
}

const mapStateToProps = ({ 
  ui: { 
    changeMenu, 
    collapse, 
    settingsTab, 
    fetchingToken,
    successMessage,
    loadingMessage,
    spinner }, 
  settings, 
  acount: { 
    templates, 
    originMarketPlaces, 
    user }, 
  ebay: { 
    policies, 
    users: ebayUsers,
    ebaySession,
    runame,
    ebaySuccess,
    refreshedPolicies }, 
  errors: { 
    netError }}) => {
  const tabs = {
    account: 0,
    ebayAccounts: 1,
    list: 2,
    scan: 3,
    extensions: 4,
    ui: 5,
  }
  const tab = tabs[settingsTab];

  return { 
    item: changeMenu, 
    collapse,
    ebayUsers,
    tab,
    settings,
    templates,
    originMarketPlaces,
    user,
    fetchingToken,
    policies,
    refreshedPolicies,
    spinner,
    netError,
    loadingMessage,
    successMessage,
    ebaySuccess,
    ebaySession,
    runame
    }
}


export default connect(mapStateToProps,
   { keepSidebarCollapsed, 
     changeSettingsTab,
     fetchSettings,
     getUserInfo,
     refreshPasteToken,
     fetchPolicies,
     setPolicy,
     refreshPolicy,
     clearRefreshedPolicies,
     getEbayUsers,
     addNewPolicy,
     deleteEbayPolicies,
     loginEbay,
     authEbayUserToDropshie,
     removeEbayUser,
     postSettings })(withRouter(Settings));