import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';

// helpers
import { validateStr } from '../../../utils';


const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    padding: '30px'
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    padding: '30px 50px',
    width: 700

  },
  inputContainer: {
    marginTop: '1.4rem',
    paddingLeft: '1.2rem',
    listStyleType: 'none',
    '& li': {
      marginBottom: '1rem'
    }
  },
  title: {
    fontFamily: 'Modserrat, sans-serif'
  },
  submit: {
    alignSelf: 'flex-end',
    marginTop: '40px',
    width: '200px'
  },
  regularInput: {
    width: 200
  },
  affiliation: {
    width: 400
  },
}))

const Profile = props => {
  const classes = useStyles();
  const [value, setValue] = useState({})

  const { settings: { FirstName, LastName, Country }} = props; 

  useEffect(() => {
    setValue({
      ...value,
      FirstName: validateStr(FirstName) ? FirstName : '',
      LastName: validateStr(LastName) ? LastName : '',
      Country: validateStr(Country) ? Country : '',
    })
  }, [FirstName, LastName, Country])

  const handleChange = id => e => {
    setValue({
      ...value,
      [id]: e.target.value
    })
  }
  
  // Map for the label and id of the inputs
  const credentials = [
    {
      id: 'FirstName',
      label: 'First Name',
    }, 
    {
      id: 'LastName',
      label: 'Last Name'
    },
    {
      id: 'Country',
      label: 'Country'
    }
  ]

  const handleSubmit = () => {
    props.handleSubmit(value)
  }

  return (
    <div className={ classes.root }>
      <form className={ classes.form }>
        <Paper className={ classes.paper}>
          <Typography variant='h3' component='h1' className={ classes.title }>Profile</Typography>
          <ul className={ classes.inputContainer }>
            { credentials.map(({ id, label }) => (
              <li key={id}>
              <TextField
                id={ id }
                label={ label }
                className={ classes.regularInput }
                value={value[id]}
                onChange={handleChange(id)}
                variant='outlined'
                size='small'
                />
              </li>
            ))}
            <li>
              <TextField
                id='affiliation'
                label='Affiliate Link'
                multiline
                className={ classes.affiliation }
              />
            </li>
          </ul>
          <Button
            className={ classes.submit }
            variant='contained'
            onClick={handleSubmit}
          >Submit</Button>
        </Paper>
      </form>
    </div>
    )
}

export default Profile;