import React from 'react';
import TextField from '@material-ui/core/TextField';

const PolicySelectBox = ({ row, name, value, handleChange, param }) => (
  <TextField
  id={row.Id.toString()}
  onChange={handleChange}
  select
  value={value || ''}
  name={name}
  SelectProps={{
    native: true
  }}
>
  {!value ? <option>no value</option> : ''}
  {row[param].map(el => (
    <option
     key={el.ProfileId}
     value={el.ProfileId}>{ el.ProfileName }</option>
  ))}
</TextField>
)

export default PolicySelectBox;