import React, { useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";


import { makeStyles } from "@material-ui/core/styles";
import Typography from '@material-ui/core/Typography';
import Paper from "@material-ui/core/Paper";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TextField from '@material-ui/core/TextField';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import DeleteForever from '@material-ui/icons/DeleteForever'
import LoopIcon from '@material-ui/icons/Loop'
import SaveIcon from '@material-ui/icons/Save'

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import SlideTransition from '../../../UI/UtilComponents/SlideTransition';


import { DropshieTableCell, DropshieTableRow } from "../../../UI/Table";

import Add from '@material-ui/icons/Add'
import arrayOfMarkets from "../../../../utils/mapMarketPlaces";

import PolicySelectBox from './PolicySelectBox';
import arrOfMarkets from "../../../../utils/mapMarketPlaces";

import ToolTip from '../../../UI/Notifications/Tooltip';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    height: "100%",
    padding: "30px",
    maxWidth: 1600,
    margin: "auto",
  },
  title: {
    fontSize: '1.3rem',
    marginBottom: 60,
  },
  tableContainer: {
    marginTop: 30,
  },
  table: {
    minWidth: 700,
  },
  extendedIcon: {
    marginRight: 5,
  },
  '@keyframes spin': {
    from: {
      transform: 'rotate(0deg)'
    },
    to: {
      transform: 'rotate(360deg)'
    }
  },
  spinner: {
    animationName: '$spin',
    animationDuration: '500ms',
    animationIterationCount: 'infinite',
    animationTimingFunction: 'linear'
  },
  addPolicyContainer: {
    marginTop: '2rem',
    display: 'flex',
    alignItems: 'flex-end',
    '& button': {
      marginLeft: '.6rem'
    }
  },
  backBtn: {
    marginTop: '100px'
  },
  addTextField: {
    width: 150
  },
  add: {
    fontSize: '30px',
    cursor: 'pointer',
    color: theme.palette.primary.main,
    '&:hover': {
      filter: 'drop-shadow(0px 0px 1px rgba(0, 0, 0, .5))',
    },
    '&:active': {
      color: theme.palette.primary.light
    }
  },
  userCaption: {
    color: theme.palette.primary.main,
    marginLeft: '10px'
  },
  deleteBtn: {
    position: 'absolute',
    top: '10px',
    right: '10px'
  }
}));

const EbayUser = (props) => {
  const [ebayPolicies, setEbayPolicies] = useState([])
  const [newMarketPlace, setNewMarketPlace ] = useState(null)
  const [spinnerId, setSpinnerId] = useState(null)
  const [deleteUserDialogOpen, setDeleteUserDialogOpen] = useState(false);
  const classes = useStyles();
  const { slug } = useParams();
  const history = useHistory();
  const { policies, refreshedPolicies } = props;

  useEffect(() => {
    const userOnlyPolicies = policies.filter(policy => {
      if (policy.EbayUsername === slug) {
        return policy
      }
    })
    setEbayPolicies(userOnlyPolicies)
  }, [policies, slug])

  useEffect(() => {
    if (Object.keys(refreshedPolicies).length > 0) {
      const copyOfPolicies = [ ...ebayPolicies ];
      const policy = copyOfPolicies.find(policy => policy.Id === refreshedPolicies.Id)
      const copyOfPolicy = { ...policy };
  
      copyOfPolicy.SelectedPaymentPolicy = refreshedPolicies.SelectedPaymentPolicy;
      copyOfPolicy.SelectedReturnPolicy = refreshedPolicies.SelectedReturnPolicy;
      copyOfPolicy.SelectedShippingPolicy = refreshedPolicies.SelectedShippingPolicy;
  
      const indx = copyOfPolicies.findIndex(policy => policy.Id === refreshedPolicies.Id);
      copyOfPolicies[indx] = copyOfPolicy;
  
      setEbayPolicies(copyOfPolicies);
      setTimeout(() => {
        props.clearRefreshedPolicies()
      }, 1000)
    }
  }, [refreshedPolicies])
  
  const handleBack = () => {
    history.push('/dashboard/settings')
  }

  const handleDeletePolicies = market => () => {
    console.log(market)
    const payload = {
      ebayUser: slug,
      market 
    }

    props.deleteEbayPolicies(payload)
  }

  const handleDeleteUser = () => {
    const user = slug;

    props.removeEbayUser(slug)
  }

  const handleChange = e => {
    const { id, name, value } = e.target;
    
    const copyOfEbayPolicies = [ ...ebayPolicies ];
    const policy = copyOfEbayPolicies.find(policy => Number(policy.Id) === Number(id)) // policy obj
    const copyOfPolicy = { ...policy };

    copyOfPolicy[name] = value; // alter the policy
    const indx = copyOfEbayPolicies.findIndex(policy => Number(policy.Id) === Number(id)); // find the index of the policy to alter
    copyOfEbayPolicies[indx] = copyOfPolicy;
     // set the new obj to the policies

    setEbayPolicies(copyOfEbayPolicies)
  }

  const handleMarketChange = e => {
    setNewMarketPlace(e.target.value)
  }

  const handleRefreshPolicy = (market, id) => () => {
    const payload = {
      user: slug,
      market
    }

    props.refreshPolicy(payload)
    setSpinnerId(id)
  }

  /**
   * 
   * @param {*} id storeId 
   */
  const handleSetPolicy = (id, market) => () => {
    const policy = ebayPolicies.find(policy =>  Number(policy.Id) === Number(id))
    const { SelectedPaymentPolicy, SelectedReturnPolicy, SelectedShippingPolicy  } = policy;
    
    const payload = {
      SelectedPaymentPolicy,
      SelectedReturnPolicy,
      SelectedShippingPolicy,
      market,
      user: slug
    }

    props.handleSetPolicy(payload)
  }

  const handleAddPolicy = market => () => {
    const payload = {
      user: slug,
      market
    }

    props.addNewPolicy(payload)
  }

  // gives the values to the add policy selectbox
  const renderAddNewPolicy = () => {
    const marketPlaces = ebayPolicies.map(policies => {
      return policies.SelingMarket
    })
    
    return arrOfMarkets.filter(market => !marketPlaces.includes(market.value))
      .map(market => (
        <option value={market.value} key={market.value}>{market.name}</option> 
      ))
  }

  // renders the first row of the table
  const renderMarket = id => {
    const market = arrayOfMarkets.find(market => market.value === id)

    return market.name
  }

  const openDeleteDialog = () => {
    setDeleteUserDialogOpen(true)
  }

  const handleDeleteDialogClose = () => {
    console.log('close')
    setDeleteUserDialogOpen(false)
  }

  return (
    <div>
      <div className={classes.root}>
        <Button
          variant='contained'
          color='secondary' 
          onClick={openDeleteDialog}
          className={classes.deleteBtn}
          >Delete User</Button>
          <Dialog
            open={deleteUserDialogOpen}
            TransitionComponent={SlideTransition}
            TransitionProps={{ transition: 'up' }}
            keepMounted
            onClose={handleDeleteDialogClose}
          >
            <DialogTitle>Delete Ebay user <span className={ classes.userCaption }>{ slug }</span>?</DialogTitle>
            <DialogContent>
              <DialogContentText>
                <Typography variant='caption'>
  The remove button will disassociate <span className={ classes.userCaption}>{slug}</span> from dropshie, and ALL of your listings on dropshie will be unmonitored. <strong>This will not end any listing from eBay.</strong>
                </Typography>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                color='secondary' 
                onClick={handleDeleteDialogClose}>Back</Button>
              <Button
                color='primary' 
                onClick={handleDeleteUser}>Delete Account</Button>
            </DialogActions>
          </Dialog>
        <Typography variant="h4">Ebay Policies</Typography>
        <Typography variant="subtitle2">
          <strong>User:</strong>
          <span className={classes.userCaption}>{slug}</span>
        </Typography>
        <TableContainer className={classes.tableContainer} component={Paper}>
          <Table
            // size='small'
            className={classes.table}
          >
            <TableHead>
              <TableRow>
                <DropshieTableCell>Store</DropshieTableCell>
                <DropshieTableCell align="left">Return</DropshieTableCell>
                <DropshieTableCell align="left">Payment</DropshieTableCell>
                <DropshieTableCell align="left">Shipping</DropshieTableCell>
                <DropshieTableCell align="right"></DropshieTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {ebayPolicies.length > 0 ?
                ebayPolicies.map(row => (
                <DropshieTableRow key={row.Id}>
                  <DropshieTableCell component="th" scope="row">
                    {renderMarket(row.SelingMarket)}
                  </DropshieTableCell>
                  <DropshieTableCell align="left">
                    <PolicySelectBox
                      value={row.SelectedReturnPolicy}
                      handleChange={handleChange}
                      name='SelectedReturnPolicy'
                      row={row}
                      param='ReturnPolicies'
                    />
                  </DropshieTableCell>
                  <DropshieTableCell align="left">
                    <PolicySelectBox
                      name='SelectedPaymentPolicy'
                      value={row.SelectedPaymentPolicy}
                      handleChange={handleChange}
                      row={row}
                      param='PaymentPolicies'
                    />
                  </DropshieTableCell>
                  <DropshieTableCell align="left" padding="none">
                    <PolicySelectBox
                      name='SelectedShippingPolicy'
                      value={row.SelectedShippingPolicy}
                      handleChange={handleChange}
                      row={row}
                      param='ShippingPolicies'
                    />
                  </DropshieTableCell>
                  <DropshieTableCell align='center' padding="none">
                    <ButtonGroup
                      size='small'
                      variant='outlined'
                    >
                      <Button
                        onClick={handleSetPolicy(row.Id, row.SelingMarket)}
                        color='primary'>

                          <SaveIcon fontSize='small'/>
                        </Button>
                      <Button
                        onClick={handleRefreshPolicy(row.SelingMarket, row.Id)}
                      >
                        <ToolTip
                          placement='right-start'
                          title='refresh from ebay'
                          arrow={true}
                        >
                          <LoopIcon fontSize='small' className={ (props.spinner && (spinnerId === row.Id)) ? classes.spinner : null }/>
                        </ToolTip>
                      </Button>
                      <Button 
                        color='secondary'
                        onClick={handleDeletePolicies(row.SelingMarket)}
                        >
                        <DeleteForever fontSize='small'/>
                      </Button>
                    </ButtonGroup>
                  </DropshieTableCell>
                </DropshieTableRow>
              )) :
              <DropshieTableRow>
                <DropshieTableCell>
                  Loading
                </DropshieTableCell>
              </DropshieTableRow>
              }
            </TableBody>
          </Table>
        </TableContainer>
        <div className={classes.addPolicyContainer}>
          <TextField
            label='Add Market'
            className={classes.addTextField}
            select
            onChange={handleMarketChange}
            SelectProps={{
              native: true
            }}
          >
          {renderAddNewPolicy()}
          </TextField>
            <Add
              onClick={handleAddPolicy(newMarketPlace)} 
              className={classes.add}/>
        </div>
        <Button
          className={classes.backBtn}
          variant='outlined'
          color='secondary'
          onClick={handleBack}
        >Back</Button>
      </div>
    </div>
  );
};

export default EbayUser;
