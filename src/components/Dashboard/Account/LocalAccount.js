import React, { useState, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx'
import { refFactory } from '../../../utils'; 

import useFormValidation from '../../../hooks/useFormValidation';
import validatePass from '../../../validators/validatePass';

import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Textfield from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from '@material-ui/core/IconButton'
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faKey } from '@fortawesome/free-solid-svg-icons';

const useStyles = makeStyles(theme => ({
  panelContainer: {
    padding: '30px 40px'
  },
  flexRow: {
    display: 'flex',
    alignItems: 'center'
  },
  gridContainer: {
    marginTop: 50
  },
  form: {
    // marginTop: 40
  },
  emailContainer: {
    // marginTop: '1.6rem'
  },
  emailLabel: {
    color: theme.palette.primary.main
  },
  emailValue: {
    marginLeft: '20px'
  },
  list: {
    listStyle: 'none',
    '& > li': {
      marginTop: 10
    }
  },
  key: {
    color: 'pink',
    marginLeft: 20
  },
  icon: {
    '&:hover': {
      boxShadow: 'none'
    }  
  },
  buttonContainer: {
    marginTop: '20px',
    display: 'flex',
    justifyContent: 'flex-end'
  },
  submit: {
    marginLeft: '10px'
    // position: 'absolute',
    // left: '40%',
    // top: 0
  }
}))

// if the password is incorect server will give the error back
const INITIAL_STATE = {
  currentPass: '',
  newPass: '',
  confirmPass: ''
}


const LocalAccount = props => {
  const classes = useStyles();
  const inputRefs = useRef(refFactory(4))
  const [ showPass, setShowPass ] = useState({
    currentPass: false,
    newPass: false,
    confirmPass: false
  })

  const { handleChange, handleBlur, handleSubmit, isSubmiting, values, errors } = useFormValidation(INITIAL_STATE, validatePass);

  useEffect(() => {
    inputRefs.current[0].current.focus();
  }, [])

  const handleShowPass = name => () => {
    setShowPass({
      ...showPass,
      [name]: !showPass[name]
    })
  }

  const handleKeyPress = id => ({ key }) => {
    if (key === 'Enter') {
      if (inputRefs.current[id + 1] !== undefined) {
        inputRefs.current[id + 1].current.focus();
      } else {
        inputRefs.current[0].current.focus();
      }
    }
  }

  const handlePassChangeSubmit = () => {
    const payload = {
      currentPass: values.currentPass,
      newPass: values.newPass,
      confirmPass: values.confirmPass
    }
    
    props.sendPassChangeRequest(payload);
  }

  const passFields = [
    {
      name: 'currentPass',
      label: 'Current Password'
    },
    {
      name: 'newPass',
      label: 'New Password'
    },
    {
      name: 'confirmPass',
      label: 'Confirm Password'
    }
  ]

  return (
  <div className={classes.panelContainer}>
  <Typography variant='h4'>Local Account</Typography>


  <Grid
    className={classes.gridContainer}
    container 
    spacing={3}>
    <Grid item xs={6}>
      <div className={ clsx(classes.flexRow, classes.emailContainer)}>
        <Typography
          className={classes.emailLabel}
          variant='h6'>Email</Typography>
        <Typography
          className={classes.emailValue}
          variant='subtitle1'
          >{props.email}</Typography>
      </div>
    </Grid>
    <Grid item xs={6}>
      <form className={classes.form}>
      <div className={classes.flexRow}>
        <Typography variant='h6'>Change Password</Typography>
        <FontAwesomeIcon
          className={classes.key}
          icon={faKey}
          size='1x'
        />
      </div>

      <ul className={classes.list}>
        {passFields.map((field, id) => (
          <li key={field.name}>
            <Textfield
              name={field.name}
              label={field.label}
              onChange={ handleChange(field.name)}
              onBlur={handleBlur}
              value={values[field.name]}
              error={ Boolean(errors[field.name]) }
              helperText={ errors[field.name] }
              type={showPass[field.name] ? 'text' : 'password'}
              inputRef={inputRefs.current[id]}
              onKeyPress={handleKeyPress(id)}
              InputProps={{
                endAdornment: (
                  <InputAdornment position='end'>
                    <IconButton
                      className={classes.icon}
                      aria-label='Toggle Password Visibility'
                      onClick={handleShowPass(field.name)}
                    >
                      { showPass[field.name] ? <Visibility/> : <VisibilityOff/> }
                    </IconButton>
                  </InputAdornment>
                )
              }}
          />      
          </li>
        ))}
      </ul>
      <div
        className={classes.buttonContainer}
      >
        <Button
          variant='outlined'
          onClick={props.handleBack}
          color='secondary'
        >
          Back
        </Button>
        <Button
          className={ classes.submit } 
          disabled={isSubmiting || Object.keys(errors).length > 0}
          variant='outlined'
          color='primary'
          ref={inputRefs.current[3]}
          onClick={handlePassChangeSubmit}
          >Submit</Button>
      </div>
      </form>
    </Grid>
  </Grid>

  {/* SECOND COLUMN */}


</div>
)}

export default LocalAccount;