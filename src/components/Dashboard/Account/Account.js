import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux';
import { fetchEmail, sendPassChangeRequest  } from '../../../actions/accountActions';
import { postAffiliationEmail } from '../../../actions/affiliationActions';
import { getItems } from '../../../actions/affiliationActions';

import LocalAccount from './LocalAccount';
import Affiliation from './Affilation/Affiliattion';

import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const styles = theme => ({
  modal: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#333'
  },
  paper: {
    // backgroundColor: '#333', // dark theme implementation --> send the prop to the global theme
    outline: 0, 
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    // padding: theme.spacing(2, 4, 3)a
  },
  container: {
    height: 450
  },
  panelContainer: {
    padding: '30px 40px'
  },

})

const AntTabs = withStyles({
  root: {
    padding: '0 40px',
    color: 'white',
    backgroundColor: '#333',
    borderBottom: '1px solid #e8e8e8'
  },
  indicator: {
    backgroundColor: '#1890ff'
  }
})(Tabs)

const AntTab = withStyles((theme) => ({
  root: {
    textTransform: 'none',
    minWidth: 72,
    fontWeight: theme.typography.fontWeightRegular,
    marginRight: theme.spacing(4),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      color: '#40a9ff',
      opacity: 1,
    },
    '&$selected': {
      color: '#1890ff',
      fontWeight: theme.typography.fontWeightMedium,
    },
    '&:focus': {
      color: '#40a9ff',
    },
  },
  selected: {},
}))((props) => <Tab disableRipple {...props} />);



class Account extends Component  {
  state = {
    value: 3,
    email: ''
  }

  // TO CONTROL THE INIT STATE OF THE MODAL GO TO loginButton.js

  componentDidUpdate(prevProps) {
    if (prevProps.email !== this.props.email) {
      this.setState({
        email: this.props.email
      })
    }
    if (this.props.modalOpen) {
      this.props.fetchEmail()
    }
  }

  handleChange = (e, value) => {
    this.setState({
      value
    })
  }

  handleBack = () => {
    this.props.closeModal()
  }

  renderTabBody = (tab, classes) => {
    switch (tab) {
      case 0:
        return <LocalAccount
                 email={this.props.email}
                 sendPassChangeRequest={this.props.sendPassChangeRequest}
                 handleBack={this.handleBack}
                />
      case 1:
        return (
          <div className={classes.panelContainer}>Panel 2</div>
        )
      case 2:
       return (
         <div className={classes.panelContainer}>Panel 3</div>
       )
       case 3:
         return (
           <div className={classes.panelContainer}>
             <Affiliation
               getItems={this.props.getItems}
               items={this.props.affiliation}
               postAffiliationEmail={this.props.postAffiliationEmail}
             />
           </div>
         )
        default:
          return <div>Cannnot load panels</div>
    }
  }

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <Modal
      className={classes.modal}
      open={this.props.modalOpen}
      onClose={this.props.closeModal}
      aria-labelledby="user account"
    >
      <div className={classes.paper}>
        <AntTabs value={value} onChange={this.handleChange}>
          <AntTab label='Local Account'/>
          <AntTab label='Facebook Account'/>
          <AntTab label='General Account Information '/>
          <AntTab label='Affiliation Info'/>
        </AntTabs>
        <div className={classes.container}>
        { this.renderTabBody(value, classes) }
        </div>
      </div>  
    </Modal>
    )
  }
}

const mapStateToProps = ({ acount: { email }, affiliation }) => {
  return {
    email,
    affiliation
  }
}

export default compose(
  withStyles(styles),
  connect(mapStateToProps, { fetchEmail, sendPassChangeRequest, getItems, postAffiliationEmail })
)(Account)








