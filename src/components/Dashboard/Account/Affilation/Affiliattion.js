import React, { Fragment, useState, useEffect, useRef } from 'react';
import clsx from 'clsx';

import { makeStyles } from '@material-ui/core/styles'; 
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Textfield from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {  faCut } from '@fortawesome/free-solid-svg-icons';

const useStyles = makeStyles(theme => ({
  root: {

  },
  fieldsContainer: {
    marginTop: 30,
    display: 'flex',
    flexDirection: 'column'
  },
  textfield: {
    width: 200,
    marginBottom: 35
  },
  link: {
    height: 40,
    width: 400,
    '& p': {
      color: 'green'
    }
  },
  paypalEmail: {
    marginTop: 40,
    width: 400
  },
  paypalEmailContainer: {
    display: 'flex',
    alignItems: 'baseline',
    '& button': {
      marginLeft: 20
    }
  },
  clip: {
    cursor: 'pointer',
    '&:hover': {
      filter: 'drop-shadow(2px 2px 4px grba(0, 0, 0, .6))'
    },
    '&:active': {
      color: 'green'
    }
  }
}))

const Affiliation = props => {
  console.log(props.items)
  const classes = useStyles()
  const { items: { TotalEarned, TotalPayed, ProcessorEmail, AffiliateCode } } = props;
  const [copySuccess, setCopySuccess] = useState('')
  const textArearef = useRef(null);
  const [inputs, setInputs] = useState({
    affiliateCode: '',
    earnings: '',
    payments: '',
    paypalEmail: 'op'
  })

  React.useEffect(() => { 
    props.getItems();
  }, [])

  useEffect(() => {
    setInputs({
      ...inputs,
      affiliateCode: AffiliateCode,
      earnings: TotalEarned,
      payments: TotalPayed,
      paypalEmail: ProcessorEmail
    })
  }, [TotalEarned, TotalPayed, ProcessorEmail, AffiliateCode])

  const copyToClipBoard = e => {
    textArearef.current.select();
    document.execCommand('copy');
    // e.target.focus();
    setCopySuccess('Copied') // send to a snack

    setTimeout(() => {
      setCopySuccess('')
    }, 1000)
  }

  const handlePaypalEmailChange = ({ target: { value } }) => {
    console.log(value)
    setInputs({
      ...inputs,
      paypalEmail: value
    })
  }

  const renderAffiliateLink = inputs => {
    const baseUrl = 'https://sandbox.dropshie.com?afc=';

    return baseUrl + inputs.affiliateCode
  }

  const handleSubmit = () => {
    const { items: { Id, AffiliateCode }} = props;
    console.log(props.items)
    const payload = {
      Id,
      AffiliateCode,
      PaypalEmail: inputs.paypalEmail
    }
    
    props.postAffiliationEmail(payload);
  }

  return (
    <Fragment>
      <Typography variant="h4">Affiliation</Typography>
      <div className={classes.fieldsContainer}>
          <Textfield
            label='Affiliate link'
            inputRef={textArearef}
            value={ inputs.affiliateCode ? renderAffiliateLink(inputs) : 'Not provided' }
            className={clsx(classes.textfield, classes.link)}
            helperText={ copySuccess && 'Copied' }
            InputProps={{
                            endAdornment: (
                              <InputAdornment
                                onClick={copyToClipBoard}
                              >
                                <FontAwesomeIcon
                                  className={classes.clip}
                                  icon={ faCut }
                                  size='1x'
                                />
                              </InputAdornment>
                            )
                          }}
          />
        <Textfield
          className={classes.textfield}
          label='Total Earnings'
          value={String(inputs.earnings)}
        />
        <Textfield
          className={classes.textfield}
          label='Payments Due'
          value={String(inputs.payments)}
        />
        <div className={classes.paypalEmailContainer}>
          <Textfield
            className={clsx(classes.textfield, classes.paypalEmail)}
            variant='outlined'
            size='small'
            label='Paypal Payments Email'
            value={inputs.paypalEmail || ''}
            onChange={handlePaypalEmailChange}
          />
          <Button
            variant='contained'
            color='primary'
            onClick={handleSubmit}
          >Update Email</Button>
        </div>
      </div>
    </Fragment>
  )
}

export default Affiliation;