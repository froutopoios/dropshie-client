import React, { useRef, useState, useEffect } from 'react';

import { withStyles } from '@material-ui/core/styles';

import { select } from 'd3-selection';
import { axisBottom, axisLeft } from 'd3-axis';
import { scaleBand, scaleLinear } from 'd3-scale';

const styles = theme => ({
  root: {
    padding: '20px 50px'
  },
  svg: {
    overflow: 'visible'
  }
})

const BarChart = props => {
  const { classes } = props; 
  const [data, setData] = useState([20, 34, 67, 23, 55, 78, 46])
  const svgRef = useRef();
  const wrapperRef = useRef();

  useEffect(() => {
    if (false) { setData() }; // To avoid the never used error from console
    const svg = select(svgRef.current);

    const xScale = scaleBand()
      .domain(data.map((value, index) => index)) // 0 to 6
      .range([0, 300])
      .padding(0.3);

    const yScale = scaleLinear()
      .domain([0, 150])
      .range([150, 0]); // origin is to the top left.

    const colorScale = scaleLinear()
      .domain([50, 70, 100])
      .range(['green', 'orange', 'red'])
      .clamp(true);

    const xAxis = axisBottom(xScale)
      .ticks(data.length)
      .tickFormat(index => index + 1);

    const yAxis = axisLeft(yScale);

    // x-axis
    svg.select('.x-axis')
       .style("transform", "translateY(150px)")
       .call(xAxis);

    // y-axis
    svg.select('.y-axis')
       .call(yAxis);

    // bar
    svg.selectAll('.bar')
        .data(data)
        .join('rect')
        .attr('class', 'bar')
        .style('transform', 'scale(1, -1)')
        .attr('x', (value, index) => xScale(index))
        .attr('y', -150)
        .attr('width', xScale.bandwidth())
        .on('mouseenter', (value, index) => {
          svg.selectAll('.tooltip')
            .data([value])
            .join(enter => enter.append('text').attr('y', yScale(value) - 4))
            .attr('class', 'tooltip')
            .text(value)
            .attr('x', xScale(index) + xScale.bandwidth() / 2)
            .attr('text-anchor', 'middle')
            .transition()
            .attr('y', yScale(value) - 8)
            .attr('opacity', 1)
        })
        .on('mouseleave', () => svg.select('.tooltip').remove())
        .transition()
        .style('fill', colorScale)
        .attr('height', value => 150 - yScale(value));

  }, [data])

  return (
    <div ref={ wrapperRef } className={ classes.root }> 
      <svg ref={ svgRef } className={ classes.svg }>
        <g className='x-axis'></g>
        <g className='y-axis'></g>
      </svg>
    </div>
  )
}

export default withStyles(styles)(BarChart);