import React, { Component, Fragment} from 'react';
import { getMaxItemsCount, getCurrentItemsCount } from '../../../actions/statsActions';
import { Link } from 'react-router-dom';
 import { connect } from 'react-redux';

import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider'
import { withStyles } from '@material-ui/core/styles';

import BarChart from './barChartTest';
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add';

const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing(1),
    // flexGrow: 1,
    marginTop: theme.spacing(2),
  },
  topPanel: {
    padding: 5
  },
  fab: {
    color: 'white',
    position: 'relative',
    '& span': {
      position: 'absolute',
      top: 8
    },
    '& a': {
      // position: 'absolute',
      // top: 2,
      textDecoration: 'none',
      color: 'white'
    }
  },
  gridContainer: {
    marginTop: 10
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: '#444'
  },
  one: {
  },
  title: {
    // marginBottom: '10px'

  },
  update: {
    padding: theme.spacing(3)
  }
})

const EnhancedPanel = withStyles(styles)(class DashBoardPanel extends Component {
  state={
    updates: [
      {id: 14, title: 'settings sidebar', body: 'now the user can set the default behaviour of the sidebar (collapsed or expanded) inside the settings/ui tab'},
      {id: 13, title: 'settings', body: 'add a basic settings screen'},
      {id: 12, title: 'submit for fetch', body: 'main properties ui created'},
      {id: 11, title: 'add item form', body: 'form to add item for fetch created'},
      {id: 10, title: 'sidebar', body: 'add functionality to the add item button on the drawer'},
      {id: 9, title: 'sidebar', body: 'refactor to the drawer'},
      {id: 8, title: 'search', body: 'during search inventory table returns to first page'},
      {id: 6, title: 'header', body: 'fix the bug in which the username didn\'t appear during the initial login of the user '},
      {id: 5, title: 'auth', body: 'fix redirection of the user to dashboard when the greds are wrong'},
      {id: 2, title: 'search', body: 'improvement to the search performance on the inventory screen'},
      {id: 3, title: 'filter', body: 'add the ui for the filter.The panel now expanses only when clicking the filter button and closes when the whole toolbar table region is clicked'},
      {id: 4, title: 'search', body: 'removed item photo description from the filter parameters. Now search creteria do not match description of photos'}
    ]
  }

  testPlan = () => {
    this.props.getMaxItemsCount();
    this.props.getCurrentItemsCount();
  }


  
  render() {
    const { classes } = this.props;
    const { updates } = this.state;
    
    return (
      <div className={classes.root}>
        <Paper
          className={classes.topPanel}
        >
          <Link to='/dashboard/addItem'>
            <Fab
              className={classes.fab}
              color='primary'
              size='small'
            >
              <AddIcon/>
            </Fab>
          </Link>
        </Paper>
        <Grid container
        className={ classes.gridContainer }
                  spacing={ 3 }
                  alignItems="stretch">
              <Grid item xs={ 6 }
                    className={ classes.one }>
                <Paper className={ classes.paper }>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur cum nostrum et minus labore iste aspernatur in nihil voluptates quisquam!
                  <button onClick={ this.testPlan }>get ItemsCount</button>
                </Paper>
              </Grid>
              <Grid item xs={ 6 }>
                <Paper className={ classes.paper }>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur cum nostrum et minus labore iste aspernatur in nihil voluptates quisquam!
                </Paper>
              </Grid>
              <Grid item xs={ 12 }>
                <BarChart/>
                <Paper className={ classes.paper }>
                  <h1 className={ classes.title }>New Updates</h1>
                  { updates.map(update =>  (
                    <Fragment key={ update.id }>
                      <Grid container spacing={ 3 } key={ update.id } className={ classes.update }>
                        <Grid item xs={ 4 } style={{ textAlign: 'left', paddingLeft: '50px'}}>
                          <h3>{ update.title }</h3>
                        </Grid>
                        <Grid item xs={ 8 }>
                          <li style={{ width: '60%', textAlign: 'left', fontSize: '14px' }}>{ update.body }</li>
                        </Grid>
                      </Grid>
                      <Divider/>
                    </Fragment>
                    )) }
                </Paper>
              </Grid>
            </Grid>
      </div>
    )
  }
})

const mapStateToProps = state => {
  // TODO: call action to fetch the plan of the user
  // console.log(state); // TODO return only the plan from the state
  return state;
}


export default connect(mapStateToProps, { getMaxItemsCount, getCurrentItemsCount })(EnhancedPanel);