import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUserPlan } from '../../../actions/accountActions';

import UserPlanUi from './UserPlanUi';

class UserPlan extends Component {
  componentDidMount() {
    this.props.getUserPlan();
  }

  render() {
    return (
        <UserPlanUi plan={ this.props.plan }/>
    )
  }
}

const mapStateToProps = ({ acount: { plan } }) => {
  return { plan }
} 

export default connect(mapStateToProps, { getUserPlan })(UserPlan);