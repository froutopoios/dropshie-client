 import React, { useRef, useState, useEffect } from 'react'
import { withStyles } from '@material-ui/styles';
import clsx from 'clsx';

import { select } from 'd3-selection';
import { axisRight } from 'd3-axis';
import { scaleLinear } from 'd3-scale';
import { transition } from 'd3-transition';


const styles = {
  root: {
      zIndex: 1000,
      borderRadius: '3px',
      height: '200px', width: '20px', 
      left: '10px', 
      bottom: '50px', 
      position: 'absolute',
      backgroundColor: 'white'
  },
  insetShadow: {
    // boxShadow: 'inset 0 0 5px #000000',
    // backgroundColor: '#74D0B9'
  },
  svg: {
    zIndex: -1, 
    overflow: 'visible'
  },
  svgContainer: {
    zIndex: 100
  }
}

function UserPlanUi(props) {
  const [ items, setItems ] = useState(0)
  const [ max, setMax ] = useState(4000)
  const { classes } = props;
  const svgRef = useRef();
  const wrapperRef = useRef();
  const { plan } = props;

  if (false) console.log(transition); // for the never used error

  const { max: planMax, current } = plan

  useEffect(() => {
    setItems(current)
    setMax(planMax)
  }, [planMax, current])

  useEffect(() => {
    const svg = select(svgRef.current);
    const wrapper = select(wrapperRef.current)

    const yScale = scaleLinear()
      .domain([0, max])
      .range([200, 0])

    const colorScale = scaleLinear()
      // .domain([max / 3, max / 2, max])
      .domain([max / 1.7, max / 1.15, max])
      .range(['#2AB4B2', '#EEE945', 'red'])
      .clamp(true)

    const yAxis = axisRight(yScale)

    svg.select('.y-axis')
      .style('transform', 'translateX(22px)')
      .call(yAxis);

    wrapper.select('.svgContainer')
      .append('div')
      .attr('class', 'tooltip-plan')
      .style('postition', 'absolute')
      .style('opacity', 0);

    svg.selectAll('.bar')
      .data([items])
      .join('rect')
      .attr('class', 'bar')
      .style('transform', 'scale(1, -1)')
      .attr('y', -200)
      .attr('width', 20)
      // .attr('filter', 'url(#innershadow)')
      .on('mouseenter', (value, index) => {
        svg.selectAll('.tooltip')
          .data([value])
          .style('opacity', 1)
          .text(value)
          // .join(enter => enter.append())
          .join(enter => enter.append('text').attr('y', yScale(value) - 4))
          // .join(enter => enter.append('div').attr('y', yScale(value) - 4)
          //   .html(value =>  `<strong>Items:</strong><span style='color: red'>${ value }</span>`))
          .attr('class', 'tooltip-plan')
          .text(value + ' items')
          .attr('x', 36) // use the bar width from jss?
          .attr('text-anchor', 'middle')
          .transition()
          .attr('y', yScale(value) - 4)
          .attr('opacity', 1)
      })
      .on('mouseleave', () => svg.select('.tooltip-plan').remove())
      .transition()
      .style('fill', colorScale)
      .attr('opacity', 1)
      .attr('height', value => 200 - yScale(value))

     
  }, [items, max])



  return (
    <div className={ clsx(classes.root, classes.insetShadow) }>
      <div className={ clsx(classes.svgContainer, 'svgContainer') } ref={ wrapperRef }>
        <svg ref={ svgRef } className={ clsx(classes.svg) } width='30' height='200'>
        <defs>
          <filter id="innershadow" x0="-50%" y0="-50%" width="200%" height="200%">
            <feGaussianBlur in="SourceAlpha" stdDeviation="3" result="blur"></feGaussianBlur>
            <feOffset dy="2" dx="3"></feOffset>
            <feComposite in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowDiff"></feComposite>
            
                <feFlood floodColor="#444444" floodOpacity="0.75"></feFlood>
            <feComposite in2="shadowDiff" operator="in"></feComposite>
            <feComposite in2="SourceGraphic" operator="over" result="firstfilter"></feComposite>
                
                
                <feGaussianBlur in="firstfilter" stdDeviation="3" result="blur2"></feGaussianBlur>
            <feOffset dy="-2" dx="-3"></feOffset>
            <feComposite in2="firstfilter" operator="arithmetic" k2="-1" k3="1" result="shadowDiff"></feComposite>
            
                <feFlood floodColor="#444444" floodOpacity="0.75"></feFlood>
            <feComposite in2="shadowDiff" operator="in"></feComposite>
            <feComposite in2="firstfilter" operator="over"></feComposite>
          </filter>    
        </defs>
          <g className='y-axis'></g>
          <g className='tip'></g>
        </svg>

      </div>
    </div>
  )
}

export default withStyles(styles)(UserPlanUi);
