import React, { Component } from 'react';
import { withStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleUp, faAngleDown, faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import amber from '@material-ui/core/colors/amber';
import grey from '@material-ui/core/colors/grey';


const styles = theme => ({
  root: {
    display: 'flex',
    padding: '5px'
  },
  iconBtn: {
    fontSize: '13px',
    // color: '#a7aaa4',
    color: amber[500],
    // color: theme.palette.primary,
    cursor: 'pointer',
    '&:hover': {
      filter: 'drop-shadow(1px 1px 1px rgba(0, 0, 0, .5))'
    },
    '&:active': {
      filter: 'none',
      opacity: '.8',
      userSelect: 'none'
    }
  },
  down: {
    marginLeft: '2px',
    // color: '#EA6E76 '
    color: grey[500]
  }
})

class GroupPlusMinus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      step: 0.01,
    }

    this.t = undefined;
    this.start = 300;
    this.onMouseDown = this.onMouseDown.bind(this);
    this.intervalId = null;
  }

  componentDidMount() {
    const { value, step } = this.props

    this.setState({
      value
    })
    if (step) {
      this.setState({ step: parseFloat(step) })

    }
  }

  handleChange = operation => {
    const { step } = this.state;
    const { value } = this.props;

    let numVal = Number(value);
    operation === 'add' ? numVal += step : numVal -= step;
    const result = numVal.toFixed(2) ; 

    this.props.handleNewValue(result)
  }

  
  // Handle continuous key press *****-----
  repeat = (operation) => () => {
    this.handleChange(operation)
    this.t = setTimeout(this.repeat(operation), this.start);
    this.start = this.start / 2
  }

  onMouseDown = operation => () => {
    // this.repeat(operation)();
    this.intervalId = setInterval(() => this.handleChange(operation), 50);
  }

  onMouseUp = () => {
    clearTimeout(this.t);
    this.start = 300;
    clearInterval(this.intervalId);
  }

  //  ***----- *** 
  
  render() {
    const { classes } = this.props;
    
    return (
    <div className={ classes.root }>
      <FontAwesomeIcon 
        icon={ faPlus }
        size='1x'
        style={{ alignSelf: 'center' }}
        className={ classes.iconBtn }  
        onMouseDown={ this.onMouseDown('add') }
        onMouseUp={ this.onMouseUp }       
        />
      <FontAwesomeIcon 
        icon={ faMinus }
        operation='sub'
        size='lg'
        color='#E76652'
        style={{ alignSelf: 'center' }}
        onMouseDown={ this.onMouseDown('sub') }
        onMouseUp={ this.onMouseUp }       
        className={ clsx(classes.iconBtn, classes.down) }
        />
    </div>
    )
  }
}

GroupPlusMinus.propTypes = {
  handleNewValue: PropTypes.func.isRequired,
  step: PropTypes.string,
  value: PropTypes.any
}

export default withStyles(styles)(GroupPlusMinus);






