import React, { Component } from 'react'
import { withStyles, makeStyles, lighten,  } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

import { connect } from 'react-redux';


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  margin: {
    margin: theme.spacing(1)
  }
})

const BorderLinearProgress = withStyles({
  root: {
    height: 20,
    backgroundColor: lighten('#3CB371', 0.5),
    width: '100%'
  },
  bar: {
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    backgroundColor: 'grey',
    transition: 'width .2s ease-in'    
  }
})(LinearProgress);

class LinearProgressBar extends Component  {
  state = {
    barValue: 0,
    time: 0
  }

  componentDidUpdate({ isLoading }, prevState) {
    if (isLoading === true && this.props.isLoading === false) {
      console.log('reset to bar ');
      this.setState({ barValue: - 5 })
    }
  }

  handleLoading = (loading) => {
    console.log('inside handle loading', loading);
    if (loading) {
      setTimeout(() => {
        this.setState(({ barValue }) => {
          return {
            barValue: barValue < 100 ? barValue + 1 : barValue,
            time: Math.round(Date.now() / 1000)
          }
        })
      }, 5)
    }
  }
  

  render() {
    const { classes, isLoading } = this.props;
    console.log(this.state.time, this.state.barValue);
    this.handleLoading(isLoading);

    return (
      <div className={ classes.root }>
        <BorderLinearProgress
          className={ classes.margin }
          variant="determinate"
          color='secondary'
          value={ this.state.barValue }
        />
      </div>
    )
  }
}

const mapStateToProps = ({ ui: { isLoading } }) => {
  return {
    isLoading
  }
}

const StyledToolBar = withStyles(styles)(LinearProgressBar);
export default connect(mapStateToProps, null)(StyledToolBar);