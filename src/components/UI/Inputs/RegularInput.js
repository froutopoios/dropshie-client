import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
// import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';


const useStyles = makeStyles(theme => ({
  wrapper: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  label: theme.Label.primary,
  textField: {
    width: props => props.width,
    fontSize: '12px',
    marginLeft: '20px',
    marginRight: '20px',
    '& input': {
      color: props => props.color ? props.color : theme.palette.primary.light,
    },
    '& .MuiOutlinedInput-root': {
      '&:hover fieldset': {
        borderColor: '#9BACA8',
      },
    },
    '& legend': {
    },
    '& label.Mui-focused': {
    },
  } 
}))

// decorate the component to use the props inside the styles 



const RegularInput = ({ wrapper, name, labelClass, textFieldClass, handleChange, value, labelValue, label, color,   ...rest }) => {
  let classes;
  // console.log(rest);
  let width = rest.width
  classes = useStyles({ width, color });

  const variantFin = rest.variant ? rest.variant : 'outlined';
  const marginFin = rest.margin ? rest.margin : 'dense';
  const styleFin = rest.labelStyle ? rest.labelStyle : null
  const InputProps = rest.InputProps ? rest.InputProps : null
  const labelClassName = labelClass ? labelClass : null
  

  if (rest.withLabel) {
    return (
      <div className={  classes.wrapper }>
      <label 
        htmlFor={ name }
        className={ clsx(classes.label, labelClassName) } // TODO: change the name of the propety
        style={ styleFin }
        >{ labelValue }</label>
      <TextField 
        className={ clsx(classes.textField, textFieldClass) }
        id={ name }
        onChange={ handleChange}
        value={ value }
        inputProps={{
          type: (name === 'netProfit' || name === 'sellPrice' || name === 'desiredQty') && 'number',
          step: name === 'desiredQty' ? `${1}`: `${0.01}`
        }}
        margin={ marginFin }
        variant={ variantFin }
        InputProps= { InputProps }
      />
    </div>
    )
  } else {
    return (
      <TextField 
      className={ clsx(classes.textField, textFieldClass) }
      id={ name }
      onChange={ handleChange}
      value={ value }
      margin={ marginFin }
      variant={ variantFin }
      InputProps= { InputProps }
      label={ label }
      width={ rest.width && classes.width }
    />
    )
  }
}

RegularInput.propTypes = {
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
}

export default (RegularInput);
