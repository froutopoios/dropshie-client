import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from '@material-ui/core/IconButton'
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';


const GeneralTextField = forwardRef((props, ref) => {
  const handleType = () => {
    if (!props.isPass || props.showPass[props.name]) {
      return 'text'
    } else {
      return 'password'
    }
  }

  return (
    <TextField
      name={props.name}
      label={props.label}
      className={props.className || null}
      onChange={props.onChange}
      onBlur={props.onBlur}
      value={props.value || ''}
      error={props.error}
      helperText={props.helperText}
      type={handleType()}
      onKeyPress={props.onKeyPress}
      inputRef={ref}
      InputProps={{
        endAdornment: (
          props.isPass ?
            <InputAdornment
              position='end'
            >
              <IconButton
                aria-label='Toggle Password Visibility'
                onClick={props.handleShowPass}
              >
                { props.showPass[props.name] ? <Visibility/> : <VisibilityOff/> }
              </IconButton>
            </InputAdornment>
            :
            null
        ) 
      }}
    />
  )
})

GeneralTextField.propTypes = {
  value: PropTypes.node,
  // showPass: PropTypes.object
}

export default GeneralTextField;