import React, { useRef, useState } from 'react';
import clsx from 'clsx';
import TextField from '@material-ui/core/TextField';

import { makeStyles } from '@material-ui/core/styles';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import amber from '@material-ui/core/colors/amber'
import grey from '@material-ui/core/colors/grey'
import { NONAME } from 'dns';

const useStyles = makeStyles(theme => ({
  root: {
    '&::-webkit-inner-spin-button': {
      display: 'none',
      // webkitAppearance: 'none'
    },
    '& :hover::-webkit-inner-spin-button': {
      display: 'none',
    },
    '& :focus::-webkit-inner-spin-button': {
      display: 'none',
    } 
  },
  arrowsRoot: {
    display: 'flex',
    padding: '5px',
  },
  icon: {
    fontSize: '13px',
    color: amber[500],
    cursor: 'pointer',
    userSelect: 'none',
    '&:hover': {
      filter: 'drop-shadow(1px 1px 1px rgba(0, 0, 0, .5))'
    },
    '&:active': {
      filter: 'none',
      opacity: '.8',
      userSelect: 'none'
    }
  },
  down: {
    marginLeft: '2px',
    color: grey[500]
  }
}))

const Arrows = props => {
  // 
  const classes = useStyles(props);
  const { handleMouseDown, handleMouseUp, handleClick } = props

  return (
    <div className={classes.arrowsRoot}>
      <FontAwesomeIcon
        icon={ faPlus }
        size='1x'
        style={{ alignSelf: 'center' }}
        className={ classes.icon }
        onMouseDown={handleMouseDown('up')}
        onMouseUp={handleMouseUp}
      />
      <FontAwesomeIcon
        icon={ faMinus }
        size='1x'
        color='#E76652'
        style={{ alignSelf: 'center' }}
        className={ clsx(classes.icon, classes.down) }
        onMouseDown={handleMouseDown('down')}
        onMouseUp={handleMouseUp}
      />
    </div>
  )
}

const NewInput = props => {
  const classes = useStyles(props);
  const input = useRef(null);
  let intervalId = null;

  const [value, setValue] = useState(0);

  const handleOperation = operation => () => {
    console.log(input.current.value)
    operation === 'up' ? input.current.stepUp() : input.current.stepDown()
  }
  
  const handleMouseDown = operation => e => {
    // console.log('trigger numchange', e.target);
    intervalId = setInterval(handleOperation(operation), 50)
  }

  const handleMouseUp = () => {
    clearInterval(intervalId);
  }

  const handleChange = ({ target: { value }}) => {
    console.log(value);
    if (value === undefined) {
      setValue(0);
    } 
  }

  const handleInput = e => {
    console.log('from input', e.target.value);
  }

  return (
    <TextField
      variant="outlined"
      size="small"
      inputRef={input}
      className={classes.root}
      onChange={handleChange}
      onInput={handleInput}
      // value={value}
      inputProps={{
        type: 'number',
        min: 0,
        step: '0.1',
      }}
      InputProps={{
        onChange: () => console.log('hey'),
        endAdornment: <Arrows 
                        handleMouseDown={handleMouseDown}
                        handleMouseUp={handleMouseUp}
                        handleClick={handleOperation}
                        />
      }}
    />
  )
}

export default NewInput;