import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import TextField from '@material-ui/core/TextField';
import GroupPlusMinus from '../Buttons/GroupPlusMinus';
import NumberFormat from 'react-number-format';
import AttachMoney from '@material-ui/icons/AttachMoney';

import { makeStyles }  from '@material-ui/core/styles'; 


const useStyles =  makeStyles(theme => ({
  focusedTextField: {
    '& legend': {
      width: '40px !important'
    },
  },
  textField: {
    marginBottom: '15px',
    fontSize: '12px',
    // marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    '& input': {
      color: '#777',
    },
    '& .MuiOutlinedInput-root': {
      '&:hover fieldset': {
        borderColor: theme.palette.primary.light,
      },
    },
    '& legend': {
      // color: '#ddd',
      // width: '40px !important'
    },
    '& label.Mui-focused': {
    },
    '.MuiInputBase-root.Mui-focused': {
      '& .PrivateNotchedOutline-legend-202': {
      },
      '& legend': {
        // color: 'pink !important'
        // width: '40px !important'
      }
    }
  },
  label: theme.Label.primary,
  labelSecondary: {
    fontSize: '0.9rem'
  },
  numeric: {
    width: props => props.width
    // width: '120px' // width of the box
  },
  testLabel: theme.Label,
}))


const Numeric = props => {
  const { inputRef, onChange, ...rest } = props;

  return (
    <NumberFormat
    { ...rest }
    getInputRef={ inputRef }
    onValueChange={values => {
      onChange({
        target: {
          value: values.value
        }
      })
    }}
    allowNegative={ false }
    isNumericString      
    />
  )
}

const PercentFormat = props => {
  const { inputRef, onChange, ...rest } = props;
  
  return (
      <NumberFormat
        { ...rest }
        getInputRef={ inputRef }
        onValueChange={values => {
          onChange({
            target: {
              value: values.value
            }
          })
        }}
        allowNegative={ false }
        suffix='%'
        isNumericString
      />
  )
}

const InputNumeric = props => {
  const width = props.width || '120px';
  const classes = useStyles({ width })
  
  if (props.notPercent === true) {
    return (
      <TextField
        disabled={props.disabled}
        label={ props.label }
        variant={props.variant || 'outlined'}
        margin='dense'
        className={ clsx(classes.textField, classes.numeric) }
        onChange={ props.handleChange(props.name) }
        value={ props.value }
        InputProps={{
          inputComponent: Numeric,
          startAdornment: props.dollar ? <AttachMoney/> : null,
          endAdornment: <GroupPlusMinus 
                         value={ props.value }
                         step={props.step}
                         handleNewValue={ (value) => props.handleArrowsChange(value) }/>
            }}
      />
    )
  }

  return (
      <TextField
        label={ props.label }
        variant={  props.variant || 'outlined' }
        margin='dense'
        disabled={props.disabled}
        className={ clsx(classes.textField, classes.numeric) }
        onChange={ props.handleChange(props.name) } 
        value={ props.value }
        InputProps={{
          inputComponent: PercentFormat, // put a condition to hadnle if its % or not 
          endAdornment: <GroupPlusMinus 
                         value={ props.value }
                         handleNewValue={ (value) => props.handleArrowsChange(value) }/>
                }}
      />
  );
} 

InputNumeric.propTypes = {
  label: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleFeeChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
}

export default InputNumeric;