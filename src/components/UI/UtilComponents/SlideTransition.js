import React, { forwardRef } from 'react';
import Slide from '@material-ui/core/Slide'

export default forwardRef((props, ref) => (
  <Slide 
    { ...props }
    ref={ref} 
    direction={props.transition || 'down'}
    mountOnEnter
    unmountOnExit
  />
))