import React, { useState, useEffect } from 'react';
import clsx from 'clsx';

import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent'
import LoopIcon from '@material-ui/icons/Loop';
import Slide from '@material-ui/core/Slide';
import { makeStyles } from '@material-ui/styles';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';

import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import WarningIcon from '@material-ui/icons/Warning';
// import  from '@material-ui/core/colors/amber';


const useStyles = makeStyles(theme => ({
  snack: {
    marginBottom: theme.spacing(3)
  },
  '@keyframes spin': {
    from: {
      transform: 'rotate(0deg)'
    },
    to: {
      transform: 'rotate(360deg)'
    }
  },
  spinner: {
    animationName: '$spin',
    animationDuration: '1000ms',
    animationIterationCount: 'infinite',
    animationTimingFunction: 'linear'
  },
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: `${ theme.palette.error.dark } `,
  },
  info: {
    backgroundColor: theme.palette.primary.main,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
}))

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

// deprecated
// function Left(props) {
//   return <Slide {...props} direction="left" />;
// }

// function Up(props) {
//   return <Slide {...props} direction="up" />;
// }

// function Right(props) {
//   return <Slide {...props} direction="right" />;
// }

// function Down(props) {
//   return <Slide {...props} direction="down" />;
// }

// deprecaded mapping
// const components = {
//   left: () => Left,
//   up: () => Up,
//   right: () => Right,
//   down: () => Down
// }

const SlideTransition = (props => {
  return <Slide { ...props } direction={ props.transition } mountOnEnter unmountOnExit />;
})


const NotifSnackBar = props => {
  const classes = useStyles();

  const [state, setState] = useState({
    open: false,
    vertical: props.vertical,
    horizontal: props.horizontal,
  });

  useEffect(() => {
    setState(prevState => ({
      ...prevState,
      open: props.open  
    }))
  }, [props.open])

  // useEffect(() => {
  //   setTransition( components[props.transition] ) // Left, RIght, Up, Down
  // }, [props.transition])

  const { vertical, horizontal, open } = state;
  let Icon;
  if (props.variant) {
    Icon = variantIcon[props.variant];
  }

  const handleClose = () => {
    setState({ ...state, open: false })
  }

  return (
    <Snackbar
      className={ clsx(classes.snack) }
      autoHideDuration={ props.type === 'netError' ? 3000 : undefined }
      anchorOrigin={{ vertical, horizontal }}
      key={`${vertical},${horizontal}`}
      open={open}
      onClose={handleClose}
      TransitionComponent={ SlideTransition }
      TransitionProps={ { transition: props.transition }}
      ContentProps={{
        'aria-describedby': 'message-id',
      }}
    >
      <SnackbarContent
        className={ classes[props.variant] || classes.snack }
        message={<span id="message-id" className={ classes.message }>
                  { props.variant ? <Icon className={  clsx(classes.icon, classes.iconVariant) } /> : null }
                  { props.message }
                </span>}
        action={
          props.type === 'netError' ? props.type :  <LoopIcon className={ classes.spinner }/>
        }
      />
    </Snackbar>
  )
}

export default NotifSnackBar;