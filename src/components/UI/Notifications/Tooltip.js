import React from 'react';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';
import { makeStyles } from '@material-ui/core/styles';
import Proptypes from 'prop-types';

const useStyles = makeStyles(theme => ({
  tooltip: {
    // backgroundColor: 'rgba(1, 1, 1, .5)',
    fontSize: theme.typography.pxToRem(12),
  },
  tooltipArrow: {
    padding: '5px',
  },
  arrow: {
    // backgroundColor: 'rgba(1, 1, 1, .5)'
  }
}))

const InfoToolTip = props => {
  const classes = useStyles();

  return (
    <Tooltip
      placement={ props.placement }
      TransitionComponent={Zoom}
      classes={{
          tooltip: classes.tooltip,
          popper: classes.popper,
          tooltipArrow: classes.tooltipArrow,
          arrow: classes.arrow
          }}
      title={ props.title }
      arrow={ props.arrow }
    >
      { props.children }
    </Tooltip>
  )
}

InfoToolTip.propTypes = {
  placement: Proptypes.oneOf(['top-start',
                              'top', 
                              'top-end', 
                              'left-start', 
                              'left', 
                              'left-end',
                              'right-start',
                              'right',
                              'right-end',
                              'bottom-start',
                              'bottom',
                              'bottom-end'
                            ]),
  arrow: Proptypes.bool,
}

// TODO: provide prop types
export default InfoToolTip