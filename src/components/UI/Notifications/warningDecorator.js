import React from 'react';
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { amber, red, green } from '@material-ui/core/colors' 

import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

const useStyles = makeStyles({
  label: props => props.color || '#333',
  success: {
    color: green['A400']
  }, 
  warning: {
    color: amber[800]
  },
  danger: {
    color: red[800]
  },
  container: {
    // position: 'relative',
    display: 'flex',
    justifyContent: 'flex-end',
  },
  dotContainer: {
    marginLeft: 5,
    color: green['A500'],
    animation: '$pulse .8s infinite ease-in-out'
  },
  '@keyframes pulse': {
    from: {
      opacity: 100
    },
    to: {
      opacity: 0
    }
  },
  dot: {
    fontSize: 10,
  }
})


const WarningDecorator = props => {
  const classes = useStyles(props)
  const { type } = props



  return (
    <span className={ clsx(classes[`${type}`] || classes.label, classes.container) }>
      {props.children}
      <span className={classes.dotContainer}>{ props.type === 'success' &&  <FiberManualRecordIcon className={classes.dot}/>}</span>
    </span>
  )
}

WarningDecorator.propTypes = {
  type: PropTypes.oneOf(['warning', 'danger', 'normal', 'success'])
}

export default WarningDecorator;