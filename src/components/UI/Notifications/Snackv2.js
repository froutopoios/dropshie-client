import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import SnackBarContent from '@material-ui/core/SnackbarContent';
import Button from '@material-ui/core/Button';
import Slide from '@material-ui/core/Slide';

import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import WarningIcon from '@material-ui/icons/Warning';
import LoopIcon from '@material-ui/icons/Loop';


import { green, amber } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  snack: {
    marginBottom: theme.spacing(3)
  },
  '@keyframes spin': {
    from: {
      transform: 'rotate(0deg)'
    },
    to: {
      transform: 'rotate(360deg)'
    }
  },
  spinner: {
    animationName: '$spin',
    animationDuration: '1000ms',
    animationIterationCount: 'infinite',
    animationTimingFunction: 'linear'
  },
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: `${ theme.palette.error.dark } `,
  },
  info: {
    backgroundColor: theme.palette.primary.main,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  messageContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  message: {
    marginLeft: 10
  }
}))

const Icon = props => {
  switch (props.variant) {
    case 'success':
      return <CheckCircleIcon {...props}/>
    case 'warning':
      return <WarningIcon {...props}/>
    case 'error':
      console.log('inside error')
      return <ErrorIcon {...props}/>
    case 'Info':
      return <InfoIcon {...props}/>
    default:
      return (
        <div style={{ color: 'teal' }}>Dropshie</div>
      )
  }
}

function SlideTransition(props) {
  return <Slide {...props} direction={props.direction} mountOnEnter unmountOnExit/>
}


export default function Snackv2(props) {
  const classes = useStyles()
  const [state, setState] = React.useState({
    open: false,
    Transition: SlideTransition,
    vertical: 'bottom',
    horizontal: 'right'
  });

  const { open: openSnack } = props;
  useEffect(() => {
    setState({
      ...state,
      open: openSnack
    })

  }, [openSnack])

  const handleClick = () => {
    setState({
      ...state,
      open: true
    })
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setState({
      ...state,
      open: false
    });
  };

  return (
    <div>
      <Snackbar
        open={state.open}
        anchorOrigin={{
          vertical: props.vertical || state.vertical,
          horizontal: props.horizontal || state.horizontal
        }}
        autoHideDuration={props.duration || 6000}
        onClose={handleClose}
        message={props.message}
        TransitionComponent={SlideTransition}
        TransitionProps={{ direction: props.direction || 'up'}}
      >
        <SnackBarContent
          className={ classes[props.variant] || classes.snack}
          message={
            <Fragment>
              <span id="message-id" className={classes.messageContainer}>
                {props.icon && <Icon variant={props.variant}/>}
                <span className={classes.message}>{props.message}</span>
              </span>
            </Fragment>
          }
          action={
            props.variant === 'loading' && <LoopIcon className={classes.spinner}/>
          }
        />
      </Snackbar>
    </div>
  );
}

Snackv2.propTypes = {
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info', 'loading']),
  vertical: PropTypes.oneOf(['top', 'middle', 'bottom']),
  horizontal: PropTypes.oneOf(['left', 'middle', 'right']),
  direction: PropTypes.oneOf(['left', 'up', 'down', 'right']),
  duration: PropTypes.number
}