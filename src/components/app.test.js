import React from 'react';
import { shallow } from 'enzyme';
import LandingFooter from './Landing/Footer/LandingFooter';
import { findByTestAttribute } from '../utils';

const setUp = (props={}) => {
  const component = shallow(<LandingFooter { ...props } />);
  return component;
}

describe('Component Landing Footer', () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it('should render', () => {
    const wrapper = findByTestAttribute(component, 'landing-footer')
    expect(wrapper.length).toBe(1);
  })
})

